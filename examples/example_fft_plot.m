% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

% This example shows how to compare an analytic Fourier transform to the
% one returned by MATLAB's :func:`fft`. The function in question is
%
%   .. math::
%
%       f(t) = \frac{1}{a^2 + t^2}
%
% which has the known Fourier transform
%
%   .. math::
%
%       \hat{f}(\theta) = \sqrt{\frac{\pi}{2 a^2}} e^{-|a \theta|}
%
% The main ingredient is that the two our out of phase by a value that one
% can compute by writing out a trapezoidal-based quadrature of the continuous
% integral.

% time interval [-T, T]
T = 100;
% number of time steps
N = 2^10;
% time step
dt = 2 * T / N;
% time instances
t = linspace(-T, T, N + 1);
t = t(1:end - 1);

% frequency
w = st_fftfreq(length(t), 2 * T);

% analytic solutions
a = 10;
f = 1 ./ (a^2 + t.^2);
fhat_exact = sqrt(pi / (2 * a^2)) * exp(-abs(a * w));
% f = exp(-a^2 * t.^2);
% fhat_exact = 1 / sqrt(2 * a^2) * exp(-w.^2 / (4 * a^2));

% compute fourier transforms
fhat = fft(f);
% multiply by phase to make it comparable to countinuous FT
phase = dt * exp(1j * w * T) / sqrt(2 * pi);
fhat = phase .* fhat;

% plot
fprintf('Error: %.5e\n', norm(fhat - fhat_exact) / norm(fhat_exact));

w = fftshift(w);
fhat_exact = fftshift(fhat_exact);
fhat = fftshift(fhat);

fig = figure();
% plot(w, log10(abs(fhat_exact - real(fhat)) + 1.0e-16), '-');
plot(w, fhat_exact, '--');
plot(w, real(fhat), ':');
st_print(fig, 'example_fft.png');
