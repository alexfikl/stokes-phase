% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function example_pozrikidis_gravity(varargin)
    % This example runs the two-phase Stokes equations to steady state.
    %
    % We attempt to reproduce some of the results from [Pozrikidis1990]_
    % regarding the evolution of a falling droplet in Figure 2b.
    %
    % .. [Pozrikidis1990] C. Pozrikidis, *The Instability of a Moving Viscous
    %   Drop*, Journal of Fluid Mechanics, 1990.

    options = st_struct_varargin(...
        'cache_id', [], ...
        'datadir', [], ...
        'from_cache', true, ...
        'enable_odeplot', true, ...
        varargin{:});

    % {{{ solve

    cache = st_cache_create(options.datadir, options.cache_id, 'pozrikidis_gravity');

    % construct problem
    problem = pozrikidis_build_problem(options);
    x = problem.geometry.x;
    y = problem.geometry.y;
    forcing = problem.forcing;
    bc = problem.bc;
    ode = problem.ode;

    filename = cache.filename(cache, 'state');
    if options.from_cache && exist(filename, 'file') == 2
        state = load(filename);

        [x, y] = st_mesh_geometry_update(x, y, state.x.x);
    else
        if options.enable_odeplot
            ode.odeplot = st_unittest_odeplot(x, 'forward', ...
                'xlim', [-4.0, 1.25], ...
                'ylim', [-1.25, 1.25], ...
                'klim', [1.25, 3.5]);
        end

        [x, y, state] = st_ode_steady(x, y, forcing, bc, cache, ode);
    end

    % }}}

    % {{{ load data

    filename = 'pozrikidis_1990_figure6b.csv';
    filename = fullfile(fileparts(pwd), 'data', filename);

    % load
    T = readtable(filename);
    px = T.Var2' + 1.0j * T.Var1';
    px = [px, px(1)];

    % rescale and recenter
    xc = st_mesh_centroid(x, y);
    pc = sum(px) / length(px);
    px = px + (xc - pc);

    % }}}

    % {{{ plot

    state.t = state.t / state.t(end);
    dv = (state.volume(2:end) - state.volume(1)) / state.volume(1);
    dv = log10(abs(dv) + 1.0e-16);

    u_dot_n = state.u_dot_n(1:length(state.t) - 1);
    dv = dv(1:length(state.t) - 1);

    zx = st_array_ax(x.x, true);
    % zv = st_array_ax(x.vertices, true);

    fig = figure();
    plot(real(zx), imag(zx), '-');
    % plot(real(zv), imag(zv), 'ko');
    plot(real(px), imag(px), 'ko-');
    xlabel('$x$');
    ylabel('$\rho$');
    axis('equal');
    st_print(fig, cache.filename(cache, 'geometry', 'png'));
    clf();

    fig = figure();
    plot(state.t(2:end), u_dot_n, '-');
    xlabel('$t / t_{final}$');
    ylabel('$\|u_n\|_\infty$');
    st_print(fig, cache.filename(cache, 'udotn', 'png'));
    cla();

    plot(state.t(2:end), dv, '-');
    xlabel('$t / t_{final}$');
    ylabel('$\log_{10} \Delta V$');
    st_print(fig, cache.filename(cache, 'volume', 'png'));

    % }}}
end

% }}}

% {{{ build_pozrikidis_problem

function [problem] = pozrikidis_build_problem(options)
    % {{{ default options

    % boundary conditions
    param.lambda = 1.0;
    param.Ca = 1.0 / 0.05;      % NOTE: this is actually the Bond number
    % param.Ca = 0.2;
    param.R = 1.0;
    param.uinf = -2.0/3.0 * (1 + param.lambda)/(2 + 3 * param.lambda);
    % param.uinf = 0.0;

    b_options.fn = 'UniformFlow';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump
    j_options.jump_type = 'gravity_capillary';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 48;
    g_options.nbasis = 2;
    g_options.nnodes = 4;
    g_options.curve = @(xi) st_mesh_curve(xi, 'spheroid', 'epsilon', 0.2);
    g_options = st_struct_update(g_options, options);

    % forcing
    f_options.forcing_type = 'normal_velocity';
    f_options.user.tangential_forcing = false;
    f_options.user.curvature_forcing = false;
    f_options = st_struct_update(f_options, options);

    % ode
    o_options.display = true;
    o_options.tmax = 10.0;
    o_options.dt = -1.0;
    % o_options.maxit = 1800;

    o_options.filter_strength = -1.0e-5;
    o_options.utol = 1.0e-10;

    o_options.solver = 'velocity';
    o_options.variables = {'two_phase', 'u'};

    o_options = st_struct_update(o_options, options);

    % }}}

    % {{{

    problem = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'forcing', f_options, ...
        'ode', o_options);
    problem.options = options;

    % }}}
end

% }}}
