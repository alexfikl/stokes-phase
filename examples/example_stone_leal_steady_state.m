% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function example_stone_leal_steady_state(test_id, varargin)
    % This example runs the two-phase Stokes equations to steady state.
    %
    % We attempt to reproduce some of the results from [stone-leal-1989]_
    % regarding the steady state shape for various Capillary numbers and
    % viscosity ratios. In particular, from the paper,
    %
    %   * ``test_id = 1`` corresponds to *Figure 2*.
    %   * ``test_id = 2`` corresponds to *Figure 3*.
    %   * ``test_id = 3`` corresponds to *Figure 4*.
    %
    % The actual data was tabulated in the `data/stone_leal_1989.csv` files.
    % This code uses :func:`st_ode_steady` for time advancement, which is
    % based on an RK2 scheme.
    %
    % .. [stone-leal-1989] H. A. Stone, L. G. Leal, *Relaxation and Breakup
    %   of an Initially Extended Drop in an Otherwise Quiescent Fluid*,
    %   Journal of Fluid Mechanics, Vol. 198, pp. 399, 1989,
    %   \url{http://dx.doi.org/10.1017/s0022112089000194}.
    %
    % :arg test_id: an integer in ``[1, 2, 3]``.

    orig_config = st_unittest_setup();

    options = st_struct_varargin(...
        'cache_id', [], ...
        'datadir', [], ...
        'from_cache', true, ...
        varargin{:});

    % {{{ load Stone and Leal data

    switch test_id
    case 1
        options.lambda = 0.1;
        data.Ca = linspace(0.01, 0.16, 5);
    case 2
        options.lambda = 1.0;
        data.Ca = linspace(0.01, 0.11, 5);
    case 3
        options.lambda = 10.0;
        data.Ca = linspace(0.01, 0.09, 5);
    otherwise
        error('unknown test_id: %d', test_id);
    end
    data.D = zeros(size(data.Ca));

    % read paper data
    sl_data = stone_leal_read_data(test_id);
    % interpolate Stone data to our points for quick comparison
    sl_interp_d = interp1(...
        sl_data.simulation.Ca, sl_data.simulation.D, data.Ca);

    cache = st_cache_create(options.datadir, options.cache_id, ...
        'example_stone_leal_steady_state');

    % }}}

    % {{{ loop

    fig = gobjects(1, 4);
    fig_ax = gobjects(size(fig));
    for m = 1:length(fig)
        fig(m) = figure('visible', 'off');
        fig_ax(m) = gca();
    end

    for m = 1:length(data.Ca)
        % construct problem
        problem = stone_leal_build_problem(data.Ca(m), options);
        x = problem.geometry.x;
        y = problem.geometry.y;
        bc = problem.bc;
        ode = problem.ode;

        checkpoint = st_cache_create(...
            fullfile(cache.datadir, sprintf('timesteps_ca_%05d', m)), ...
            [], 'evolution');

        filename = checkpoint.filename(checkpoint, 'state');
        if options.from_cache && exist(filename, 'file') == 2
            state = load(filename);

            [x, ~] = st_mesh_geometry_update(x, y, state.x.x);
        else
            [x, ~, state] = st_ode_steady(x, y, bc, checkpoint, ode);
        end

        % compute deformation
        L = max(real(x.x));
        B = max(imag(x.x));
        data.D(m) = (L - B) / (L + B);
        fprintf('[%05d] D = %.5e Stone %.5e\n', ...
            m, data.D(m), sl_interp_d(m));

        % plot
        state.t = state.t / state.t(end);
        dv = (state.volume(2:end) - state.volume(1)) / state.volume(1);
        dv = log10(abs(dv) + 1.0e-16);

        u_dot_n = state.u_dot_n(1:length(state.t) - 1);
        dv = dv(1:length(state.t) - 1);

        zx = st_array_ax(x.x, true);
        plot(fig_ax(1), real(zx), imag(zx), '-');
        plot(fig_ax(2), state.t(2:end), u_dot_n, '-');
        plot(fig_ax(3), state.t(2:end), dv, '-');
    end

    xlabel(fig_ax(1), '$x$');
    ylabel(fig_ax(1), '$\rho$');
    annotation(fig(1), 'textarrow', [0.65, 0.9], [0.517, 0.517]);
    xlim(fig_ax(1), [-1.65, 1.65]);
    axis(fig_ax(1), 'equal');
    st_print(fig(1), cache.filename(cache, 'geometry', 'png'));

    xlabel(fig_ax(2), '$t / t_{final}$');
    ylabel(fig_ax(2), '$\|u_n\|_\infty$');
    st_print(fig(2), cache.filename(cache, 'udotn', 'png'));

    xlabel(fig_ax(3), '$t / t_{final}$');
    ylabel(fig_ax(3), '$\log_{10} \Delta V$');
    annotation(fig(3), 'textarrow', [0.5, 0.5], [0.5, 0.9]);
    st_print(fig(3), cache.filename(cache, 'volume', 'png'));

    fig = figure();
    h1 = plot(sl_data.simulation.Ca, sl_data.simulation.D, '-');
    h2 = plot(sl_data.first_order.Ca, sl_data.first_order.D, '--');
    h3 = plot(sl_data.second_order.Ca, sl_data.second_order.D, ':');
    h4 = plot(data.Ca, data.D, 'ko');
    xlim([0.0, 0.16]);
    legend([h1, h2, h3, h4], {'Stone (1989)', ...
        '$\mathcal{O}(\mathrm{Ca})$', ...
        '$\mathcal{O}(\mathrm{Ca}^2)$', ...
        'Current'}, 'Location', 'northwest');
    xlabel('$\mathrm{Ca}$');
    ylabel('$D$');
    st_print(fig, cache.filename(cache, 'deformation', 'png'));

    % }}}

    st_unittest_setup(orig_config, true);
end

% {{{ read_stone_leal_data

function [data] = stone_leal_read_data(test_id)
    filename = sprintf('stone_leal_1989_figure%d.csv', test_id + 1);
    filename = fullfile(fileparts(pwd), 'data', filename);

    T = readtable(filename);
    T = T.Variables;

    data.simulation = to_struct(1);
    data.first_order = to_struct(3);
    data.second_order = to_struct(5);

    function [d] = to_struct(ncol)
        mask = ~isnan(T(:, ncol));
        d.Ca = T(mask, ncol);
        d.D = T(mask, ncol + 1);
    end
end

% }}}

% {{{ build_stone_leal_problem

function [problem] = stone_leal_build_problem(Ca, options)
    % {{{ default options

    % boundary conditions
    param.lambda = 1.0;
    param.Ca = Ca;
    param.R = 1.0;
    b_options.param = param;

    b_options.fn.uinf = @(x) real(x.x) - 0.5j * imag(x.x);
    b_options.fn.finf = @(x) zeros(size(x.x));
    b_options.fn.jump = @(x) x.kappa / Ca .* x.n;

    b_options = st_struct_update(b_options, options);

    % geometry
    g_options.npanels = 16;
    g_options.curve = @(xi) st_mesh_curve(xi, 'circle', 'r', b_options.param.R);
    g_options.s_singularity = {'fn', 'u'};
    g_options = st_struct_update(g_options, options);

    % ode
    o_options.filter_strength = 1.0e-5;
    o_options.utol = 1.0e-4;
    o_options.tangential_forcing = true;

    o_options.display = true;
    o_options.solver = 'velocity';
    o_options.variables = {'two_phase', 'u'};

    o_options = st_struct_update(o_options, options);

    % }}}

    % {{{

    problem.options = options;
    problem.bc = st_boundary_condition_options(b_options);
    problem.geometry = st_geometry(g_options);
    problem.ode = st_ode_options(o_options);

    % }}}
end

% }}}
