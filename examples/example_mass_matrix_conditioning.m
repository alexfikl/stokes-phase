% SPDX-FileCopyrightText: 2021 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function example_mass_matrix_conditioning(curve, nbasis, varargin)
    % This example looks at the conditioning and eigenvalues of the mass matrix
    % and different methods of applying boundary conditions to the stiffness
    % operator.
    %
    % :arg curve: name of the curve geometry to use (see :func:`st_mesh_curve`).
    % :arg order: approximation order.
    %

    options = st_struct_varargin(...
        'cache_id', datestr(now, 'yyyy_mm_dd'), ...
        'datadir', [], ...
        'nbasis', nbasis, 'nnodes', nbasis, ...
        'curve', curve, ...
        varargin{:});

    before = st_unittest_setup();
    cache = st_cache_create(options.datadir, ...
        options.cache_id, 'operator_conditioning');

    % {{{ plot eigenvalues

    fig = gobjects(1, 2);
    fax = gobjects(1, length(fig));
    for m = 1:2
        fig(m) = figure();
        fax(m) = gca();

        xlabel(fax(m), '$\Re$');
        ylabel(fax(m), '$\Im$');
    end

    for npanels = 2.^(3:8)
        geometry = st_geometry(options, 'npanels', npanels);
        x = geometry.x;
        y = geometry.y;
        b = ones(size(x.x));

        M = st_ops_mass_2d(x, y);
        fprintf('cond(M) = %.5e\n', cond(M));

        M = st_ops_mass(x, y);
        S = st_ops_stiffness(x, y);
        S = M \ S;
        S = st_ops_boundary(x, M \ S, b, 'neumann');
        fprintf('cond(M) = %.5e cond(S) = %.5e\n', cond(M), cond(S));

        sigma = eig(M);
        plot(fax(1), real(sigma), imag(sigma), 'o');

        sigma = eig(S);
        plot(fax(2), real(sigma), imag(sigma), 'o');
    end

    st_print(fig(1), cache.filename(cache, 'eigenvalues_mass', 'png'));
    st_print(fig(2), cache.filename(cache, 'eigenvalues_stiffness', 'png'));

    % }}}

    st_unittest_setup(before, true);
end

function [S] = st_ops_stiffness(x, y)
    g = struct('g_out', ones(size(x.x)), ...
               'g_in', ones(size(x.x)), ...
               'dg_out', zeros(size(x.x)));

    S = st_ops_adjoint_normal(x, y, g);
end

function [M] = st_ops_mass_2d(x, y)
    % number of targets
    ntargets = length(x.x);
    % number of panels
    npanels = length(x.panels) - 1;
    % number of basis functions
    nbasis = length(x.nodes.basis);
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);

    % evaluate basis on each point of the reference element
    [phi, ~] = st_interp_evaluate_basis(x, y.xi(1:nnodes));
    phi = phi';
    % compute area element
    dS = y.J .* y.w;

    % compute operator
    M = zeros(ntargets, ntargets);

    for n = 1:npanels
        % source (quadrature) points in the current panel
        k = ((n - 1) * nnodes + 1):(n * nnodes);

        % evaluate block
        for p = 1:nbasis
            i = (n - 1) * (nbasis - 1) + p;
            for q = 1:nbasis
                j = (n - 1) * (nbasis - 1) + q;

                M(i, j) = M(i, j) + sum(phi(q, :) .* phi(p, :) .* dS(k));
            end
        end
    end
end

