% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

% capillary number
param.Ca = 0.01;
% viscosity ratio
param.lambda = 5.0;

% number of panels
npanels = 64;
% number of smooth Gauss-Legendre quadrature nodes
nnodes = 8;
% number of basis functions on each panel
nbasis = nnodes;
% interface definition
curve_fn = @(xi) exp(2.0j * pi * xi);

% construct collocation (target) and quadrature (source) points
x = st_point_collocation(npanels, nbasis, curve_fn);
y = st_point_quadrature(npanels, nnodes, curve_fn);
% attach geometry: normals, curvatures, etc.
[x, y] = st_mesh_geometry(x, y);
% singular quadrature
y.quad = st_layer_quadrature(x, y, {'reg', 'log'});

% define freestream boundary conditions
bc.uinf = @(x) ones(1, length(x.x));
bc.finf = @(x) zeros(1, length(x.x));
bc.jump = @(x) x.kappa / param.Ca .* x.n;
bc = st_boundary_condition_options('param', param, 'fn', bc);

% solve for the density in a single-layer representation
q = st_repr_density(x, y, param.lambda, bc);
% compute velocity
u = st_repr_density_velocity(x, y, q, bc);
