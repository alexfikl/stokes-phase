% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = test_exterior_data(varargin)
    % Description: This test is for an exterior problem only, where we wish to
    % minimize stress on the interface with respect to the velocity
    % boundary condition on the surface. Uses the known analytic solution
    % `SolidQuadratic` in :func:`st_sym_solutions`.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    b_options.fn = 'SolidQuadratic';
    b_options.param.R = 1.0;

    % geometry
    g_options.npanels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'circle', 'r', b_options.param.R);

    % cost
    c_options.cost_type = 'exterior_traction';
    c_options.desired_curve = @(xi) zeros(size(xi));

    c_options.fn.grad = @fn_cost_gradient;

    c_options.user.fdx = 0.0;
    c_options.user.fdy = 0.0;

    % }}}

    % {{{

    b_options = st_struct_update(b_options, options);
    g_options = st_struct_update(g_options, options);
    c_options = st_struct_update(c_options, options);

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'cost', c_options);

    r.extra_options = struct();

    % }}}
end

function result = fn_cost_gradient(cost, t, x, y, name, st)
    switch lower(name)
    case 'u'
        result = cost.fn.gradient_u(cost, t, x, y, st);
    case 's'
        result = cost.fn.gradient_s(cost, t, x, y, st);
    case 'b'
        result = cost.fn.gradient_b(cost, t, x, y, st);
    otherwise
        error('unknown variable: %s', name);
    end
end
