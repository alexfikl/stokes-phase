% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_gradients(varargin)
    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.test_type = 'boundary';
    options.reload_options = true;

    options.test_id = st_struct_field(options, 'test_id', 'exterior');

    options = st_unittest_example_runner(...
        @run_plot_gradients, ...
        options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function run_plot_gradients(options, ~)
    cache = options.cache;

    fig = gobjects(1, 3);
    fig_ax = gobjects(1, 3);
    for i = 1:length(fig)
        fig(i) = figure();
        fig_ax(i) = gca();
    end

    eoc = st_util_eoc({'gradx', 'grady'}, length(options.resolutions));

    for i = 1:length(options.resolutions)
        npanels = options.resolutions(i);
        filename = cache.filename(cache, sprintf('%04d_gradient', npanels));
        load(filename, 'fd_gradJ', 'ad_bumped_gradJ', 'x', 'y');

        ad_gradJ_r = real(ad_bumped_gradJ);
        ad_gradJ_i = imag(ad_bumped_gradJ);
        fd_gradJ_r = real(fd_gradJ);
        fd_gradJ_i = imag(fd_gradJ);

        % compute errors
        eoc.errors.gradx(i) = st_ax_error(x, y, ad_gradJ_r, fd_gradJ_r);
        eoc.errors.grady(i) = st_ax_error(x, y, ad_gradJ_i, fd_gradJ_i);
        eoc.h(i) = st_mesh_h_max(x, y);
        eoc.npoints(i) = npanels;
        eoc = st_util_eoc(eoc);

        % plot
        h1r = plot(fig_ax(1), x.xi, fd_gradJ_r, '--');
        h2r = plot(fig_ax(1), x.xi, ad_gradJ_r, ':', ...
                   'Color', get(h1r, 'Color'));

        h1i = plot(fig_ax(2), x.xi, fd_gradJ_i, '--');
        h2i = plot(fig_ax(2), x.xi, ad_gradJ_i, ':', ...
                   'Color', get(h1i, 'Color'));

        dv = log10(abs(ad_gradJ_r - fd_gradJ_r) + 1.0e-16);
        plot(fig_ax(3), x.xi, dv, '--');
        dv = log10(abs(ad_gradJ_i - fd_gradJ_i) + 1.0e-16);
        plot(fig_ax(3), x.xi, dv, ':');
    end

    plot(fig_ax(1), x.xi, 0.0 * x.xi, 'k.');
    legend([h1r, h2r], {'$FD \cdot h$', '$AD \cdot h$'}, 'Location', 'Best');

    plot(fig_ax(2), x.xi, 0.0 * x.xi, 'k.');
    legend([h1i, h2i], {'$FD \cdot h$', '$AD \cdot h$'}, 'Location', 'Best');

    st_print(fig(1), cache.filename(cache, 'gradient_x', 'png'));
    st_print(fig(2), cache.filename(cache, 'gradient_y', 'png'));
    st_print(fig(3), cache.filename(cache, 'gradient_error', 'png'));

    if length(options.resolutions) > 1
        labels = struct(...
            'gradx', '\nabla_u \mathcal{J}', ...
            'grady', '\nabla_v \mathcal{J}');
        expected_orders = struct(...
            'gradx', x.nbasis - 1.0, ...
            'grady', x.nbasis - 1.0);

        st_util_eoc_show(cache.filename(cache, 'error', 'png'), ...
            eoc.errors, eoc.h, labels, expected_orders);
    end
end
