% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [options] = generate_gradients(varargin)
    % This function generates adjoint and finite difference gradients for
    % multiple mesh resolutions. The results can be plotted with
    % :func:`plot_gradients`.
    %
    % A typical run will be like
    %
    %   .. code:: matlab
    %
    %       o = generate_gradients('test_id', 'exterior')
    %       plot_gradients('test_id', 'exterior', 'datadir', o.datadir);
    %
    % which will generate a folder named `cache/exterior_{CURRENT_DATE}` with
    % some `mat` files and `png` files.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.test_type = 'boundary';

    options.test_id = st_struct_field(options, 'test_id', 'exterior');
    options.resolutions = st_struct_field(options, 'resolutions', ...
        [8, 12, 16, 20, 24, 28, 32]);

    options = st_unittest_example_runner(...
        @run_generate_gradients, ...
        options);

    % }}}

    fprintf('datadir: %s\n', options.cache.datadir);
    st_unittest_setup(orig_config, true);
end

function run_generate_gradients(options, problem_init)
    % {{{ compute gradients

    cache = options.cache;
    for i = 1:length(options.resolutions)
        options.npanels = options.resolutions(i);
        problem = problem_init(options);

        % unroll
        x = problem.geometry.x;
        y = problem.geometry.y;
        cost = problem.cost;
        bc = problem.bc;

        % adjoint gradient
        st.forward = st_repr_density_solution(x, y, bc, cost.forward_variables);
        st.adjoint = st_repr_density_adjoint_solution(x, y, st, cost, ...
            cost.adjoint_variables);

        ad_gradJ = cost.fn.grad(cost, 0.0, x, y, 'b', st);
        ad_bumped_gradJ = cost.bump(cost, x, y, ad_gradJ);

        % finite difference gradient
        fd = get_finite_gradient(x, y, bc, cost);
        fd_gradJ = fd.gradJ;

        % save
        filename = cache.filename(cache, sprintf('%04d_gradient', options.npanels));
        save(filename, 'fd_gradJ', 'ad_gradJ', 'ad_bumped_gradJ', 'x', 'y');

        err_r = st_ax_error(x, y, real(ad_bumped_gradJ), real(fd_gradJ));
        err_i = st_ax_error(x, y, imag(ad_bumped_gradJ), imag(fd_gradJ));
        fprintf('npanels %4d error %.5e %.5e\n', x.npanels, err_r, err_i);
    end

    % }}}
end

function [fd] = get_finite_gradient(x0, y0, boundary, cost)
    % number of target points
    ntargets = length(x0.x);

    % save boundary conditions
    usurf = boundary.fn.usurf(x0);
    % unperturbed boundary condition
    boundary.fn.usurf = @(~) usurf;
    % compute unperturbed cost functional
    st = st_repr_density_solution(x0, y0, boundary, cost.variables);
    J0 = cost.fn.evaluate(cost, 0.0, x0, y0, st);

    fd.gradJ = zeros(1, ntargets, 'like', x0.x);
    for i = 1:ntargets
        % perturbation bump at current position
        bump = cost.bump(cost, x0, y0, i);

        %
        % perturb x velocity component
        %

        boundary.fn.usurf = @(~) usurf + cost.eps * bump;

        st_eps = st_repr_density_solution(x0, y0, boundary, cost.variables);
        Jeps_u = cost.fn.evaluate(cost, 0.0, x0, y0, st_eps);

        %
        % perturb y velocity component
        %

        boundary.fn.usurf = @(~) usurf + 1.0j * cost.eps * bump;

        st_eps = st_repr_density_solution(x0, y0, boundary, cost.variables);
        Jeps_v = cost.fn.evaluate(cost, 0.0, x0, y0, st_eps);

        % compute gradient
        fd.gradJ(i) = (Jeps_u - J0) / cost.eps + ...
            1.0j * (Jeps_v - J0) / cost.eps;

        fprintf('[%05d / %05d] gradJ_x = %+.5e gradJ_y = %+.5e\n', ...
            i, ntargets, ...
            real(fd.gradJ(i)), imag(fd.gradJ(i)));
    end
end
