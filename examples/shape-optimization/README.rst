This folder has some examples / tests of the shape optimization part of the
code. Its main goal is to generate some finite difference based gradients
and some adjoint based gradients and check proper convergence to expected
orders.

There are some preexisting setups in the ``data/`` directory with the naming
convention of ``test_{test_id}_data``. We'll see these ``test_id`` later on,
so when running the code, make sure you check what's in those files. All the
scripts here are then run using

    .. code:: matlab

        function(Name, Value, ...)

with given key-value pairs. However, only the keys in the respective
``data/test_{test_id}_data.m`` files can be passed in and modified
(see that they're all using ``st_struct_update``, which just updates existing
fields).

Convergence
===========

Since some of these are slow (I'm looking at you finite difference!), the
scripts are broken up into pieces. The intended uses are something like this.

1. First, generate some finite difference gradients for one of the examples
  in the ``data/`` directory with

    .. code:: matlab

        o = generate_gradient_finite('test_id', 'extensional');
        plot_gradient_finite('test_id', 'extensional', 'cache_id', o.cache_id);

  which will create a nice cache in ``cache/finite_extensional_{timestamp}``
  with all the data and plot some stuff. We can also pass in a fixed ``cache_id``
  to the first call, so that its not generated automatically.

2. We can do the same thing for the adjoint-based gradients using

    .. code:: matlab

        generate_gradient_adjoint('test_id', 'extensional', 'cache_id', o.cache_id);
        plot_gradient_adjoint('test_id', 'extensional', 'cache_id', o.cache_id);

    where the ``cache_id`` must match betwee the finite difference and the adjoint
    runs. This is relied on in ``plot_gradient_adjoint``, which tries to retrieve
    both and do some convergence plots and things like that.

There's also an even slower script in ``generate_gradient_convergence``,
with the same calling conventions. This script will run both the finite
difference and the adjoint gradient computations for different finite difference
step sizes and different grid sizes.

Optimization
============

There is also a little ``optim_shape`` function that does actual shape
optimization. It runs the same as the others.
