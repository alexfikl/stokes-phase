% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_extensional_data(varargin)
    % Description: This test uses the setup we plan to use for the unsteady
    % problem: an extensional flow with surface tension. It is meant as a playground
    % for that, with different jump conditions, etc.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    param.Ca = 1.0;
    param.lambda = 1.0;
    param.alpha = 1.0;
    param.a = 1.0;
    param.b = 2.0;
    param = st_struct_update(param, options);

    b_options.fn = 'ExtensionalFlow';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump options
    j_options.jump_type = 'capillary';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'ellipse', ...
        'a', b_options.param.a, 'b', b_options.param.b);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'normal_velocity';
    c_options.bump_normalized = true;
    c_options.desired_curve = @(xi) st_mesh_curve(xi, 'ellipse', ...
        'a', b_options.param.b, 'b', b_options.param.a);

    c_options.user.udx = [];
    c_options = st_struct_update(c_options, options);

    % optim
    o_options.beta_update = 'flecherreeves';
    o_options.maxit = 128;
    o_options.outputfreq = 1;
    o_options.gtol = 1.0e-6;
    o_options.alpha_min = 1.0e-8;
    o_options.alpha_max = @st_optim_geometry_alpha_max;

    o_options.user.filter_strength = 1.0e-2;
    o_options = st_struct_update(o_options, options);

    % }}}

    % {{{

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'cost', c_options, ...
        'optim', o_options);

    % NOTE: doing this after so that the cost geometry is properly initialized
    if isempty(r.cost.user.udx)
        r.cost.user.udx = 0.0;
        r.cost.user.udy = 0.0;
    elseif isa(r.cost.user.udx, 'function_handle')
        r.cost.user.udy = r.cost.user.udx(r.cost.y);
        r.cost.user.udx = r.cost.user.udx(r.cost.x);
    else
        % assumes this is just a constant now
        r.cost.user.udy = r.cost.user.udx;
    end

    r.extra_options = struct();

    % }}}
end
