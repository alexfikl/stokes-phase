% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_exterior_data(varargin)
    % Description: This test is for an exterior problem only, where we wish to
    % minimize stress on the interface. Uses the known analytic solution
    % SolidQuadratic in :func:`st_sym_solutions`.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    b_options.param.R = 1.0;
    b_options.fn = 'SolidQuadratic';
    b_options = st_struct_update(b_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.nnodes = 4;
    g_options.nbasis = 4;
    g_options.curve = @(xi) st_mesh_curve(xi, 'circle', ...
        'r', b_options.param.R);

    % NOTE: most defaults are set for the two-phase problem, but for the
    % exterior problem we use slightly different quadrature choices. these
    % seemed to work well in the convergence tests in `autotests/`.

    s_log = struct('nnodes', 4 * g_options.nbasis, ...
        'npoly', g_options.nbasis, 'method', 'carley');
    s_reg = struct('nnodes', g_options.nbasis, 'npoly', g_options.nbasis);
    s_hfp = struct('nnodes', 2 * g_options.nbasis, 'npoly', g_options.nbasis);
    g_options.s_quad = struct(...
        'exterior', s_log, 'log', s_log, 'dudn', s_log, ...
        'reg', s_reg, 'fn', s_reg, 'grads', s_hfp);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'exterior_traction';
    c_options.desired_curve = @(xi) zeros(size(xi));

    c_options.eps = 1.0e-6;
    c_options.sigma = 1.0e-2;
    c_options.bump_type = 'x';
    c_options = st_struct_update(c_options, options);

    % }}}

    % {{{

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'cost', c_options);

    r.cost.user.fdx = 0.0;
    r.cost.user.fdy = 0.0;
    if isfield(r.bc.fn, 'sym')
        r.cost.user.sym = r.bc.fn.sym;
    else
        % NOTE: this is not strictly necessary, but our stress gradient
        % computation in st_repr_density_grads sucks a bit, so for this test
        % we're sticking to the analytic stuff
        error('this test case requires and analytic formula');
    end

    % }}}
end
