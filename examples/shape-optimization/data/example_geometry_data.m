% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_geometry_data(varargin)
    % Description: This test uses a cost functional based solely on the interface
    % parametrization (and not on the fluid properties). As such, the adjoint
    % Stokes problem is competely homogeneous and we know the gradients
    % analytically.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    param.Ca = 1.0;
    param.lambda = 2.0;
    param.tmax = 0.0;

    b_options.param = param;
    b_options.fn = 'ExtensionalFlow';
    b_options = st_struct_update(b_options, options);

    % jump options
    j_options.jump_type = 'capillary';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', 1.0, 'b', 1.0);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'geometry';
    c_options.desired_curve = @(xi) st_mesh_curve(xi, 'ufo', 'k', 1.0, 'a', 1.0, 'b', 1.0);

    c_options.user.is_static = true;
    c_options = st_struct_update(c_options, options);

    % optim
    o_options.beta_update = 'PolakRibiere';
    o_options.maxiter = 2*2048;
    o_options.outputfreq = 1;
    o_options.gtol = 1.0e-5;
    o_options.alpha_min = 1.0e-8;
    o_options.alpha_max = 0.05;

    o_options.user.filter_strength = 1.0e-2;
    o_options = st_struct_update(o_options, options);

    % }}}

    % {{{

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'cost', c_options, ...
        'optim', o_options);

    r.extra_options = struct();

    % }}}
end
