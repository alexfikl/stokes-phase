% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_paper3_data(varargin)
    options = st_struct_varargin(varargin{:});

    % {{{ exact parameters from the paper

    % boundary conditions
    param.Ca = 1.0;
    param.lambda = 48.02;
    % param.lambda = 1.0;
    param.alpha = 1.0;
    param.R = 1.0;
    param = st_struct_update(param, options);

    b_options.fn = 'FourRoller';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump conditions
    j_options.jump_type = 'capillary';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 64;
    g_options.curve = @(xi) st_mesh_curve(xi, 'circle', 'R', b_options.param.R);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'normal_velocity';
    c_options.desired_curve = @(xi) st_mesh_curve(xi, 'ufo', 'k', 1, 'a', 1);

    c_options.user.udx = [];
    c_options = st_struct_update(c_options, options);

    % optim
    o_options.beta_update = 'fletcherreeves';
    o_options.maxiter = 128;
    o_options.outputfreq = 1;
    o_options.gtol = 1.0e-6;
    o_options.alpha_min = 1.0e-8;
    o_options.alpha_max = @st_optim_geometry_alpha_max;

    o_options.user.filter_strength = 1.0e-2;
    o_options = st_struct_update(o_options, options);

    % }}}

    % {{{

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'cost', c_options, ...
        'optim', o_options);

    if isempty(r.cost.user.udx)
        r.cost = get_desired_velocity_cached(options, r.cost, r.bc);
    end

    % }}}
end

function [cost] = get_desired_velocity_cached(options, cost, bc)
    % create cache
    cache = st_cache_create(options.cache_datadir, [], options.cache_prefix);

    % get a filename
    filename = [];
    if ~isempty(cache)
        filename = cache.filename(cache, 'desired_velocity');
    end

    % if the filename exists, check if the data is fresh
    from_cache = false;
    if ~isempty(filename) && exist(filename, 'file') == 2
        load(filename, 'udx', 'udy');
        from_cache = length(udx) == length(cost.x.x);
    end

    % if the cache is stale or doesn't exist, recompute
    if ~from_cache
        quad = st_layer_quadrature(cost.x, cost.y, cost.variables);
        cost.y.quad = st_struct_merge(cost.y.quad, quad, true);

        st = st_repr_density_solution(cost.x, cost.y, bc, cost.variables);

        udx = st.u;
        udy = st_interpolate(cost.x, udx, cost.y.xi);

        if ~isempty(filename)
            save(filename, 'udx', 'udy');
        end
    end

    cost.user.udx = st_dot(udx, cost.x.n);
    cost.user.udy = st_dot(udy, cost.y.n);
end
