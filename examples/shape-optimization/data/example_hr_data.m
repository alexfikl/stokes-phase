% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_hr_data(varargin)
    % Description: This test uses the Hadamard-Rybczynski analytical solution
    % for both the desired and current guess of the geometry.

    options = st_struct_varargin(varargin{:});

    % {{{ forward problem

    % boundary conditions
    param.Ca = 1.0;
    param.lambda = 1.0;
    param.uinf = 1.0;
    param.alpha = 0.25;
    param.R = 2.0;

    b_options.fn = 'HadamardRybczynski';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % adjoint boundary conditions
    % param.uinf = 0.0;
    % param.alpha = 1.25;
    %
    % a_options.param = param;
    % a_options.fn = 'HadamardRybczynski';

    % jump conditions
    j_options.jump_type = 'hadamard';
    j_options.user.enable_capillary = true;
    j_options.user.enable_angle = false;
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'circle', 'R', param.R);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'normal_velocity';
    c_options.desired_curve = @(xi) zeros(size(xi));

    c_options.user.udx = 0.0;
    c_options.user.udy = 0.0;
    c_options = st_struct_update(c_options, options);

    % }}}

    % {{{

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'cost', c_options);

    r.extra_options = struct();
end
