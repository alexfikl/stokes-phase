% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_paper1_data(varargin)
    options = st_struct_varargin(varargin{:});

    % {{{ exact parameters from the paper

    % boundary conditions
    param.Ca = 1.0;
    param.lambda = 1.0 / 5.0;
    param.alpha = 1.0;
    param.a = 1.0;
    param.b = 2.0;
    options.param = st_struct_update(param, options);

    % geometry
    options.curve = @(xi) st_mesh_curve(xi, 'ellipse', ...
        'a', options.param.a, 'b', options.param.b);

    % cost
    options.eps = 1.0e-6;
    options.sigma = 1.0e-2;
    options.bump_normalized = false;
    options.user.udx = 0.0;

    % grid sizes used for convergence study
    extra_options.resolutions = [8, 16, 24, 32];
    extra_options.epsilons = 10.^-(1:7);

    % }}}

    % {{{

    r = example_extensional_data(options);
    r.extra_options = st_struct_merge(r.extra_options, extra_options, true);

    % }}}

end
