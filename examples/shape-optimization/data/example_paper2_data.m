% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = st_paper2_data(varargin)
    options = st_struct_varargin(varargin{:});

    % {{{ exact parameters from the paper

    % boundary conditions
    options.lambda = 1.0;

    % grid sizes used for convergence study
    extra_options.resolutions = [8, 12, 16, 20, 24, 28, 32, 44, 48];

    % }}}

    % {{{

    r = example_paper1_data(opts);
    r.extra_options = st_struct_merge(r.extra_options, extra_options, true);

    % }}}
end
