% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_ufo_data(varargin)
    % Description: This test case uses a more complicated geometry
    % and desired velocity field for stress testing the adjoint gradient.

    options = st_struct_varargin(varargin{:});

    % {{{ defaults

    % boundary conditions
    param.Ca = 1.0;
    param.lambda = 5.0;
    param.alpha = 0.25;
    param.R = 1.0;

    b_options.fn = 'ExtensionalFlow';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump condition
    j_options.jump_type = 'capillary';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'ufo', 'R', b_options.param.R, 'k', 2);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'normal_velocity';

    c_options.user.udx = 0.0;
    c_options.user.udy = 0.0;
    c_options = st_struct_update(c_options, options);

    % }}}

    % {{{

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'cost', c_options);

    % }}}
end
