% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_graphical_abstract_data(varargin)
    opts = st_struct_varargin(varargin{:});

    %
    % parameters
    %

    % fluid
    opts.param.Ca = 1.0;
    opts.param.lambda = 1.0;
    opts.param.enable_capillary = true;
    opts.param.enable_angle = false;

    % number of panels
    opts.npanels = 128;
    % curves
    opts.curve = @(xi) st_mesh_curve(xi, 'ufo', 'k', 4, 'a', 2);
    opts.desired_curve = @(xi) st_mesh_curve(xi, 'circle', 'R', 3.5);
    % desired velocity
    hr = st_sym_boundary('HadamardRybczynski', struct('R', 3.5));
    opts.ud = @(x) st_dot(hr.u_ext(x), x.n);

    %
    % generate
    %

    r = example_extensional_data(opts);
end
