% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_infinity_data(varargin)
    % Description: This example has an infinite Capillary number (i.e. no
    % surface tension) and a very large viscosity ratio corresponding to
    % something like an air bubble in water.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    param.Ca = Inf;
    param.lambda = 0.01;
    param.alpha = 1.0;
    param.uinf = 1.0;
    param.pinf = 10.0;
    param.a = 1.0;
    param.b = 1.0;

    b_options.fn = 'UniformFlow';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump options
    j_options.jump_type = 'zero';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'ellipse', ...
        'a', b_options.param.a, 'b', b_options.param.b);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'normal_velocity';
    c_options.desired_curve = @(xi) st_mesh_curve(xi, 'ellipse', ...
        'a', b_options.param.b, 'b', b_options.param.a);

    c_options.user.udx = [];
    c_options = st_struct_update(c_options, options);

    % }}}

    % {{{

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'cost', c_options);

    % NOTE: doing this after so that the cost geometry is properly initialized
    if isempty(r.cost.user.udx)
        r.cost.user.udx = 0.0;
        r.cost.user.udy = 0.0;
    elseif isa(r.cost.user.udx, 'function_handle')
        r.cost.user.udy = r.cost.user.udx(r.cost.y);
        r.cost.user.udx = r.cost.user.udx(r.cost.x);
    else
        % assumes this is just a constant now
        r.cost.user.udy = r.cost.user.udx;
    end

    r.extra_options = struct();

    % }}}
end
