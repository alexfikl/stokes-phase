% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_gradient_finite(varargin)
    % Plot data from :func:`shp_gradient_finite`.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       plot_gradient_finite(...
    %           'example_id', 'extensional', 'cache_id', cache_id);
    %
    % where the call should be the same as for a previous call to
    % :func:`shp_gradient_finite`.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'finite';
    options.reload_options = true;

    options.example_id = st_struct_field(options, 'example_id', 'geometry');
    options.resolutions = st_struct_field(options, 'resolutions', ...
        [4, 8, 12, 16, 20, 24, 28, 32, 36, 40]);

    st_unittest_example_runner(@run_plot_gradient_finite, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_gradient_finite(~, cache, options)
    % {{{ plot gradients

    fig = gobjects(1, 2);
    fig_ax = gobjects(1, 2);
    for i = 1:length(fig)
        fig(i) = figure();
        fig_ax(i) = gca();
    end
    fig_lines = gobjects(1, length(options.resolutions));

    eoc = st_util_eoc({'grad'}, length(options.resolutions));

    for i = 1:length(options.resolutions)
        npanels = options.resolutions(i);

        % reload
        filename = cache.filename(cache, sprintf('%04d_gradient', npanels));
        r = load(filename);
        x = r.x;
        y = r.y;

        % compute errors
        eoc.errors.grad(i) = st_ax_error(x, y, r.ad_grad_bump, r.fd_grad);
        eoc.h(i) = st_mesh_h_max(x, y);
        eoc.npoints(i) = npanels;
        eoc = st_util_eoc(eoc);

        % plot gradients
        h1 = plot(fig_ax(1), x.xi, r.fd_grad, '-');
        h2 = plot(fig_ax(1), x.xi, r.ad_grad_bump, '--', 'Color', get(h1, 'Color'));
        h3 = plot(fig_ax(1), x.xi, r.ad_grad, ':', 'Color', get(h1, 'Color'));

        % plot pointwise errors
        err = log10(abs(r.ad_grad_bump - r.fd_grad) + 1.0e-16);
        fig_lines(i) = plot(fig_ax(2), x.xi, err);
    end

    xlabel('$\xi$');
    plot(fig_ax(1), x.xi, 0.0 * x.xi, 'k.');
    legend([h1, h2], {'$FD \cdot h$', '$AD \cdot h$'}, 'Location', 'Best');
    st_print(fig(1), cache.filename(cache, 'gradient', 'png'));

    % set(fig_ax(2), 'YMinorTick', 'on');
    set(get(fig_ax(2), 'YAxis'), 'TickDirection', 'both');
    xlabel(fig_ax(2), '$\xi$');
    ylabel(fig_ax(2), '$|\nabla_{AD} - \nabla_{FD}|$');
    legend(fig_lines, strsplit(num2str(options.resolutions)), 'Location', 'EastOutside');
    st_print(fig(2), cache.filename(cache, 'error', 'png'));

    % }}}

    % {{{ plot errors

    filename = cache.filename(cache, 'convergence', 'png');
    [~, fig] = st_util_eoc_show(eoc, filename, ...
        'legend_labels', {'\nabla \mathcal{J}'}, ...
        'ylabel', 'E', ...
        'expected_order', x.nbasis - 1.0, ...
        'legend_location', 'none');

    % add a O(h^3) line
    x0 = min(eoc.h); y0 = min(eoc.errors.grad);
    x1 = max(eoc.h);
    y1 = 10^(3 * (log10(x1) - log10(x0)) + log10(y0));

    loglog([x0, x1], [y0, y1], 'k-', 'LineWidth', 4);
    st_print(fig, filename);

    % }}}

    % {{{ plot gradient components

    options.npanels = options.resolutions(end);
    options.npanels = 32;
    problem = options.problem_init(options);

    plot_gradient_components(cache, problem.geometry, problem.bc, problem.cost);

    % }}}

    % {{{ plot geometry

    cost = problem.cost;

    grad = st_array_ax(-r.ad_grad .* x.n, true);
    xc = st_array_ax(x.x, true);
    vc = st_array_ax(x.vertices, true);

    if isfield(cost.x, 'x')
        xd = st_array_ax(cost.x.x, true);
        vd = st_array_ax(cost.x.vertices, true);
    end

    axis('equal');
    plot(real(xc), imag(xc), '--');
    plot(real(vc), imag(vc), 'k.', 'MarkerSize', 20);
    quiver(real(xc), imag(xc), real(grad), imag(grad));

    if isfield(cost.x, 'x')
        plot(real(xd), imag(xd), '--');
        plot(real(vd), imag(vd), 'k.', 'MarkerSize', 20);
    end

    xlabel('$x$');
    ylabel('$\rho$');
    st_print(fig, cache.filename(cache, 'geometry', 'png'));

    % }}}
end

function plot_gradient_components(cache, geometry, bc, cost)
    x = geometry.x;
    y = geometry.y;

    % compute forward and adjoint solutions
    [~, st] = st_shape_adjoint_gradient(x, y, cost, bc);
    terms = st.terms;
    fields = fieldnames(terms);

    % plot
    if length(fields) > 0
        nterms = length(fields);
        names = cell(1, nterms);

        fig(1) = figure(); fig_ax(1) = gca();
        fig(2) = figure(); fig_ax(2) = gca();
        for i = 1:nterms
            iterm = terms.(fields{i});

            plot(fig_ax(1), x.xi, iterm);
            st_print(fig(1), cache.filename(cache, ...
                sprintf('component_%02d', i), 'png'));
            cla(fig_ax(1));

            plot(fig_ax(2), x.xi, cost.bump(cost, x, y, iterm));
            st_print(fig(2), cache.filename(cache, ...
                sprintf('component_bump_%02d', i), 'png'));
            cla(fig_ax(2));
        end
    end
end


