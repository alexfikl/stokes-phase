% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_optim_shape(varargin)
    orig_config = st_unittest_setup();
    opts = st_struct_varargin(varargin{:}, ...
        'test_type', 'optim', ...
        'optim', true);

    % {{{

    d.example_prefix = 'optim';
    d.example_id = 'extensional';
    d.reload_options = true;
    d.niters = [];

    options = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    st_unittest_example_runner(@run_plot_optim_shape, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_optim_shape(problem, cache, options)
    % {{{ load solutions

    if isfield(problem, 'geometry')
        cost = problem.cost;
        x0 = problem.geometry.x;
        y0 = problem.geometry.y;
    else
        % NOTE: allows to reload some older files
        cost = problem.cost;
        x0 = problem.problem.x;
        y0 = problem.problem.y;
    end

    % last solution file
    [~, filename] = cache.filenext(cache, 'iterations');
    r = load(filename);
    if ~isempty(options.niters)
        r.niters = options.niters;
    end

    % construct final geometry
    [xf, yf] = st_mesh_geometry_update(x0, y0, r.x);

    % }}}

    % {{{ plot initial gradient

    state = load_iteration(cache, x0, y0, 0);

    fig = figure();
    plot(x0.xi, real(state.direction))
    plot(x0.xi, imag(state.direction))
    xlabel('$\xi$');
    ylabel('$\mathbf{d}$');
    st_print(fig, cache.filename(cache, 'gradient_value', 'png'));

    % }}}

    % {{{ plot optimal geometry

    z0 = st_array_ax(x0.x, true);
    zd = st_array_ax(cost.x.x, true);
    zf = st_array_ax(xf.x);
    xi = x0.xi;

    fig = figure();
    h1 = plot(real(z0), imag(z0), '--');
    h2 = plot(real(zd), imag(zd), ':');
    h3 = plot(real(zf), imag(zf), '.', 'MarkerSize', 14);
    axis('equal');
    xlabel('$x$');
    ylabel('$\rho$');
    % legend([h1, h2, h3], {'$Initial$', '$Desired$', '$Final$'});
    st_print(fig, cache.filename(cache, 'geometry', 'png'));

    fig = figure();
    plot(xi, x0.kappa, '--');
    plot(xi, cost.x.kappa, ':');
    plot(xi, xf.kappa, '.', 'MarkerSize', 14);
    xlabel('$\xi$');
    ylabel('$\kappa$');
    st_print(fig, cache.filename(cache, 'curvature', 'png'));

    % }}}

    % {{{ plot optimal velocity

    % NOTE: this handles older file versions
    if isfield(cost.user, 'udx')
        udx = cost.user.udx;
    elseif isfield(cost, 'ud');
        udx = cost.ud;
    else
        udx = [];
    end

    if ~isempty(udx)
        state0 = load_iteration(cache, x0, y0, 0);
        statef = load_iteration(cache, x0, y0, r.niters);

        fig = figure();
        plot(xi, state0.u_dot_n, '--');
        plot(xi, udx, ':');
        plot(xi, statef.u_dot_n, '.', 'MarkerSize', 14);

        xlabel('$\xi$');
        ylabel('$\mathbf{u} \cdot \mathbf{n}$');
        st_print(fig, cache.filename(cache, 'velocity', 'png'));
    end

    % }}}

    % {{{ plot stats

    stats = r.stats;

    relf = log10(abs(stats.func) / abs(stats.func(1)));
    f = log10(abs(stats.func));
    ticks = floor(min([f, relf])):ceil(max([f, relf]));

    fig = figure();
    % set(gca(), 'YScale', 'log');
    plot(f, 'k-');
    plot(relf, 'k--');
    grid('on');
    set(gca, 'YTick', ticks);
    xlabel('$Iteration$');
    ylabel('$\log_{10} \hat{\mathcal{J}}$');
    st_print(fig, cache.filename(cache, 'cost', 'png'));

    relf = log10(abs(stats.func(2:end) - stats.func(1:end - 1)));

    fig = figure();
    plot(relf, 'k');
    grid('on');
    xlabel('$Iteration$');
    ylabel('$\mathcal{J}^{k + 1} - \mathcal{J}^k$');
    st_print(fig, cache.filename(cache, 'cost_relative', 'png'));

    g = log10(stats.grad);
    relg = log10(stats.grad / stats.grad(1));
    reld = log10(stats.ddir / stats.ddir(1));
    ticks = floor(min([g, relg, reld])):ceil(max([g, relg, reld]));

    fig = figure();
    h1 = plot(g, 'k-');
    h2 = plot(relg, 'k--');
    h3 = plot(reld, 'k:');
    grid('on');
    xlabel('$Iteration$');
    ylabel('$\log_{10} \|\hat{\mathbf{g}}\|_\infty$');
    % legend([h1, h2], {'$g_k$', '$d_k$'}, 'Location', 'EastOutside');
    st_print(fig, cache.filename(cache, 'gradient', 'png'));

    % }}}

    % {{{ evolution

    return;

    if ~isempty(udx)
        variables = {'geometry', 'velocity'};
    else
        variables = {'geometry'};
    end

    nvariables = length(variables);

    vid = cell(1, nvariables);
    fig = gobjects(1, nvariables);
    fax = gobjects(1, nvariables);

    for m = 1:nvariables
        vid{m} = VideoWriter(cache.filename(cache, variables{m}, 'avi'));   %#ok
        vid{m}.FrameRate = max(1, r.niters / 10);
        open(vid{m});

        fig(m) = figure('Color', 'white', 'Position', [0, 0, 1024, 1024]);
        fax(m) = gca();
    end

    zx = [z0, zf, zd];
    xlabel(fax(1), '$x$');
    ylabel(fax(1), '$\rho$');
    axis(fax(1), 'equal');
    xlim(fax(1), [min(real(zx)) - 0.25, max(real(zx)) + 0.25]);
    ylim(fax(1), [min(imag(zx)) - 0.25, max(imag(zx)) + 0.25]);
    clear zx;

    if ~isempty(udx)
        xlabel(fax(2), '$\xi$');
        ylabel(fax(2), '$\mathbf{u} \cdot \mathbf{n}$');
    end

    for n = 1:r.niters
        last_line_size = fprintf('[%05d/%05d] saving frames...', n, r.niters);
        state = load_iteration(cache, x0, y0, n - 1);

        zn = st_array_ax(state.x.x, true);
        dn = st_array_ax(state.direction, true);

        plot(fax(1), real(z0), imag(z0), '--');
        plot(fax(1), real(zd), imag(zd), '-');
        plot(fax(1), real(zn), imag(zn), 'k:');
        quiver(fax(1), real(zn), imag(zn), real(dn), imag(dn), 'k');

        if ~isempty(udx)
            plot(fax(2), xi, state0.u_dot_n, '--');
            plot(fax(2), xi, udx, '-');
            plot(fax(2), xi, state.u_dot_n, 'k:');
        end

        for m = 1:nvariables
            writeVideo(vid{m}, getframe(fig(m)));

            if n < r.niters
                cla(fax(m));
            end
        end
        fprintf(repmat('\b', 1, last_line_size));
    end
    fprintf('\n');

    for m = 1:nvariables
        st_print(fig(m), cache.filename(cache, variables{m}, 'png'));

        close(vid{m});
        close(fig(m))
    end

    % }}}
end

function [state] = load_iteration(cache, x0, y0, n)
    filename = cache.filename(cache, sprintf('solution_%05d', n));
    state = load(filename);

    [state.x, state.y] = st_mesh_geometry_update(x0, y0, state.x);

    if ~isempty(state.velocity)
        state.u_dot_n = st_dot(state.velocity, state.x.n);
    end
end
