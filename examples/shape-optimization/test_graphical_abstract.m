% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function test_graphical_abstract(varargin)
    %
    % initialize
    %

    % default matlab setup
    orig_config = st_unittest_setup();
    % user options
    options = st_struct_varargin(varargin{:});

    % get cache
    datadir = fullfile(pwd, 'cache', options.cachedir);
    if ~isfolder(datadir)
        error('datadir does not exist: %s', datadir);
    end

    % load data
    test_id = st_struct_field(options, 'test_id', 'paper3');
    filename = fullfile(datadir, sprintf('%s_optim_setup.mat', test_id));
    d = load(filename);
    problem = d.problem;
    cost = d.cost;
    assert(strcmp(d.test_id, test_id));

    %
    % plot
    %

    % get total number of files
    nfiles = 0;
    while true
        filename = fullfile(datadir, ...
            sprintf('%s_optim_%04d_solution.mat', test_id, nfiles));
        if exist(filename, 'file') ~= 2
            break;
        end

        nfiles = nfiles + 1;
    end

    fig = figure();
    set(gca, 'visible', 'off');

    % plot evolution
    freq = floor(nfiles / 8);
    for n = [1, 3, 5, freq:freq:nfiles - 1]
        x = loadx(datadir, test_id, n);
        plot(real(x), imag(x), 'k--', 'LineWidth', 3);
    end

    % plot initial and desired states
    x = loadx(datadir, test_id, 0);
    plot(real(x), imag(x), 'LineWidth', 6);
    x = loadx(datadir, test_id, nfiles - 1);
    plot(real(x), imag(x), 'LineWidth', 6);

    % plot streamfunctions
    X = linspace(-2.4, 2.4, 64);
    Y = linspace(-2, 2, 64);
    [X, Y] = meshgrid(X, Y);
    U = X;
    V = -0.5 * Y;
    startx = linspace(-2.4, 2.4, 16);
    starty = 2 * ones(size(startx));
    h = streamline(X, Y, U, V, startx, starty);
    set(h, 'Color', [0, 0, 0, 0.15]);
    h = streamline(X, Y, U, V, startx, -starty);
    set(h, 'Color', [0, 0, 0, 0.15]);

    axis('equal');
    st_print(fig, 'graphical_abstract.jpg');

    %
    % finalize
    %

    st_unittest_setup(orig_config, true);
end

function [x] = loadx(datadir, test_id, n)
    filename = fullfile(datadir, ...
        sprintf('%s_optim_%04d_solution.mat', test_id, n));
    d = load(filename);
    x = st_array_ax(d.x, true);
end
