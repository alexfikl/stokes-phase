% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_gradient_convergence(varargin)
    % Plot data from :func:`shp_gradient_convergence`.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       plot_gradient_convergence(...
    %           'example_id', 'extensional', 'cache_id', cache_id);
    %
    % where the call should be the same as for a previous call to
    % :func:`shp_gradient_convergence`.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'convergence';
    options.reload_options = true;

    options.example_id = st_struct_field(options, 'example_id', 'geometry');

    st_unittest_example_runner(@run_plot_gradient_convergence, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_gradient_convergence(~, cache, options)

    % {{{ plot

    fig = figure();
    fig_ax = gca();
    set(fig_ax, 'XScale', 'log', 'YScale', 'log');

    for i = 1:length(options.resolutions)
        npanels = options.resolutions(i);
        [errors, h] = plot_epsilon_convergence(cache, npanels, options.epsilons);

        p = polyfit(log(errors.eps(1:4)), log(errors.grad(1:4)), 1);
        fprintf('Order: %.2f\n', p(1));

        cv_error(i) = min(errors.grad);
        cv_dx(i) = h;

        loglog(fig_ax, errors.eps, errors.grad, 'o--');
    end

    for i = 1:length(options.resolutions)
        npanels = options.resolutions(i);
        if i == 1
            fprintf('Error: %5d %.5e ---- \n', ...
                npanels, real(cv_error(i)));
        else
            cv_order = polyfit(log(cv_dx(i - 1:i)), log(cv_error(i - 1:i)), 1);
            fprintf('Error: %5d %.5e %.2f\n', ...
                npanels, real(cv_error(i)), cv_order(1));
        end
    end

    % plot an O(h) line
    y0 = errors.grad(1); x0 = errors.eps(1);
    y1 = min(errors.grad);
    x1 = 10^((log10(y1) - log10(y0)) + log10(x0));

    h1 = loglog(fig_ax, [x0, x1], [y0, y1], 'k-', 'LineWidth', 4);
    xlabel(fig_ax, '$\epsilon$');
    ylabel(fig_ax, '$E$');

    set(fig_ax, 'YMinorTick', 'on', 'YMinorGrid', 'on');
    set(get(fig_ax, 'YAxis'), 'TickDirection', 'both');
    set(fig_ax, 'YTick', fliplr(10.^-(1:4)));
    set(fig_ax, 'XTick', fliplr(errors.eps(2:end - 1)));

    % legend(h1, '$\mathcal{O}(\epsilon)$', 'Location', 'northwest');
    st_print(fig, cache.filename(cache, 'gradient_error', 'png'));

    % }}}
end

function [errors, h] = plot_epsilon_convergence(cache, npanels, epsilons)
    filename = cache.filename(cache, sprintf('%04d_gradient', npanels));
    load(filename, 'fd_gradJ', 'ad_gradJ', 'ad_bumped_gradJ', 'x', 'y');

    errors.eps = epsilons;
    errors.grad = zeros(size(errors.eps));

    fig = figure();
    % plot(x.xi, ad_gradJ, '-');
    % plot(x.xi, 0.0, '-');
    for i = 1:length(epsilons)
        errors.grad(i) = st_ax_error(x, y, ad_bumped_gradJ, fd_gradJ{i});
        fprintf('[%3d] %.5e: %.5e\n', npanels, errors.eps(i), errors.grad(i));

        dv = log10(abs(fd_gradJ{i} - ad_bumped_gradJ) + 1.0e-16);
        plot(x.xi, dv);
    end

    xlabel('$\xi$');
    ylabel('$\log_{10} E$');
    st_print(fig, cache.filename(cache, ...
        sprintf('%04d_gradient_error', npanels), 'png'));

    h = st_mesh_h_max(x, y);
end
