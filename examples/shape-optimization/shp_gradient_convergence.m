% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function shp_gradient_convergence(varargin)
    % Compute finite difference and adjoint gradients.
    % Convergence can be checked by providing multiple resolutions and finite
    % difference step sizes.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       shp_gradient_convergence(...
    %           'example_id', 'extensional', ...
    %           'cache_id', 'now', ...
    %           'resolutions', [4, 8, 16, 32], ...
    %           'epsilons', 10.^-(1:7));
    %
    % where the parameters are defined in `data/test_extensional_data.m`.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();

    % {{{ run

    d.example_prefix = 'convergence';
    d.example_id = 'geometry';
    d.resolutions = [8, 12, 16, 20, 24, 28, 32];
    d.epsilons = 10.^-(1:7);

    options = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    st_unittest_example_runner(@run_gradient_convergence, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_gradient_convergence(~, cache, options)
    for i = 1:length(options.resolutions)
        options.npanels = options.resolutions(i);
        problem = options.problem_init(options);

        % unroll
        x = problem.geometry.x;
        y = problem.geometry.y;
        cost = problem.cost;
        bc = problem.bc;

        tic();
        compute_gradients(cache, x, y, bc, cost, options.epsilons);
        t_end = toc();

        fprintf('npanels %5d toc %.2fs\n', x.npanels, t_end);
    end
end

function compute_gradients(cache, x, y, bc, cost, epsilons)
    % {{{ adjoint gradient

    [gradJ, ~] = st_shape_adjoint_gradient(x, y, cost, bc);
    ad_gradJ = gradJ;
    ad_bumped_gradJ = cost.bump(cost, x, y, gradJ);

    % }}}

    % {{{ finite difference gradient

    fd_gradJ = cell(size(epsilons));

    for n = 1:length(epsilons)
        cost.eps = epsilons(n);
        fd_gradJ{n} = st_shape_finite_gradient(cost, x, y, bc, true);

        err = st_ax_error(x, y, ad_bumped_gradJ, fd_gradJ{n});
        fprintf('npanels %5d eps %.5e error %.5e\n', x.npanels, cost.eps, err);
    end

    % }}}

    filename = cache.filename(cache, sprintf("%04d_gradient", x.npanels));
    save(filename, 'ad_gradJ', 'ad_bumped_gradJ', 'fd_gradJ', 'x', 'y');
end
