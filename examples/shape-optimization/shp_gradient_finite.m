% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function shp_gradient_finite(varargin)
    % Compute finite difference and adjoint gradients.
    % Convergence can be checked by providing multiple resolutions.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       shp_gradient_finite(...
    %           'example_id', 'extensional', ...
    %           'cache_id', 'now', ...
    %           'resolutions', [4, 8, 16, 32]);
    %
    % where the parameters are defined in `data/test_extensional_data.m`.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'finite';
    options.example_id = st_struct_field(options, 'example_id', 'geometry');
    options.resolutions = st_struct_field(options, 'resolutions', ...
        [4, 8, 12, 16, 20, 24, 28, 32, 36, 40]);

    st_unittest_example_runner(@run_gradient_finite, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_gradient_finite(~, cache, options)
    for n = 1:length(options.resolutions)
        options.npanels = options.resolutions(n);
        problem = options.problem_init(options);

        % unroll
        x = problem.geometry.x;
        y = problem.geometry.y;
        cost = problem.cost;
        bc = problem.bc;

        % {{{ finite difference gradient

        fd_grad = st_shape_finite_gradient(cost, x, y, bc, true);

        % }}}

        % {{{ adjoint gradient

        [ad_grad, ~] = st_shape_adjoint_gradient(x, y, cost, bc);
        ad_grad_bump = cost.bump(cost, x, y, ad_grad);

        % }}}

        filename = cache.filename(cache, sprintf('%04d_gradient', x.npanels));
        save(filename, 'fd_grad', 'ad_grad', 'ad_grad_bump', 'x', 'y');

        grad_error = norm(fd_grad - ad_grad_bump, Inf) / norm(fd_grad, Inf);
        fprintf('error: %6d %.6e\n', x.npanels, grad_error);
    end
end
