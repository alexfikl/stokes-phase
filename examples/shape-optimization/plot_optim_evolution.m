% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_optim_evolution(varargin)
    orig_config = st_unittest_setup();
    opts = st_struct_varargin(varargin{:}, ...
        'test_type', 'optim', ...
        'optim', true);

    % {{{ cache setup

    % test_id: see data directory for files named `test_{test_id}_data.m`
    test_id = st_struct_field(opts, 'test_id', 'geometry');

    cache_id = st_struct_field(opts, 'cache_id', []);
    datadir = st_struct_field(opts, 'datadir', []);

    cache = st_cache_create(datadir, cache_id, ...
        sprintf('%s_%s', opts.test_type, test_id));

    % }}}

    % {{{ load data

    % initial conditions
    filename = cache.filename(cache, 'setup');
    load(filename, 'problem', 'cost', 'options');

    x0 = problem.x;
    y0 = problem.y;

    [~, filename] = cache.filenext(cache, 'iterations');
    load(filename, 'niters');

    % }}}

    % {{{ plot

    zd = st_array_ax(cost.x.x); zd = [zd, zd(1)];

    fig = figure();
    for n = 0:niters - 1
        filename = cache.filename(cache, sprintf('solution_%05d', n));
        load(filename, 'x', 'direction', 'velocity');

        % get geometry
        x = st_struct_update(x0, struct('x', x));
        y = y0;
        [x, y] = st_mesh_geometry(x, y);

        zc = st_array_ax(x.x); zc = [zc, zc(1)];
        dc = st_array_ax(direction); dc = [dc, dc(1)];

        axis('equal');
        plot(real(zc), imag(zc));
        plot(real(zd), imag(zd), 'k-');
        quiver(real(zc), imag(zc), real(dc), imag(dc));
        xlabel('$x$');
        ylabel('$\rho$');
        ylim([-2.5, 2.5]);
        xlim([-2.5, 2.5]);
        st_print(fig, cache.filename(cache, sprintf('geometry_%05d', n), 'png'));
        clf(fig);

        if ~isempty(velocity)
            plot(x.xi, st_dot(velocity, x.n));
            plot(x.xi, cost.ud, 'k--');
            xlabel('$\xi$');
            ylabel('$\mathbf{u} \cdot \mathbf{n}$');
            st_print(fig, cache.filename(cache, sprintf('velocity_%05d', n), 'png'));
            clf(fig);
        end
    end

    % }}}
end
