% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function example_optim_shape(varargin)
    % Perform shape optimization.
    %
    % Example run:
    %
    %   .. code:: matlab
    %
    %       shp_gradient_finite(...
    %           'example_id', 'extensional', 'cache_id', 'now');
    %
    % where the parameters are defined in `data/test_extensional_data.m`.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();

    % {{{ options

    d.example_prefix = 'optim';
    d.example_id = 'extensional';

    d.from_restart = false;
    d.restartfile = [];
    d.display = 1;

    options = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    if options.from_restart
        options.reload_options = true;
    end

    % }}}

    % {{{ run

    st_unittest_example_runner(@run_optim_shape, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_optim_shape(problem, cache, options)
    if ~isfield(problem, 'optim')
        error('optimization not set up');
    end

    % {{{ restart

    [filename, restartfile] = cache.filenext(cache, 'iterations');
    fprintf('output: %s\n', filename);

    if ~isempty(options.restartfile)
        restartfile = options.restartfile;
    end

    if isempty(restartfile) && options.from_restart
        error('cannot restart if no restart file is available');
    end

    if options.from_restart
        r = load(restartfile);
        if length(r.x) ~= length(problem.geometry.x.x)
            error('invalid restart solution size');
        end

        x0 = r.x;
        problem.user.start_it = r.niters;

        fprintf('restartfile: %s\n', restartfile);
        fprintf('after %d iterations\n', r.niters);
    else
        x0 = problem.geometry.x.x;
        problem.user.start_it = 0;
    end

    % }}}

    % {{{ setup optimization


    % set optimization functions
    cg = problem.optim;

    cg.x0 = x0;
    cg.fn = @(z) fn_optim_gradient(z, problem);
    cg.project = @(z, d) fn_optim_project(z, d, problem);
    cg.output = @(info) fn_optim_output(info, cache, problem);

    % }}}

    % {{{ optimize

    [x, stats] = st_optim_cg(cg);

    if options.from_restart
        stats.fn_call = stats.fn_call + r.stats.fn_call;
        stats.gn_call = stats.gn_call + r.stats.gn_call;

        stats.func = [r.stats.func(1:end - 1), stats.func];
        stats.grad = [r.stats.grad(1:end - 1), stats.grad];

        stats.ddir = [r.stats.ddir, stats.ddir];
        stats.alpha = [r.stats.alpha, stats.alpha];
    end

    niters = length(stats.func) - 1;
    save(filename, 'stats', 'x', 'niters');

    % }}}

    % delete for next simulation
    filename = cache.filename(cache, 'user', 'stop');
    if exist(filename, 'file') == 2
        delete(filename);
    end
end

function [fn, grad] = fn_optim_gradient(x, problem)
    % unroll
    geometry = problem.geometry;
    bc = problem.bc;
    cost = problem.cost;

    % update geometry
    [x, y] = st_mesh_geometry_update(geometry.x, geometry.y, x);

    % compute function and gradient
    if nargout > 1
        [grad, st] = st_shape_adjoint_gradient(x, y, cost, bc);
        st = st.forward;

        % smooth out the gradient
        filter_strength = problem.optim.user.filter_strength;
        if filter_strength > 0
            grad = st_ax_filter(grad, 'fft', filter_strength);
        end

        grad = grad .* x.n;
    else
        st = st_repr_density_solution(x, y, problem.bc, cost.variables);
    end

    fn = cost.fn.evaluate(cost, 0.0, x, y, st);
end

function [xp] = fn_optim_project(x, d, problem)
    filter_strength = problem.optim.user.filter_strength;

    % smooth out geometry
    xp = x;
    if filter_strength > 0
        xp = st_ax_filter(x, 'fft', 1.0e-1 * filter_strength);
    end

    % center it at the origin
    xp = xp - sum(real(xp)) / length(xp);
end

function [stop] = fn_optim_output(values, cache, problem)
    stopfile = cache.filename(cache, 'user', 'stop');
    stop = exist(stopfile, 'file') == 2;

    it = problem.user.start_it + values.iteration;

    % {{{ get velocity

    cost = problem.cost;

    if any(contains(cost.variables, 'u'))
        geometry = problem.geometry;
        [x, y] = st_mesh_geometry_update(geometry.x, geometry.y, values.x);

        st = st_repr_density_solution(x, y, problem.bc, cost.variables);
        velocity = st.u;
    else
        velocity = [];
    end

    % }}}

    % {{{ save

    x = values.x;
    direction = values.d;

    filename = cache.filename(cache, sprintf('solution_%05d', it));
    save(filename, 'x', 'direction', 'velocity');

    % }}}
end
