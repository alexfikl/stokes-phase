% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_evolution(plotter, problem, cache, options)
    state = load(cache.filename(cache, 'state'));
    ncheckpoints = length(state.checkpoints);

    % set defaults
    d.variables = {};
    d.fig_init = [];
    d.fig_plot = [];
    d.start = [];
    d.freq = 1;
    d.framerate = [];
    d.position = [0, 0, 1024, 1024];

    plotter = st_struct_update(d, plotter);

    if isempty(plotter.framerate)
        plotter.framerate = max(23, floor(ncheckpoints / plotter.freq / 10));
    end

    if isempty(plotter.start)
        if startsWith(options.example_prefix, 'adjoint')
            plotter.start = 2;
        else
            plotter.start = 1;
        end
    end

    if isempty(plotter.variables)
        error('nothig to plot :(');
    end

    % unroll
    x = problem.geometry.x;
    y = problem.geometry.y;

    % {{{ plot

    nvariables = length(plotter.variables);
    vid = cell(1, nvariables);
    fig = gobjects(1, nvariables);
    fax = gobjects(1, nvariables);

    for m = 1:nvariables
        % NOTE: mp4 does not seem to be supported on linux
        vid{m} = VideoWriter(cache.filename(cache, plotter.variables{m}, 'avi'));   %#ok
        vid{m}.FrameRate = plotter.framerate;
        open(vid{m});

        fig(m) = figure('Color', 'white', 'Position', plotter.position);
        fax(m) = gca();
        xlabel(fax(m), '$\xi$');
    end
    [fig, fax] = plotter.fig_init(fig, fax);

    progress = st_util_progress('nitems', floor(ncheckpoints / plotter.freq));

    for n = plotter.start:ncheckpoints
        if mod(n - 1, plotter.freq) ~= 0 && n ~= ncheckpoints && n ~= plotter.start
            continue;
        end

        progress = st_util_progress(progress, 'action', 'tic');

        chk = load(state.checkpoints{n});
        plotter.fig_plot(x, y, chk, fax);

        for m = 1:nvariables
            writeVideo(vid{m}, getframe(fig(m)));
            if n == plotter.start || n == ncheckpoints
                st_print(fig(m), cache.filename(cache, ...
                    sprintf('%s_%04d', plotter.variables{m}, n - 1), 'png'));
            end

            cla(fax(m));
        end
        progress = st_util_progress(progress, 'action', 'toc');

        fprintf('[%05d/%05d] t = %.5e (%s)\n', ...
            n - 1, ncheckpoints - 1, chk.t, progress.strfmt(progress));
    end

    for m = 1:nvariables
        close(vid{m});
        close(fig(m));
    end

    % }}}
end


