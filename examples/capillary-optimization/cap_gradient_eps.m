% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function cap_gradient_eps(varargin)
    % Compute finite difference and adjoint gradients.
    % Convergence can be checked by providing multiple resolutions.
    %
    % This caches the results, so two runs with the same parameters will not
    % overwrite existing data unless specified.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       cap_gradient_eps(...
    %           'example_id', 'extensional', ...
    %           'enable_odeplot', true, ...
    %           'overwrite', true, ...
    %           'resolutions', [0.1, 0.01, 0.001]);
    %
    % where the parameters are defined in `data/test_extensional_data.m`. The
    % default settings can be seen by calling that function with no parameters.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'finite_eps';
    options.example_id = st_struct_field(options, 'example_id', 'extensional');
    options.resolutions = st_struct_field(options, 'resolutions', 10.^-(1:7));

    st_unittest_example_runner(@run_gradient_finite_eps, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

% {{{ run_gradient_finite_eps

function [options] = run_gradient_finite_eps(problem, cache, options)
    % disable so that `cap_evolve_forward` does not look for it
    options.param.Ca_d = [];
    param = problem.bc.param;

    % save initial geometry
    options.x = problem.geometry.x;
    options.y = problem.geometry.y;

    % {{{ compute desired geometry

    if ~isempty(param.Ca_d)
        state_d = run_desired(options, param.Ca_d, 'ca_desired');

        options.desired_curve = @(xi) ...
            st_interpolate(state_d.x, state_d.x.x, xi);
        options.desireddir = fullfile(cache.datadir, 'ca_desired');
    end

    % }}}

    % {{{ compute cost functional for each `resolution`

    % compute unperturbed solution
    state_c = run_forward(options, param.Ca, sprintf('ca_%04d', 0));
    J0 = state_c.cost;

    % compute perturbed solutions
    nresolutions = length(options.resolutions);
    ca_eps = options.resolutions;

    J = zeros(size(options.resolutions));
    for n = 1:nresolutions
        Ca = param.Ca + ca_eps(n);
        state_p = run_forward(options, Ca, sprintf('ca_%04d', n));

        J(n) = state_p.cost;
    end

    % }}}

    % {{{ compute finite difference gradients

    fd_grad = zeros(1, nresolutions);
    for n = 1:nresolutions
        fd_grad(n) = (J(n) - J0) / ca_eps(n);

        fprintf('eps %.2e cost %.6e grad %+.6e\n', ca_eps(n), J(n), fd_grad(n));
    end

    % }}}

    % {{{ compute adjoint gradient

    state_c.checkpoints{1};

    options.fddir = fileparts(state_c.checkpoints{1});
    options.user.grad = fd_grad(end);

    state_a = run_adjoint(options, param.Ca, 'ca_adjoint');
    ad_grad = state_a.grad;

    % }}}

    filename = cache.filename(cache, 'gradient');

    ca_eps = [0, ca_eps];
    cost = [J0, J];
    save(filename, 'ca_eps', 'cost', 'fd_grad', 'ad_grad');
end

% }}}

function [state] = run_desired(options, Ca, cache_id)
    options.cost_type = [];
    state = run_forward(options, Ca, cache_id);
end

function [state] = run_forward(options, Ca, cache_id)
    options.verbose = false;
    options.param.Ca = Ca;

    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.cache_id = [];
    options.cache_prefix = sprintf('forward_%s', options.example_id);

    state = cap_evolve_forward(options);
end

function [state] = run_adjoint(options, Ca, cache_id)
    options.param.Ca = Ca;

    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.cache_prefix = sprintf('adjoint_%s', options.example_id);

    state = cap_evolve_adjoint(options);
end

% vim:foldmethod=marker:
