% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_optim_surf(varargin)
    % Plots results from `cap_optim_surf`.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       plot_optim_surf('example_id', 'extensional', 'cache_id', 'same');
    %
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ options

    d.example_prefix = 'optim_surf';
    d.example_id = 'paper5';

    d.reload_options = true;
    d.niters = [];

    options = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    % }}}

    % {{{ run

    st_unittest_example_runner(@run_plot_optim_surf, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_optim_surf(problem, cache, options)
    [~, filename] = cache.filenext(cache, 'iterations');
    r = load(filename);
    if ~isempty(options.niters)
        r.niters = options.niters;
    end

    % {{{ initial conditions

    x = problem.geometry.x;
    h0 = problem.jump.user.h0;
    eps = problem.jump.user.eps;
    gamma = problem.jump.param.gamma;

    fig = figure();
    % plot(x.xi, (gamma - h0) / eps, '-');
    plot(x.xi, gamma, '-');
    ylim([0.0, 2.0]);
    xlabel('$\xi$');
    ylabel('$\gamma$');
    st_print(fig, cache.filename(cache, 'initial_gamma', 'png')); close(fig);

    zx = st_array_ax(x.x, true);

    fig = figure();
    plot(real(zx), imag(zx), '-');
    xlabel('$x$');
    ylabel('$\rho$');
    axis('equal');
    st_print(fig, cache.filename(cache, 'initial_geometry', 'png')); close(fig);

    % }}}

    % {{{ plot evolution

    variables = {'geometry', 'gamma', 'direction'};
    nvariables = numel(variables);

    framerate = max(1, r.niters / 10);

    vid = cell(1, nvariables);
    fig = gobjects(1, nvariables);
    fax = gobjects(1, nvariables);

    for m = 1:nvariables
        vid{m} = VideoWriter(cache.filename(cache, variables{m}, 'avi'));   %#ok
        vid{m}.FrameRate = framerate;
        open(vid{m});

        fig(m) = figure('Color', 'white', 'Position', [0, 0, 1024, 1024]);
        fax(m) = gca();
        xlabel(fax(m), '$\xi$');
    end

    xlabel(fax(1), '$x$');
    ylabel(fax(1), '$\rho$');
    xlim(fax(1), [-1.5, 11.5]);
    ylim(fax(1), [-3.5, 3.5]);
    axis(fax(1), 'equal');

    ylabel(fax(2), '$\gamma$');
    xlim(fax(2), [0, 0.5]);
    ylim(fax(2), [0, 2.0]);

    ylabel(fax(3), '$d$');
    % xlim(fax(3), [0, 0.5]);
    % ylim(fax(3), [0, 2.0]);

    h0 = problem.jump.user.h0;
    eps = problem.jump.user.eps;

    func = zeros(1, r.niters);
    grad = zeros(1, r.niters);
    ddir = zeros(1, r.niters);
    volume = zeros(1, r.niters);
    centroid = zeros(1, r.niters);

    for n = 1:r.niters
        filename = cache.filename(cache, sprintf('solution_%05d', n - 1));
        state = load(filename);

        x = state.state_f.x;
        y = state.state_f.y;
        xc = st_mesh_centroid(x, y);

        func(n) = state.state_f.cost;
        grad(n) = norm(state.state_a.grad, Inf);
        ddir(n) = norm(state.direction, Inf);
        volume(n) = st_mesh_volume(x, y);
        centroid(n) = xc;

        gamma = h0 + eps * state.gamma;

        xc = 0.0;
        zx = st_array_ax(x.x - xc, true);
        % zv = st_array_ax(x.vertices - xc, true);

        plot(fax(1), real(zx), imag(zx), '-');
        % plot(fax(1), real(zv), imag(zv), 'ko');
        title(fax(1), sprintf('n = %d', n - 1));

        plot(fax(2), x.xi, gamma, '-');
        title(fax(2), sprintf('n = %d', n - 1));
        plot(fax(3), x.xi, state.direction, '-');
        title(fax(3), sprintf('n = %d', n - 1));

        for m = 1:nvariables
            writeVideo(vid{m}, getframe(fig(m)));
        end
        % cla(fax(2));
    end

    for m = 1:nvariables
        title(fax(m), '');
        st_print(fig(m), cache.filename(cache, variables{m}, 'png'));

        close(vid{m});
        close(fig(m));
    end

    % {{{ plot convergence

    func

    cost_r = log10(func / abs(func(1)) + 1.0e-16);
    cost_a = log10(func + 1.0e-16);
    ticks = floor(min([cost_a, cost_r])):ceil(max([cost_a, cost_r]));

    fig = figure();
    plot(cost_r, 'k--');
    plot(cost_a, 'k-')
    yticks(ticks);
    ylabel('$\log_{10} \hat{\mathcal{J}}$');
    xlabel('$Iteration$');
    st_print(fig, cache.filename(cache, 'cost', 'png')); cla();

    grad_r = grad / grad(1);
    ddir_r = ddir / ddir(1);

    % plot(log10(grad + 1.0e-15), 'ko-');
    plot(ddir, 'ko--');
    % plot(grad_r, 'k--');
    % plot(ddir_r, 'k:');
    xlabel('$Iteration$');
    ylabel('$\|\nabla_\gamma \mathcal{J}\|_\infty$');
    st_print(fig, cache.filename(cache, 'gradient', 'png')); cla();

    volume = (volume - volume(1)) / volume(1);
    plot(volume, 'ko-');
    xlabel('$Iteration$');
    ylabel('$V$');
    st_print(fig, cache.filename(cache, 'volume', 'png')); cla();

    xd = 10.0;
    plot(centroid, 'ko-');
    yline(xd, 'k--');
    xlabel('$Iteration$');
    ylabel('$x_c$');
    ylim([0.0, 10.1]);
    st_print(fig, cache.filename(cache, 'centroid', 'png')); cla();

    close(fig);

    % }}}
end
