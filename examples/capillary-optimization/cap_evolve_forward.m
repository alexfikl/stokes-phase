% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [state] = cap_evolve_forward(varargin)
    % Evolve the forward system.
    %
    % This caches the results, so two runs with the same parameters will not
    % overwrite existing data unless specified.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       cap_evolve_forward(...
    %           'example_id', 'extensional', ...
    %           'enable_odeplot', true, ...
    %           'overwrite', false);
    %
    % where the parameters are defined in `data/example_extensional_data.m`. The
    % default settings can be seen by calling that function with no parameters.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'forward';
    options.example_id = st_struct_field(options, 'example_id', 'extensional');

    options.enable_odeplot = st_struct_field(options, 'enable_odeplot', false);
    options.overwrite = st_struct_field(options, 'overwrite', false);

    options = st_unittest_example_runner(...
        @run_evolve_forward, ...
        options);

    % }}}

    % {{{ reload state

    if nargout == 1
        state = options.user.state;
    end

    % }}}

    st_unittest_setup(orig_config, true);
end

% {{{ run_evolve_forward

function [options] = run_evolve_forward(problem, cache, options)
    % unroll
    x = problem.geometry.x;
    y = problem.geometry.y;
    bc = problem.bc;
    ode = problem.ode;
    forcing = problem.forcing;
    cost = st_struct_field(problem, 'cost', []);

    if ~isempty(cost)
        state_d = run_desired_geometry(options);

        if ~st_isempty(state_d)
            xd = st_interpolate(state_d.x, state_d.x.x, x.xi);
            [cost.x, cost.y] = st_mesh_geometry_update(x, y, xd);
        end
    end

    if options.enable_odeplot
        ode.odeplot = st_unittest_odeplot(x, 'forward', options);
    end

    % {{{ check if solution exists

    if options.overwrite
        recompute = true;
    else
        filename = cache.filename(cache, 'state');
        if exist(filename, 'file') == 2
            state = load(filename);
            recompute = ...
                abs(state.Ca - bc.param.Ca) > 1.0e-14 ...
                || abs(state.lambda - bc.param.lambda) > 1.0e-14 ...
                || abs(state.tmax - ode.tmax) > 1.0e-14 ...
                || length(state.x) ~= length(x.x);
        else
            recompute = true;
        end
    end

    if ~recompute
        fprintf('solution with desired parameters exists. skipping...\n');
        [state.x, state.y] = st_mesh_geometry_update(x, y, state.x);
        options.user.state = state;
        return;
    end

    % }}}

    if isfield(bc.param, 'gamma')
        gamma = norm(bc.param.gamma, Inf);
    else
        gamma = bc.param.Ca;
    end

    fprintf('computing forward solution for Ca = %.8e lambda = %.2f to tmax = %.5e\n', ...
        gamma, bc.param.lambda, ode.tmax);
    [x, y, state] = st_ode_unsteady(x, y, bc, forcing, cost, cache, ode);
    state.x = x;
    state.y = y;

    options.user.state = state;
end

% }}}

% {{{ ensure_desired_geometry

function [state] = run_desired_geometry(options)
    problem = options.problem_init(options);
    bc = problem.bc;

    state = struct();
    if isempty(bc.param.Ca_d)
        return;
    end

    datadir = st_struct_field(options, 'desireddir', []);
    options.cache_datadir = datadir;
    options.cache_prefix = sprintf('desired_%s', options.example_id);
    options.cache_id = options.cache_id;
    options.enable_odeplot = false;

    options.param.Ca = bc.param.Ca_d;
    options.param.Ca_d = [];
    options.cost_type = [];

    state = cap_evolve_forward(options);
end

% }}}
