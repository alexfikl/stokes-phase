% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function cap_gradient_panels(varargin)
    % Compute finite difference and adjoint gradients.
    % Convergence can be checked by providing multiple resolutions.
    %
    % This caches the results, so two runs with the same parameters will not
    % overwrite existing data unless specified.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       cap_gradient_panels(...
    %           'example_id', 'extensional', ...
    %           'enable_odeplot', true, ...
    %           'overwrite', true, ...
    %           'resolutions', [8, 16, 24, 32, 40]);
    %
    % where the parameters are defined in `data/test_extensional_data.m`. The
    % default settings can be seen by calling that function with no parameters.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'finite_panels';
    options.example_id = st_struct_field(options, 'example_id', 'extensional');
    options.resolutions = st_struct_field(options, 'resolutions', ...
        [8, 16, 24, 32, 40]);

    st_unittest_example_runner(@run_gradient_finite_panels, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

% {{{ run_gradient_finite_panels

function [options] = run_gradient_finite_panels(problem, cache, options)
    % disable so that `cap_evolve_forward` does not look for it
    options.param.Ca_d = [];
    param = problem.bc.param;

    % {{{ compute desired geometry

    if ~isempty(param.Ca_d)
        options.npanels = options.resolutions(end);
        state_d = run_forward(options, param.Ca_d, 'ca_desired', false);

        options.desired_curve = @(xi) ...
            st_interpolate(state_d.x, state_d.x.x, xi);
        options.desireddir = fullfile(cache.datadir, 'ca_desired');
    end

    % }}}

    % {{{ compute finite difference gradient for each `resolutions`

    nresolutions = length(options.resolutions);
    eps = problem.cost.eps;

    J = zeros(size(options.resolutions));
    hmax = zeros(size(options.resolutions));
    fd_grad = zeros(size(options.resolutions));
    ad_grad = zeros(size(options.resolutions));

    for n = 1:nresolutions
        options.npanels = options.resolutions(n);

        % {{{ finite difference gradient

        % current geometry
        state_c = run_forward(options, ...
            param.Ca, sprintf('ca_current_%04d', options.npanels), true);
        % perturbed geometry
        state_p = run_forward(options, ...
            param.Ca + eps, sprintf('ca_perturb_%04d', options.npanels), true);

        J(n) = state_c.cost;
        fd_grad(n) = (state_p.cost - state_c.cost) / eps;
        hmax(n) = st_mesh_h_max(state_c.x, state_c.y);

        % }}}

        % {{{ adjoint gradient

        options.fddir = fileparts(state_c.checkpoints{1});
        options.user.grad = fd_grad(n);

        state_a = run_adjoint(options, param.Ca, ...
            sprintf('ca_adjoint_%04d', options.npanels));

        ad_grad(n) = state_a.grad;

        % }}}
    end

    filename = cache.filename(cache, 'gradient');
    cost = J;
    save(filename, 'hmax', 'cost', 'fd_grad', 'ad_grad');

    % }}}
end

% }}}

function [state] = run_forward(options, Ca, cache_id, with_cost)
    options.verbose = false;
    options.param.Ca = Ca;
    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.cache_id = [];

    if with_cost
        options.prefix = sprintf('forward_%s', options.example_id);
    else
        options.cost_type = [];
        options.prefix = sprintf('desired_%s', options.example_id);
    end

    state = cap_evolve_forward(options);
end

function [state] = run_adjoint(options, Ca, cache_id)
    options.param.Ca = Ca;
    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.prefix = sprintf('adjoint_%s', options.example_id);

    state = cap_evolve_adjoint(options);
end

% vim:foldmethod=marker:
