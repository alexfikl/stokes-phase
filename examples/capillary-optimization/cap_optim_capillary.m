% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function cap_optim_capillary(varargin)
    % Optimize for the capillary number.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       cap_optim_capillary(...
    %           'example_id', 'extensional', ...
    %           'enable_odeplot', true, ...
    %           'overwrite', false);
    %
    % where the parameters are defined in `data/test_extensional_data.m`. The
    % default settings can be seen by calling that function with no parameters.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();

    % {{{ options

    d.example_prefix = 'optim_capillary';
    d.example_id = 'extensional';

    d.from_restart = false;
    d.restartfile = [];

    options = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    % }}}

    % {{{ run

    st_unittest_example_runner(@run_optim_unsteady, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_optim_capillary(problem, cache, options)
    if ~isfield(problem, 'cg')
        error('optimization not set up')
    end

    % {{{ restart

    [filename, restartfile] = cache.filenext(cache, 'iterations');
    fprintf('output: %s\n', filename);

    if ~isempty(options.restartfile)
        restartfile = options.restartfile;
    end

    if isempty(restartfile) && options.from_restart
        error('cannot restart if no restart file is available');
    end

    param = problem.bc.param;
    if options.from_restart
        r = load(restartfile);

        options.param.Ca = r.Ca;
        options.desired_curve = @(xi) r.xd;
        options.user.start_it = r.niters;
    else
        state_d = run_forward(options, ...
            param.Ca_d, 'desired', false);

        options.desired_curve = @(xi) st_interpolate(state_d.x, state_d.x.x, xi);
        options.desireddir = fullfile(cache.datadir, 'desired');

        options.user.start_it = 0;
    end

    % }}}

    % {{{ setup optimization

    cg = problem.cg;

    cg.x0 = param.Ca;
    cg.fn = @(Ca) fn_optim_gradient(Ca, cache, options);
    cg.output = @(info) fn_optim_output(info, cache, options);
    cg.project = @(x, d) x;
    cg.alpha_max = 200;
    cg.maxiter = 5;

    % }}}

    % {{{ optimize

    [Ca, stats] = st_optim_cg(cg);

    if options.from_restart
        stats.fn_call = stats.fn_call + r.stats.fn_call;
        stats.gn_call = stats.gn_call + r.stats.gn_call;

        stats.x = [r.x(1:end - 1), stats.x];
        stats.func = [r.stats.func(1:end - 1), stats.func];
        stats.grad = [r.stats.grad(1:end - 1), stats.grad];
        stats.ddir = [r.stats.ddir, stats.ddir];
        stats.alpha = [r.stats.alpha, stats.alpha];
    end

    xd = cost.x.x;
    niters = length(stats.func) - 1;
    save(filename, 'stats', 'Ca', 'xd', 'niters');

    % }}}
end

% {{{ optim functions

function [fn, grad] = fn_optim_gradient(Ca, cache, options)
    options.param.Ca = Ca;

    % {{{ compute cost functional + forward solution

    tic();
    state_f = run_forward(options, Ca, 'last_call_forward', true);
    t_end = toc();
    fprintf('Forward solve time %.2fs\n', t_end);

    fn = state_f.cost;

    % }}}

    % {{{ compute gradient + adjoint solution

    if nargout == 2
        % NOTE: this sort of assumes this just gets called once per iteration

        tic();
        state_a = run_adjoint(options, Ca, 'last_call_adjoint', 'last_call_forward');
        t_end = toc();
        fprintf('Adjoint solve %.2fs\n', t_end);

        grad = state_a.grad;
    end

    % }}}
end

function [stop] = fn_optim_output(values, cache, options)
    stop = false;
    it = options.user.start_it + values.iteration;

    % {{{ parameters

    options.param.Ca = values.x;
    problem = options.problem_init(options);

    x = problem.geometry.x;
    y = problem.geometry.y;
    cost = problem.cost;
    ode = problem.ode;

    % }}}

    % {{{ load solutions

    % load forward solution
    state_f = run_forward(options, values.x, 'last_call_forward', true);
    % load adjoint solution
    state_a = run_adjoint(options, values.x, 'last_call_adjoint', 'last_call_forward');

    % }}}

    % {{{ save solutions

    filename = cache.filename(cache, sprintf('solution_%05d', it));

    Ca = options.param.Ca;
    direction = values.d;
    save(filename, 'Ca', 'direction', 'state_f', 'state_a');

    % }}}
end

% }}}

% {{{ helpers

function [state] = run_forward(options, Ca, cache_id, with_cost)
    options.verbose = false;

    options.param.Ca = Ca;
    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.cache_id = [];

    if with_cost
        options.prefix = sprintf('forward_%s', options.example_id);
    else
        options.cost_type = [];
        options.prefix = sprintf('desired_%s', options.example_id);
    end

    state = cap_evolve_forward(options);
end

function [state] = run_adjoint(options, Ca, cache_id, fddir)
    options.verbose = false;

    options.param.Ca = Ca;
    options.fddir = fullfile(options.cache_datadir, fddir);
    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.prefix = sprintf('adjoint_%s', options.example_id);

    state = cap_evolve_adjoint(options);
end

% }}}
