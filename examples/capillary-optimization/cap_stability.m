% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function cap_stability(varargin)
    % Check stability of forward system at initial time vs Ca.
    %
    % This caches the results, so two runs with the same parameters will not
    % overwrite existing data unless specified.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       cap_stability(...
    %           'example_id', 'extensional', ...
    %           'overwrite', false);
    %
    % where the parameters are defined in `data/test_extensional_data.m`. The
    % default settings can be seen by calling that function with no parameters.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'stability';

    options.example_id = st_struct_field(options, 'example_id', 'extensional');
    options.overwrite = st_struct_field(options, 'overwrite', false);

    options.mode = st_struct_field(options, 'mode', 'ca');
    options.resolutions = st_struct_field(options, 'resolutions', ...
        linspace(1.0e-3, 1.0, 32));

    options.npanels = st_struct_field(options, 'npanels', 64);
    st_unittest_example_runner(@run_stability, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_stability(problem, cache, options)
    nvariables = 3;
    fig = gobjects(1, nvariables);
    fax = gobjects(1, nvariables);
    for m = 1:nvariables
        fig(m) = figure();
        fax(m) = gca();
        xlabel(fax(m), '$\Re\{\lambda\}$');
        ylabel(fax(m), '$\Im\{\lambda\}$');
    end
    xlabel(fax(3), '$\xi$');
    ylabel(fax(3), '$\mathbf{v}$');

    if contains(options.mode, {'h0', 'eps'})
        if ~isfield(problem.bc.param, 'gamma')
            error('need variable surface tension for `%s` mode', options.mode);
        else
            % NOTE: setting this to empty so it gets recomputed
            options.gamma = [];
        end
    end

    % inverse mass operators
    x = problem.geometry.x; y = problem.geometry.y;
    M = st_ops_mass(x, y);
    [Mx, ~] = st_ops_boundary(x, M, x.xi, 'neumann');
    [My, ~] = st_ops_boundary(x, M, x.xi, 'dirichlet');
    M = [inv(Mx), zeros(size(M)); zeros(size(M)), inv(My)];

    nresolutions = length(options.resolutions);
    max_lambda_re = zeros(1, nresolutions);
    for n = 1:nresolutions
        switch options.mode
        case 'ca'
            label = '\mathrm{Ca}';
            options.param.Ca = options.resolutions(n);
            options.user.h0 = 0.75;
            options.user.eps = 0.1;
        case 'h0'
            label = '\gamma_0';
            options.user.h0 = options.resolutions(n);
            options.user.eps = 0.1;
        case 'eps'
            label = '\epsilon';
            options.user.h0 = 0.5;
            options.user.eps = options.resolutions(n);
        case 'lambda'
            label = '\lambda';
            options.param.lambda = options.resolutions(n);
            options.user.h0 = 0.75;
            options.user.eps = 0.1;
        otherwise
            error('unknown mode: %s', options.mode);
        end

        fprintf('[%04d/%04d] %s = %.8e\n', n, nresolutions, ...
            options.mode, options.resolutions(n));
        problem = options.problem_init(options);

        % unroll
        x = problem.geometry.x;
        y = problem.geometry.y;
        bc = problem.bc;
        cost = problem.cost;
        jump = problem.jump;
        forcing = problem.forcing;

        variables = [cost.variables, cost.forward_variables, forcing.variables];

        % create adjoint forcing
        grads = st_gradient_combine(...
            'cost', cost, ...
            'jump', jump, ...
            'forcing', forcing);
        forcingstar = st_forcing_adjoint(...
            'param', bc.param, ...
            'user', struct('grads', grads));

        % solve forward problem
        st.forward = st_repr_density_solution(x, y, bc, variables);
        st.forward.forcing = forcing.fn.evaluate(forcing, 0.0, x, y, st);

        % compute operator
        H = forcingstar.fn.operator_x(forcingstar, 0.0, x, y, st);
        H = M * H;
        lambda = eigs(H, size(H, 1));
        [V, max_lambda_re(n)] = eigs(H, 1, 'largestreal', ...
            'Tolerance', 1.0e-8, 'MaxIterations', 2 * size(H, 1));
        V = st_array_unstack(V);

        if isfield(bc.param, 'gamma')
            fprintf('            gamma = %.5e lambda = %.5e\n', ...
                norm(bc.param.gamma), max_lambda_re(n));
        else
            fprintf('            lambda = %.5e\n', max_lambda_re(n));
        end

        % eigenvalues
        plot(fax(1), real(lambda), imag(lambda), 'o');
        ylim(fax(1), [min(imag(lambda)) - 0.1, max(imag(lambda)) + 0.1]);
        title(fax(1), sprintf('$%s = %.8e$', label, options.resolutions(n)));

        st_print(fig(1), cache.filename(cache, sprintf('eigs_%04d', n - 1), 'png'));
        cla(fax(1));

        plot(fax(2), real(lambda), imag(lambda), 'o');

        % most unstable eigenmode
        plot(fax(3), x.xi, real(V), '-');
        plot(fax(3), x.xi, imag(V), '--');
        title(fax(3), sprintf('$%s = %.8e$', label, options.resolutions(n)));

        st_print(fig(3), cache.filename(cache, sprintf('eigv_%04d', n - 1), 'png'));
        cla(fax(3));
    end
    st_print(fig(2), cache.filename(cache, 'eigs', 'png'));

    fig = figure();
    plot(options.resolutions, real(max_lambda_re), 'o--');
    xlabel(sprintf('$%s$', label));
    ylabel('$\mathrm{max} \Re\{\lambda\}$');
    st_print(fig, cache.filename(cache, 'max_eigs', 'png'));
end
