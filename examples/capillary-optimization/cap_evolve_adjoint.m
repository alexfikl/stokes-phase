% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [state] = cap_evolve_adjoint(varargin)
    % Evolve the forward system.
    %
    % This caches the results, so two runs with the same parameters will not
    % overwrite existing data unless specified.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       cap_evolve_adjoint(...
    %           'example_id', 'extensional', ...
    %           'enable_odeplot', true, ...
    %           'overwrite', false);
    %
    % where the parameters are defined in `data/test_extensional_data.m`. The
    % default settings can be seen by calling that function with no parameters.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();

    % {{{ options

    d.example_prefix = 'adjoint';
    d.example_id = 'extensional';

    d.enable_odeplot = false;
    d.overwrite = false;

    options = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    % }}}

    % {{{ run

    options = st_unittest_example_runner(@run_evolve_adjoint, options);

    if nargout == 1
        state = options.user.state;
    end

    % }}}

    st_unittest_setup(orig_config, true);
end

% {{{ run_evolve_adjoint

function [options] = run_evolve_adjoint(problem, cache, options)
    % unroll
    x0 = problem.geometry.x;
    y0 = problem.geometry.y;
    bc = problem.bc;
    ode = problem.ode;
    forcing = problem.forcing;
    cost = problem.cost;

    if options.enable_odeplot
        ode.odeplot = create_odeplot(x0);
    end

    % {{{ check if solution exists

    if options.overwrite
        recompute = true;
    else
        filename = cache.filename(cache, 'state');
        if exist(filename, 'file') == 2
            state = load(filename);
            recompute = ...
                abs(state.Ca - bc.param.Ca) > 1.0e-14 ...
                || abs(state.lambda - bc.param.lambda) > 1.0e-14 ...
                || abs(state.tmax - ode.tmax) > 1.0e-14 ...
                || length(state.xstar) ~= length(x0.x);
        else
            recompute = true;
        end
    end

    if ~recompute
        options.user.state = state;
        fprintf('solution with desired parameters exists. skipping...\n');
        return;
    end

    % }}}

    % {{{ reload forward solution

    % load
    state_f = load_forward(options, cache, 'fddir', 'forward');

    % update ODE options
    ode.tmax = state_f.tmax;
    ode.maxit = state_f.maxit;
    ode.checkpoints = state_f.checkpoints;
    ode.user.forward = state_f;

    % load desired geometry
    if ~isempty(bc.param.Ca_d)
        state_d = load_forward(options, cache, 'desireddir', 'desired');

        xd = st_interpolate(state_d.x, state_d.x.x, x0.xi);
        [cost.x, cost.y] = st_mesh_geometry_update(x0, y0, xd);
    end

    % }}}

    if isfield(bc.param, 'gamma')
        gamma = norm(bc.param.gamma, Inf);
    else
        gamma = bc.param.Ca;
    end

    fprintf('computing adjoint solution for Ca = %.8e lambda = %.2f to tmax = %.5e\n', ...
        gamma, bc.param.lambda, ode.tmax);

    [~, state] = st_ode_unsteady_adjoint(state_f.x, state_f.y, ...
        bc, forcing, cost, cache, ode);

    options.user.state = state;
end

function [state] = load_forward(options, cache, datadir, name)
    datadir = st_struct_field(options, datadir, []);
    if isempty(datadir)
        datadir = fullfile(fileparts(cache.datadir), ...
            sprintf('%s_%s_%s', cache.cache_id, name, options.example_id));
    end
    prefix = sprintf('%s_%s', name, options.example_id);

    state = st_ode_unsteady_load(datadir, [], [], [], prefix);
end

% }}}

% {{{ plotting

function [fn] = create_odeplot(x)
    figure('visible', 'on');
    subplot(1, 2, 1);
    h1 = plot(x.xi, 0.0 * x.xi);
    h2 = plot(x.xi, 0.0 * x.xi, '--');
    xlabel('$x$');
    ylabel('$\mathbf{X}^*$');
    subplot(1, 2, 2);
    h3 = plot(x.xi, 0.0 * x.xi);
    h4 = plot(x.xi, 0.0 * x.xi, '--');
    xlabel('$\xi$');
    ylabel('$\mathbf{u}^*$');
    drawnow;

    fn = @(t, x, y, st) fn_odeplot(t, x, y, st, h1, h2, h3, h4);

    function fn_odeplot(~, x, ~, st, h1, h2, h3, h4)
        x_dot_n = st_dot(st.adjoint.x, x.n);
        x_dot_t = st_dot(st.adjoint.x, x.t);
        set(h1, 'YData', x_dot_n);
        set(h2, 'YData', x_dot_t);

        u_dot_n = st_dot(st.adjoint.u, x.n);
        u_dot_t = st_dot(st.adjoint.u, x.t);
        set(h3, 'YData', u_dot_n);
        set(h4, 'YData', u_dot_t);

        drawnow;
        pause(0.01);
    end
end

% }}}
