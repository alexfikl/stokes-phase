% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_evolve_adjoint(varargin)
    % Plots data from `cap_evolve_adjoint`.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       plot_evolve_forward('example_id', 'extensional', 'cache_id', 'same');
    %
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.reload_options = true;

    options.example_prefix = 'adjoint';
    options.example_id = st_struct_field(options, 'example_id', 'extensional');

    st_unittest_example_runner(@run_plot_evolve_forward, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_evolve_forward(problem, cache, options)
    % {{{ progress

    state = load(cache.filename(cache, 'state'));

    fig = figure();
    plot(problem.geometry.x.xi, state.grad);
    xlabel('$\xi$');
    ylabel('$\nabla_\gamma \mathcal{J}$');
    st_print(fig, cache.filename(cache, 'gradient', 'png')); cla();

    timing = rmfield(state.timing, {'update', 'total'});
    fields = cellfun(@(z) [upper(z(1)), z(2:end)], ...
        fieldnames(timing), 'UniformOutput', false);
    timing = reshape(struct2array(timing), length(timing.geometry), []);

    h1 = area(timing); clear timing;
    xlabel('$n$');
    ylabel('$t$');
    legend(h1, fields);
    st_print(fig, cache.filename(cache, 'timing', 'png')); cla();

    plot(state.xstar_norm, '--');
    plot(state.fstar_norm);
    st_print(fig, cache.filename(cache, 'norms', 'png'));
    close(fig);

    % }}}

    % {{{ evolution

    plotter.variables = {'xstar', 'ustar'};
    plotter.fig_init = @fig_init;
    plotter.fig_plot = @fig_plot;
    plotter.freq = 16;

    plot_evolution(plotter, problem, cache, options);

    % }}}
end

function [fig, fax] = fig_init(fig, fax)
    % nothing to do
end

function [skip] = fig_plot(x0, ~, chk, fax)
    skip = ~isfield(chk.st.adjoint, 'u');
    if skip
        return;
    end

    plot(fax(1), x0.xi, real(chk.st.adjoint.x), '--');
    plot(fax(1), x0.xi, imag(chk.st.adjoint.x), ':');

    plot(fax(2), x0.xi, real(chk.st.adjoint.u), '--');
    plot(fax(2), x0.xi, imag(chk.st.adjoint.u), ':');
end
