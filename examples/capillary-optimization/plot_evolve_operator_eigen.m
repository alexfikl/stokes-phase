% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_evolve_operator_eigen(varargin)
    % Plots data from `cap_evolve_adjoint`. In particular, the eigenvalues of
    % the linear operator acting on :math:`\mathbf{X}^*`.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       plot_evolve_operator_eigen('example_id', 'extensional', 'cache_id', 'same');
    %
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.reload_options = true;

    options.example_prefix = 'adjoint';
    options.example_id = st_struct_field(options, 'example_id', 'extensional');

    st_unittest_example_runner(@run_plot_evolve_forward, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_evolve_forward(problem, cache, options)
    if ~isfield(problem.jump.fn, 'operator_x')
        problem.jump = st_jump_capillary(problem.jump);
    end

    grads = st_gradient_combine(...
        'cost', problem.cost, ...
        'jump', problem.jump, ...
        'forcing', problem.forcing);
    forcingstar = st_forcing_adjoint(...
        'param', problem.bc.param, ...
        'user', struct('grads', grads));

    plotter.variables = {'weak', 'strong'};
    plotter.fig_init = @fig_init;
    plotter.fig_plot = @(x0, y0, chk, fax) fig_plot(forcingstar, x0, y0, chk, fax);
    plotter.freq = 16;

    plot_evolution(plotter, problem, cache, options);
end

function [fig, fax] = fig_init(fig, fax)
    for m = 1:length(fig)
        xlabel(fax(m), '$\Re\{\lambda\}$');
        ylabel(fax(m), '$\Im\{\lambda\}$');
    end
end

function fig_plot(forcingstar, x0, y0, chk, fax)
    if ~isfield(chk.st.adjoint, 'u')
        return;
    end

    % reload geometry
    [x, y] = st_mesh_geometry_update(x0, y0, chk.st.x);

    % reconstruct right-hand side forcing for adjoint equation
    fstar = forcingstar.fn.evaluate(forcingstar, chk.t, x, y, chk.st, false);
    fstar = st_dot(fstar, x.n) .* x.n;
    fprintf('fstar: %.6e\n', ...
        norm(fstar - chk.st.adjoint.forcing) / norm(chk.st.adjoint.forcing));

    % NOTE: use a random xstar to test the operators
    xstar = chk.st.adjoint.x;
    % xstar = rand(size(x.x));
    % chk.st.adjoint.x = xstar;

    % inverse mass
    M = st_ops_mass(x, y);
    [Mx, ~] = st_ops_boundary(x, M, x.xi, 'neumann');
    [My, ~] = st_ops_boundary(x, M, x.xi, 'dirichlet');
    M = [inv(Mx), zeros(size(M)); zeros(size(M)), inv(My)];
    % construct operator
    % fn = forcingstar.user.grads.user.jump;
    % H = fn.fn.operator_x(fn, chk.t, x, y, chk.st);
    H = forcingstar.fn.operator_x(forcingstar, chk.t, x, y, chk.st);

    % check we can match the forcing
    fstar = forcingstar.fn.evaluate(forcingstar, chk.t, x, y, chk.st, true);
    % fstar = fn.fn.gradient_x(fn, chk.t, x, y, chk.st);
    hstar = st_array_unstack(H * st_array_stack(xstar));
    fprintf('fstar: %.6e\n', norm(fstar - hstar) / norm(fstar));

    % plot eigenvalues
    lambda_weak = eigs(H, size(H, 1));
    lambda_strong = eigs(M * H, size(H, 1));

    plot(fax(1), real(lambda_weak), imag(lambda_weak), 'o');
    plot(fax(2), real(lambda_strong), imag(lambda_strong), 'o');
end
