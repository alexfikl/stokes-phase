% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_paper1_data(varargin)
    % This setup is meant to generate data for the `adjoint vs finite difference
    % gradient error` vs the `finite difference step size` largely based on
    % the setup described in :func:`example_extesional_data`.

    % {{{ default options

    % convergence study
    extra_options.resolutions = 10.^-(1:7);

    % flow parameters
    d.lambda = 1.0;
    d.Ca = 0.05;
    d.Ca_d = 0.1;

    % geometry
    d.npanels = 32;
    d.nnodes = 4;
    d.nbasis = 2;

    % ode
    d.display = 1;
    d.tmax = 0.1;
    d.dt = 1.0e-4;
    d.stepper = 'euler';
    d.filter_strength = -1.0;

    % }}}

    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    r = example_extensional_data(d);
    r.extra_options = st_struct_merge(r.extra_options, extra_options, true);
end
