% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_paper4_data(varargin)
    % This setup is meant to be used for optimization. It is mainly the same
    % as :func:`test_extensional_data`, with added optimization options.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    % NOTE: need to keep Ca low enough for stable steady state solution
    param.Ca = 0.1;
    param.Ca_d = 0.05;
    param.lambda = 48.01;
    param.R = 1.0;

    d.fn = 'FourRoller';
    d.param = param;

    % geometry
    d.npanels = 16;

    % forcing
    d.tangential_forcing = true;

    % ode options
    d.tmax = 0.5;
    d.dt = 1.0e-3;
    d.filter_strength = 1.0e-4;
    d.display = false;

    % optimization
    o_options.beta_update = 'steepest';
    o_options.maxiter = 64;
    o_options.outputfreq = 1;
    o_options.gtol = 1.0e-6;
    o_options.alpha_min = 1.0e-8;
    o_options.alpha_max = [];

    o_options = st_struct_update(o_options, options);
    o_options.display = true;

    % }}}

    % {{{

    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);
    r = example_extensional_data(d);

    r.cg = st_optim_options(o_options);

    % }}}
end
