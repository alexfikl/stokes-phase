% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_paper2_data(varargin)
    % This setup is meant to generate data for the `adjoint vs finite difference
    % gradient error` vs the `mesh size` largely based on
    % the setup described in `example_extesional_data`.
    %
    % See also `example_paper3_data`, where we check the gradient from the other
    % direction as well, i.e. for :math:`Ca > Ca_d`.

    % {{{ default options

    % convergence study
    extra_options.resolutions = [8, 12, 16, 24, 32];

    % flow setup
    d.lambda = 1.0;
    d.Ca = 0.05;
    d.Ca_d = 0.1;

    % geometry
    % NOTE: not actually used; overwritten by resolutions
    d.npanels = 32;
    d.nnodes = 4;
    d.nbasis = 2;

    % ode
    d.display = 1;
    d.tmax = 0.1;
    d.dt = 1.0e-4;
    d.stepper = 'euler';
    d.filter_strength = -1.0;

    % }}}

    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    r = example_paper1_data(d);
    r.extra_options = st_struct_merge(r.extra_options, extra_options, true);
end
