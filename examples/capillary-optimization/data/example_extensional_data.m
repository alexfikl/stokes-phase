% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_extensional_data(varargin)
    % Description: This test uses an extensional flow with constant surface
    % tension. It sets up the time stepping, but not the optimization.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    % NOTE: need to keep Ca low enough for stable steady state solution
    param.Ca = 0.1;
    param.Ca_d = 0.05;
    param.lambda = 1.0;
    param.R = 1.0;
    param = st_struct_update(param, options);

    b_options.fn = 'ExtensionalFlow';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump
    j_options.jump_type = 'capillary';
    j_options.optype = 'vector';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'circle', 'R', b_options.param.R);

    g_options.x = [];
    g_options.y = [];
    g_options = st_struct_update(g_options, options);

    % cost
    % NOTE: desired geometry needs to be computed by advancing with Ca_d.
    % This needs to be done and cached nicely by the calling scripts.
    c_options.cost_type = 'geometry';
    c_options.optype = 'vector';
    c_options.desired_curve = [];
    c_options.eps = 1.0e-5;

    c_options.variables = {'two_phase'};
    c_options.forward_variables = {'two_phase'};
    c_options.adjoint_variables = {'two_phase'};
    c_options = st_struct_update(c_options, options);

    % forcing
    f_options.forcing_type = 'normal_velocity';
    f_options.user.tangential_forcing = false;
    f_options.user.curvature_forcing = false;
    f_options = st_struct_update(f_options, options);

    % ode
    o_options.display = true;
    o_options.tmax = 1.0;
    o_options.maxit = Inf;
    o_options.dt = 1.0e-3;
    o_options.filter_strength = -1.0;
    o_options.stepper = 'euler';

    o_options.checkpoints = {};
    o_options.user = struct();
    o_options = st_struct_update(o_options, options);

    % }}}

    % {{{

    b_options.param.tmax = o_options.tmax;

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'cost', c_options, ...
        'forcing', f_options, ...
        'ode', o_options);

    r.extra_options = struct();

    % }}}
end
