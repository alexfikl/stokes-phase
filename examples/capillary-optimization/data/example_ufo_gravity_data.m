% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = test_paper5_data(varargin)
    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % boundary conditions
    % param.Ca = 1.0 / 0.05;      % NOTE: this is actually the Bond number
    param.Ca = 0.05;
    param.Ca_d = [];
    param.lambda = 1.0;
    param.R = 1.0;
    % param.uinf = -2.0/3.0 * (1.0 + param.lambda)/(2.0 + 3.0 * param.lambda);
    param.uinf = 0.0;
    param.gamma = [];
    param = st_struct_update(param, options);

    b_options.fn = 'UniformFlow';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump
    j_options.jump_type = 'gravity_capillary_variable';
    j_options.optype = 'vector';

    j_options.user.h0 = 1.0;
    j_options.user.eps = 0.15;
    j_options.user.k = 3;
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.npanels = 32;
    g_options.nbasis = 4;
    g_options.nnodes = 4;
    g_options.curve = @(xi) st_mesh_curve(xi, 'spheroid', 'R', b_options.param.R);
    g_options = st_struct_update(g_options, options);

    % cost
    c_options.cost_type = 'surface_area';
    c_options.optype = 'vector';
    c_options.eps = 1.0e-5;

    c_options.variables = {};
    c_options.forward_variables = {'two_phase', 'u', 'dudn', 'eps', 'fn'};
    c_options.adjoint_variables = {'two_phase', 'u', 'dudn', 'eps', 'fn'};

    c_options.user.x0 = 0.0;
    c_options.user.alpha = -1.0;
    c_options = st_struct_update(c_options, options);

    % forcing
    f_options.forcing_type = 'normal_velocity';
    f_options.user.tangential_forcing = false;
    f_options.user.curvature_forcing = false;
    f_options = st_struct_update(f_options, options);

    % ode
    e_options.display = true;
    e_options.tmax = 1.0;
    e_options.maxit = Inf;
    e_options.dt = -1.0e-3;
    e_options.theta = 0.5;

    e_options.odefreq = 1;
    e_options.odeplot = [];
    e_options.stepper = 'ssprk3a';
    e_options.checkpoints = {};

    e_options.filter_strength = -1.0e-4;
    e_options.enable_equidistance = true;
    e_options.equidistance.freq = 64;
    e_options.equidistance.pfilter = 1.0e-4;

    e_options.user = struct();
    e_options = st_struct_update(e_options, options);

    % optimization
    o_options.beta_update = 'steepest';
    o_options.maxiter = 10;
    o_options.outputfreq = 1;
    o_options.gtol = 1.0e-6;
    o_options.alpha_min = 1.0e-8;
    o_options.alpha_max = [];
    o_options.alpha_maxit = 2;

    o_options = st_struct_update(o_options, options);
    o_options.display = true;

    % }}}

    % {{{ compute surface tension

    if isempty(b_options.param.gamma)
        npoints = g_options.npanels * (g_options.nbasis - 1) + 1;
        xi = linspace(0.0, 0.5, npoints);

        b_options.param.gamma = st_jump_surface_tension('constant', xi, j_options.user);
    end

    % }}}

    % {{{

    b_options.param.tmax = e_options.tmax;

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'cost', c_options, ...
        'forcing', f_options, ...
        'ode', e_options, ...
        'optim', o_options);
    r.extra_options = struct();

    assert(isempty(r.bc.param.Ca_d));
    assert(~isempty(r.bc.param.gamma));

    % }}}
end
