% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_paper3_data(varargin)
    % This setup is meant to generate data for the `adjoint vs finite difference
    % gradient error` vs the `mesh size` largely based on
    % the setup described in `example_extesional_data`.
    %
    % See also `example_paper2_data`, where we check the gradient from the other
    % direction as well, i.e. for :math:`Ca < Ca_d`.

    % {{{ options

    % convergence study
    extra_options.resolutions = [8, 12, 16, 24, 32];

    % flow setup
    d.lambda = 1.0;
    d.Ca = 0.1;
    d.Ca_d = 0.05;

    % geometry
    d.npanels = 32;

    % ode
    d.tmax = 0.1;
    d.dt = 1.0e-4;

    % }}}

    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    r = example_paper1_data(d);
    r.extra_options = st_struct_merge(r.extra_options, extra_options, true);
end
