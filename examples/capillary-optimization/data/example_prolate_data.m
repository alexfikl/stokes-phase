% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = example_prolate_data(varargin)
    % Setup matching [pozrikidis-1992]_ Figure 6.

    options = st_struct_varargin(varargin{:});

    % {{{ default options

    % flow setup
    param.lambda = 1.0;
    param.Ca = 1.0 / 0.05;
    param.Ca_d = [];
    param.uinf = 1.0;
    param = st_struct_update(param, options);

    b_options.fn = 'UniformFlow';
    b_options.param = param;
    b_options = st_struct_update(b_options, options);

    % jump
    j_options.jump_type = 'gravity_capillary';
    j_options = st_struct_update(j_options, options);

    % geometry
    g_options.panels = 32;
    g_options.curve = @(xi) st_mesh_curve(xi, 'spheroid', 'epsilon', 0.2);
    g_options = st_struct_update(g_options, options);

    % forcing
    f_options.forcing_type = 'normal_velocity';
    f_options.user.tangential_forcing = false;
    f_options.user.curvature_forcing = false;
    f_options = st_struct_update(f_options, options);

    % ode
    e_options.display = true;
    e_options.tmax = 10.0;
    e_options.dt = 1.0e-3;
    e_options.stepper = 'euler';
    e_options.filter_strength = 1.0e-4;

    e_options.odefreq = 1;
    e_options.odeplot = [];
    e_options = st_struct_update(e_options, options);

    % }}}

    b_options.param.tmax = e_options.tmax;

    r = st_unittest_problem(...
        'geometry', g_options, ...
        'boundary_condition', b_options, ...
        'jump', j_options, ...
        'forcing', f_options, ...
        'ode', e_options);
    r.extra_options = struct();
end
