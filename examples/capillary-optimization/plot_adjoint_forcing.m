% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_adjoint_forcing(varargin)
    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    % NOTE: the refinement can be set afterwards, so we do the cache prefix lazily
    options.prefix = @(o) sprintf('%s_%s_%s', o.test_type, o.refinement, o.test_id);

    options.test_type = 'adjoint';
    options.test_id = st_struct_field(options, 'test_id', 'extensional');
    options.refinement = st_struct_field(options, 'refinement', 'eps');

    options = st_unittest_example_runner(...
        @run_plot_adjoint_forcing, ...
        options);

    % }}}
    st_unittest_setup(orig_config, true);
end

function run_plot_adjoint_forcing(options)
    acache = options.cache;

    datadir = st_struct_field(options, 'fddir', []);
    fcache = st_cache_create(datadir, options.cache_id, ...
        sprintf('%s_%s_%s', 'finite', options.refinement, options.test_id));

    acheckpoint = st_cache_create(...
        fullfile(acache.datadir, 'timesteps'), ...
        [], 'evolution');
    fcheckpoint = st_cache_create(...
        fullfile(fcache.datadir, sprintf('timesteps_ca_%04d', 0)), ...
        [], 'evolution');

    % get initial conditions
    problem = load(acache.filename(acache, 'setup'));
    x = problem.geometry.x;
    y = problem.geometry.y;

    grads = st_gradient_combine(...
        'cost', problem.cost, ...
        'jump', problem.jump, ...
        'forcing', problem.forcing);

    forcingstar = st_forcing_adjoint(...
        'param', problem.bc.param, ...
        'user', struct('grads', grads));

    % {{{ plot forcing at a given timestep n

    astate = load(acheckpoint.filename(acheckpoint, 'state'));
    fstate = load(fcheckpoint.filename(fcheckpoint, 'state'));

    n = length(fstate.checkpoints) - 1;
    r0 = compute_forcing(n);
    r1 = compute_forcing(n - 1);

    dt = r0.t - r1.t;
    fstar = (r1.xstar - r0.xstar) / dt;

    fig = figure();
    h1 = plot(x.xi, real(r0.fstar), '--');
    plot(x.xi, imag(r0.fstar), ':', 'Color', get(h1, 'Color'));
    h1 = plot(x.xi, real(fstar), '--');
    plot(x.xi, imag(fstar), ':', 'Color', get(h1, 'Color'));
    xlabel('$\xi$');
    ylabel('$\mathbf{f}^*$');
    st_print(fig, acache.filename(acache, sprintf('fstar_%05d', n), 'png'));
    clf();

    h1 = plot(x.xi, real(r0.xstar), '--');
    plot(x.xi, imag(r0.xstar), ':', 'Color', get(h1, 'Color'));
    h1 = plot(x.xi, real(r1.xstar), '--');
    plot(x.xi, imag(r1.xstar), ':', 'Color', get(h1, 'Color'));
    xlabel('$\xi$');
    ylabel('$\mathbf{X}^*$');
    st_print(fig, acache.filename(acache, sprintf('xstar_%05d', n), 'png'));
    clf();

    function [r] = compute_forcing(n)
        achk = load(astate.checkpoints{end - n + 1});
        fchk = load(fstate.checkpoints{n});
        st = achk.st;

        [xt, yt] = st_mesh_geometry_update(x, y, fchk.x);
        fstar = forcingstar.fn.evaluate(forcingstar, achk.t, xt, yt, st);

        r.fstar = fstar;
        r.xstar = st.adjoint.x;
        r.t = achk.t;
    end
end

