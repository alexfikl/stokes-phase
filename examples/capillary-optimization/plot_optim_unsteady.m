% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_optim_unsteady(varargin)
    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:}, 'test_type', 'optim');

    % {{{ cache setup

    % test_id: see data directory for files named `test_{test_id}_data.m`
    options.test_id = st_struct_field(options, 'test_id', 'extensional');

    cache_id = st_struct_field(options, 'cache_id', []);
    datadir = st_struct_field(options, 'datadir', []);

    cache = st_cache_create(datadir, cache_id, ...
        sprintf('%s_%s', options.test_type, options.test_id));

    % save cache in case test case wants to save something
    options.cache = cache;

    % }}}

    % {{{ load problem

    filename = cache.filename(cache, 'setup');
    load(filename, 'problem', 'options');

    % }}}

    % {{{ plot cost and gradient

    Ca = stats.x;
    Ca_d = problem.param.Ca_d;

    f = log10(abs(Ca - Ca_d) / Ca_d);
    ticks = floor(min(f)):ceil(max(f));

    fig = figure();
    plot(f, '-');
    set(gca, 'YTick', ticks);
    xlabel('$Iteration$');
    ylabel('$\log_{10} \Delta \mathrm{Ca}$');
    st_print(fig, cache.filename(cache, 'capillary', 'png'));
    clf();

    f = log10(stats.func);
    relf = log10(abs(stats.func) / abs(stats.func(1)));
    ticks = floor(min(f)):ceil(max(f));

    plot(f, 'k-');
    plot(relf, 'k--');
    set(gca, 'YTick', ticks);
    xlabel('$Iteration$');
    ylabel('$\log_{10} \hat{\mathcal{J}}$');
    st_print(fig, cache.filename(cache, 'cost', 'png'));
    clf();

    g = log10(stats.grad);
    relg = log10(stats.grad / stats.grad(1));
    reld = log10(stats.ddir / stats.ddir(1));

    plot(g, 'k-');
    plot(relg, 'k--');
    plot(reld, 'k:');
    xlabel('$Iteration$');
    ylabel('$\log_{10} \|\hat{\mathbf{g}}\|_\infty$')
    st_print(fig, cache.filename(cache, 'gradient', 'png'));
    clf();

    plot(stats.alpha, 'k');
    xlabel('$Iteration$');
    ylabel('$\alpha$');
    st_print(fig, cache.filename(cache, 'alpha', 'png'));
    clf();

    % }}}

    % {{{ plot evolution

    z0 = st_array_ax(problem.x.x); z0 = [z0, z0(1)];
    zd = st_array_ax(cost.x.x); zd = [zd, zd(1)];

    for n = 1:stats.niters
        state_c = ode_load_state(cache, problem, ...
            sprintf('', n), true);

        zc = st_array_ax(state_c.x.x); zc = [zc, zc(1)];

        fig = figure();
        plot(real(zd), imag(zd), 'k--');
        plot(real(z0), imag(z0), ':');
        plot(real(zc), imag(zc), 'o-');
        axis('equal');
        xlabel('$x$');
        ylabel('$\rho$');
        st_print(fig, cache.filename(cache, sprintf('geometry_%04d', n), 'png'));
        close(fig);
    end

    % }}}
end
