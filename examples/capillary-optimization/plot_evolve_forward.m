% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_evolve_forward(varargin)
    % Plots data from `cap_evolve_forward`.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       plot_evolve_forward('example_id', 'extensional', 'cache_id', 'same');
    %
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.reload_options = true;

    options.example_prefix = 'forward';
    options.example_id = st_struct_field(options, 'example_id', 'extensional');

    st_unittest_example_runner(@run_plot_evolve_forward, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_evolve_forward(problem, cache, options)
    state = load(cache.filename(cache, 'state'));
    t = state.times / state.times(end);

    % {{{ progress

    fig = figure();

    plot(t, state.deformation);
    xlabel('$t / t_{\mathrm{final}}$');
    ylabel('$D$');
    st_print(fig, cache.filename(cache, 'deformation', 'png')); cla();

    if isfield(state, 'surface_area')
        plot(t, state.surface_area);
        xlabel('$t / t_{\mathrm{final}}$');
        ylabel('$|\Sigma|$');
        st_print(fig, cache.filename(cache, 'surface_area', 'png')); cla();
    end

    volume = log10(abs(state.volume - state.volume(1)) / state.volume(1) + 1.0e-16);

    plot(t, volume);
    xlabel('$t / t_{\mathrm{final}}$');
    ylabel('$\Delta V$');
    st_print(fig, cache.filename(cache, 'volume', 'png')); cla();

    timing = rmfield(state.timing, {'update', 'total'});
    fields = cellfun(@(z) [upper(z(1)), z(2:end)], ...
        fieldnames(timing), 'UniformOutput', false);
    timing = reshape(struct2array(timing), length(timing.geometry), []);

    h1 = area(timing); clear timing;
    xlabel('$n$');
    ylabel('$t$');
    legend(h1, fields);
    st_print(fig, cache.filename(cache, 'timing', 'png'));
    close(fig);

    % }}}

    % {{{ evolution

    plotter.variables = {'x', 'u'};
    plotter.fig_init = @fig_init;
    plotter.fig_plot = @fig_plot;
    plotter.freq = 16;

    plot_evolution(plotter, problem, cache, options);

    % }}}
end

function [fig, fax] = fig_init(fig, fax)
    xlabel(fax(1), '$x$');
    ylabel(fax(1), '$\rho$');
    axis(fax(1), 'equal');
    % xlim(fax(1), [-1.25, 5.0]);
    % ylim(fax(1), [-1.25, 1.25]);

    xlabel(fax(2), '$\xi$');
    ylabel(fax(2), '$\mathbf{u} \cdot \mathbf{n}$');
    xlim(fax(2), [0.0, 0.5]);
    ylim(fax(2), [-10.5, 8.5]);
end

function [skip] = fig_plot(x0, y0, chk, fax)
    skip = false;
    [x, ~] = st_mesh_geometry_update(x0, y0, chk.stages.k0);

    z = st_array_ax(x.x, true);
    v = st_array_ax(x.vertices, true);

    plot(fax(1), real(z), imag(z));
    plot(fax(1), real(v), imag(v), 'ko');

    plot(fax(2), x.xi, st_dot(chk.st.u, x.n));
end
