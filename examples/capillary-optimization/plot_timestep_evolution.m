% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_timestep_evolution(varargin)
    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    % NOTE: the refinement can be set afterwards, so we do the cache prefix lazily
    options.prefix = @(o) sprintf('%s_%s_%s', o.test_type, o.refinement, o.test_id);

    options.test_type = st_struct_field(options, 'test_type', 'finite');
    options.test_id = st_struct_field(options, 'test_id', 'extensional');
    options.refinement = st_struct_field(options, 'refinement', 'eps');

    options = st_unittest_example_runner(...
        @run_plot_timestep_evolution, ...
        options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function run_plot_timestep_evolution(options)
    name = lower(st_struct_field(options, 'plot', 'forward'));

    switch name
    case 'volume'
        run_plot_volume(options);
    case 'forward'
        run_plot_evolution_forward(options);
    case 'adjoint'
        run_plot_evolution_adjoint(options);
    case 'eigen'
        run_plot_forcing_eigen(options);
    otherwise
        error('unknown plot type: %s', name);
    end
end

% {{{ run_plot_volume

function run_plot_volume(options)
    cache = options.cache;
    checkpoint = st_cache_create(...
        fullfile(cache.datadir, sprintf('timesteps_ca_%04d', 0)), ...
        [], 'evolution');

    filename = checkpoint.filename(checkpoint, 'state');
    state = load(filename);

    fig = figure();
    plot(state.times, state.volume);
    xlabel('$t$');
    ylabel('$V$');
    st_print(fig, cache.filename(cache, 'volume', 'png'));
end

% }}}

% {{{ run_plot_evolution

function run_plot_evolution(options, datadir, plotter)
    cache = options.cache;
    checkpoint = st_cache_create(datadir, [], 'evolution');

    filename = checkpoint.filename(checkpoint, 'state');
    state = load(filename);

    filename = cache.filename(cache, 'setup');
    problem = load(filename);

    % unroll
    x = problem.geometry.x;
    y = problem.geometry.y;

    % {{{ plot

    nvariables = length(plotter.variables);
    vid = cell(1, length(nvariables));
    fig = gobjects(1, length(nvariables));
    fax = gobjects(1, length(nvariables));

    for m = 1:nvariables
        % NOTE: mp4 does not seem to be supported on linux
        vid{m} = VideoWriter(cache.filename(cache, plotter.variables{m}, 'avi'));
        vid{m}.FrameRate = floor(length(state.checkpoints) / 10);
        open(vid{m});

        fig(m) = figure(...
            'Color', 'white', ...
            'Position', [0, 0, 1024, 1024]);
        fax(m) = gca();
        xlabel(fax(m), '$\xi$');
    end
    [fig, fax] = plotter.fig_init(fig, fax);

    ncheckpoints = length(state.checkpoints);
    first = 1;
    plotted_first = false;

    progress = st_util_progress('nitems', ncheckpoints);

    for n = 1:ncheckpoints
        progress = st_util_progress(progress, 'action', 'tic');

        chk = load(state.checkpoints{n});
        skip = plotter.fig_plot(x, y, chk, fax);

        if skip
            if ~plotted_first
                first = first + 1;
            end
            continue;
        end

        for m = 1:nvariables
            writeVideo(vid{m}, getframe(fig(m)));
            if n == first || n == ncheckpoints
                plotted_first = true;
                st_print(fig(m), cache.filename(cache, ...
                    sprintf('%s_%04d', plotter.variables{m}, n), 'png'));
            end

            cla(fax(m));
        end
        progress = st_util_progress(progress, 'action', 'toc');

        fprintf('[%05d/%05d] t = %.5e (%s)\n', ...
            n, length(state.checkpoints), chk.t, ...
            progres.strfmt(progress));
    end

    for m = 1:nvariables
        close(vid{m});
        close(fig(m));
    end

    % }}}
end

% }}}

% {{{ run_plot_evolution_forward

function run_plot_evolution_forward(options)
    datadir = fullfile(options.cache.datadir, sprintf('timesteps_ca_%04d', 0));

    plotter.fig_init = @fw_fig_init;
    plotter.fig_plot = @fw_fig_plot;
    plotter.variables = {'x', 'u'};
    plotter.labels = {'$\mathbf{X}$', '$\mathbf{u} \cdot \mathbf{n}$'};

    run_plot_evolution(options, datadir, plotter);

    function [fig, fax] = fw_fig_init(fig, fax)
        xlabel(fax(1), '$x$');
        ylabel(fax(1), '$\rho$');
        axis(fax(1), 'equal');
        xlim(fax(1), [-2.25, 1.25]);
        ylim(fax(1), [-1.25, 1.15]);

        xlabel(fax(2), '$\xi$');
        ylabel(fax(2), '$\mathbf{u} \cdot \mathbf{n}$');
        xlim(fax(2), [0.0, 0.5]);
        ylim(fax(2), [-10.5, 8.5]);
    end

    function [skip] = fw_fig_plot(x0, y0, chk, fax)
        skip = false;
        [x, y] = st_mesh_geometry_update(x0, y0, chk.stages.k0);

        z = st_array_ax(x.x, true);
        v = st_array_ax(x.vertices, true);

        plot(fax(1), real(z), imag(z));
        plot(fax(1), real(v), imag(v), 'ko');

        plot(fax(2), x.xi, st_dot(chk.st.u, x.n));
    end
end

function run_plot_evolution_adjoint(options)
    datadir = fullfile(options.cache.datadir, 'timesteps_adjoint');

    plotter.fig_init = @ad_fig_init;
    plotter.fig_plot = @ad_fig_plot;
    plotter.variables = {'x', 'u'};
    plotter.labels = {'$\mathbf{X}^*$', '$\mathbf{u}^*$'};

    run_plot_evolution(options, datadir, plotter);

    function [fig, fax] = ad_fig_init(fig, fax)
        % nothing to do
    end

    function [skip] = ad_fig_plot(x0, ~, chk, fax)
        skip = ~isfield(chk.st.adjoint, 'u');
        if skip
            return;
        end

        plot(fax(1), x0.xi, real(chk.st.adjoint.x), '--');
        plot(fax(1), x0.xi, imag(chk.st.adjoint.x), ':');

        plot(fax(2), x0.xi, real(chk.st.adjoint.u), '--');
        plot(fax(2), x0.xi, imag(chk.st.adjoint.u), ':');
    end
end

% }}}

% {{{ run_plot_forcing_eigen

function run_plot_forcing_eigen(options)
    acache = options.cache;

    datadir = st_struct_field(options, 'fddir', []);
    fcache = st_cache_create(datadir, options.cache_id, ...
        sprintf('%s_%s_%s', 'finite', options.refinement, options.test_id));

    % get forward solution
    datadir = fullfile(fcache.datadir, sprintf('timesteps_ca_%04d', 0));
    checkpoint = st_cache_create(datadir, [], 'evolution');

    filename = checkpoint.filename(checkpoint, 'state');
    state_f = load(filename);

    problem = options.problem_init(options);
    forcing = problem.forcing;
    problem.ode

    grads = st_gradient_combine(...
        'cost', problem.cost, ...
        'jump', problem.jump, ...
        'forcing', problem.forcing);
    forcingstar = st_forcing_adjoint(...
        'param', problem.bc.param, ...
        'user', struct('grads', grads));

    % setup ploting
    plotter.fig_init = @ff_fig_init;
    plotter.fig_plot = @ff_fig_plot;
    plotter.variables = {'eigs_weak', 'eigs_strong'};

    datadir = fullfile(acache.datadir, 'timesteps_adjoint');
    run_plot_evolution(options, datadir, plotter);

    function [fig, fax] = ff_fig_init(fig, fax)
        for m = 1:length(fig)
            xlabel(fax(m), '$\Re\{\lambda\}$');
            ylabel(fax(m), '$\Im\{\lambda\}$');
        end
    end

    function [skip] = ff_fig_plot(x0, y0, chk, fax)
        skip = ~isfield(chk.st.adjoint, 'u');
        if skip
            return;
        end

        % reload geometry
        fchk = load(state_f.checkpoints{chk.it});
        [x, y] = st_mesh_geometry_update(x0, y0, chk.st.x);

        % get solution at it + 1
        xstar = chk.st.adjoint.x;
        % reconstruct right-hand side forcing for adjoint equation
        fstar = forcingstar.fn.evaluate(forcingstar, chk.t, x, y, chk.st, false);
        fstar = st_dot(fstar, x.n) .* x.n;
        fprintf('fstar: %.6e\n', ...
            norm(fstar - chk.st.adjoint.forcing) / norm(chk.st.adjoint.forcing));

        % inverse mass
        M = inv(st_ops_mass(x, y));
        M = [M, zeros(size(M)); zeros(size(M)), M];
        % construct operator
        H = forcingstar.fn.operator_x(forcingstar, chk.t, x, y, chk.st);
        % H = fn.fn.operator_x(fn, chk.t, x, y, chk.st);

        % check we can match the forcing
        fstar = forcingstar.fn.evaluate(forcingstar, chk.t, x, y, chk.st, true);
        hstar = st_array_unstack(H * st_array_stack(xstar));
        fprintf('fstar: %.6e\n', norm(fstar - hstar) / norm(fstar));

        % plot eigenvalues
        lambda_weak = eigs(H, size(H, 1));
        lambda_strong = eigs(M * H, size(H, 1));

        plot(fax(1), real(lambda_weak), imag(lambda_weak), 'o');
        plot(fax(2), real(lambda_strong), imag(lambda_strong), 'o');
    end
end

% }}}
