% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function cap_optim_surf(varargin)
    % Optimize for the capillary number.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       cap_optim_surf(...
    %           'example_id', 'extensional', ...
    %           'enable_odeplot', true, ...
    %           'overwrite', false);
    %
    % where the parameters are defined in `data/test_extensional_data.m`. The
    % default settings can be seen by calling that function with no parameters.
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();

    % {{{ options

    d.example_prefix = 'optim_surf';
    d.example_id = 'extensional';

    d.from_restart = false;
    d.restartfile = [];
    d.display = 1;

    options = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    % if options.from_restart
    %     options.reload_options = true;
    % end

    % }}}

    % {{{ run

    st_unittest_example_runner(@run_optim_surface_tension, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_optim_surface_tension(problem, cache, options)
    if ~isfield(problem, 'optim')
        error('optimization not set up')
    end

    % {{{ restart

    [filename, restartfile] = cache.filenext(cache, 'iterations');
    fprintf('output: %s\n', filename);

    if ~isempty(options.restartfile)
        restartfile = options.restartfile;
    end

    if isempty(restartfile) && options.from_restart
        error('cannot restart if no restart file is available');
    end

    param = problem.bc.param;
    h0 = problem.jump.user.h0;
    eps = problem.jump.user.eps;

    if options.from_restart
        r = load(restartfile);
        if length(r.gamma) ~= length(problem.geometry.x.x)
            error('invalid restart solution size');
        end

        gamma = r.gamma;
        options.param.gamma = h0 + eps * gamma;

        options.user.start_it = r.niters;
        fprintf('restartfile: %s\n', restartfile);
        fprintf('after %d iterations\n', r.niters);
    else
        gamma = (param.gamma - h0) / eps;

        options.param.gamma = param.gamma;
        options.user.start_it = 0;
    end

    % }}}

    % {{{ setup optimization

    % NOTE: keeping geometry so it doesn't get recomputed all the time
    options.x = problem.geometry.x;
    options.y = problem.geometry.y;

    % set optimization functions
    cg = problem.optim;

    cg.x0 = gamma;
    cg.fn = @(gamma) fn_optim_gradient(gamma, problem, cache, options);
    cg.output = @(info) fn_optim_output(info, problem, cache, options);
    cg.project = @(x, d) x;
    cg.alpha_max = @(x, d) fn_optim_alpha_max(x, d, 0.05, problem, cache, options);

    % }}}

    % {{{ optimize

    [gamma, stats] = st_optim_cg(cg);

    if options.from_restart
        stats.fn_call = stats.fn_call + r.stats.fn_call;
        stats.gn_call = stats.gn_call + r.stats.gn_call;

        stats.x = [r.stats.x(1:end - 1), stats.x];
        stats.func = [r.stats.func(1:end - 1), stats.func];
        stats.grad = [r.stats.grad(1:end - 1), stats.grad];
        stats.ddir = [r.stats.ddir, stats.ddir];
        stats.alpha = [r.stats.alpha, stats.alpha];
    end

    niters = length(stats.func) - 1;
    save(filename, 'stats', 'gamma', 'niters');

    % delete for next simulation
    filename = cache.filename(cache, 'user', 'stop');
    if exist(filename, 'file') == 2
        delete(filename);
    end

    % }}}
end

% {{{ optim functions

function [fn, grad] = fn_optim_gradient(gamma, problem, cache, options)
    gamma = fn_optim_gamma(problem, gamma);
    gamma = st_ax_filter(gamma, 'fft', 1.0e-3);

    % make sure everything is nicely recomputed
    options.overwrite = true;
    options.reload_options = false;

    % {{{ compute cost functional

    t_end = tic();
    state_f = run_forward(options, gamma, 'last_call_forward');
    t_end = toc(t_end);
    fprintf('Forward solve time %.2fs\n', t_end);

    zx = st_array_ax(state_f.x.x, true);
    zv = st_array_ax(state_f.x.vertices, true);

    fig = figure();
    plot(state_f.x.xi, state_f.x.kappa);
    st_print(fig, cache.filename(cache, 'last_curvature', 'png')); cla();

    plot(real(zx), imag(zx));
    plot(real(zv), imag(zv), 'ko');
    axis('equal');
    st_print(fig, cache.filename(cache, 'last_geometry', 'png')); cla();

    close(fig);
    fn = state_f.cost;

    % }}}

    % {{{ compute gradient + adjoint solution

    if nargout == 2
        % NOTE: this sort of assumes this just gets called once per iteration

        t_end = tic();
        state_a = run_adjoint(options, gamma, 'last_call_adjoint', 'last_call_forward');
        t_end = toc(t_end);
        fprintf('Adjoint solve %.2fs\n', t_end);

        grad_norm = norm(state_a.grad);
        if grad_norm > 2.0
            grad = state_a.grad / grad_norm;
        end

        eps = problem.jump.user.eps;
        grad = eps * st_ax_filter(grad, 'fft', 1.0e-3);

        fig = figure();
        plot(state_f.x.xi, eps * state_a.grad / grad_norm, '-');
        plot(state_f.x.xi, grad, '--');
        plot(state_f.x.xi, eps * state_a.grad, ':');
        st_print(fig, cache.filename(cache, 'last_gradient', 'png')); cla();
        close(fig);
    end


    % }}}
end

function [stop] = fn_optim_output(values, problem, cache, options)
    stopfile = cache.filename(cache, 'user', 'stop');
    stop = exist(stopfile, 'file') == 2;

    it = options.user.start_it + values.iteration;

    % {{{ load solutions

    % load forward solution
    state_f = run_forward(options, values.x, 'last_call_forward');
    % load adjoint solution
    state_a = run_adjoint(options, values.x, 'last_call_adjoint', 'last_call_forward');

    % }}}

    % {{{ save solutions

    gamma = values.x;
    direction = values.d;
    save(cache.filename(cache, sprintf('solution_%05d', it)), ...
        'gamma', 'direction', 'state_f', 'state_a');

    gamma = fn_optim_gamma(problem, values.x);

    fig = figure();
    plot(state_f.x.xi, gamma);
    xlabel('$\xi$');
    ylabel('$\gamma$');
    ylim([min(gamma) - 0.25, max(gamma) + 0.25]);

    prefix = sprintf('solution_gamma_%05d', it);
    st_print(fig, cache.filename(cache, prefix, 'png')); cla();

    zx = st_array_ax(state_f.x.x, true);
    zv = st_array_ax(state_f.x.vertices, true);

    plot(real(zx), imag(zx));
    plot(real(zv), imag(zv), 'ko');
    xlabel('$x$');
    ylabel('$\rho$');
    xlim([min(real(zv)) - 0.25, max(real(zv)) + 0.25]);
    ylim([min(imag(zv)) - 0.25, max(imag(zv)) + 0.25]);
    axis('equal');

    prefix = sprintf('solution_geometry_%05d', it);
    st_print(fig, cache.filename(cache, prefix, 'png')); cla();
    close(fig);

    % }}}
end

function [alpha] = fn_optim_alpha_max(x, d, alpha_max, problem, cache, options)
    h0 = problem.jump.user.h0;
    eps = problem.jump.user.eps;

    alpha_p = min(abs((h0 / eps + x) ./ d)) + 1.0e-12;
    alpha = min(alpha_p, alpha_max);

    gamma = (h0 + eps * x) + alpha * d;
    if ~all(gamma > 0)
        warning('gamma looks like it will become negative');
    end

    fprintf('alpha: %.5e (%.5e)\n', alpha_p, alpha_max);
end

% }}}

% {{{ helpers

function [gamma] = fn_optim_gamma(problem, gamma)
    h0 = problem.jump.user.h0;
    eps = problem.jump.user.eps;
    gamma = h0 + eps * gamma;

    if length(gamma) == 1
        gamma = gamma * ones(size(problem.geometry.x.xi));
    end
end

function [state] = run_forward(options, gamma, cache_id)
    options.verbose = false;

    options.param.gamma = gamma;
    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.cache_id = [];
    options.cache_prefix = sprintf('forward_%s', options.example_id);

    state = cap_evolve_forward(options);
end

function [state] = run_adjoint(options, gamma, cache_id, fddir)
    options.verbose = false;

    options.param.gamma = gamma;
    options.fddir = fullfile(options.cache_datadir, fddir);
    options.cache_datadir = fullfile(options.cache_datadir, cache_id);
    options.cache_prefix = sprintf('adjoint_%s', options.example_id);

    state = cap_evolve_adjoint(options);
end

% }}}
