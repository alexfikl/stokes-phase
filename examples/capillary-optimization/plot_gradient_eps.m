% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function plot_gradient_eps(varargin)
    % Plots data from `cap_gradient_eps`.
    %
    % Example run
    %
    %   .. code:: matlab
    %
    %       plot_gradient_eps('example_id', 'extensional', 'cache_id', 'same');
    %
    % Any parameters in the `example_id` file can be passed in as key-value pairs.

    orig_config = st_unittest_setup();
    options = st_struct_varargin(varargin{:});

    % {{{ run

    options.example_prefix = 'finite_eps';
    options.reload_options = true;

    options.example_id = st_struct_field(options, 'example_id', 'extensional');
    options.resolutions = st_struct_field(options, 'resolutions', 10.^-(1:7));

    st_unittest_example_runner(@run_plot_gradient_eps, options);

    % }}}

    st_unittest_setup(orig_config, true);
end

function [options] = run_plot_gradient_eps(problem, cache, options)
    x0 = problem.geometry.x;
    y0 = problem.geometry.y;

    % {{{ plot geometry

    fig = gobjects(1, 2);
    fig_ax = gobjects(1, 2);
    for n = 1:length(fig)
        fig(n) = figure();
        fig_ax(n) = gca();
    end

    prefix = sprintf('forward_%s', options.example_id);
    state_d = st_ode_unsteady_load(cache, x0, y0, 'ca_desired', prefix);

    x = st_array_ax(state_d.x.x, true);
    plot(fig_ax(1), real(x), imag(x), 'k-');
    plot(fig_ax(2), state_d.x.xi, state_d.x.kappa, 'k-');

    for n = 1:length(options.resolutions) + 1
        state = st_ode_unsteady_load(cache, x0, y0, sprintf('ca_%04d', n - 1), prefix);

        x = st_array_ax(state.x.x, true);
        plot(fig_ax(1), real(x), imag(x), ':');
        plot(fig_ax(2), state.x.xi, state.x.kappa, ':');
    end

    xlabel(fig_ax(1), '$x$');
    ylabel(fig_ax(1), '$\rho$');
    axis(fig_ax(1), 'equal');
    st_print(fig(1), cache.filename(cache, 'geometry', 'png'));

    xlabel(fig_ax(2), '$\xi$');
    ylabel(fig_ax(2), '$\kappa$');
    st_print(fig(2), cache.filename(cache, 'curvature', 'png'));

    close(fig(1));
    close(fig(2));

    % }}}

    % {{{ plot cost and gradient

    grad = load(cache.filename(cache, 'gradient'));
    ad_grad = grad.ad_grad;
    fd_grad = grad.fd_grad;

    fprintf('         %12s %13s %13s\n', 'cost', 'fd', 'ad');
    fprintf('%.2e %.6e %13s %13s\n', grad.ca_eps(1), grad.cost(1), '---', '---');
    for n = 2:length(grad.ca_eps)
        fprintf('%.2e %.6e %+.6e %+.6e\n', ...
            grad.ca_eps(n), grad.cost(n), fd_grad(n - 1), ad_grad);
    end

    fig = figure();
    semilogx(grad.ca_eps, grad.cost);
    xlabel('$\epsilon$');
    ylabel('$\mathcal{J}$');
    st_print(fig, cache.filename(cache, 'cost', 'png'));
    clf();

    semilogx(grad.ca_eps(2:end), fd_grad, 'o--');
    semilogx(grad.ca_eps(2:end), ad_grad * ones(size(fd_grad)), 'k-');
    xlabel('$\epsilon$');
    ylabel('$\mathcal{J}$');
    st_print(fig, cache.filename(cache, 'gradient', 'png'));
    clf();

    % }}}

    % {{{ plot orders

    eoc = st_util_eoc({'grad'}, length(grad.fd_grad));

    for n = 1:length(fd_grad)
        eoc.errors.grad(n) = abs(fd_grad(n) - ad_grad); % / abs(fd_grad(n));
        eoc.h(n) = options.resolutions(n);

        eoc = st_util_eoc(eoc);
    end
    errors = eoc.errors;

    % get O(h) line
    y0 = log10(errors.grad(1)); x0 = log10(eoc.h(1));
    y1 = log10(min(errors.grad)); x1 = (y1 - y0) + x0;

    plot(log10(eoc.h), log10(errors.grad), 'o--');
    loglog([x0, x1], [y0, y1], 'k-', 'LineWidth', 4);
    xlabel('$\log_{10} \epsilon$');
    ylabel('$\log_{10} E$');
    st_print(fig, cache.filename(cache, 'error', 'png'));

    % }}}
end
