% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

% This example shows the bump functions used to perturb the geometry for
% the finite difference approximations.

% number of panels
npanels = 64;
% number of basis functions
nbasis = 4;
% geometry
x = st_point_collocation(npanels, nbasis, @(xi) exp(2.0j * pi * xi));

% bump function
bump.eps = 1.0e-5;
bump.beta = 1.0e-2;
bump.sigma = 1.0e-3;
bump.bump_type = 'xi';

% find point at suitable distance
dxi = x.xi(2) - x.xi(1);
x0 = sqrt(-bump.sigma * log(bump.eps));
b_lim = ceil(x0 / dxi) + 1;
% set limit points
b_first = x.xi(b_lim);
b_last = x.xi(end - b_lim);
% flip coordinates
xi = [-fliplr(x.xi), x.xi];

fig = figure('visible', 'on');
ylim([-1, 1]);
h1 = plot(xi, 0 * xi, '-');

ntargets = length(x.xi);
for n = 1:ntargets
    z = st_shape_finite_bump(bump, x, x, n);

    zo = [fliplr(z), z];
    set(h1, 'YData', zo);
    title(sprintf('$%4d < %4d < %4d$', b_lim, n, ntargets - b_lim));
    drawnow;
    pause(0.1);
end
clf(fig);
