PYTHON := "python -X dev"

_default:
    @just --list

# {{{ linting

[doc("Check REUSE license compliance")]
reuse:
    {{ PYTHON }} -m reuse lint
    @echo -e "\e[1;32mREUSE compliant!\e[0m"

# }}}

# {{{ develop

[doc("Generate ctags")]
ctags:
    ctags --recurse=yes \
        --tag-relative=yes \
        --exclude=.git \
        --exclude=docs \
        --languages=MatLab \
        --langmap=MatLab:+.m

# }}}
