% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_stokes_solutions
  tests = functiontests({@setup, @teardown, ...
                         @test_solution_identities, ...
                         @test_solution_plot});
end

% {{{ helpers

function util_plot_volume(st, filename, fn_int, fn_ext)
    % number of points
    npoints = 512;
    % get points
    x = linspace(-2.5, 2.5, 2 * npoints);
    y = linspace(-1.5, 1.5, npoints);
    theta = linspace(0.0, 2.0 * pi, npoints);

    [X, Y] = meshgrid(x, y);
    mask = sqrt(X.^2 + Y.^2) < (st.param.R - 1.0e-4);

    % compute function
    fn = matlabFunction(fn_ext, 'Vars', st.x);
    fn_ext = real(fn(X, Y));
    fn = matlabFunction(fn_int, 'Vars', st.x);
    fn_int = real(fn(X, Y));

    fn = mask .* fn_int + (1 - mask) .* fn_ext;
    if norm(fn - mean(fn(:))) < 1.0e-8
        return;
    end

    fig = figure();
    axis('equal');
    contourf(X, Y, fn, 64, '--');
    plot(cos(theta), sin(theta), 'k', 'LineWidth', 2);
    % colorbar;
    st_print(fig, filename);
    close(fig);
end

function util_plot_surface(st, filename, fn)
    theta = linspace(0.0, pi, 256);

    fn = matlabFunction(fn, 'Vars', st.theta);
    fn = real(fn(theta));
    if length(fn) == 1
        fn = fn * ones(length(theta));
    end

    fig = figure();
    plot(theta, fn);
    st_print(fig, filename);
    close(fig);
end

% }}}

% {{{ test_solution_identities

function test_solution_identities(testCase)
    %
    % Test that the solutions from :func:`st_sym_solutions` satisfy the
    % Stokes equations and boundary conditions.
    %

    % options
    opts.lambda = 2.0;
    opts.pinf = 0.0;

    solutions = {'HadamardRybczynski', ...
                 'SolidSphere', ...
                 'SolidQuadratic'};

    for name = solutions
        fprintf('Solution: %s\n', name{:});
        st = st_sym_solutions(name{:}, opts);

        % check solution
        % 1. continuity of normal velocity
        un_int = simplify(subs(dot(st.u_int, st.n), st.x, st.xs));
        un_ext = simplify(subs(dot(st.u_ext, st.n), st.x, st.xs));
        testCase.verifyEqual(double(un_ext - un_int), 0.0);

        % 2. continuity of tangential velocity
        ut_int = simplify(subs(dot(st.u_int, st.t), st.x, st.xs));
        ut_ext = simplify(subs(dot(st.u_ext, st.t), st.x, st.xs));
        testCase.verifyEqual(double(ut_ext - ut_int), 0.0);

        if ~isfield(st, 'lambda')
            continue
        end

        % 3. continuity of tangential stress
        st_int = simplify(subs(dot(st.sigma_int * st.n', st.t), st.x, st.xs));
        st_ext = simplify(subs(dot(st.sigma_ext * st.n', st.t), st.x, st.xs));
        testCase.verifyEqual(double(st_ext - st.lambda * st_int), 0.0);

        % 4. jump in normal stress
        st_int = simplify(subs(dot(st.sigma_int * st.n', st.n), st.x, st.xs));
        st_ext = simplify(subs(dot(st.sigma_ext * st.n', st.n), st.x, st.xs));
        fprintf('Stress Jump: %s\n', st_ext - st.lambda * st_int);

        testCase.verifyFalse(isequaln(st_ext - st.lambda * st_int, 0));
    end
end

% }}}

% {{{ test_solution_plot

function test_solution_plot(~)
    %
    % Plots solutions from :func:`st_sym_solutions`.
    %

    % options
    opts.lambda = 2.0;
    opts.capillary = 0.01;
    opts.pinf = 0.0;

    cache = unittest_create_cache('stokes_solution');

    solutions = {'HadamardRybczynski', ...
                 'SolidSphere', ...
                 'SolidQuadratic'};

    for name = solutions
        fprintf('Solution: %s\n', name{:});

        st = st_sym_solutions(name{:}, opts);
        to_filename = @(prefix) cache.filename(cache, ...
            sprintf('%s_%s', lower(name{:}), prefix), 'png');

        % plot volume solutions
        if isfield(st, 'psi_int')
            util_plot_volume(st, to_filename('stream'), st.psi_int, st.psi_ext);
        end

        if isfield(st, 'p_int')
            util_plot_volume(st, to_filename('pressure'), st.p_int, st.p_ext);
        end

        if isfield(st, 'u_int')
            util_plot_volume(st, to_filename('velocity'), norm(st.u_int), norm(st.u_ext));
        end

        % plot surface solutions
        if ~isfield(st, 'u_ext')
            continue;
        end

        % plot surface velocity
        u = subs(st.u_int(1), st.x, st.xs);
        util_plot_surface(st, to_filename('velocity_x'), u);

        u = subs(st.u_int(2), st.x, st.xs);
        util_plot_surface(st, to_filename('velocity_y'), u);

        % plot surface gradient and normal component
        gradu = simplify(subs(st.gradu_ext, st.x, st.xs));
        gradn = simplify(subs(dot(gradu * st.n', st.n), st.x, st.xs));

        util_plot_surface(st, to_filename('dudn'), gradn);
        util_plot_surface(st, to_filename('dudx_ext'), gradu(1, 1));
        util_plot_surface(st, to_filename('dudy_ext'), gradu(1, 2));
        util_plot_surface(st, to_filename('dvdx_ext'), gradu(2, 1));
        util_plot_surface(st, to_filename('dvdy_ext'), gradu(2, 2));

        % plot v along the x = 0.0 line
        y = linspace(0.0, 1.0, 128);
        u_int = matlabFunction(st.u_int(2), 'Vars', st.x);
        u_ext = matlabFunction(st.u_ext(2), 'Vars', st.x);

        fig = figure();
        plot(y, u_int(0, y));
        plot(y, u_ext(0, y + 1.0))
        st_print(fig, to_filename('velocity_y_axis'));
        close(fig);
    end
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
