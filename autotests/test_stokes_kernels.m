% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_stokes_kernels
  tests = functiontests({@setup, @teardown, ...
                         @test_ellint_order3, ...
                         @test_ellint_order5, ...
                         @test_ellint_order7, ...
                         @test_ellint_order3_axis_limit, ...
                         @test_ellint_order5_axis_limit, ...
                         @test_ellint_order7_axis_limit, ...
                         @test_knl_ax_stress_source, ...
                         @test_knl_ax_stress_target, ...
                         @test_knl_ax_pressure_source, ...
                         @test_knl_ax_pressure_target, ...
                         @test_knl_ax_pressure_ss, ...
                         @test_knl_ax_gradu, ...
                         @test_knl_ax_grads, ...
                         @test_knl_ax_grads_axis_limit});
end

% {{{ helpers: st_ellint wrappers

function [I10, I11, I30, I31, I32] = util_ellint_order3(x, x0, F, E)
    if abs(imag(x0)) < 1.0e-8
        [I10, I11, I30, I31, I32] = st_ellint_order3_axis(x, x0, F, E);
    else
        [I10, I11, I30, I31, I32] = st_ellint_order3(x, x0, F, E);
    end

    if nargout == 1
        I10 = struct('I10', I10, 'I11', I11, 'I30', I30, 'I31', I31, 'I32', I32);
    end
end

function [I50, I51, I52, I53] = util_ellint_order5(x, x0, F, E)
    if abs(imag(x0)) < 1.0e-8
        [I50, I51, I52, I53] = st_ellint_order5_axis(x, x0, F, E);
    else
        [I50, I51, I52, I53] = st_ellint_order5(x, x0, F, E);
    end

    if nargout == 1
        I50 = struct('I50', I50, 'I51', I51, 'I52', I52, 'I53', I53);
    end
end

function [I70, I71, I72, I73, I74] = util_ellint_order7(x, x0, F, E)
    if abs(imag(x0)) < 1.0e-8
        [I70, I71, I72, I73, I74] = st_ellint_order7_axis(x, x0, F, E);
    else
        [I70, I71, I72, I73, I74] = st_ellint_order7(x, x0, F, E);
    end

    if nargout == 1
        I70 = struct('I70', I70, 'I71', I71, 'I72', I72, 'I73', I73, 'I74', I74);
    end
end

% }}}

% {{{ helpers: 3D kernels

function [R] = util_knl_pressure(phi, X, X0, i)
    % Compute 3D pressure kernel for :func:`st_knl_ax_pressure`.
    %
    % :arg phi: azymuthal angle :math:`\phi` for source points.
    % :arg X: :math:`(x, \rho)` coordinates for source points.
    % :arg X0: :math:`(x_0, \rho_0)` coordinates for target point, for which
    %   :math:`\phi_0 = 0` always.
    % :arg i: component of the kernel.

    R = zeros(size(phi));

    for p = 1:length(phi)
        xs = [real(X.x), imag(X.x) * cos(phi(p)), imag(X.x) * sin(phi(p))];
        xt = [real(X0.x), imag(X0.x), 0.0];

        R(p) = 2.0 * imag(X.x) * (xs(i) - xt(i)) / norm(xs - xt)^3;
    end
end

function [R] = util_knl_pressure_ss(phi, x, x0, i)
    % Compute 3D pressure singularity subtraction kernel
    % for :func:`st_knl_ax_pressure_ss`.
    %
    % :arg phi: azymuthal angle :math:`\phi` for source points.
    % :arg X: :math:`(x, \rho)` coordinates for source points.
    % :arg X0: :math:`(x_0, \rho_0)` coordinates for target point, for which
    %   :math:`\phi_0 = 0` always.
    % :arg i: component of the kernel.

    R1 = util_knl_pressure(phi, x, x0, 1);
    R2 = util_knl_pressure(phi, x, x0, 2);
    R3 = util_knl_pressure(phi, x, x0, 3);

    R = zeros(size(R1));
    for p = 1:length(phi)
        % pressure kernel
        R0 = [R1(p), R2(p), R3(p)];

        % point normals
        ns = [real(x.n), imag(x.n) * cos(phi(p)), imag(x.n) * sin(phi(p))];
        nt = [real(x0.n), imag(x0.n), 0.0];

        R(p) = dot(R0, ns) * nt(i) + ...
               dot(ns, nt) * R0(i) - ...
               dot(R0, nt) * ns(i);
    end
end

function [R] = util_knl_stress(phi, X, X0, i)
    % Compute 3D stress kernel for :func:`st_knl_ax_stress_source`
    % and :func:`st_knl_ax_stress_target`.
    %
    % :arg phi: azymuthal angle :math:`\phi` for source points.
    % :arg X: :math:`(x, \rho)` coordinates for source points.
    % :arg X0: :math:`(x_0, \rho_0)` coordinates for target point, for which
    %   :math:`\phi_0 = 0` always.
    % :arg i: component of the kernel.

    R = zeros(size(phi));

    for p = 1:length(phi)
        xs = [real(X.x), imag(X.x) * cos(phi(p)), imag(X.x) * sin(phi(p))];
        xt = [real(X0.x), imag(X0.x), 0.0];

        R(p) = -6.0 * imag(X.x) * prod(xs(i) - xt(i)) / norm(xs - xt)^5;
    end
end

function [R] = util_knl_gradu(phi, X, X0, i)
    % Compute 3D velocity gradient kernel for :func:`st_knl_ax_gradu`.
    %
    % :arg phi: azymuthal angle :math:`\phi` for source points.
    % :arg X: :math:`(x, \rho)` coordinates for source points.
    % :arg X0: :math:`(x_0, \rho_0)` coordinates for target point, for which
    %   :math:`\phi_0 = 0` always.
    % :arg i: component of the kernel.

    R = zeros(size(phi));

    % deltas
    d = [i(1) == i(2), -(i(2) == i(3)), -(i(1) == i(3))];
    i_k = [i(3), i(1), i(2)];

    for p = 1:length(phi)
        rs = [real(X.x) - real(X0.x), ...
              imag(X.x) * cos(phi(p)) - imag(X0.x), ...
              imag(X.x) * sin(phi(p))];

        R(p) = 3.0 * prod(rs(i)) / norm(rs)^5 + sum(rs(i_k) .* d) / norm(rs)^3;
        R(p) = imag(X.x) * R(p);
    end
end

function [R] = util_knl_grads(phi, X, X0, i)
    % Compute 3D stress gradient kernel for :func:`st_knl_ax_stress_gradient`.
    %
    % :arg phi: azymuthal angle :math:`\phi` for source points.
    % :arg X: :math:`(x, \rho)` coordinates for source points.
    % :arg X0: :math:`(x_0, \rho_0)` coordinates for target point, for which
    %   :math:`\phi_0 = 0` always.
    % :arg i: component of the kernel.

    R = zeros(size(phi));

    for p = 1:length(phi)
        r = [real(X.x) - real(X0.x), ...
            imag(X.x) * cos(phi(p)) - imag(X0.x), ...
            imag(X.x) * sin(phi(p))];
        n = [real(X0.n), imag(X0.n), 0];

        R(p) = 6.0 * (2.0 * dot(r, n) * r(i) + dot(r, n)^2 * n(i)) / norm(r)^5 - ...
            30.0 * dot(r, n)^3 * r(i) / norm(r)^7;
        R(p) = imag(X.x) * R(p);
    end
end

% }}}

% {{{ helpers: comparisons

function util_ellint_comparison(testCase, variables, problem)
    % Compare elliptic integrals integrated with :func:`quadgk` vs
    % the ones obtained from :func:`st_ellint_order3` and friends.
    %
    % :arg testCase: a :class:`TestCase`.

    cache = unittest_create_cache(problem.name);

    % number of randomly generated points
    nruns = st_struct_field(problem, 'nruns', 16);
    % relative tolerance
    rtol = st_struct_field(problem, 'rtol', 1.0e-10);
    % labels
    if ~isfield(problem, 'labels')
        problem.labels = cell(1, length(variables));
        for k = 1:length(variables)
            name = variables{k};
            problem.labels{k} = sprintf('$%s_{%s}$', name(1), name(2:end));
        end
    end

    % define integral
    syms k w n m real;
    Jnm = (2.0 * cos(w)^2 - 1.0)^m / (1.0 - k^2 * cos(w)^2)^(n / 2.0); %#ok
    Jnm = matlabFunction(Jnm, 'Vars', [k, m, n, w]);

    for n = 1:nruns
        % generate random points
        X0 = -4.0 + 8.0 * rand();
        Y0 = 0.0 + 2.0 * rand();
        X1 = -4.0 + 8.0 * rand();
        Y1 = 0.5 + 2.0 * rand();

        X = X0 + 1j * Y0;
        Y = X1 + 1j * Y1;

        % exact solution
        K = sqrt(4.0 * Y1 * Y0 / ((X1 - X0)^2 + (Y1 + Y0)^2));
        L = K / sqrt(4.0 * Y1 * Y0);

        ex = struct();
        for k = 1:length(variables)
            name = variables{k};
            n_ = str2double(name(2)); m_ = str2double(name(3)); p_ = n_;

            ex.(name) = quadgk(@(w) 4.0 * L^p_ * Jnm(K, m_, n_, w), ...
                0.0, pi / 2.0, ...
                'AbsTol', rtol / 1000);
        end
        % approximate solution
        [F, E] = st_ellipke(Y, X);
        ap = problem.evaluate(Y, X, F, E);

        for k = 1:length(variables)
            name = variables{k};

            % fprintf('Error: %s %13.5e\n', name, ex.(name) - ap.(name));
            testCase.verifyEqual(ex.(name), ap.(name), 'RelTol', rtol);
        end
    end

    % plot integrands in phi direction
    fig = figure();
    fig_handle = gobjects(1, length(variables));

    w = linspace(0.0, pi / 2.0, 256);
    for k = 1:length(variables)
        name = variables{k};
        n_ = str2double(name(2)); m_ = str2double(name(3)); p_ = m_;

        fig_handle(k) = ...
            plot(w, 4.0 * L^p_ * Jnm(K, m_, n_, w));
    end

    legend(fig_handle, problem.labels, 'Location', 'eastoutside');
    st_print(fig, cache.filename(cache, 'integrand', 'png'));
end

function util_knl_comparison(testCase, problem)
    % Compare kernels obtained from :func:`st_knl_ax_velocity` and friends
    % with ones obtained by numerically integrating in the azymuthal direction.
    %
    % :arg testCase: a :class:`TestCase`.

    % number of runs
    nruns = st_struct_field(problem, 'nruns', 16);
    % relative tolerance
    rtol = st_struct_field(problem, 'rtol', 1.0e-8);
    % number of points
    npoints = st_struct_field(problem, 'npoints', 512);
    % parametrization
    xi = linspace(0.0, 0.5, npoints);
    % define an ellipse
    x = exp(2.0j * pi * xi);

    % test kernel components
    n = 1;
    while n < nruns
        % pick random source and target points
        i = randi([2, npoints - 1], 1);
        X.x = x(i); X.n = x(i);
        j = randi([2, npoints - 1], 1);
        X0.x = x(j); X0.n = x(j);
        if i == j
            continue;
        end

        % compute approximate solution
        ap = problem.evaluate(X, X0);
        % compute exact solution
        knl = zeros(size(ap.knl));
        for m = 1:length(knl)
            knl(m) = quadgk(@(p) problem.knl{m}(p, X, X0), 0, 2 * pi);
        end

        err = norm(ap.knl - knl) / norm(knl);
        if err > rtol
            for m = 1:length(knl)
                err = abs(ap.knl(m) - knl(m)) / abs(knl(m));
                fprintf('error: [%d] %.5e %.8e %.8e\n', ...
                    m, err, ap.knl(m), knl(m));
            end
        end

        testCase.verifyLessThan(norm(ap.knl - knl) / norm(knl), rtol);
        n = n + 1;
    end
end

function util_ellint_axis_limits(testCase, variables, problem)
    % Compute axis limits for the :math:`I_{mn}` elliptic integrals and
    % compare against analytic approximations.
    %
    % :arg testCase: a :class:`TestCase`.

    cache = unittest_create_cache(sprintf('ellint_axis_%s', problem.name));

    % relative tolerance
    rtol = st_struct_field(problem, 'rtol', 1.0e-12);
    % labels
    if ~isfield(problem, 'labels')
        problem.labels = cell(1, length(variables));
        for k = 1:length(variables)
            name = variables{k};
            problem.labels{k} = sprintf('$%s_{%s}$', name(1), name(2:end));
        end
    end

    % number of points
    npoints = st_struct_field(problem, 'npoints', 512);
    % parametrization
    xi = linspace(0.0, 0.5, npoints);
    % curve
    R = 1.0;
    x = R * exp(2j * pi * xi);

    %
    % loop to target point on axis
    %

    j = floor(3 / 5 * npoints);
    irange = (j - 1):-1:1;

    ell = struct();
    for m = 1:length(variables)
        ell.(variables{m}) = zeros(1, length(irange));
    end

    % compute elliptic integrals
    for i = irange
        k = i - irange(end) + 1;
        [F, E] = st_ellipke(x(j), x(i));
        r = problem.evaluate(x(j), x(i), F, E);

        for m = 1:length(variables)
            ell.(variables{m})(k) = r.(variables{m});
        end
    end

    % check asymptotic behavior
    if isfield(problem, 'limits')
        R = abs(x(j) - x(1));
        limits = problem.limits(R);
        for m = 1:length(variables)
            v_ap = ell.(variables{m})(1);
            v_ex = limits.(variables{m});

            % fprintf('Error: %s %.6e\n', variables{m}, v_ap - v_ex);
            testCase.verifyEqual(v_ex, v_ap, 'RelTol', rtol);
        end
    end

    % plot
    for m = 1:length(variables)
        fig = figure();
        plot(irange, fliplr(ell.(variables{m})));
        if isfield(problem, 'limits')
            plot([irange(1), irange(end)], limits.(variables{m}) * [1, 1], 'k--');
        end

        xlabel('$i$');
        ylabel(problem.labels{m});
        st_print(fig, cache.filename(cache, lower(variables{m}), 'png'));
    end
end

% }}}

% {{{ test_ellint

function test_ellint_order3(testCase)
    %
    % Test :func:`st_ellint_order3`.
    %

    problem.name = 'ellint_order3';
    variables = {'I10', 'I11', 'I30', 'I31', 'I32'};

    problem.evaluate = @util_ellint_order3;
    util_ellint_comparison(testCase, variables, problem);
end

function test_ellint_order5(testCase)
    %
    % Test :func:`st_ellint_order7`.
    %

    problem.name = 'ellint_order5';
    variables = {'I50', 'I51', 'I52', 'I53'};

    problem.rtol = 1.0e-7;
    problem.evaluate = @util_ellint_order5;
    util_ellint_comparison(testCase, variables, problem);
end

function test_ellint_order7(testCase)
    %
    % Test :func:`st_ellint_order7`.
    %

    problem.name = 'ellint_order7';
    variables = {'I70', 'I71', 'I72', 'I73', 'I74'};

    problem.rtol = 1.0e-6;
    problem.evaluate = @util_ellint_order7;
    util_ellint_comparison(testCase, variables, problem);
end

% }}}

% {{{ test_ellint_axis_limits

function test_ellint_order3_axis_limit(testCase)
    %
    % Test :func:`st_ellint_order3_axis`.
    %

    problem.name = 'ellint_order3_axis';
    variables = {'I10', 'I11', 'I30', 'I31', 'I32'};

    problem.evaluate = @util_ellint_order3;
    problem.limits = @fn_limits;
    util_ellint_axis_limits(testCase, variables, problem);

    function [r] = fn_limits(R)
        r.I10 = 2.0 * pi / R;
        r.I11 = 0.0;
        r.I30 = 2.0 * pi / R^3;
        r.I31 = 0.0;
        r.I32 = 1.0 * pi / R^3;
    end
end

function test_ellint_order5_axis_limit(testCase)
    %
    % Test :func:`st_ellint_order5_axis`.
    %

    problem.name = 'ellint_order5_axis';
    variables = {'I50', 'I51', 'I52', 'I53'};

    problem.evaluate = @util_ellint_order5;
    problem.limits = @fn_limits;
    util_ellint_axis_limits(testCase, variables, problem);

    function [r] = fn_limits(R)
        r.I50 = 2.0 * pi / R^5;
        r.I51 = 0.0;
        r.I52 = 1.0 * pi / R^5;
        r.I53 = 0.0;
    end
end

function test_ellint_order7_axis_limit(testCase)
    %
    % Test :func:`st_ellint_order7_axis`.
    %

    problem.name = 'ellint_order7';
    variables = {'I70', 'I71', 'I72', 'I73', 'I74'};

    problem.rtol = 1.0e-8;
    problem.evaluate = @util_ellint_order7;
    problem.limits = @fn_limits;
    util_ellint_axis_limits(testCase, variables, problem);

    function [r] = fn_limits(R)
        r.I70 = 2.0 * pi / R^7;
        r.I71 = 0.0;
        r.I72 = 1.0 * pi / R^7;
        r.I73 = 0.0;
        r.I74 = 3.0 / 4.0 * pi / R^7;
    end
end

% }}}

% {{{ test_knl_ax

function test_knl_ax_stress_source(testCase)
    %
    % Test :func:`st_knl_ax_stress_source`.
    %

    problem.name = 'knl_stress_source';
    Q = zeros(2, 2, 2);

    K = @util_knl_stress;
    knl = cell(1, numel(Q));

    knl{1} = @(p, x, x0) K(p, x, x0, [1, 1, 1]);
    knl{2} = @(p, x, x0) K(p, x, x0, [1, 1, 2]) .* cos(p) + ...
                         K(p, x, x0, [1, 1, 3]) .* sin(p);
    knl{3} = @(p, x, x0) K(p, x, x0, [2, 1, 1]) .* cos(p) + ...
                         K(p, x, x0, [3, 1, 1]) .* sin(p);
    knl{4} = @(p, x, x0) K(p, x, x0, [2, 1, 2]) .* cos(p).^2 + ...
                         K(p, x, x0, [3, 1, 3]) .* sin(p).^2 + ...
                         K(p, x, x0, [2, 1, 3]) .* 2.0 .* cos(p) .* sin(p);

    knl{5} = @(p, x, x0) K(p, x, x0, [1, 2, 1]);
    knl{6} = @(p, x, x0) K(p, x, x0, [1, 2, 2]) .* cos(p) + ...
                         K(p, x, x0, [1, 2, 3]) .* sin(p);
    knl{7} = @(p, x, x0) K(p, x, x0, [2, 2, 1]) .* cos(p) + ...
                         K(p, x, x0, [3, 2, 1]) .* sin(p);
    knl{8} = @(p, x, x0) K(p, x, x0, [2, 2, 2]) .* cos(p).^2 + ...
                         K(p, x, x0, [3, 2, 3]) .* sin(p).^2 + ...
                         K(p, x, x0, [2, 2, 3]) .* 2.0 .* cos(p) .* sin(p);

    problem.knl = knl;
    problem.evaluate = @fn_evaluate;
    util_knl_comparison(testCase, problem);

    function [r] = fn_evaluate(x, x0)
        [F, E] = st_ellipke(x.x, x0.x);
        [I50, I51, I52, I53] = util_ellint_order5(x.x, x0.x, F, E);
        Q = st_knl_ax_stress_source(Q, x.x, x0.x, I50, I51, I52, I53);

        k = [1, 5, 3, 7, 2, 6, 4, 8];
        r.knl = Q(k);
    end
end

function test_knl_ax_stress_target(testCase)
    %
    % Test :func:`st_knl_ax_stress_target`.
    %

    problem.name = 'knl_stress_target';
    Q = zeros(2, 2, 2);

    K = @util_knl_stress;
    knl = cell(1, numel(Q));

    knl{1} = @(p, x, x0) K(p, x, x0, [1, 1, 1]);
    knl{2} = @(p, x, x0) K(p, x, x0, [1, 1, 2]);
    knl{3} = @(p, x, x0) K(p, x, x0, [1, 2, 1]) .* cos(p) + ...
                         K(p, x, x0, [1, 3, 1]) .* sin(p);
    knl{4} = @(p, x, x0) K(p, x, x0, [1, 2, 2]) .* cos(p) + ...
                         K(p, x, x0, [1, 3, 2]) .* sin(p);

    knl{5} = @(p, x, x0) K(p, x, x0, [2, 1, 1]);
    knl{6} = @(p, x, x0) K(p, x, x0, [2, 1, 2]);
    knl{7} = @(p, x, x0) K(p, x, x0, [2, 2, 1]) .* cos(p) + ...
                         K(p, x, x0, [2, 3, 1]) .* sin(p);
    knl{8} = @(p, x, x0) K(p, x, x0, [2, 2, 2]) .* cos(p) + ...
                         K(p, x, x0, [2, 3, 2]) .* sin(p);

    problem.knl = knl;
    problem.evaluate = @fn_evaluate;
    util_knl_comparison(testCase, problem);

    function [r] = fn_evaluate(x, x0)
        [F, E] = st_ellipke(x.x, x0.x);
        [I50, I51, I52, I53] = util_ellint_order5(x.x, x0.x, F, E);
        Q = st_knl_ax_stress_target(Q, x.x, x0.x, I50, I51, I52, I53);

        k = [1, 5, 3, 7, 2, 6, 4, 8];
        r.knl = Q(k);
    end
end

function test_knl_ax_pressure_source(testCase)
    %
    % Test :func:`st_knl_ax_pressure`.
    %

    problem.name = 'knl_pressure';
    Q = zeros(1, 2);

    K = @util_knl_pressure;
    knl = cell(1, numel(Q));

    knl{1} = @(p, x, x0) K(p, x, x0, 1);
    knl{2} = @(p, x, x0) K(p, x, x0, 2) .* cos(p) + ...
                         K(p, x, x0, 3) .* sin(p);

    problem.knl = knl;
    problem.evaluate = @fn_evaluate;
    util_knl_comparison(testCase, problem);

    function [r] = fn_evaluate(x, x0)
        [F, E] = st_ellipke(x.x, x0.x);
        [~, ~, I30, I31, ~] = util_ellint_order3(x.x, x0.x, F, E);
        Q = st_knl_ax_electric_dlp_source(Q, x.x, x0.x, I30, I31);

        r.knl = 2.0 * Q(:)';
    end
end

function test_knl_ax_pressure_target(testCase)
    %
    % Test :func:`st_knl_ax_pressure_t`.
    %

    problem.name = 'knl_pressure';
    Q = zeros(1, 2);

    K = @util_knl_pressure;
    knl = cell(1, numel(Q));

    knl{1} = @(p, x, x0) K(p, x, x0, 1);
    knl{2} = @(p, x, x0) K(p, x, x0, 2);

    problem.knl = knl;
    problem.evaluate = @fn_evaluate;
    util_knl_comparison(testCase, problem);

    function [r] = fn_evaluate(x, x0)
        [F, E] = st_ellipke(x.x, x0.x);
        [~, ~, I30, I31, ~] = util_ellint_order3(x.x, x0.x, F, E);
        Q = st_knl_ax_electric_dlp_target(Q, x.x, x0.x, I30, I31);

        r.knl = 2.0 * Q(:)';
    end
end

function test_knl_ax_pressure_ss(testCase)
    %
    % Test :func:`st_knl_ax_pressure_ss`.
    %

    problem.name = 'knl_pressure_ss';
    Q = zeros(1, 2);

    K = @util_knl_pressure_ss;
    knl = cell(1, numel(Q));

    knl{1} = @(p, x, x0) K(p, x, x0, 1);
    knl{2} = @(p, x, x0) K(p, x, x0, 2);

    problem.knl = knl;
    problem.evaluate = @fn_evaluate;
    util_knl_comparison(testCase, problem);

    function [r] = fn_evaluate(x, x0)
        [F, E] = st_ellipke(x.x, x0.x);
        [~, ~, I30, I31, ~] = util_ellint_order3(x.x, x0.x, F, E);
        Q = st_knl_ax_pressure_ss(Q, x.x, x0.x, x.n, x0.n, I30, I31);

        r.knl = Q(:)';
    end
end

function test_knl_ax_gradu(testCase)
    %
    % Test :func:`st_knl_ax_gradu`.
    %

    problem.name = 'knl_gradu';
    Q = zeros(2, 2, 2);

    K = @util_knl_gradu;
    knl = cell(1, numel(Q));

    knl{1} = @(p, x, x0) K(p, x, x0, [1, 1, 1]);
    knl{2} = @(p, x, x0) K(p, x, x0, [1, 1, 2]);
    knl{3} = @(p, x, x0) K(p, x, x0, [1, 2, 1]) .* cos(p) + ...
                         K(p, x, x0, [1, 3, 1]) .* sin(p);
    knl{4} = @(p, x, x0) K(p, x, x0, [1, 2, 2]) .* cos(p) + ...
                         K(p, x, x0, [1, 3, 2]) .* sin(p);

    knl{5} = @(p, x, x0) K(p, x, x0, [2, 1, 1]);
    knl{6} = @(p, x, x0) K(p, x, x0, [2, 1, 2]);
    knl{7} = @(p, x, x0) K(p, x, x0, [2, 2, 1]) .* cos(p) + ...
                         K(p, x, x0, [2, 3, 1]) .* sin(p);
    knl{8} = @(p, x, x0) K(p, x, x0, [2, 2, 2]) .* cos(p) + ...
                         K(p, x, x0, [2, 3, 2]) .* sin(p);

    problem.knl = knl;
    problem.evaluate = @fn_evaluate;
    util_knl_comparison(testCase, problem);

    function [r] = fn_evaluate(x, x0)
        [F, E] = st_ellipke(x.x, x0.x);
        [~, ~, I30, I31, ~] = util_ellint_order3(x.x, x0.x, F, E);
        [I50, I51, I52, I53] = util_ellint_order5(x.x, x0.x, F, E);
        Q = st_knl_ax_gradu(Q, x.x, x0.x, I30, I31, I50, I51, I52, I53);

        k = [1, 5, 3, 7, 2, 6, 4, 8];
        r.knl = Q(k);
    end
end

function test_knl_ax_grads(testCase)
    %
    % Test :func:`st_knl_ax_stress_gradient`.
    %

    Q = zeros(1, 2);

    K = @util_knl_grads;
    knl = cell(1, numel(Q));

    knl{1} = @(p, x, x0) K(p, x, x0, 1);
    knl{2} = @(p, x, x0) K(p, x, x0, 2) .* cos(p) + ...
                         K(p, x, x0, 3) .* sin(p);

    problem.name = 'knl_grads';
    problem.knl = knl;
    problem.rtol = 1.0e-7;
    problem.evaluate = @fn_evaluate;
    util_knl_comparison(testCase, problem);

    function [r] = fn_evaluate(x, x0)
        [F, E] = st_ellipke(x.x, x0.x);
        [I50, I51, I52, I53] = util_ellint_order5(x.x, x0.x, F, E);
        [I70, I71, I72, I73, I74] = util_ellint_order7(x.x, x0.x, F, E);
        Q = st_knl_ax_stress_gradient(Q, x.x, x0.x, x0.n, ...
            I50, I51, I52, I53, I70, I71, I72, I73, I74);

        r.knl = Q(:)';
    end
end

% }}}

% {{{ test_knl_ax_limits

function test_knl_ax_grads_axis_limit(testCase)
    %
    % Test axis limits for :func:`st_knl_ax_stress_gradient`.
    %

    problem.name = "grads";
    variables = {'Rx', 'Ry'};

    R = zeros(1, 2);

    problem.rtol = 1.0e-8;
    problem.npoints = 8 * 4096;

    problem.evaluate = @fn_evaluate;
    util_ellint_axis_limits(testCase, variables, problem);

    function [r] = fn_evaluate(x, x0, F, E)
        % normal assumming a circle
        n0 = x0 / norm(x0);
        % elliptic integrals
        [I50, I51, I52, I53] = util_ellint_order5(x, x0, F, E);
        [I70, I71, I72, I73, I74] = util_ellint_order7(x, x0, F, E);
        % kernel
        R = st_knl_ax_stress_gradient(R, x, x0, n0, ...
            I50, I51, I52, I53, I70, I71, I72, I73, I74);

        R = R * abs(x - x0)^2;
        r.Rx = R(1) * imag(x);
        r.Ry = R(2) * imag(x);
    end
end

% }}}

function setup(testCase)
    rng(42, 'philox');
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    rng('default')
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
