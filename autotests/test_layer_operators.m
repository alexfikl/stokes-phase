% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_layer_operators
    tests = functiontests({@setup, @teardown, ...
                           @test_compare_slp_density, ...
                           @test_compare_slp_normal, ...
                           @test_compare_slp_complete, ...
                           @test_compare_dlp_density, ...
                           @test_compare_dlp_velocity, ...
                           @test_compare_pressure, ...
                           @test_compare_normal_gradu, ...
                           @test_compare_normal_grads, ...
                           @test_compare_tangent_gradu, ...
                           @test_compare_tangent_stress, ...
                           @test_compare_electric_dlp, ...
                           @test_compare_slp_vs_slp_normal, ...
                           @test_operator_symmetry, ...
                           @test_operator_pressure_cusp, ...
                           @test_operator_stress_gradient_singularity});
end

% {{{ helpers

function [xi] = util_row_source(x, y, sng, i)
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);
    % number of basis functions
    nbasis = length(x.nodes.basis);

    % adjacent panels
    iprev = discretize(x.xi(i), x.panels, 'IncludedEdge', 'right');
    inext = discretize(x.xi(i), x.panels, 'IncludedEdge', 'left');
    % position of row i in element collocation points
    ielem = mod(i - 1, nbasis - 1) + 1;

    % get singular quadrature nodes
    if i == 1
        xi = y.quad.(sng).unit_xi{end - 1};
    elseif i == length(x.x)
        xi = y.quad.(sng).unit_xi{end};
    else
        xi = y.quad.(sng).unit_xi{ielem};
        a = x.panels(iprev); b = x.panels(inext + 1);
        xi = (b + a) / 2 + (b - a) / 2 * xi;
    end

    % get source point indices
    iprev = (iprev - 1) * nnodes;
    if iprev == 1
        iprev = 0;
    end
    inext = inext * nnodes + 1;
    if inext == length(x.panels) - 1
        inext = length(y.xi) + 1;
    end

    xi = [y.xi(1:iprev), ...
          xi, ...
          y.xi(inext:end)];
end

function util_plot_eigenspectrum(cache, A, index)
    to_filename = @(name) cache.filename(cache, ...
        sprintf('%02d_%s', index, name), 'png');

    % points
    npoints = size(A, 1);
    xi = linspace(0.0, 0.5, npoints);

    % eigenvalues
    e = eigs(A, npoints);
    er = real(e); ei = imag(e);

    % plot
    h = figure();
    imagesc(A);
    st_print(h, to_filename('matrix')); cla();

    plot(er, ei, 'o');
    st_print(h, to_filename('eigs')); cla();

    plot(log10(sort(abs(e), 'descend') + 1.0e-15), 'o');
    st_print(h, to_filename('eig_abs')); cla();

    N = null(A);
    if size(N, 2) ~= 0
        h = figure();
        for i = 1:size(N, 2)
            plot(xi, N(:, i));
        end

        title(sprintf('Nullspace: %d', size(N, 2)));
        st_print(h, to_filename('nullspace'));
        fprintf('[INFO] `%s` has a nullspace of size %d\n', ...
                cache.prefix, size(N, 2));
    else
        fprintf('[INFO] `%s` has no nullspace\n', cache.prefix);
    end

    close(h);
end

function test_compare_apply_vs_matrix(testCase, layer)
    cache = unittest_create_cache(sprintf('operator_compare_%s', layer.name));

    % number of runs
    nruns = 3;
    % number of panels
    npanels = 32;
    % number of quadrature nodes
    nnodes = 8;
    % number of basis functions
    nbasis = 4;
    % singular quadrature
    s_sng = struct('nnodes', 4 * nbasis, 'npoly', nbasis);
    % shape
    curve = @(xi) st_mesh_curve(xi, 'ellipse');

    % generate target and source points
    x = st_point_collocation(npanels, nbasis, curve);
    y = st_point_quadrature(npanels, nnodes, curve);

    s_quad = {'reg', 'log', 'cpv', 'hfp'};
    s_quad = st_layer_quadrature(x, y, s_quad, s_sng);
    y.quad = st_struct_merge(y.quad, s_quad);

    % compute geometry
    [x, y] = st_mesh_geometry(x, y);
    ntargets = length(x.x);

    % construct operator
    tic();
    L = layer.evaluate(x, y, []);
    t_end = toc();
    fprintf('[%d] time: %.5f\n', 0, t_end);

    nrows = size(L, 1) / ntargets;
    ncols = size(L, 2) / ntargets;
    if testCase.TestData.verbose
        for i = 1:nrows
            for j = 1:ncols
                dd = 10 * i + j;
                L0 = L((i - 1) * ntargets + 1:i * ntargets, ...
                       (j - 1) * ntargets + 1:j * ntargets);

                    util_plot_eigenspectrum(cache, L0, dd);
            end
        end
    end

    testCase.verifyEqual(mod(size(L, 1), ntargets), 0);
    testCase.verifyEqual(mod(size(L, 2), ntargets), 0);
    scalar_target = floor(size(L, 1) / ntargets) == 1;
    scalar_source = floor(size(L, 2) / ntargets) == 1;

    a = -1.0;
    b = 1.0;

    matches = 0;
    rtol = 1.0e-11;

    for i = 1:nruns
        % define density
        if scalar_source
            q = a + (b - a) * rand(size(x.xi));
            r = q';
        else
            q = a + (b - a) * rand(size(x.xi)) + ...
                1.0j * (a + (b - a) * rand(size(x.xi)));
            r = [real(q), imag(q)]';
        end

        % apply matrix-free layer potential
        tic();
        Lq0 = layer.evaluate(x, y, q);
        t_end = toc();
        fprintf('[%d] time: %.5f\n', i, t_end);

        % apply matrix operator
        Lq1 = (L * r)';
        if ~scalar_target
            Lq1 = Lq1(1:ntargets) + 1.0j * Lq1(ntargets + 1:end);
        end


        rerr = norm(Lq0 - Lq1, Inf) / norm(Lq0, Inf);
        if rerr > rtol
            matches = matches + 1;
            fprintf('verifyLessThan failed\n\tActual Value: %.15f\n\tMaximum Value: %.15g\n', ...
                rerr, rtol);
        end
    end
    testCase.verifyEqual(matches, 0);

    h = figure();
    h1 = plot(x.xi, real(Lq0));
    h2 = plot(x.xi, real(Lq1));
    legend([h1, h2], {'Apply', 'Matrix'});
    st_print(h, cache.filename(cache, 'x', 'png')); cla();

    if ~scalar_target
        h1 = plot(x.xi, imag(Lq0));
        h2 = plot(x.xi, imag(Lq1));
        legend([h1, h2], {'Apply', 'Matrix'});
        st_print(h, cache.filename(cache, 'y', 'png')); cla();
    end

    plot(x.xi, abs(Lq0 - Lq1));
    st_print(h, cache.filename(cache, 'error', 'png'));

    close(h);
end

% }}}

% {{{ compare layer potentials

function test_compare_slp_density(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'slp0', ...
               'evaluate', @(x, y, q) st_layer_slp(x, y, q, 'density')));
end

function test_compare_slp_normal(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'slp1', ...
               'evaluate', @(x, y, q) st_layer_slp(x, y, q, 'normal')));
end

function test_compare_slp_complete(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'slp2', ...
               'evaluate', @st_layer_slp_complete));
end

function test_compare_dlp_density(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'dlp0', ...
               'evaluate', @(x, y, q) st_layer_dlp(x, y, q, 'density')));
end

function test_compare_dlp_velocity(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'dlp0', ...
               'evaluate', @(x, y, q) st_layer_dlp(x, y, q, 'velocity')));
end

function test_compare_pressure(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'plp0', ...
               'evaluate', @st_layer_pressure));
end

function test_compare_normal_gradu(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'vlp0', ...
               'evaluate', @st_layer_normal_gradu));
end

function test_compare_tangent_gradu(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'tlp0', ...
               'evaluate', @st_layer_tangent_gradu));
end

function test_compare_tangent_stress(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'tlp1', ...
               'evaluate', @st_layer_tangent_stress));
end

function test_compare_normal_grads(testCase)
    testCase.assumeFalse('skipping');

    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'grads', ...
               'evaluate', @st_layer_normal_grads));
end

function test_compare_electric_dlp(testCase)
    test_compare_apply_vs_matrix(testCase, ...
        struct('name', 'electric', ...
               'evaluate', @st_layer_electric_dlp));
end

% }}}

% {{{ test_compare_slp_vs_slp_normal

function test_compare_slp_vs_slp_normal(testCase)
    % number of runs
    nruns = 3;
    % number of panels
    npanels = 64;
    % number of quadrature nodes
    nnodes = 4;
    % number of basis functions
    nbasis = 4;
    % shape
    shape = @(xi) st_mesh_curve(xi, 'flower');

    % generate target and source points
    x = st_point_collocation(npanels, nbasis, shape);
    y = st_point_quadrature(npanels, nnodes, shape);
    y.quad = st_struct_merge(y.quad, ...
        st_layer_quadrature(x, y, {'reg', 'log'}));
    % compute geometry
    [x, y] = st_mesh_geometry(x, y);

    % construct operator
    a = -1.0;
    b = 1.0;
    for i = 1:nruns
        % define density
        q = a + (b - a) * rand(size(x.xi));

        % compute full slp with normal density
        Lq0 = st_layer_slp(x, y, q .* x.n, 'density');
        % compute normal slp with scalar density
        Lq1 = st_layer_slp(x, y, q, 'normal');

        rtol = 1.0e-11 * norm(Lq0, Inf);
        testCase.verifyLessThan(norm(Lq0 - Lq1, Inf), rtol);
    end
end

% }}}

% {{{ test_operator_symmetry

function test_operator_symmetry(~)
    % number of quadrature nodes
    nnodes = 8;
    % number of basis functions
    nbasis = 4;
    % singular quadrature
    s_sng = struct('nnodes', 2 * nbasis, 'npoly', 1 * nbasis);
    % curve
    curve = @(xi) st_mesh_curve(xi, 'circle');

    % figures
    fig = gobjects(1, 4);
    fig_ax = gobjects(1, length(fig));
    for m = 1:length(fig)
        fig(m) = figure();
        fig_ax(m) = gca();
    end

    % number of panels
    npanels_arr = [16, 32, 64];

    for n = 1:length(npanels_arr)
        % number of panels
        npanels = npanels_arr(n);
        % geometry
        x = st_point_collocation(npanels, nbasis, curve);
        y = st_point_quadrature(npanels, nnodes, curve);
        y.quad = st_struct_merge(y.quad, ...
            st_layer_quadrature(x, y, {'reg', 'log', 'cpv'}, s_sng));

        [x, y] = st_mesh_geometry(x, y);

        % construct operator
        S = st_layer_slp(x, y);
        % get blocks
        ntargets = length(x.x);
        Sxx = S(1:ntargets, 1:ntargets);
        Sxy = S(1:ntargets, ntargets + 1:end);
        Syx = S(ntargets + 1:end, 1:ntargets);
        Syy = S(ntargets + 1:end, ntargets + 1:end);
        % sum rows
        s_sym = {sum(Sxx, 2)', sum(Sxy, 2)', sum(Syx, 2)', sum(Syy, 2)'};
        s_sgn = [-1, 1, 1, -1];

        fprintf('Error: %5d ', npanels);
        for m = 1:length(s_sym)
            dv = s_sym{m} + s_sgn(m) * fliplr(s_sym{m});

            fprintf('%.6e ', norm(dv));
            plot(fig_ax(m), x.xi, log10(abs(dv) + 1.0e-16));
        end
        fprintf('\n');
    end

    cache = unittest_create_cache('operator_symmetry');
    fields = {'Sxx', 'Sxy', 'Syx', 'Syy'};
    for m = 1:length(fields)
        xlabel(fig_ax(m), '$\xi$');
        ylabel(fig_ax(m), sprintf('$%s_{%s}$', fields{m}(1), fields{m}(2:end)));
        st_print(fig(m), cache.filename(cache, lower(fields{m}), 'png'));
    end
end

% }}}

% {{{ test_operator_pressure_cusp

function test_operator_pressure_cusp(testCase)
    % NOTE: this test case requires compiling the mex files
    r = exist('st_knl_ax_pressure_c');
    if r == 0
        assumeFail(testCase)
    end

    % number of panels
    npanels = 256;
    % number of quadrature nodes
    nnodes = 8;
    % number of basis functions
    nbasis = 6;
    % singular quadrature
    s_reg = struct('nnodes', 2 * nbasis, 'npoly', 1 * nbasis);
    % curve
    curve = @(xi) st_mesh_curve(xi, 'circle');

    % discretization
    x = st_point_collocation(npanels, nbasis, curve);
    y = st_point_quadrature(npanels, nnodes, curve);
    y.quad = st_struct_merge(y.quad, ...
        st_layer_quadrature(x, y, {'reg', 'log'}, s_reg));

    cache = unittest_create_cache('operator_pressure');

    fig = gobjects(1, 2);
    fax = gobjects(1, 2);
    for m = 1:numel(fig)
        fig(m) = figure();
        fax(m) = gca();
    end

    for n = 4:8
        % get a target point
        i = floor(length(x.x) / 3) + n;
        ielem = mod(i - 1, nbasis - 1) + 1;

        % geometry
        xi = util_row_source(x, y, 'log', i);
        x.n = x.x;

        ys.xi = xi;
        ys.x = curve(xi);
        ys.n = ys.x;

        % density
        qx = sin(exp(2j * pi * x.xi));
        qs = sin(exp(2j * pi * ys.xi));

        % evaluate kernel at given source points
        Px = zeros(size(xi));
        Py = zeros(size(xi));
        Rx = zeros(size(xi));
        Ry = zeros(size(xi));

        for j = 1:length(ys.xi)
            [P0, P1, R0, R1] = st_knl_ax_pressure_c(...
                real(ys.x(j)), imag(ys.x(j)), real(x.x(i)), imag(x.x(i)), ...
                real(ys.n(j)), imag(ys.n(j)), ...
                real(x.n(i)), imag(x.n(i)));

            Px(j) = P0 * real(qs(j));
            Rx(j) = R0 * real(qx(i));
            Py(j) = P1 * imag(qs(j));
            Ry(j) = R1 * imag(qx(i));
        end

        k = discretize(x.xi(i), xi);
        js = k - 2 * nbasis + 1;
        je = k + 2 * nbasis;

        Px = Px - Rx;
        Py = Py - Ry;

        % plot
        plot(fax(1), xi(js:k), Px(js:k), 'o-');
        plot(fax(1), xi(k + 1:je), Px(k + 1:je), 'o-');
        xline(fax(1), x.xi(i), 'k--', 'LineWidth', 3);
        title(fax(1), sprintf('$%d$', ielem));
        st_print(fig(1), ...
            cache.filename(cache, sprintf('x_%03d', ielem), 'png'));
        cla(fax(1));

        plot(fax(2), xi(js:k), Py(js:k), 'o-');
        plot(fax(2), xi(k + 1:je), Py(k + 1:je), 'o-');
        xline(fax(2), x.xi(i), 'k--', 'LineWidth', 3);
        title(fax(2), sprintf('$%d$', ielem));
        st_print(fig(2), ...
            cache.filename(cache, sprintf('y_%03d', ielem), 'png'));
        cla(fax(2));
    end

    for m = 1:numel(fig)
        close(fig(m));
    end
end

% }}}

% {{{ test_operator_stress_gradient_singularity

function test_operator_stress_gradient_singularity(~)
    cache = unittest_create_cache('operator_singularity');

    % number of panels
    npanels = 512;
    % number of quadrature nodes
    nnodes = 8;
    % number of basis functions
    nbasis = 4;
    % singular quadrature
    s_hfp = struct('nnodes', nbasis, 'npoly', nbasis);
    % curve
    curve = @(xi) st_mesh_curve(xi, 'circle');

    % discretization
    x = st_point_collocation(npanels, nbasis, curve);
    y = st_point_quadrature(npanels, nnodes, curve);
    [x, y] = st_mesh_geometry(x, y);
    y.quad = st_struct_merge(y.quad, ...
        st_layer_quadrature(x, y, {'reg', 'hfp'}, s_hfp));

    % figures
    fig = gobjects(1, 2);
    axs = gobjects(1, length(fig));
    for n = 1:length(fig)
        fig(n) = figure();
        axs(n) = gca();
    end

    for n = 4:8
        % get a target point
        i = floor(length(x.x) / 3) + n;
        ielem = mod(i - 1, nbasis - 1) + 1;

        % get source nodes for the current row
        xi = util_row_source(x, y, "hfp", i);
        % get source geometry for the current row
        ys = st_mesh_source_geometry(x, xi);
        % make sure normals are exact (assumes `circle`)
        ys.n = ys.x;
        x.n = x.x;
        % get source point
        x_i = x.x(i);
        xi_i = x.xi(i);
        nx_i = x.n(i);
        % x_i = ys.x(i);
        % xi_i = ys.xi(i);
        % nx_i = ys.n(i);

        % evaluate kernel at give source points
        Rx = zeros(size(xi));
        Ry = zeros(size(xi));
        R = zeros(1, 2);
        for j = 1:length(xi)
            [F, E] = st_ellipke(ys.x(j), x_i);
            [I50, I51, I52, I53] = st_ellint_order5(ys.x(j), x_i, F, E);
            [I70, I71, I72, I73, I74] = st_ellint_order7(ys.x(j), x_i, F, E);

            R = st_knl_ax_stress_gradient(R, ys.x(j), x_i, nx_i, ...
                I50, I51, I52, I53, I70, I71, I72, I73, I74);

            Rx(j) = R(1) * imag(ys.x(j)) * norm(ys.xi(j) - xi_i)^2;
            Ry(j) = R(2) * imag(ys.x(j)) * norm(ys.xi(j) - xi_i)^2;
        end
        % Rx(i) = (Rx(i - 1) + Rx(i + 1)) / 2.0;
        % Ry(i) = (Ry(i - 1) + Ry(i + 1)) / 2.0;

        [dxi, j] = min(abs(ys.xi - xi_i));
        fprintf('[%d] dx: %.5e j: %d\n', mod(i - 1, nbasis - 1) == 0, dxi, j);
        disp(Rx(j - 2:j + 2));
        disp(Ry(j - 2:j + 2));

        plot(axs(1), xi, Rx, '.');
        xline(axs(1), xi_i, 'k--', 'LineWidth', 3);
        st_print(fig(1), ...
            cache.filename(cache, sprintf('x_%02d_%02d', n, ielem), 'png'));
        cla(axs(1));

        plot(axs(2), xi, Ry, '.');
        xline(axs(2), xi_i, 'k--', 'LineWidth', 3);
        st_print(fig(2), ...
            cache.filename(cache, sprintf('y_%02d_%02d', n, ielem), 'png'));
        cla(axs(2));
    end

    for n = 1:length(fig)
        close(fig(n));
    end
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
