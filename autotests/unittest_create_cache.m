% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cache] = unittest_create_cache(prefix)
    % Create a cache for tests.
    %
    % Tests only output when the global ``verbosity`` is set to ``true``. This
    % creates a dummy cache when it's ``false`` so the tests still work, but not
    % files or folders are created.
    %
    % :arg prefix: cache prefix.

    global verbosity;
    if ~isempty(verbosity) && verbosity
        cache = st_cache_create([], datestr(now, 'yyyy_mm_dd'), prefix);
    else
        cache = struct();
        cache.filename = @(a, b, c) 'xxx_unused_xxx.png';
    end

end
