% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_benchmarks
  tests = functiontests({@setup, @teardown, ...
                         @test_benchmark_ellipke, ...
                         @test_benchmark_ax_elliptic, ...
                         @test_benchmark_filtering});
end

% {{{ helpers

function util_timeit_show(name, fn, nruns)
    [t_avg, t_std, t] = st_util_timeit(fn, nruns);
    fprintf('Timing[%s]:\n\t runs: %7d total: %13.6fs avg: %13.6es (+- %13.6es)\n', ...
        name, nruns, t, t_avg, t_std);
end

% }}}

% {{{ test_benchmark_ellipke

function test_benchmark_ellipke(~)
    %
    % Benchmark :func:`st_ellipke` vs MATLAB's :func:`ellipke`.
    %

    % number of runs
    nruns = 500000;

    util_timeit_show('ellipke', @fn_ellipke, nruns);
    util_timeit_show('matlab', @fn_ellipke_matlab, nruns);

    function fn_ellipke()
        x =  (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());
        x0 = (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());

        [F, E] = st_ellipke(x, x0); %#ok
    end

    function fn_ellipke_matlab()
        x =  (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());
        x0 = (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());

        K = 4.0 * imag(x) * imag(x0) / abs(x - conj(x0))^2;
        [F, E] = ellipke(K); %#ok
    end
end

% }}}

% {{{ test_benchmark_ax_elliptic

function test_benchmark_ax_elliptic(~)
    %
    % Benchmark a few axisymmetric kernels.
    %

    % number of runs
    nruns = 500000;

    % NOTE: doing one for warm-up
    util_timeit_show('warmup', @fn_stresslet_gn, nruns);

    util_timeit_show('stokeslet', @fn_stokeslet, nruns);
    util_timeit_show('stresslet', @fn_stresslet, nruns);
    util_timeit_show('dsdnlet', @fn_stresslet_gn, nruns);

    function fn_stokeslet()
        x =  (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());
        x0 = (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());

        [F, E] = st_ellipke(x, x0);
        I00 = st_ellint_order3(x, x0, F, E); %#ok
    end

    function fn_stresslet()
        x =  (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());
        x0 = (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());

        [F, E] = st_ellipke(x, x0);
        I00 = st_ellint_order5(x, x0, F, E); %#ok
    end

    function fn_stresslet_gn()
        x =  (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());
        x0 = (-1.0 + 2.0 * rand()) + 1.0j * (1.0 + rand());

        [F, E] = st_ellipke(x, x0);
        I00 = st_ellint_order7(x, x0, F, E); %#ok
    end
end

% }}}

% {{{ test_benchmark_filtering

function test_benchmark_filtering(~)
    %
    % Benchmark filtering methods.
    %

    cache = unittest_create_cache('benchmark_filter');

    % number of tests
    ntimes = 10000;
    % number of points
    npoints = 128;
    % geometry
    x = st_mesh_curve(npoints, 'ellipse');
    % add noise
    y = x + 0.05 * (randn(1, npoints) .* real(x) + ...
            1.0j * randn(1, npoints) .* imag(x));

    % fft smoothing parameter
    p_fft = 1.0e-2;
    p_lle = 5;
    p_lsq = 1.0;

    x_fft = st_ax_filter(y, 'fft', p_fft);
    x_lle = st_ax_filter(y, 'lele', p_lle); %#ok
    x_lsq = st_ax_filter(y, 'lsq', p_lsq);

    % timing
    util_timeit_show('fft', @() st_ax_filter(y, 'fft', p_fft), ntimes);
    util_timeit_show('lele', @() st_ax_filter(y, 'lele', p_lle), ntimes);
    util_timeit_show('lsq', @() st_ax_filter(y, 'lsq', p_lle), ntimes);

    % plot
    x = st_array_ax(x);
    y = st_array_ax(y);

    h = figure();
    plot(real(x), imag(x));
    h1 = plot(real(y), imag(y), 'k--');
    h2 = plot(real(x_fft), imag(x_fft), 'o--');
    h3 = plot(real(x_lsq), imag(x_lsq), 'o--');
    axis('equal')
    legend([h1, h2, h3], {'Orig', 'FFT', 'LSQ'});
    st_print(h, cache.filename(cache, 'geometry', 'png'));
end

% }}}

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
