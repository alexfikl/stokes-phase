% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_template
  tests = functiontests({@setup, @teardown, ...
                         @test_some_function_name});
end

function test_some_function_name(testCase)
    testCase.verifyEqual(1, 1);
end

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
