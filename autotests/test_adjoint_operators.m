% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_adjoint_operators()
  tests = functiontests({@setup, @teardown, ...
                         @test_mass_operator, ...
                         @test_mass_operator_eigenspectrum, ...
                         @test_normal_operator, ...
                         @test_curvature_operator, ...
                         @test_vector_normal_operator, ...
                         @test_vector_curvature_operator, ...
                         @test_adjoint_gradient_normal, ...
                         @test_adjoint_gradient_curvature, ...
                         @test_adjoint_gradient_cost_geometry, ...
                         @test_adjoint_gradient_cost_centroid, ...
                         @test_adjoint_gradient_cost_surface_area});
end

% {{{ mms

function [ustar, vstar] = util_mms_functions(xi)
    % Constructs an adjoint velocity field for testing the adjoint operators.
    %
    % :arg xi: symbolic variable.

    if ~isfield('xi', 'var')
        xi = sym('xi', 'real');
    end
    theta = 2.0 * pi * xi;

    % NOTE: need to satisfy axisymmetric assumptions and boundary conditions.
    %   ustar(x, y) = ustar(x, -y)
    %   dustar(x, 0) = 0
    ustar = cos(pi * sin(theta)).^2;

    %   vstar(x, y) = -vstar(x, -y)
    %   vstar(x, 0) = 0
    vstar = sin(3.0 * pi * sin(theta))^3;

    if nargout == 1
        ustar = struct('ustar', ustar, 'vstar', vstar, 'xi', xi);
    end
end

function mms = util_mms_adjoint_operators(a, b, name)
    % Constructs adjoint normal and curvature operators on an ellipse.
    %
    % :arg a, b: ellipse parameters.
    % :arg name: operator name, e.g. ``'scalar_normal'``.

    % define geometry
    g = st_sym_geometry('ellipse', struct('a', a, 'b', b), true);
    xi = g.xi;
    % define functions
    [ustar, vstar] = util_mms_functions(xi);

    % define coefficients
    d_ds = @(z) diff(z, xi) / g.J;
    mms.g_i = g.tx + 1j * g.ty;
    mms.dg_i = d_ds(mms.g_i);
    mms.g_o = g.nx + 1j * g.ny;
    mms.dg_o = d_ds(mms.g_o);
    mms.xi = xi;

    % adjoint operators
    s = split(name, '_');
    optype = s{1};
    name = s{2};

    switch name
    case 'normal'
        nstar = @(z) 1 / g.y * d_ds(g.y * z);

        switch optype
        case 'scalar'
            mms.nstar = nstar(real(mms.g_i) * ustar) * real(mms.g_o);
            mms.ustar = ustar;
        case 'vector'
            u_dot_t = ustar * real(mms.g_i) + vstar * imag(mms.g_i);
            mms.nstar = nstar(u_dot_t) * mms.g_o;
            mms.ustar = ustar + 1j * vstar;
        otherwise
            error('unknown operator type: `%s`', optype);
        end

        % NOTE: taking axis limits symbolically because otherwise they would
        % not be computed accurately due to the 1/y term which goes to infinity
        mms.nstar_axis = double([limit(mms.nstar, 0), limit(mms.nstar, 1/2)]);
    case 'curvature'
        kxstar = @(z) -g.kappa_x^2 * z - 1/g.y * d_ds(d_ds(g.y * z));
        kpstar = @(z) -g.kappa_p^2 * z + 1/g.y * d_ds(g.nx * z);

        switch optype
        case 'scalar'
            mms.kxstar = kxstar(imag(mms.g_o) * vstar) * imag(mms.g_o);
            mms.kpstar = kpstar(imag(mms.g_o) * vstar) * imag(mms.g_o);
            mms.ustar = vstar;
        case 'vector'
            u_dot_n = ustar * real(mms.g_o) + vstar * imag(mms.g_o);
            mms.kxstar = kxstar(u_dot_n) * mms.g_o;
            mms.kpstar = kpstar(u_dot_n) * mms.g_o;
            mms.ustar = ustar + 1j * vstar;
        otherwise
            error('unknown operator type: `%s`', optype);
        end

        mms.kxstar_axis = double([limit(mms.kxstar, 0), limit(mms.kxstar, 1/2)]);
        mms.kpstar_axis = double([limit(mms.kpstar, 0), limit(mms.kpstar, 1/2)]);
    otherwise
        error('unknown operators: %s', name);
    end
end

function [mms] = util_mms_adjoint_gradient(a, b, name)
    % Construct analytic shape derivatives of cost functionals.
    %
    % :arg a, b: ellipse parameters.
    % :arg name: cost functional name: ``'normal'`` and ``'curvature'``.

    % define geometry
    g = st_sym_geometry('ellipse', struct('a', a, 'b', b), true);
    mms.xi = g.xi;
    % define functions
    [ustar, vstar] = util_mms_functions(g.xi);

    d_ds = @(z) diff(z, g.xi)/g.J;
    switch name
    case 'normal'
        u_dot_t = ustar * g.tx + vstar * g.ty;
        u_dot_n = ustar * g.nx + vstar * g.ny;
        nstar_u = 1/g.y * d_ds(g.y * u_dot_t) + g.kappa * u_dot_n;

        % NOTE: taking axis limits symbolically because otherwise they would
        % not be computed accurately due to the 1/y term which goes to infinity
        mms.nstar_axis = double([limit(nstar_u, 0), limit(nstar_u, 1/2)]);
        mms.nstar = nstar_u;
        mms.u = ustar + 1j * vstar;
    case 'curvature'
        kxstar_u = -g.kappa_x^2 * ustar - 1/g.y * d_ds(d_ds(g.y * ustar));
        kpstar_u = -g.kappa_p^2 * ustar + 1/g.y * d_ds(g.nx * ustar);
        kstar_u = kxstar_u + kpstar_u + g.kappa^2 * ustar;

        % NOTE: same issue with axis limits
        mms.kstar_axis = double([limit(kstar_u, 0), limit(kstar_u, 1/2)]);
        mms.kstar = kstar_u;
        mms.u = ustar;
    otherwise
        error('unknown gradient type: %s', name);
    end
end

% }}}

% {{{ convergence

function [cache] = util_operator_convergence(testCase, variables, problem)
    % Wraps :func:`st_unittest_convergence`.
    %
    % :arg testCase: used to check convergence.

    % {{{ default cost finite difference gradient options

    if isfield(problem, 'cost')
        d.cost_type = 'custom';
        d.eps = 1.0e-8;
        d.sigma = 1.0e-4;
        d.bump_type = 'xi';
        d.variables = {};

        problem.cost = st_struct_merge(d, problem.cost, true);
    end

    % }}}

    % {{{ run

    [eoc, convergence] = st_unittest_convergence(variables, problem);
    orders = convergence.expected_order;
    cache = convergence.cache;

    for m = 1:length(variables)
        name = variables{m};

        v_or = eoc.global_orders.(name);
        v_er = eoc.errors.(name)(end);

        if abs(v_er) > 1.0e-14
            testCase.verifyGreaterThan(v_or, orders(m));
        end
    end

    % }}}
end

% }}}

% {{{ test_mass_operator

function test_mass_operator(testCase)
    %
    % Test that we can invert a mass matrix properly.
    %

    % {{{

    % geometry
    geometry.nbasis = 6;
    geometry.nnodes = 6;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', 1.0, 'b', 2.0);
    problem.geometry = geometry;

    % solution
    mms = util_mms_functions();
    problem.user.mms = st_sym_handle(mms, mms.xi);

    % output
    problem.prefix = 'operator_mass';
    problem.resolutions = 5 * (1:8);
    problem.expected_order = [geometry.nbasis, geometry.nbasis];

    problem.evaluate = @fn_evaluate;

    % }}}

    util_operator_convergence(testCase, {'neumann', 'dirichlet'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        ustar = p.user.mms.ustar(x);
        vstar = p.user.mms.vstar(x);

        switch method
        case 'ex'
            % nothing to do
        case 'ap'
            M = st_ops_mass(x, y);
            ustar = st_ops_solve(x, M, M * ustar', 'neumann');
            vstar = st_ops_solve(x, M, M * vstar', 'dirichlet');
        end

        r.neumann = ustar;
        r.dirichlet = vstar;
    end
end

function test_mass_operator_eigenspectrum(testCase)
    %
    % Plot the eigenvalues for the Dirichlet and Neumann modified mass matrices.
    %

    % {{{

    % geometry
    geometry.nbasis = 4;
    geometry.nnodes = 4;
    % geometry.curve = @(xi) st_mesh_curve(xi, 'ufo');
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', 1.0, 'b', 2.0);
    problem.geometry = geometry;

    % output
    problem.prefix = 'operator_mass_eigen';
    problem.labels = {'\kappa_d', '\kappa_n'};
    problem.enable_plotting = true;

    problem.resolutions = 10 * (1:12);
    problem.expected_order = [-42, -42];
    problem.evaluate = @fn_evaluate;

    % }}}

    fig = gobjects(1, 4);
    fax = gobjects(1, length(fig));
    for m = 1:length(fig)
        fig(m) = figure();
        fax(m) = gca();
    end

    problem.evaluate = @fn_evaluate;
    cache = util_operator_convergence(testCase, {'cond_d', 'cond_n'}, problem);

    for m = 1:2
        ylim(fax(m), [-1, 1]);
        xlabel(fax(m), '$\Re$');
        ylabel(fax(m), '$\Im$');
    end

    st_print(fig(1), cache.filename(cache, 'neumann', 'png'));
    st_print(fig(2), cache.filename(cache, 'dirichlet', 'png'));
    st_print(fig(3), cache.filename(cache, 'neumann_abs', 'png'));
    st_print(fig(4), cache.filename(cache, 'dirichlet_abs', 'png'));

    function [r] = fn_evaluate(~, x, y, method)
        switch method
        case 'ex'
            neumann = 0.0;
            dirichlet = 0.0;
        case 'ap'
            M = st_ops_mass(x, y);
            M_n = st_ops_boundary(x, M, x.xi, 'neumann');
            neumann = eig(M_n);

            M_d = st_ops_boundary(x, M, x.xi, 'dirichlet');
            dirichlet = eig(M_d);

            plot(fax(1), real(neumann), imag(neumann), 'o');
            plot(fax(2), real(dirichlet), imag(dirichlet), 'o');

            dv = log10(sort(abs(neumann), 'descend') + 1.0e-16);
            plot(fax(3), dv, ':');
            dv = log10(sort(abs(dirichlet), 'descend') + 1.0e-16);
            plot(fax(4), dv, ':');

            neumann = cond(M_n);
            dirichlet = cond(M_d);
        end

        r.cond_n = neumann * ones(size(y.x));
        r.cond_d = dirichlet * ones(size(y.x));
    end
end

% }}}

% {{{ scalar adjoint operators

function test_normal_operator(testCase)
    %
    % Test convergence of :func:`st_ops_adjoint_normal`.
    %

    % {{{

    % geometry
    a = 1.0;
    b = 2.0;

    geometry.nbasis = 4;
    geometry.nnodes = 4;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', a, 'b', b);
    problem.geometry = geometry;

    % solution
    mms = util_mms_adjoint_operators(a, b, 'scalar_normal');
    problem.user.mms = st_sym_handle(mms, mms.xi, true);

    % output
    problem.prefix = 'operator_scalar_normal';
    problem.labels = {'n^*'};

    problem.resolutions = 2.^(2:10);
    problem.expected_order = geometry.nbasis - 1.25;
    % problem.norm = @(x, y, f, g) st_ax_error(x, y, f, g, 'inf');

    problem.enable_caching = false;
    problem.enable_latex_eoc = true;
    problem.evaluate = @fn_evaluate;

    % }}}

    util_operator_convergence(testCase, {'nstar'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            nstar = mms.nstar(x);
            nstar([1, end]) = mms.nstar_axis;
        case 'ap'
            % evaluate coefficients
            ustar = mms.ustar(x);
            sigma = struct('g_out', real(mms.g_o(y)), ...
                           'g_in', real(mms.g_i(y)), ...
                           'dg_out', real(mms.dg_o(y)));

            % construct matrices
            M = st_ops_mass(x, y);
            N = st_ops_adjoint_normal(x, y, sigma);

            nstar = st_ops_solve(x, M, N * ustar', 'neumann');
        end

        r.nstar = nstar;
    end
end

function test_curvature_operator(testCase)
    %
    % Test convergence of :func:`st_ops_adjoint_curvature`.
    %

    % {{{

    % geometry
    a = 1.0;
    b = 2.0;

    geometry.nbasis = 4;
    geometry.nnodes = 4;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', a, 'b', b);
    problem.geometry = geometry;

    % solution
    mms = util_mms_adjoint_operators(a, b, 'scalar_curvature');
    problem.user.mms = st_sym_handle(mms, mms.xi, true);

    % output
    problem.prefix = 'operator_scalar_curvature';
    problem.labels = {'\kappa_x^*', '\kappa_\phi^*'};

    problem.resolutions = 2.^(3:10);
    problem.expected_order = geometry.nbasis - [2.25, 1.25];
    % problem.norm = @(x, y, f, g) st_ax_error(x, y, f, g, 'inf');

    problem.enable_caching = false;
    problem.enable_latex_eoc = true;
    problem.evaluate = @fn_evaluate;

    % }}}

    util_operator_convergence(testCase, {'kxstar', 'kpstar'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            kxstar = mms.kxstar(x);
            kxstar([1, end]) = mms.kxstar_axis;
            kpstar = mms.kpstar(x);
            kpstar([1, end]) = mms.kpstar_axis;
        case 'ap'
            % coefficients
            ustar = mms.ustar(x);
            sigma = struct('g_out',     imag(mms.g_o(x)), ...
                           'g_in',      imag(mms.g_o(x)), ...
                           'dg_out',    imag(mms.dg_o(x)), ...
                           'dg_in',     imag(mms.dg_o(x)));

            % operators
            M = st_ops_mass(x, y);
            [KX, KP] = st_ops_adjoint_curvature(x, y, sigma);

            kxstar = st_ops_solve(x, M, KX * ustar', 'neumann');
            kpstar = st_ops_solve(x, M, KP * ustar', 'neumann');
        end

        r.kxstar = kxstar;
        r.kpstar = kpstar;
    end
end

% }}}

% {{{ vector adjoint operators

function test_vector_normal_operator(testCase)
    %
    % Test convergence of :func:`st_ops_adjoint_vector_normal`.
    %

    % {{{

    % geometry
    a = 1.0;
    b = 2.0;

    geometry.nbasis = 6;
    geometry.nnodes = 8;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', a, 'b', b);
    problem.geometry = geometry;

    % solution
    mms = util_mms_adjoint_operators(a, b, 'vector_normal');
    problem.user.mms = st_sym_handle(mms, mms.xi, true);

    % output
    problem.prefix = 'operator_vector_normal';
    problem.labels = {'N_x^*', 'N_\rho^*'};

    problem.resolutions = 2.^(3:8);
    problem.expected_order = geometry.nbasis - 1.25;
    % problem.norm = @(x, y, f, g) st_ax_error(x, y, f, g, 'inf');

    problem.evaluate = @fn_evaluate;

    % }}}

    util_operator_convergence(testCase, {'nxstar', 'nystar'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            nstar = mms.nstar(x);
            nstar([1, end]) = mms.nstar_axis;
        case 'ap'
            % coefficients
            ustar = mms.ustar(x);
            sigma = mms.g_i(x);

            % operators
            M = st_ops_mass(x, y);
            N = st_ops_adjoint_vector_normal(x, y, sigma);

            % solve
            nstar = st_ops_vector_solve(x, M, N, ustar, ...
                {'neumann', 'dirichlet'});
        end

        r.nxstar = real(nstar);
        r.nystar = imag(nstar);
    end
end

function test_vector_curvature_operator(testCase)
    %
    % Test convergence of :func:`st_ops_adjoint_vector_curvature`.
    %

    % {{{

    % geometry
    a = 1.0;
    b = 2.0;

    geometry.nbasis = 6;
    geometry.nnodes = 8;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', a, 'b', b);
    problem.geometry = geometry;

    % solution
    mms = util_mms_adjoint_operators(a, b, 'vector_curvature');
    problem.user.mms = st_sym_handle(mms, mms.xi, true);

    % output
    problem.prefix = 'operator_vector_curvature';
    problem.labels = {'K_x^*', 'K_\rho^*', 'K_x^*', 'K_\rho^*'};

    problem.resolutions = 2.^(3:8);
    problem.expected_order = geometry.nbasis - [2.25, 2.25, 1.25, 1.25];
    % problem.norm = @(x, y, f, g) st_ax_error(x, y, f, g, 'inf');

    problem.evaluate = @fn_evaluate;

    % }}}

    util_operator_convergence(testCase, ...
        {'kxstar_x', 'kxstar_y', 'kpstar_x', 'kpstar_y'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            kxstar = mms.kxstar(x);
            kxstar([1, end]) = mms.kxstar_axis;
            kpstar = mms.kpstar(x);
            kpstar([1, end]) = mms.kpstar_axis;
        case 'ap'
            % coefficients
            ustar = mms.ustar(x);
            sigma = mms.g_o(x);

            % operators
            M = st_ops_mass(x, y);
            [KX, KP] = st_ops_adjoint_vector_curvature(x, y, sigma);

            % solve
            kxstar = st_ops_vector_solve(x, M, KX, ustar, ...
                {'neumann', 'dirichlet'});
            kpstar = st_ops_vector_solve(x, M, KP, ustar, ...
                {'neumann', 'dirichlet'});
        end

        r.kxstar_x = real(kxstar);
        r.kxstar_y = imag(kxstar);
        r.kpstar_x = real(kpstar);
        r.kpstar_y = imag(kpstar);
    end
end

% }}}

% {{{ analytic finite difference comparison

function test_adjoint_gradient_analytic(testCase, problem, name)
    % {{{

    % geometry
    ellipse_a = 2;
    ellipse_b = 1;

    geometry.nbasis = 4;
    geometry.nnodes = 4;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', ...
        'a', ellipse_a, 'b', ellipse_b);
    problem.geometry = geometry;

    % cost
    mms = util_mms_adjoint_gradient(ellipse_a, ellipse_b, name);

    cost.fn.evaluate = problem.cost_evaluate;
    cost.user.mms = st_sym_handle(mms, mms.xi, true);
    problem.cost = cost;

    % output
    problem.prefix = sprintf('adjoint_gradient_%s', name);
    problem.labels = {'\nabla_{AD} \mathcal{J}', '\nabla_{FD} \mathcal{J}'};
    problem.expected_order = geometry.nbasis - problem.expected_order_offset;

    % }}}

    util_operator_convergence(testCase, {'grad_ad', 'grad_fd'}, problem);
end

function test_adjoint_gradient_normal(testCase)
    %
    % Test gradient of :math:`\mathbf{u} \cdot \mathbf{n}` integral.
    %

    problem.evaluate = @fn_evaluate;
    problem.cost_evaluate = @fn_cost_evaluate;
    problem.resolutions = 10 * (2:10);
    problem.expected_order_offset = [1.5, 0.5];

    test_adjoint_gradient_analytic(testCase, problem, 'normal');

    function [J] = fn_cost_evaluate(cost, ~, ~, y, ~)
        u = cost.user.mms.u(y);
        J = st_ax_integral(y, st_dot(u, y.n));
    end

    function [r] = fn_evaluate(p, x, y, method)
        cost = p.cost;
        mms = cost.user.mms;

        switch method
        case 'ex'
            grad_ad = mms.nstar(x);
            grad_ad([1, end]) = mms.nstar_axis;
            grad_fd = cost.bump(cost, x, y, grad_ad);
        case 'ap'
            % get test function
            u = mms.u(x);
            % operators
            M = st_ops_mass(x, y);
            N = st_ops_adjoint_normal(x, y);

            % get gradient
            grad = N * st_dot(u, x.t)' + ...
                   M * (x.kappa .* st_dot(u, x.n))';

            grad_ad = st_ops_solve(x, M, grad, 'neumann');
            grad_fd = st_shape_finite_gradient(cost, x, y, struct());
        end

        r.grad_ad = grad_ad;
        r.grad_fd = grad_fd;
    end
end

function test_adjoint_gradient_curvature(testCase)
    %
    % Test cost functional with :math:`u \kappa_i`
    %

    problem.evaluate = @fn_evaluate;
    problem.cost_evaluate = @fn_cost_evaluate;
    problem.resolutions = 10 * (1:8);
    problem.expected_order_offset = [2.5, 0.5];

    test_adjoint_gradient_analytic(testCase, problem, 'curvature');

    function [J] = fn_cost_evaluate(cost, ~, ~, y, ~)
        u = cost.user.mms.u(y);
        J = st_ax_integral(y, u .* y.kappa);
    end

    function [r] = fn_evaluate(p, x, y, method)
        cost = p.cost;
        mms = cost.user.mms;

        switch method
        case 'ex'
            grad_ad = mms.kstar(x);
            grad_ad([1, end]) = mms.kstar_axis;
            grad_fd = cost.bump(cost, x, y, grad_ad);
        case 'ap'
            % get test function
            u = mms.u(x);
            % operators
            M = st_ops_mass(x, y);
            [KX, KP] = st_ops_adjoint_curvature(x, y);

            % get gradient
            kappa = (KX + KP) * u' + M * (x.kappa.^2 .* u)';

            grad_ad = st_ops_solve(x, M, kappa, 'neumann');
            grad_fd = st_shape_finite_gradient(cost, x, y, struct());
        end

        r.grad_ad = grad_ad;
        r.grad_fd = grad_fd;
    end
end

% }}}

% {{{ finite difference comparison

function test_adjoint_gradient_cost(testCase, name)
    % {{{

    % geometry
    geometry.nbasis = 4;
    geometry.nnodes = 4;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ufo');
    problem.geometry = geometry;

    % cost
    cost.cost_type = name;
    cost.optype = 'vector';
    cost.desired_curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', 5, 'b', 2);

    cost.param.tmax = 0.0;
    cost.param.gamma = 1.0;     % see st_cost_centroid
    cost.user.x0 = 3.0;         % see st_cost_centroid
    cost.user.alpha = -1.0;     % see st_cost_centroid
    problem.cost = cost;

    % output
    problem.prefix = sprintf('cost_gradient_%s', name);
    problem.labels = {'\nabla \mathcal{J}'};
    problem.resolutions = 10 * (1:7);
    problem.expected_order = geometry.nbasis - 1.25;

    problem.evaluate = @fn_evaluate;
    % }}}

    util_operator_convergence(testCase, {'grad'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        cost = p.cost;

        cost.x.x = cost.desired_curve(x.xi);
        cost.y.x = cost.desired_curve(y.xi);

        switch method
        case 'ex'
            grad = st_shape_finite_gradient(cost, x, y, struct());
        case 'ap'
            grad = cost.fn.gradient_x(cost, 0.0, x, y, struct());
            % grad = st_dot(grad, x.n);
            grad = cost.bump(cost, x, y, st_dot(grad, x.n));
        end

        r.grad = grad;
    end
end

function test_adjoint_gradient_cost_geometry(testCase)
    %
    % Compare adjoint vs finite difference gradient for :func:`st_cost_geometry`.
    %

    test_adjoint_gradient_cost(testCase, 'geometry');
end

function test_adjoint_gradient_cost_centroid(testCase)
    %
    % Compare adjoint vs finite difference gradient for :func:`st_cost_centroid`.
    %

    test_adjoint_gradient_cost(testCase, 'centroid');
end

function test_adjoint_gradient_cost_surface_area(testCase)
    %
    % Compare adjoint vs finite difference gradient for :func:`st_cost_surface_area`.
    %

    test_adjoint_gradient_cost(testCase, 'surface_area');
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
