% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_layer_identities
    tests = functiontests({@setup, @teardown, ...
                           @test_density_slp_identity, ...
                           @test_density_dlp_identity});
end

% {{{ helpers

function util_layer_convergence(testCase, variables, problem)
    % Wrapper around :func:`st_unittest_convergence`.
    %
    % :arg testCase: a :class:`TestCase` instance.

    [eoc, convergence] = st_unittest_convergence(variables, problem);

    % verify convergence
    orders = convergence.expected_order;
    for m = 1:length(variables)
        name = variables{m};

        v_or = eoc.global_orders.(name);
        testCase.verifyGreaterThan(v_or, orders(m));
    end
end

% }}}

% {{{ test_density_slp_identity

function test_density_slp_identity(testCase)
    %
    % Tests that S[n] \equiv 0, i.e. the normal is in the nullspace of the
    % single-layer potential operator
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ufo');

    % singular quadrature
    s_log = struct('nnodes', 4 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis, ...
                   'method', 'alpert');
    geometry.s_quad.log = s_log;
    problem.geometry = geometry;

    % output
    problem.prefix = 'identity_slp';
    problem.labels = {'E', 'E'};
    problem.resolutions = 2.^(3:9);
    problem.expected_order = geometry.nbasis - 0.5;

    problem.enable_geometry_plot = true;
    problem.enable_caching = false;
    problem.enable_latex_eoc = true;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'sx', 'sy'}, problem);

    function [r] = fn_evaluate(~, x, y, method)
        switch method
        case 'ex'
            S = zeros(size(x.x));
        case 'ap'
            S = st_layer_slp(x, y, x.n);
        end

        r.sx = real(S);
        r.sy = imag(S);
    end
end

% }}}

% {{{ test_density_dlp_identity

function test_density_dlp_identity(testCase)
    %
    % Tests on surface double-layer identity
    % Equation 2.3.18 in [Pozrikidis1992]_
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.s_quad.reg = struct('nnodes', 2 * geometry.nbasis);
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', struct('a', 1.0, 'b', 1.0));
    problem.geometry = geometry;

    % output
    problem.prefix = 'identity_dlp';
    problem.labels = {'E[\mathcal{D}_x]', 'E[\mathcal{D}_\rho]'};
    problem.resolutions = 2.^(2:6);
    problem.expected_order = geometry.nbasis - [1.25, 2.25];

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'dx', 'dy'}, problem);

    function [r] = fn_evaluate(~, x, y, method)
        switch method
        case 'ex'
            D = 4.0 * pi * ones(size(x.x));
        case 'ap'
            q = ones(1, length(x.x));
            D = st_layer_dlp(x, y, q);
        end

        r.dx = real(D);
        r.dy = imag(D);
    end
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:foldmethod=marker:
