% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_point
  tests = functiontests({@setup, @teardown, ...
                         @test_plot_collocation_geometry, ...
                         @test_interpolation, ...
                         @test_derivative, ...
                         @test_derivative_fft_precision, ...
                         @test_mesh_volume, ...
                         @test_mesh_target_associate});
end

% {{{ helpers

function util_point_convergence(testCase, variables, problem)
    [eoc, convergence] = st_unittest_convergence(variables, problem);

    orders = convergence.expected_order;
    for m = 1:length(variables)
        v_or = eoc.global_orders.(variables{m});
        testCase.verifyGreaterThan(v_or, orders(m));
    end
end

% }}}

% {{{ test_collocation_geometry

function test_plot_collocation_geometry(~)
    cache = unittest_create_cache('geometry_collocation');

    % parameters
    npanels = 64;
    nnodes = 6;
    shape = @(xi) st_mesh_curve(xi, 'ufo');

    % generate quadrature and collocation points
    x = st_point_collocation(npanels, nnodes, shape);
    y = st_point_quadrature(npanels, nnodes, shape);
    [x, y] = st_mesh_geometry(x, y);

    fprintf('ncoll: %d %d\n', size(x.xi));
    fprintf('nquad: %d %d\n', size(y.xi));

    % {{{ plot

    h = figure();
    plot(x.xi, x.kappa);
    plot(y.xi, y.kappa, '--');
    st_print(h, cache.filename(cache, 'curvature', 'png')); cla();

    plot(x.xi, real(x.n));
    plot(x.xi, imag(x.n));
    plot(y.xi, real(y.n), '--');
    plot(y.xi, imag(y.n), '--');
    st_print(h, cache.filename(cache, 'normal', 'png')); cla();

    zx = st_array_ax(x.x, true);
    zv = st_array_ax(x.vertices, true);
    zy = st_array_ax(y.x, true);

    plot(real(zx), imag(zx), 'x-');
    plot(real(zv), imag(zv), 'ko');
    plot(real(zy), imag(zy), 'd');
    axis('equal');
    st_print(h, cache.filename(cache, 'geometry', 'png')); cla();

    axis('normal');
    xi = linspace(-1.0, 1.0, 128);
    for i = 1:nnodes
        basis = x.nodes.basis{i};
        h1 = plot(xi, basis(xi)); color = get(h1, 'Color');
        plot(x.nodes.unit_xi, basis(x.nodes.unit_xi), ...
             'o', 'Color', color);
    end
    st_print(h, cache.filename(cache, 'basis', 'png')); cla();

    for i = 1:nnodes
        h1 = plot(xi, x.nodes.dbasis{i}(xi)); color = get(h1, 'Color');
        plot(x.nodes.unit_xi, basis(x.nodes.unit_xi), ...
             'o', 'Color', color);
    end
    st_print(h, cache.filename(cache, 'derivative', 'png')); cla();

    close(h);

    % }}}
end

% }}}

% {{{ test_interpolation

function test_interpolation(testCase)
    %
    % Test :func:`st_interpolate` on simple functions.
    %

    % {{{

    % geometry
    geometry.nnodes = 6;
    geometry.nbasis = 6;
    geometry.curve = @(xi) st_mesh_curve(xi, 'circle');
    problem.geometry = geometry;

    % function to interpolate
    problem.user.fn = @(z) exp(cos(4.0 * pi * z.xi) + 3j * sin(5.0 * pi * z.xi));

    % output
    problem.prefix = 'point_interpolate';
    problem.labels = {'\Re\{f\}', '\Im\{f\}'};

    problem.expected_order = geometry.nbasis - 0.5;
    problem.resolutions = 2.^(2:10);

    problem.evaluate = @fn_evaluate;

    % }}}

    util_point_convergence(testCase, {'fx', 'fy'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        switch method
        case 'ex'
            fn = p.user.fn(y);
        case 'ap'
            fn = p.user.fn(x);
            fn = st_interpolate(x, fn, y.xi);
        end

        r.fx = real(fn);
        r.fy = imag(fn);
    end
end

% }}}

% {{{ test_derivative

function test_derivative(testCase)
    %
    % Test :func:`st_derivative` on simple functions
    %

    % {{{

    % geometry
    geometry.nnodes = 6;
    geometry.nbasis = 6;
    geometry.curve = @(xi) st_mesh_curve(xi, 'circle');
    problem.geometry = geometry;

    % function to differentiate
    gn = @(xi) cos(2.0 * pi * xi) + 1j * sin(2.0 * pi * xi);
    dg = @(xi) 2.0 * pi * (-sin(2.0 * pi * xi) + 1.0j * cos(2.0 * pi * xi));

    problem.user.fn = @(z) exp(gn(z.xi));
    problem.user.df = @(z) dg(z.xi) .* exp(gn(z.xi));

    % output
    problem.prefix = 'point_derivative';
    problem.labels = {'\Re\{\dot{f}\}', '\Im\{\dot{f}\}'};

    problem.expected_order = geometry.nbasis - 1.75;
    problem.resolutions = 2.^(3:9);

    problem.evaluate = @fn_evaluate;

    % }}}

    util_point_convergence(testCase, {'dfx', 'dfy'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        switch method
        case 'ex'
            df = p.user.df(y);
        case 'ap'
            fn = p.user.fn(x);
            df = st_derivative(x, fn, y.xi, 'lagrange_xi');
        end

        r.dfx = real(df);
        r.dfy = imag(df);
    end
end

function test_derivative_fft_precision(testCase)
    %
    % Test if :func:`st_derivative` based on FFT can reach machine precision.
    %

    % {{{

    % geometry
    geometry.nnodes = -1;
    geometry.nbasis = -1;
    problem.geometry = geometry;

    % function to differentiate
    a = 10.0;
    T = 20.0;
    problem.user.a = a;
    problem.user.T = T;
    problem.user.f = @(xi) exp(-a^2 * xi.^2);
    problem.user.df = @(xi) -2 * a^2 * xi .* exp(-a^2 * xi.^2);
    problem.user.ddf = @(xi) 2 * a^2 * exp(-a^2 * xi.^2) .* (2 * a^2 * xi.^2 - 1);
    problem.user.fhat = @(k) exp(-(k / (2 * a)).^2) / sqrt(2 * a^2);

    % output
    problem.prefix = 'point_derivative_fft';
    problem.labels = {'\hat{f}', '\widehat{\ddot{f}}', '\ddot{f}'};

    problem.expected_order = geometry.nbasis - 1.75;
    problem.resolutions = 100:10:800;
    problem.norm = @(x, y, f, g) norm(f - g) / norm(g);

    problem.evaluate = @fn_evaluate;

    % }}}

    util_point_convergence(testCase, {'f_hat', 'ddf_hat', 'ddf'}, problem);

    function [r] = fn_evaluate(p, x, ~, method)
        mms = p.user;

        % sizes
        n = x.npanels;
        m = 2 * (n - 1);
        % spatial variable
        xi = linspace(-mms.T, mms.T, m + 1); xi = xi(1:end - 1);
        dxi = xi(2) - xi(1);
        % frequency variable
        k = st_fftfreq(m, 2 * mms.T);

        switch method
        case 'ex'
            % compute fourier transform
            fhat = mms.fhat(k);
            % compute fourier transform derivative
            ddf_hat = (1j * k).^2 .* fhat;
            % compute derivative
            ddf = mms.ddf(xi);
        case 'ap'
            f = mms.f(xi);
            phase = dxi * exp(1j * k * mms.T) / sqrt(2 * pi);

            % compute fourier transform
            fhat_orig = fft(f);
            fhat = phase .* fhat_orig;
            % compute fourier transform derivative
            ddf_hat_orig = (1j * k).^2 .* fhat_orig;
            ddf_hat = phase .* ddf_hat_orig;
            % compute derivative
            ddf = ifft(ddf_hat_orig);
        end

        r.xi.f_hat = fftshift(k);
        r.xi.ddf_hat = fftshift(k);
        r.xi.ddf = xi;

        r.f_hat = real(fftshift(fhat));
        r.ddf_hat = real(fftshift(ddf_hat));
        r.ddf = real(ddf);
    end
end

% }}}

% {{{ test_mesh_volume

function test_mesh_volume(testCase)
    %
    % Test convergence of :func:`st_mesh_volume`.
    %

    % {{{

    % geometry
    a = 4.0;
    b = 2.0;

    geometry.nnodes = 4;
    geometry.nbasis = 4;
    geometry.curve = @(xi) st_mesh_curve(xi, 'ellipse', 'a', a, 'b', b);
    problem.geometry = geometry;

    % analytic volume for an ellipse
    problem.user.volume = 4.0 / 3.0 * pi * a^2 * b;

    % output
    problem.prefix = 'mesh_volume';
    problem.labels = {'V'};

    problem.expected_order = 2 * geometry.nnodes - 1;
    problem.resolutions = 5 * (1:9);

    problem.evaluate = @fn_evaluate;

    % }}}

    util_point_convergence(testCase, {'volume'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        switch method
        case 'ex'
            volume = p.user.volume;
        case 'ap'
            volume = st_mesh_volume(x, y);
        end

        r.volume = volume * ones(size(x.x));
    end
end

% }}}

% {{{ test_mesh_target_associate

function test_mesh_target_associate(testCase)
    %
    % Test :func:`st_mesh_target_associate`.
    %

    cache = unittest_create_cache('mesh_target_associate');

    % parameters
    npanels = 8;
    nnodes = 4;
    shape = @(xi) exp(2.0j * pi * xi);

    % generate target and source points
    x = st_point_collocation(npanels, nnodes, shape);
    y = st_point_quadrature(npanels, nnodes, shape);
    [x, y] = st_mesh_geometry(x, y);

    % {{{ associate on-surface targets to panels

    [panel_ids, ~] = st_mesh_target_associate(x, y);

    % plot targets closest to each panel
    h = figure();
    plot(real(x.x), imag(x.x), 'k');

    for j = 1:npanels
        source_ids = ((j - 1) * nnodes + 1):(j * nnodes);
        target_ids = find(panel_ids(2, :) == j);

        h1 = plot(real(y.x(source_ids)), imag(y.x(source_ids)), 'o');
        plot(real(x.x(target_ids)), imag(x.x(target_ids)), 'x', ...
            'Color', get(h1, 'Color'));
    end

    st_print(h, cache.filename(cache, 'surface', 'png'));

    % }}}

    % {{{ associate off-surface targets to panels

    dxi = max(diff(x.xi));

    z = x.x + 2.0 * dxi * x.n;
    [panel_ids, ~] = st_mesh_target_associate(z, y);

    % plot targets closest to each panel
    h = figure();
    plot(real(x.x), imag(x.x), 'k');

    for j = 1:npanels
        source_ids = ((j - 1) * nnodes + 1):(j * nnodes);
        target_ids = find(panel_ids(2, :) == j);

        h1 = plot(real(y.x(source_ids)), imag(y.x(source_ids)), 'o');
        plot(real(z(target_ids)), imag(z(target_ids)), 'x', ...
            'Color', get(h1, 'Color'));
    end

    st_print(h, cache.filename(cache, 'off_surface', 'png'));

    for i = 1:length(z) - 1
        ipanel = floor((i - 1) / (nnodes - 1)) + 1;
        testCase.verifyLessThan(abs(ipanel - panel_ids(1, i)), 2);
        testCase.verifyLessThan(abs(ipanel - panel_ids(2, i)), 2);
    end

    % }}}
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
