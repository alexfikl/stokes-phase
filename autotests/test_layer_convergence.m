% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_layer_convergence
    tests = functiontests({@setup, @teardown, ...
                           @test_repr_layer_evaluate, ...
                           @test_repr_density_lambda_1, ...
                           @test_repr_density_exterior, ...
                           @test_repr_velocity_lambda_1, ...
                           @test_repr_velocity_lambda_5, ...
                           @test_repr_density_velocity_lambda_1, ...
                           @test_repr_density_velocity_lambda_5, ...
                           @test_repr_density_pressure_lambda_1, ...
                           @test_repr_density_pressure_lambda_5, ...
                           @test_repr_density_traction_lambda_1, ...
                           @test_repr_density_traction_lambda_5, ...
                           @test_repr_density_tangent_traction_lambda_1, ...
                           @test_repr_density_tangent_traction_lambda_5, ...
                           @test_repr_density_tangent_gradu_lambda_1, ...
                           @test_repr_density_tangent_gradu_lambda_5, ...
                           @test_repr_density_normal_gradu_lambda_1, ...
                           @test_repr_density_normal_gradu_lambda_5, ...
                           @test_repr_density_normal_grads_lambda_1, ...
                           @test_repr_density_normal_grads_lambda_5, ...
                           @test_repr_density_drag_lambda_1, ...
                           @test_repr_density_drag_lambda_5});
end

% {{{ helpers

function r = util_convergence_evaluate(p, x, y, method)
    switch method
    case 'ex'
        r = p.user.inner_evaluate(p, x, y, [], 'ex');
    case 'ap'
        switch p.user.layer_type
        case 'exterior'
            q = st_repr_density_exterior(x, y, p.bc);
        case 'twophase'
            q = st_repr_density(x, y, p.bc.param.lambda, p.bc);
        otherwise
            q = ones(size(x.x)) + 1.0j * ones(size(x.x));
        end

        r = p.user.inner_evaluate(p, x, y, q, 'ap');
    end
end

function util_layer_convergence(testCase, variables, problem)
    % Wrapper around :func:`st_unittest_convergence`.
    %
    % :arg testCase: a :class:`TestCase` instance.

    % {{{ setup

    problem.user = st_struct_field(problem, 'user', struct());
    problem.user.layer_type = st_struct_field(problem.user, 'layer_type', 'twophase');
    problem.param.Ca = st_struct_field(problem.param, 'Ca', 1.0);

    problem.prefix = sprintf('layer_%s_%.2f', problem.prefix, problem.param.lambda);
    problem.user.inner_evaluate = problem.evaluate;
    problem.evaluate = @util_convergence_evaluate;

    % }}}

    [eoc, convergence] = st_unittest_convergence(variables, problem);
    orders = convergence.expected_order;

    % verify convergence
    for m = 1:length(variables)
        v_or = eoc.global_orders.(variables{m});

        if min(eoc.errors.(variables{m})) > 1.0e-12
            testCase.verifyGreaterThan(v_or, orders(m));
        end
    end
end

% }}}

% {{{ test_repr_layer_evaluate

function test_repr_layer_evaluate(testCase)
    %
    % Test the regular kernel at `st_layer_regular`.
    %

    % {{{

    % geometry
    geometry.nnodes = 4;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_reg = struct('nnodes', 1 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad.reg = s_reg;
    problem.geometry = geometry;

    % using a custom kernel here
    problem.user.layer_type = 'custom';
    r = fn_boundary_condition();
    problem.user.fn = r.fn;
    problem.user.fn_axis = r.fn_axis;

    problem.param.lambda = 0.0;

    % output
    problem.prefix = 'regular';
    problem.labels = {'f'};
    problem.resolutions = 2.^(1:6);
    problem.expected_order = 2 * geometry.nnodes - 1.5;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'fn'}, problem);

    function [result] = fn_boundary_condition()
        theta = sym('theta', 'real');
        phi = sym('phi', 'real');
        x = sym('x', 'real');
        y = sym('y', 'real');

        % NOTE: this is the kernel in st_layer_regular
        knl = 5 / (theta + 1) * abs(cos(theta) - cos(phi));
        % integrate for a density of 1 (see util_convergence_evaluate above)
        int_k = simplify(int(knl, theta, 0, pi));

        % transform into (x, y) coordinates instead
        int_f = simplify(subs(subs(int_k, phi, atan2(y, x)), ...
                    abs(x + y * 1i), sqrt(x^2 + y^2)));

        % NOTE: taking axis limits symbolically because otherwise they would
        % not be computed accurately due to the 1/0 term
        result.fn_axis = double([limit(int_k, 0), limit(int_k, pi)]);
        result.fn = st_sym_handle(int_f, [x, y]);
    end

    function [result] = fn_evaluate(p, x, y, q, method)
        switch method
        case 'ex'
            f = p.user.fn(x);
            f([1, end]) = p.user.fn_axis;
        case 'ap'
            f = st_layer_regular(x, y, q);
        end

        result.fn = real(f);
    end
end

% }}}

% {{{ test_repr_density

function test_repr_density_lambda_1(testCase)
    %
    % Test that for lambda = 1 the density is just the jump.
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_reg = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad.reg = s_reg;
    problem.geometry = geometry;

    % parameters
    param.lambda = 1.0;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'density';
    problem.labels = {'q_x', 'q_\rho'};
    problem.resolutions = 2.^(3:9);
    problem.expected_order = geometry.nbasis - 0.5;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'qx', 'qy'}, problem);

    function [r] = fn_evaluate(p, x, y, q, method)
        bc = p.bc;
        param = bc.param;

        switch method
        case 'ex'
            deltaf = bc.fn.jump(y);
            q = -1.0 / (4.0 * pi) * (deltaf / (1.0 + param.lambda));
        case 'ap'
            q = st_interpolate(x, q, y.xi, 'lagrange');
        end

        r.qx = real(q);
        r.qy = imag(q);
    end
end

% }}}

% {{{ test_repr_density_exterior

function test_repr_density_exterior(testCase)
    %
    % Test solving the exterior problem using `st_repr_density_exterior`.
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 4 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng, 'log', s_sng);
    problem.geometry = geometry;

    % boundary conditions
    problem.boundary_condition.fn = 'SolidQuadratic';

    problem.param.lambda = 0.0;
    problem.user.layer_type = 'exterior';

    % output
    problem.prefix = 'exterior';
    problem.labels = {'f_x', 'f_y'};
    problem.resolutions = 2.^(1:5);
    problem.expected_order = geometry.nbasis - 0.75;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'fx', 'fy'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        switch method
        case 'ex'
            f = p.bc.fn.fn_ext(x);
        case 'ap'
            [f, ~] = st_repr_density_traction(x, y, q, p.bc);
        end

        result.fx = real(f);
        result.fy = imag(f);
    end
end

% }}}

% {{{ test_repr_velocity

function test_repr_velocity(testCase, lambda)
    %
    % Test the velocity representation `st_repr_velocity`.
    %

    % {{{

    % geometry
    geometry.nnodes = 4;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 1 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_log = struct('nnodes', 6 * geometry.nbasis, ...
                   'npoly', 2 * geometry.nbasis, ...
                   'method', 'carley');
    geometry.s_quad = struct('reg', s_sng, 'log', s_log);
    problem.geometry = geometry;

    % parameters
    param.lambda = lambda;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'dlp_velocity';
    problem.labels = {'u', 'v'};
    problem.resolutions = 2.^(1:5);
    problem.expected_order = geometry.nbasis - 0.5;

    problem.evaluate = @fn_evaluate;

    % }}}


    util_layer_convergence(testCase, {'u', 'v'}, problem);

    function [result] = fn_evaluate(p, x, y, ~, method)
        bc = p.bc;

        switch method
        case 'ex'
            u = bc.fn.u_ext(x);
        case 'ap'
            u = st_repr_velocity(x, y, bc.param.lambda, bc);
        end

        result.u = real(u);
        result.v = imag(u);
    end
end

function test_repr_velocity_lambda_1(testCase)
    test_repr_velocity(testCase, 1.0);
end

function test_repr_velocity_lambda_5(testCase)
    test_repr_velocity(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_velocity

function test_repr_density_velocity(testCase, lambda)
    %
    % Tests `st_repr_density_velocity`.
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_log = struct('nnodes', 4 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng, 'log', s_log);
    problem.geometry = geometry;

    % boundary conditions
    param.lambda = lambda;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'slp_velocity';
    problem.labels = {'u_x', 'u_\rho'};
    problem.resolutions = 2.^(1:6);
    problem.expected_order = geometry.nbasis - 0.5;

    problem.evaluate = @fn_evaluate;

    % }}}

    % problem.npanels = 2.^(1:9);

    util_layer_convergence(testCase, {'u', 'v'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        switch method
        case 'ex'
            u = p.bc.fn.u_ext(x);
        case 'ap'
            u = st_repr_density_velocity(x, y, q, p.bc);
        end

        result.u = real(u);
        result.v = imag(u);
    end
end

function test_repr_density_velocity_lambda_1(testCase)
    test_repr_density_velocity(testCase, 1.0);
end

function test_repr_density_velocity_lambda_5(testCase)
    test_repr_density_velocity(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_pressure

function test_repr_density_pressure(testCase, lambda)
    %
    % Tests `st_repr_density_pressure`
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_log = struct('nnodes', 4 * geometry.nbasis, ...
                   'npoly', 2 * geometry.nbasis, ...
                   'method', 'carley');
    if abs(lambda - 1.0) < 1.0e-14
        geometry.s_quad = struct('reg', s_sng);
    else
        geometry.s_quad = struct('reg', s_sng, 'log', s_log);
    end
    problem.geometry = geometry;

    % parameters
    param.lambda = lambda;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'pressure';
    problem.labels = {'p_+', 'p_-'};
    problem.resolutions = 2.^(1:5);
    problem.expected_order = geometry.nbasis - 0.5;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'p_ext', 'p_int'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        param = p.bc.param;

        switch method
        case 'ex'
            p_ext = p.bc.fn.p_ext(x);
            p_int = p.bc.fn.p_int(x) + 2.0 / param.Ca / param.lambda;
        case 'ap'
            [p_ext, p_int] = st_repr_density_pressure(x, y, q, p.bc);
        end

        result.p_ext = p_ext;
        result.p_int = p_int;
    end
end

function test_repr_density_pressure_lambda_1(testCase)
    test_repr_density_pressure(testCase, 1.0);
end

function test_repr_density_pressure_lambda_5(testCase)
    test_repr_density_pressure(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_traction

function test_repr_density_traction(testCase, lambda)
    %
    % Tests `st_repr_density_traction` normal version.
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng);
    problem.geometry = geometry;

    % boundary conditions
    param.lambda = lambda;
    param.Ca = 0.01;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'traction';
    problem.labels = {'f_{x, +}', 'f_{\rho, +}', 'f_{x, -}', 'f_{\rho, -}'};
    problem.resolutions = 2.^(1:6);
    problem.expected_order = geometry.nbasis - 0.5;

    problem.enable_caching = false;
    problem.enable_latex_eoc = true;
    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'fx_ext', 'fy_ext', 'fx_int', 'fy_int'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        param = p.bc.param;

        switch method
        case 'ex'
            f_ext = p.bc.fn.fn_ext(x);
            f_int = p.bc.fn.fn_int(x) - 2.0 / param.Ca / param.lambda * x.x;
        case 'ap'
            [f_ext, f_int] = st_repr_density_traction(x, y, q, p.bc);
        end

        result.fx_ext = real(f_ext);
        result.fy_ext = imag(f_ext);
        result.fx_int = real(f_int);
        result.fy_int = imag(f_int);
    end
end

function test_repr_density_traction_lambda_1(testCase)
    test_repr_density_traction(testCase, 1.0);
end

function test_repr_density_traction_lambda_5(testCase)
    test_repr_density_traction(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_tangent_traction

function test_repr_density_tangent_traction(testCase, lambda)
    %
    % Tests `st_repr_density_traction` tangential version.
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_cpv = struct('nnodes', 4 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng, 'cpv', s_cpv);
    problem.geometry = geometry;

    % boundary conditions
    param.lambda = lambda;
    param.Ca = Inf;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'tangent_traction';
    problem.labels = {'t_{x, +}', 't_{\rho, +}', 't_{x, -}', 't_{\rho, -}'};
    problem.resolutions = 2.^(1:6);
    problem.expected_order = geometry.nbasis - 1.0;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'tx_ext', 'ty_ext', 'tx_int', 'ty_int'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        param = p.bc.param;

        switch method
        case 'ex'
            t_ext = p.bc.fn.ft_ext(x);
            t_int = p.bc.fn.ft_int(x);
        case 'ap'
            [t_ext, t_int] = st_repr_density_traction(x, y, q, p.bc, 'tangent');
            t_int = t_int + 2.0 / param.Ca / param.lambda * x.t;
        end

        result.tx_ext = real(t_ext);
        result.ty_ext = imag(t_ext);
        result.tx_int = real(t_int);
        result.ty_int = imag(t_int);
    end
end

function test_repr_density_tangent_traction_lambda_1(testCase)
    test_repr_density_tangent_traction(testCase, 1.0);
end

function test_repr_density_tangent_traction_lambda_5(testCase)
    test_repr_density_tangent_traction(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_normal_gradu

function test_repr_density_normal_gradu(testCase, lambda)
    %
    % Tests `st_repr_density_gradu` normal version.
    %

    % FIXME: for some reason the y components are doing a lot worse.
    % Problem with the singularity subtraction?

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_log = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng, 'log', s_log);
    problem.geometry = geometry;

    % boundary conditions
    param.lambda = lambda;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'dudn';
    problem.labels = {...
        '\mathbf{n} \cdot \nabla u_+', '\mathbf{n} \cdot \nabla v_+', ...
        '\mathbf{n} \cdot \nabla u_-', '\mathbf{n} \cdot \nabla v_-'};
    problem.resolutions = 2.^(1:5);
    problem.expected_order = geometry.nbasis - 1.0;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, ...
        {'dudn_x_ext', 'dudn_y_ext', 'dudn_x_int', 'dudn_y_int'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        switch method
        case 'ex'
            dudn_ext = p.bc.fn.dudn_ext(x);
            dudn_int = p.bc.fn.dudn_int(x);
        case 'ap'
            [dudn_ext, dudn_int] = st_repr_density_gradu(x, y, q, p.bc);
        end

        result.dudn_x_ext = real(dudn_ext);
        result.dudn_y_ext = imag(dudn_ext);
        result.dudn_x_int = real(dudn_int);
        result.dudn_y_int = imag(dudn_int);
    end
end

function test_repr_density_normal_gradu_lambda_1(testCase)
    test_repr_density_normal_gradu(testCase, 1.0);
end

function test_repr_density_normal_gradu_lambda_5(testCase)
    test_repr_density_normal_gradu(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_tangent_gradu

function test_repr_density_tangent_gradu(testCase, lambda)
    %
    % Tests `st_repr_density_gradu` tangential version.
    %

    % FIXME: why is the CPV integral first order here? was this always the case?

    % {{{

    % geometry
    geometry.nnodes = 4;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 4 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_cpv = struct('nnodes', 4 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng, 'cpv', s_cpv);
    problem.geometry = geometry;

    % boundary conditions
    param.lambda = lambda;
    param.Ca = 1.0;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'dudt';
    problem.labels = {'\mathbf{t} \cdot \nabla u', '\mathbf{t} \cdot \nabla v'};
    problem.resolutions = 2.^(1:5);
    problem.expected_order = 1.0;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'dudt_x', 'dudt_y'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        switch method
        case 'ex'
            dudt = p.bc.fn.dudt_ext(x);
        case 'ap'
            if isfield(y.quad, 'log')
                [dudt, ~] = st_repr_density_gradu(x, y, q, p.bc, 'tangentfft');
            else
                [dudt, ~] = st_repr_density_gradu(x, y, q, p.bc, 'tangent');
            end
        end

        result.dudt_x = real(dudt);
        result.dudt_y = imag(dudt);
    end
end

function test_repr_density_tangent_gradu_lambda_1(testCase)
    test_repr_density_tangent_gradu(testCase, 1.0);
end

function test_repr_density_tangent_gradu_lambda_5(testCase)
    test_repr_density_tangent_gradu(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_normal_grads

function test_repr_density_normal_grads(testCase, lambda)
    %
    % Test `st_repr_density_grads`
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_hfp = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng, 'cpv', s_hfp, 'hfp', s_hfp);
    problem.geometry = geometry;

    % boundary conditions
    param.lambda = lambda;
    param.Ca = 1.0;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'grads';
    problem.labels = {...
        '\nabla_n \sigma_+', '\nabla_n \sigma_-', ...
        '[[\nabla_n \sigma]]_x', '[[\nabla_n \sigma]]_y'};
    problem.resolutions = 2.^(1:5);
    problem.expected_order = geometry.nbasis - [2.75, 2.75, 1.5, 1.5];

    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, ...
        {'grads_ext', 'grads_int', 'grads_jump_x', 'grads_jump_y'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        fn = p.bc.fn;

        switch method
        case 'ex'
            grads_ext = fn.grads_ext(x);
            grads_int = fn.grads_int(x);
            grads_x = grads_ext - grads_int;
            grads_y = fn.grads_ext(y) - fn.grads_int(y);
        case 'ap'
            [grads_ext, grads_int] = st_repr_density_grads(x, y, q, p.bc);

            dqds = st_dot(x.t, st_derivative(x, q, x.xi, 'fft'));
            grads_x = -8.0 * pi * (x.kappa .* st_dot(q, x.n) - 2.0 * dqds);

            dqds = st_dot(y.t, st_derivative(x, q, y.xi, 'lagrange'));
            q = st_interpolate(x, q, y.xi);
            grads_y = -8.0 * pi * (y.kappa .* st_dot(q, y.n) - 2.0 * dqds);
        end

        result.grads_ext = grads_ext;
        result.grads_int = grads_int;
        result.grads_jump_x = grads_x;
        result.grads_jump_y = grads_y;
    end
end

function test_repr_density_normal_grads_lambda_1(testCase)
    test_repr_density_normal_grads(testCase, 1.0);
end

function test_repr_density_normal_grads_lambda_5(testCase)
    assumeFail(testCase);
    test_repr_density_normal_grads(testCase, 5.0);
end

% }}}

% {{{ test_repr_density_drag

function test_repr_density_drag(testCase, lambda)
    %
    % Tests surface drag by integrating `st_repr_density_traction`
    %

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng);
    problem.geometry = geometry;

    % boundary conditions
    param.lambda = lambda;
    param.Ca = 0.01;
    problem.param = param;

    bc.fn = 'HadamardRybczynski';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'drag';
    problem.labels = {'D_+', 'D_-'};
    problem.resolutions = 2.^(1:8);
    problem.expected_order = geometry.nbasis - 0.5;

    problem.evaluate = @fn_evaluate;
    % }}}

    util_layer_convergence(testCase, {'d_ext', 'd_int'}, problem);

    function [result] = fn_evaluate(p, x, y, q, method)
        sym = p.bc.fn.sym;
        param = p.bc.param;

        switch method
        case 'ex'
            fx = sym.sigma_ext * sym.n';
            fx = simplify(subs(fx(1), sym.x, sym.xs));
            d_ext = 2 * pi * param.R * ...
                double(int(fx * sym.xs(2), sym.theta, 0, pi));

            fx = sym.sigma_int * sym.n';
            fx = simplify(subs(fx(1), sym.x, sym.xs));
            d_int = 2 * pi * param.R * ...
                double(int(fx * sym.xs(2), sym.theta, 0, pi));
        case 'ap'
            [f_ext, f_int] = st_repr_density_traction(x, y, q, p.bc);
            f_int = f_int + 2.0 / param.Ca / param.lambda * x.n;

            d_ext = st_ax_integral(y, ...
                st_interpolate(x, real(f_ext), y.xi));
            d_int = st_ax_integral(y, ...
                st_interpolate(x, real(f_int), y.xi));
        end

        result.d_ext = d_ext * ones(size(x.x));
        result.d_int = d_int * ones(size(x.x));
    end
end

function test_repr_density_drag_lambda_1(testCase)
    test_repr_density_drag(testCase, 1.0);
end

function test_repr_density_drag_lambda_5(testCase)
    test_repr_density_drag(testCase, 5.0);
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:foldmethod=marker:
