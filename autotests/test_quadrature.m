% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_quadrature
  tests = functiontests({@setup, @teardown, ...
                         @test_quad_leggauss, ...
                         @test_quad_fejer, ...
                         @test_quad_ax_integral, ...
                         @test_quad_log_kress_kress, ...
                         @test_quad_log_kress_chun, ...
                         @test_quad_log_kress_sidi, ...
                         @test_quad_log_kress_kress_grid, ...
                         @test_quad_log_kress_chun_grid, ...
                         @test_quad_log_kress_sidi_grid, ...
                         @test_quad_carley_log, ...
                         @test_quad_carley_cpv, ...
                         @test_quad_carley_hfp, ...
                         @test_quad_carley_log_ab, ...
                         @test_quad_carley_cpv_ab, ...
                         @test_quad_carley_hfp_ab, ...
                         @test_quad_carley_cpv_grid, ...
                         @test_quad_carley_log_grid, ...
                         @test_quad_carley_hfp_grid, ...
                         @test_quad_log_alpert, ...
                         @test_quad_log_alpert_grid, ...
                         @test_cpv_singularity_subtraction});
end

% {{{ analytic solutions

function [f, intf] = util_sym_reg_sinpoly(k, p)
    f = @(x) 2 * pi * (sin(2 * pi * k * x) + (2 * pi * x).^p);
    intf = 2 * sin(k * pi / 2) / k + pi^(p + 1) / (p + 1);
end

function [f, intf] = util_sym_reg_flower(a, b, R)
    geometry = st_sym_geometry('flower', struct('R', R), true);

    xi = geometry.xi;
    dS = simplify(2.0 * pi * geometry.y * geometry.J);

    f = @(x) cos(8 * pi * x);
    intf = matlabFunction(f(xi) * dS, 'Vars', xi);
    intf = integral(intf, a, b, ...
        'AbsTol', 1.0e-14, 'RelTol', 1.0e-14);
end

function [f, intf] = util_sym_sng_carley(n, d, side)
    % Implements Equations (3.1)-(3.3) from [1]
    %
    % [1] https://doi.org/10.1137/060666093
    % M. Carley, Numerical Quadratures for Singular and Hypersingular
    % Integrals in Boundary Element Methods, SIAM, 2007.
    x = sym('x', 'real');
    t = sym('t', 'real');

    m = floor(n / 2);
    k = 0:m;

    % integrand
    switch d
    case 0
        f = t^n * log(abs(x - t));
    otherwise
        f = t^n / (x - t)^d;
    end

    % integral
    intf = 0.0;
    if side <= 0
        intf = intf + ((1.0 - x^(n + 1)) * log(1.0 - x));
    end
    if side >= 0
        intf = intf + ((-1.0)^n + x^(n + 1)) * log(1.0 + x);
    end

    intf = intf - 2.0 * sum(x.^(2.0 * k + mod(n, 2)) ./ (2 * m - 2 * k + 1));
    intf = intf / (n + 1);

    if d > 0
        intf = diff(intf, x, d);
        if d == 2
            intf = -intf;
        end

        if side ~= 0
            switch d
            case 1
                intf = intf - x^(n + 1);
            case 2
                intf = intf + x^n * n / 2;
            end
        end
    end

    f = matlabFunction(f, 'Vars', [x, t]);
    intf = matlabFunction(intf, 'Vars', x);
end

function [f, intf] = util_sym_sng_sqrt(y, lambda)
    if abs(abs(y) - 1.0) < 1.0e-14
        error('NotImplementedError: abs(y) == 1');
    end

    f = @(x) sqrt(1.0 - x.^2) ./ ((x.^2 + lambda^2) .* (x - y).^2);
    intf = -pi * sqrt(1.0 + lambda^2) * (lambda^2 - y^2) / ...
        lambda / (lambda^2 + y^2)^2;
end

function [f, intf] = util_sym_sng_exp(y)
    if abs(abs(y) - 1.0) < 1.0e-14
        error('NotImplementedError: abs(y) == 1');
    end

    f = @(x) exp(x) / (x - y).^2;
    intf = @(x) -exp(x) / (x - y) + exp(y) * real(-expint(y - x));
    intf = intf(1.0) - intf(-1.0);
end

function [f, intf] = util_sym_sng_hfp(y, a, b, p)
    f = @(x) 1.0 ./ (x - y).^(p + 1);
    switch p
    case 0
        if abs(y - a) < 1.0e-15
            intf = +log(b - a);
        elseif abs(y - b) < 1.0e-15
            intf = -log(b - a);
        else
            intf = log(b - y) - log(y - a);
        end
    otherwise
        if abs(y - a) < 1.0e-15 || abs(y - b) < 1.0e-15
            intf = (-1.0)^p / (p * (b - a)^p);
        else
            intf = -((b - y)^p + (-1)^(p + 1) * (y - a)^p) / ...
                    (p * (y - a)^p * (b - y)^p);
        end
    end
end

function [f, intf] = util_sym_sng_log(y, a, b)
    f = @(x) log(abs(y - x));
    if abs(y - a) < 1.0e-15
        intf = (b - a) * (log(b - a) - 1.0);
    elseif abs(y - b) < 1.0e-15
        intf = (b - a) * (log(b - a) - 1.0);
    else
        intf = (a - b) + (y - a) * log(y - a) + (b - y) * log(b - y);
    end
end

function [f, intf] = util_sym_sng_double_log(y)
    f = @(xi) log(y - xi.^2);

    if abs(y - 1.0) < 1.0e-14
        intf = -4.0 + log(16.0);
    else
        intf = -4.0 + log(1 - y) + 2 * sqrt(y) * atan(sqrt(y));
    end
end

function [f, intf] = util_sym_sng_gamma(y, alpha, n)
    if abs(abs(y) - 1.0) > 1.0e-14
        error('NotImplementedError: abs(y) ~= 1');
    end

    f = @(xi) (1 - xi.^2).^(alpha - 1.0) .* cos(n * xi);
    intf = sqrt(pi) * gamma(alpha) / (n / 2.0)^(alpha - 0.5) * ...
              besselj(alpha - 0.5, n);
end

function [f, intf] = util_sym_sng_cos_log(y, omega, a, b)
    x = sym('x', 'real');
    fn_sym = cos(omega * x) * log(x - y) + cos(omega * x + 1/3);

    f = matlabFunction(fn_sym, 'Vars', x);
    intf = double(int(fn_sym, x, a, b));
end

function [f, intf, g] = util_sym_sng_cos_cpv(y, omega, a, b)
    x = sym('x', 'real');
    f_sym = cos(omega * x) / (x - y);

    f = matlabFunction(f_sym, 'Vars', x);
    g = matlabFunction(cos(omega * x), 'Vars', x);
    intf = double(int(f_sym, x, a, b, 'PrincipalValue', true));
end

function [f, intf, g] = util_sym_sng_poly_cpv(y, p, a, b)
    x = sym('x', 'real');
    f_sym = (1 - x^p) / (x - y);

    f = matlabFunction(f_sym, 'Vars', x);
    g = matlabFunction(1 - x^p, 'Vars', x);
    intf = double(int(f_sym, x, a, b, 'PrincipalValue', true));
end

function mms = util_mms(solution, varargin)
    opts = st_struct_varargin(varargin{:});

    d.y = 0.0;
    d.a = -1.0;
    d.b = +1.0;

    solution = lower(solution);
    switch solution
    case 'reg_sinpoly'
        d.p = 7;
        d.k = 1;
        opts = st_struct_update(d, opts);

        [f, intf] = util_sym_reg_sinpoly(opts.k, opts.p);
    case 'reg_flower'
        d.R = 2.0;
        opts = st_struct_update(d, opts);

        [f, intf] = util_sym_reg_flower(opts.a, opts.b, opts.R);
    case 'sng_carley'
        d.power = 0;
        d.order = 0;
        opts = st_struct_update(d, opts);

        if abs(opts.y - 1.0) < 1.0e-14
            opts.side = +1;
        elseif abs(opts.y + 1) < 1.0e-14
            opts.side = -1;
        else
            opts.side = 0;
        end

        [fn, intf] = util_sym_sng_carley(opts.power, opts.order, opts.side);
        f = @(x) fn(opts.y, x);
        intf = intf(opts.y);
    case 'sng_sqrt'
        d.lambda = 5.0;
        opts = st_struct_update(d, opts);

        [f, intf] = util_sym_sng_sqrt(opts.y, opts.lambda);
    case 'sng_exp'
        opts = st_struct_update(d, opts);

        [f, intf] = util_sym_sng_exp(opts.y);
    case 'sng_hfp'
        d.p = 2;
        opts = st_struct_update(d, opts);

        [f, intf] = util_sym_sng_hfp(opts.y, opts.a, opts.b, opts.p - 1);
    case 'sng_log'
        opts = st_struct_update(d, opts);
        [f, intf] = util_sym_sng_log(opts.y, opts.a, opts.b);
    case 'sng_double_log'
        opts = st_struct_update(d, opts);
        [f, intf] = util_sym_sng_double_log(opts.y);
    case 'sng_gamma'
        d.alpha = 1.0;
        d.n = 4;
        opts = st_struct_update(d, opts);

        [f, intf] = util_sym_sng_gamma(opts.y, opts.alpha, opts.n);
    case 'sng_cos_log'
        d.omega = 129;
        opts = st_struct_update(d, opts);

        [f, intf] = util_sym_sng_cos_log(opts.y, opts.omega, opts.a, opts.b);
    case 'sng_cos_cpv'
        d.omega = 2.0 * pi;
        opts = st_struct_update(d, opts);

        [f, intf, mms.g] = util_sym_sng_cos_cpv(opts.y, opts.omega, opts.a, opts.b);
    case 'sng_poly_cpv'
        d.p = 13;
        opts = st_struct_update(d, opts);

        [f, intf, mms.g] = util_sym_sng_poly_cpv(opts.y, opts.p, opts.a, opts.b);
    otherwise
        error('unknown solution: %s', solution);
    end

    mms.f = f;
    mms.intf = intf;
    mms.y = opts.y;
    mms.a = opts.a;
    mms.b = opts.b;
    mms.param = opts;
end

% }}}

% {{{ quadrature matrix row

function [y] = util_row_source(y, quad, yi)
    % Replaces panel containing ``yi`` with quadrature rules generated by
    % ``quad(yi, a, b)``.
    %
    % :arg y: regular panel-based quadrature.
    % :arg quad: function handle that generates quadrature rules on :math:`(a, b)`.
    % :arg yi: location of the singularity.

    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);
    % number of panels
    npanels = length(y.panels) - 1;

    % adjacent panels
    iprev = discretize(yi, y.panels, 'IncludedEdge', 'right');
    inext = discretize(yi, y.panels, 'IncludedEdge', 'left');
    % get quadrature
    a = y.panels(iprev); b = y.panels(inext + 1);
    [xi, w] = quad(yi, a, b);

    % get source point indices
    iprev = (iprev - 1) * nnodes;
    if iprev == 1
        iprev = 0;
    end
    inext = inext * nnodes + 1;
    if inext == npanels
        inext = length(y.xi) + 1;
    end

    xi = [y.xi(1:iprev), ...
          xi, ...
          y.xi(inext:end)];
    w = [y.w(1:iprev), ...
         w, ...
         y.w(inext:end)];

    y = struct('xi', xi, 'w', w);
end

% }}}

% {{{ util_quadrature_convergence

function [xi, w] = util_leggauss_ab(nnodes, y, a, b)
    [xi, w] = st_quad_leggauss(nnodes);
    [xi, w] = st_quad_translate(xi, w, a, b, y);
end

function util_quadrature_convergence(testCase, variables, problem)
    % Wrapper around :func:`st_unittest_convergence`.
    %
    % Test convergence for quadrature rules.
    %
    % :arg testCase: a :class:`TestCase` instance.

    % {{{

    % disable interpolation by default
    if ~isfield(problem.geometry, 'nbasis')
        problem.geometry.nbasis = -1;
    end

    problem.norm = @(x, y, f, g) norm(f - g, Inf) / norm(g, Inf);
    problem.geometry_update = @fn_geometry_update;

    % }}}

    [eoc, convergence] = st_unittest_convergence(variables, problem);
    orders = convergence.expected_order;

    for m = 1:length(variables)
        v_or = eoc.global_orders.(variables{m});
        testCase.verifyGreaterThan(v_or, orders(m));
    end

    function [g] = fn_geometry_update(~, nnodes)
        g.npanels = nnodes;
        g.x.npanels = nnodes;
        g.y = problem.user.quadrature(nnodes);
    end
end

function util_grid_convergence(testCase, variables, problem)
    % Wrapper around :func:`st_unittest_convergence`.
    %
    % Tests grid convergence of regular quadrature rules.
    %
    % :arg testCase: a :class:`TestCase` instance.

    % {{{

    % disable interpolation by default
    if ~isfield(problem.geometry, 'nbasis')
        problem.geometry.nbasis = -1;
    end

    problem.norm = @(x, y, f, g) norm(f - g, Inf) / norm(g, Inf);

    % }}}

    [eoc, convergence] = st_unittest_convergence(variables, problem);
    orders = convergence.expected_order;

    for m = 1:length(variables)
        v_or = eoc.global_orders.(variables{m});
        testCase.verifyGreaterThan(v_or, orders(m));
    end
end

function util_singular_grid_convergence(testCase, variables, problem)
    % Wrapper around :func:`st_unittest_convergence`.
    %
    % Tests convergence when one panel requires singular quadrature.
    %
    % :arg testCase: a :class:`TestCase` instance.

    % {{{

    % disable interpolation by default
    if ~isfield(problem.geometry, 'nbasis')
        problem.geometry.nbasis = -1;
    end

    problem.norm = @(x, y, f, g) norm(f - g, Inf) / norm(g, Inf);
    problem.geometry_update = @fn_geometry_update;

    % }}}

    [eoc, convergence] = st_unittest_convergence(variables, problem);
    orders = convergence.expected_order;

    for m = 1:length(variables)
        v_or = eoc.global_orders.(variables{m});
        testCase.verifyGreaterThan(v_or, orders(m));
    end

    function [g] = fn_geometry_update(geometry, npanels)
        mms = problem.user.mms;

        % redo regular quadrature on desired interval
        y = st_point_quadrature(npanels, geometry.nnodes, [], [], mms.a, mms.b);
        % add singular quadrature
        y = util_row_source(y, problem.user.quadrature, mms.y);

        g.nnodes = geometry.nnodes;
        g.npanels = npanels;
        g.x.npanels = npanels;
        g.y = y;
    end
end

% }}}

% {{{ test regular quadrature

function test_quad_leggauss(testCase)
    %
    % Test grid convergence of :func:`st_quad_leggauss`
    %

    % {{{

    % quadrature
    geometry.nnodes = 4;
    geometry.quadtype = 'leggauss';
    problem.geometry = geometry;

    % solution
    problem.user.mms = util_mms('reg_sinpoly', 'p', 2 * geometry.nnodes + 7);

    % output
    problem.prefix = 'quadrature_leggauss';
    problem.labels = {'I[f]'};
    problem.expected_order = 2 * geometry.nnodes - 0.5;
    problem.resolutions = 2.^(1:6);

    problem.evaluate = @fn_evaluate;

    % }}}

    % quadrature rule
    problem.quad = 'leggauss';

    util_grid_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = intf * ones(size(y.xi));
    end
end

function test_quad_fejer(testCase)
    %
    % Test grid convergence of :func:`st_quad_fejer`.
    %

    % {{{

    % quadrature
    geometry.nnodes = 8;
    geometry.quadtype = 'fejer';
    problem.geometry = geometry;

    % solution
    problem.user.mms = util_mms('reg_sinpoly', 'p', 2 * geometry.nnodes + 13);

    % output
    problem.prefix = 'quadrature_fejer';
    problem.labels = {'I[f]'};
    problem.expected_order = geometry.nnodes - 0.5;
    problem.resolutions = 2.^(2:7);

    problem.evaluate = @fn_evaluate;

    % }}}

    util_grid_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = intf * ones(size(y.xi));
    end
end

function test_quad_ax_integral(testCase)
    %
    % Test grid convergence on a curve.
    %

    % {{{

    % geometry
    R = 2.0;

    geometry.nnodes = 4;
    geometry.nbasis = 4;
    geometry.quadtype = 'leggauss';
    geometry.curve = @(xi) st_mesh_curve(xi, 'flower', 'R', R);
    problem.geometry = geometry;

    % solution
    problem.user.mms = util_mms('reg_flower', 'R', R, 'a', 0.0, 'b', 0.5);

    % output
    problem.prefix = 'quadrature_integral';
    problem.labels = {'I[f]'};
    problem.resolutions = 2.^(4:9);
    % NOTE: normally the order would be `nbasis`, because there's geometry
    % that needs differentiating here, but because we use FFTs for that,
    % which is spectral, the quadrature order is the smallest one.
    problem.expected_order = 2 * geometry.nnodes - 0.5;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_grid_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = st_ax_integral(y, mms.f(y.xi));
        end

        r.intf = intf * ones(size(y.xi));
    end
end

% }}}

% {{{ test_quad_log_kress

function test_quad_log_kress(testCase, weight, p, resolutions)
    %
    % Test convergence of :func:`st_quad_log_kress` for a given `weight`.
    %

    % {{{

    % quadrature
    geometry.nnodes = 4;
    problem.geometry = geometry;

    problem.user.quadrature = @(nnodes) st_quad_log_kress(nnodes, weight, p);

    % solution
    problem.user.mms = util_mms('sng_double_log', 'y', 1.0);

    % output
    problem.prefix = sprintf('quadrature_kress_%s', lower(weight));
    problem.labels = {'I[f]'};
    problem.expected_order = p - 0.5;
    problem.resolutions = resolutions;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_quadrature_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = intf * ones(size(y.xi));
    end
end

function test_quad_log_kress_kress(testCase)
    % NOTE: chosen to reach machine precision
    nnodes = (50:50:650);

    test_quad_log_kress(testCase, 'kress', 6, nnodes);
end

function test_quad_log_kress_chun(testCase)
    % NOTE: chosen to reach machine precision
    nnodes = (10:40:410);

    test_quad_log_kress(testCase, 'chun', 6, nnodes);
end

function test_quad_log_kress_sidi(testCase)
    % NOTE: chosen to reach machine precision
    nnodes = (10:40:370);

    test_quad_log_kress(testCase, 'sidi', 6, nnodes);
end

% }}}

% {{{ test_quad_log_kress_grid

function test_quad_log_kress_grid(testCase, weight, p)
    %
    % Test grid convergence of :func:`st_quad_log_kress`.
    %

    % {{{

    % singularity
    a = -1.0; %#ok
    b = +1.0; %#ok
    y = 1.0;

    % quadrature
    problem.geometry.npanels = 32;
    geometry.nnodes = 8;
    problem.geometry = geometry;

    % singular quadrature
    problem.user.s_nnodes = 32;
    problem.user.quadrature = @(yi, ai, bi) ...
        st_quad_log_kress(problem.user.s_nnodes, weight, p, yi, ai, bi);

    % solution
    problem.user.mms = util_mms('sng_double_log', 'y', y);

    % output
    problem.prefix = sprintf('quadrature_kress_%s_grid', weight);
    problem.labels = {'I[f]'};
    problem.expected_order = 0.5;
    problem.resolutions = 2.^(2:8);

    problem.evaluate = @fn_evaluate;

    % }}}

    util_singular_grid_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = real(intf) * ones(size(y.xi));
    end
end

function test_quad_log_kress_kress_grid(testCase)
    test_quad_log_kress_grid(testCase, 'kress', 7);
end

function test_quad_log_kress_chun_grid(testCase)
    test_quad_log_kress_grid(testCase, 'chun', 6);
end

function test_quad_log_kress_sidi_grid(testCase)
    test_quad_log_kress_grid(testCase, 'sidi', 4);
end

% }}}

% {{{ test_quad_carley

function test_quad_carley(testCase, name, order)
    %
    % Tests convergence of :func:`st_quad_carley`.
    %

    % {{{ solutions

    yi = -1;
    s_npoly = 7;

    % polynomial orders
    npoly = [1, floor(s_npoly / 2), s_npoly, s_npoly + 2];

    % construct solutions
    variables = cell(1, length(npoly));
    labels = cell(1, length(npoly));
    f = cell(1, length(npoly));
    intf = zeros(1, length(npoly));

    for m = 1:length(npoly)
        mms = util_mms('sng_carley', 'y', yi, 'power', npoly(m), 'order', order - 1);

        variables{m} = sprintf('intf_%d', npoly(m));
        labels{m} = sprintf('I_{%d}[f]', npoly(m));
        f{m} = mms.f;
        intf(m) = mms.intf;
    end

    % }}}

    % {{{

    problem.geometry.nnodes = -1;
    problem.geometry.nbasis = -1;

    % quadrature
    problem.user.quadrature = @(nnodes) ...
        st_quad_carley(nnodes, yi, order, s_npoly, [], [], 'qr');

    % solutions
    problem.user.mms.f = f;
    problem.user.mms.intf = intf;

    % output
    problem.prefix = sprintf('quadrature_carley_%s', name);
    problem.labels = labels;
    problem.expected_order = -42;
    problem.resolutions = 5:5:50;

    problem.enable_caching = true;
    problem.evaluate = @fn_evaluate;

    % }}}

    util_quadrature_convergence(testCase, variables, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = zeros(1, length(mms.intf));
            for n = 1:length(intf)
                intf(n) = sum(mms.f{n}(y.xi) .* y.w);
            end
        end

        r = struct();
        for n = 1:length(intf)
            name = sprintf('intf_%d', npoly(n));
            r.(name) = intf(n);
        end
    end
end

function test_quad_carley_log(testCase)
    test_quad_carley(testCase, 'log', 1);
end

function test_quad_carley_cpv(testCase)
    test_quad_carley(testCase, 'cpv', 2);
end

function test_quad_carley_hfp(testCase)
    test_quad_carley(testCase, 'hfp', 3);
end

% }}}

% {{{ test_quad_carley_ab

function test_quad_carley_ab(testCase, name, order, mms)
    %
    % Test convergence of :func:`st_quad_carley` on a random interval.
    %

    % {{{

    problem.geometry.nnodes = -1;

    % quadrature
    a = -1.5 + rand();
    b = +1.5 - rand();
    y = a + (b - a) * rand();
    npoly = 4;

    problem.user.quadrature = @(nnodes) ...
        st_quad_carley(nnodes, y, order, npoly, a, b);

    % solution
    problem.user.mms = util_mms(mms, ...
        'y', y, 'a', a, 'b', b, 'p', order - 1);

    % output
    problem.prefix = sprintf('quadrature_carley_%s_ab', name);
    problem.labels = {'I[f]'};
    problem.resolutions = 2.^(3:9);
    problem.expected_order = -42;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_quadrature_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = intf;
    end
end

function test_quad_carley_log_ab(testCase)
    test_quad_carley_ab(testCase, 'log', 1, 'sng_log');
end

function test_quad_carley_cpv_ab(testCase)
    test_quad_carley_ab(testCase, 'cpv', 2, 'sng_hfp');
end

function test_quad_carley_hfp_ab(testCase)
    test_quad_carley_ab(testCase, 'hfp', 3, 'sng_hfp');
end

% }}}

% {{{ test_quad_carley_grid

function test_quad_carley_grid(testCase, name, order)
    %
    % Test grid convergence of :func:`st_quad_carley`.
    %

    % {{{

    % quadrature
    problem.geometry.npanels = 32;
    problem.geometry.nnodes = 8;

    % singular quadrature
    a = -1.0; %#ok
    b = +1.0; %#ok
    y = 1/3;
    s_npoly = 8;
    s_nnodes = 12;

    problem.user.quadrature = @(yi, ai, bi)...
        st_quad_carley(s_nnodes, yi, order, s_npoly, ai, bi);

    % solutions
    problem.user.mms = util_mms('sng_carley', ...
        'y', y, 'power', s_npoly, 'order', order - 1);

    % output
    problem.prefix = sprintf('quadrature_carley_%s_grid', name);
    problem.labels = {'I[f]'};
    problem.expected_order = 1.0;
    problem.resolutions = 2.^(2:10);

    problem.evaluate = @fn_evaluate;

    % }}}

    util_singular_grid_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = real(intf);
    end
end

function test_quad_carley_log_grid(testCase)
    test_quad_carley_grid(testCase, 'log', 1);
end

function test_quad_carley_cpv_grid(testCase)
    test_quad_carley_grid(testCase, 'cpv', 2);
end

function test_quad_carley_hfp_grid(testCase)
    test_quad_carley_grid(testCase, 'hfp', 3);
end

% }}}

% {{{ test_quad_log_alpert

function test_quad_log_alpert(testCase)
    %
    % Test convergence of :func:`st_quad_log_alpert`.
    %

    % {{{

    problem.geometry.nnodes = -1;
    problem.geometry.nbasis = -1;

    % singularity
    a = -1.0;
    b = +1.0;
    y = -1.0;

    % singular quadrature
    npoly = 8;
    problem.user.quadrature = @(nnodes) st_quad_log_alpert(nnodes, y, npoly, a, b);

    % solutions
    problem.user.mms = util_mms('sng_cos_log', ...
        'y', y, 'a', a, 'b', b, 'omega', 129);

    % output
    problem.prefix = 'quadrature_alpert';
    problem.labels = {'I[f]'};
    problem.resolutions = 2.^(5:11);
    problem.expected_order = npoly - 0.5;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_quadrature_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = intf;
    end
end

% }}}

% {{{ test_quad_log_alpert_grid

function test_quad_log_alpert_grid(testCase)
    %
    % Test grid convergence of :func:`st_quad_log_alpert`
    %

    % {{{

    problem.geometry.npanels = 32;
    problem.geometry.nnodes = 12;

    % singularity
    a = -1.0;
    b = +1.0;
    y = 1/3;

    % singular quadrature
    s_nnodes = 12;
    s_npoly = 6;
    problem.user.quadrature = @(yi, ai, bi) ...
        st_quad_log_alpert(s_nnodes, yi, s_npoly, ai, bi);

    % solutions
    problem.user.mms = util_mms('sng_cos_log', ...
        'y', y, 'a', a, 'b', b, 'omega', 12 * pi);

    % output
    problem.prefix = 'quadrature_alpert_grid';
    problem.labels = {'I[f]'};
    % problem.resolutions = (5:5:100);
    problem.resolutions = 2.^(2:8);
    problem.expected_order = 1.0;

    problem.evaluate = @fn_evaluate;

    % }}}

    util_singular_grid_convergence(testCase, {'intf'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf = mms.intf;
        case 'ap'
            intf = sum(mms.f(y.xi) .* y.w);
        end

        r.intf = real(intf);
    end
end

% }}}

% {{{ test_cpv_singularity_subtraction

function test_cpv_singularity_subtraction(testCase)
    %
    % Test convergence of convergence using singularity subtraction.
    %

    % {{{

    nnodes = 4;
    problem.geometry.nnodes = nnodes;

    % singular quadrature
    y = 0.55;
    problem.user.quadrature = @(yi, ai, bi) ...
        util_leggauss_ab(nnodes, yi, ai, bi);

    % solutions
    cpv = util_mms('sng_cos_cpv', 'y', y, 'omega', 2 * pi);
    hfp = util_mms('sng_hfp', 'y', y, 'p', 1);
    problem.user.mms = cpv;
    problem.user.mms.int_pv = hfp.intf;

    % output
    problem.prefix = 'quadrature_cpv_ss';
    problem.labels = {'I[f]', 'I[f]'};
    problem.resolutions = 2.^(1:6);
    problem.expected_order = [-1, 2 * nnodes - 0.5];

    problem.evaluate = @fn_evaluate;

    % }}}

    util_singular_grid_convergence(testCase, {'intf0', 'intf1'}, problem);

    function [r] = fn_evaluate(p, ~, y, method)
        mms = p.user.mms;

        switch method
        case 'ex'
            intf0 = mms.intf;
            intf1 = mms.intf;
        case 'ap'
            yi = mms.y;

            intf0 = sum(mms.f(y.xi) .* y.w);
            intf1 = sum((mms.g(y.xi) - mms.g(yi)) ./ (y.xi - yi) .* y.w) + ...
                    mms.g(yi) * mms.int_pv;
        end

        r.intf0 = real(intf0);
        r.intf1 = real(intf1);
    end
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
