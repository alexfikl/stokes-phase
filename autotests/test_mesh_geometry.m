% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_mesh_geometry
  tests = functiontests({@setup, @teardown, ...
                         @test_circle, ...
                         @test_ellipse, ...
                         @test_curvature_axis_limits, ...
                         @test_metric_convergence, ...
                         @test_normal_convergence, ...
                         @test_curvature_convergence, ...
                         @test_mesh_centroid, ...
                         @test_mesh_equidistance, ...
                         @test_mesh_interp});
end

% {{{ helpers

function util_plot_geometry(~, basename, x)
    % Computes the geometry using :func:`st_mesh_target_geometry` and plots
    % all the resulting variables, i.e. normal, curvature, etc.
    %
    % :arg testCase: :class:`TestCase` instance.
    % :arg basename: output basename.
    % :arg x: a set of points as a complex array.

    cache = unittest_create_cache(basename);
    x = st_mesh_target_geometry(x);

    % plot reconstructed geometry
    npoints = length(x.zk);
    xi = linspace(0.0, 1.0, 2 * npoints);
    px = x.zhat * exp(-x.zk' * xi) / npoints;

    zx = st_array_ax(x.x, true);
    px = st_array_ax(px, true);

    % plot geometry
    h = figure();
    axis equal;
    plot(real(px), imag(px));
    plot(real(zx), imag(zx), 'o');
    xlabel('$x$');
    ylabel('$y$');
    title('Surface Geometry');
    st_print(h, cache.filename(cache, 'geometry', 'png'));

    % plot normal vectors
    h = figure();
    axis equal
    plot(real(x.x), imag(x.x));
    plot(real(x.x), imag(x.x), 'o');
    for i = 1:length(x.x)
        quiver(real(x.x(i)), imag(x.x(i)), ...
               0.25 * real(x.n(i)), 0.25 * imag(x.n(i)), ...
               0.0, 'MaxHeadSize', 0.5);
    end
    xlabel('$x$');
    ylabel('$y$');
    title('Normal Vectors');
    st_print(h, cache.filename(cache, 'normal', 'png'));

    % plot curvature
    xi = linspace(0.0, 0.5, length(x.x));
    h = figure();
    plot(xi, x.kappa, 'LineWidth', 2);
    ylim([min(x.kappa) - 0.5, max(x.kappa) + 0.5]);
    xlabel('$\xi$');
    ylabel('$\kappa$');
    title('Curvature');
    st_print(h, cache.filename(cache, 'curvature', 'png'));

    % plot jacobian
    h = figure();
    plot(xi, x.J, 'x-', 'LineWidth', 2);
    xlabel('$\xi$');
    ylabel('$J$');
    title('Metric');
    st_print(h, cache.filename(cache, 'metric', 'png'));
end

function util_geometry_convergence(testCase, variables, problem)
    % Wrapper around :func:`st_unittest_convergence`.
    %
    % :arg testCase: a :class:`TestCase` instance.

    % problem.enable_caching = true;
    eoc = st_unittest_convergence(variables, problem);

    % FFTs are spectral, so this should give some high order
    for m = 1:length(variables)
        v_or = eoc.global_orders.(variables{m});
        testCase.verifyGreaterThan(v_or, problem.geometry.nbasis + 1);
    end
end

% }}}

% {{{ test st_mesh_target_geometry

function test_circle(testCase)
    %
    % Plot geometry on circle
    %

    npoints = 64;
    xi = linspace(0, 0.5, npoints);
    x = st_mesh_curve(xi, 'circle', 'R', 2.0);

    util_plot_geometry(testCase, 'geometry_circle', x);
end

function test_ellipse(testCase)
    %
    % Plot geometry on ellipse
    %

    npoints = 64;
    xi = linspace(0, 0.5, npoints);
    x = st_mesh_curve(xi, 'ellipse', 'a', 4.0, 'b', 1.0);

    util_plot_geometry(testCase, 'geometry_ellipse', x);
end

% }}}

% {{{ test_curvature_axis_limits

function test_curvature_axis_limits(~)
    %
    % Test axis limits of the azimuthal curvature
    %

    cache = unittest_create_cache('geometry_curvature');

    npoints = 512;
    xi = linspace(0.0, 0.5, npoints);

    x = st_mesh_curve(xi, 'ellipse');
    x = st_mesh_target_geometry(x);

    h = figure();
    plot(xi(2:end - 1), imag(x.n(2:end - 1)) ./ imag(x.x(2:end - 1)));
    plot(xi(2:end - 1), real(x.n(2:end - 1)) ./ imag(x.x(2:end - 1)));
    st_print(h, cache.filename(cache, 'axis_limits', 'png'));
end

% }}}

% {{{ test_metric_convergence

function test_metric_convergence(testCase)
    %
    % Test spectral convergence for metric
    %

    % {{{ options

    problem.user.ellipse = st_sym_geometry('ellipse', struct('a', 1.0, 'b', 4.0));

    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = problem.user.ellipse.curve;
    problem.geometry = geometry;

    problem.prefix = 'geometry_metric';
    problem.labels = {'J_x', 'J_y'};

    problem.evaluate = @fn_evaluate;

    % }}}

    util_geometry_convergence(testCase, {'metric_x', 'metric_y'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        switch method
        case 'ex'
            ellipse = p.user.ellipse;
            metric_y = ellipse.J(y);
            metric_x = ellipse.J(x);
        case 'ap'
            metric_y = y.J;
            metric_x = x.J;
        end

        r.metric_y = metric_y;
        r.metric_x = metric_x;
    end
end

% }}}

% {{{ test_normal_convergence

function test_normal_convergence(testCase)
    %
    % Test spectral convergence for normals
    %

    % {{{

    problem.user.ellipse = st_sym_geometry('ellipse', struct('a', 1.0, 'b', 4.0));

    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = problem.user.ellipse.curve;
    problem.geometry = geometry;

    problem.prefix = 'geometry_normal';
    problem.labels = {'n_x', 'n_\rho', 'n_x', 'n_\rho'};

    problem.evaluate = @fn_evaluate;

    % }}}

    util_geometry_convergence(testCase, ...
        {'source_nx', 'source_ny', 'target_nx', 'target_ny'}, problem);

    function [r] = fn_evaluate(p, x, y, method)
        switch method
        case 'ex'
            ellipse = p.user.ellipse;
            nx = ellipse.n(x);
            ny = ellipse.n(y);
        case 'ap'
            nx = x.n;
            ny = y.n;
        end

        r.target_nx = real(nx);
        r.target_ny = imag(nx);
        r.source_nx = real(ny);
        r.source_ny = imag(ny);
    end
end

% }}}

% {{{ test_curvature_convergence

function test_curvature_convergence(testCase)
    %
    % Test spectral convergence for curvature
    %

    % {{{

    problem.user.ellipse = st_sym_geometry('ellipse', struct('a', 1.0, 'b', 4.0));

    geometry.nnodes = 4;
    geometry.nbasis = 5;
    geometry.curve = problem.user.ellipse.curve;
    problem.geometry = geometry;

    problem.prefix = 'geometry_curvature';
    problem.labels = {'\kappa_x', '\kappa_\phi'};

    problem.evaluate = @fn_evaluate;
    problem.resolutions = 2.^(2:7);

    % }}}

    util_geometry_convergence(testCase, {'kappa_x', 'kappa_p'}, problem);

    function [r] = fn_evaluate(p, x, ~, method)
        switch method
        case 'ex'
            ellipse = p.user.ellipse;
            kappa_x = ellipse.kappa_x(x);
            kappa_p = ellipse.kappa_p(x);

            % NOTE: sym does not seem to handle the endpoint singularities
            % gracefully, so we use the analytic definition instead.
            kappa_p([1, end]) = kappa_x([1, end]);
        case 'ap'
            kappa_x = x.kappa_x;
            kappa_p = x.kappa_p;
        end

        r.kappa_x = kappa_x;
        r.kappa_p = kappa_p;
    end
end

% }}}

% {{{ test_mesh_centroid

function test_mesh_centroid(testCase)
    %
    % Test centroid computation
    %

    cache = unittest_create_cache('mesh_centroid');

    % {{{ geometry

    npanels = 64;
    nbasis = 4;
    nnodes = 4;

    offset = pi;
    curve = @(xi) st_mesh_curve(xi, 'ufo', 'a', 1.0, 'k', 4.0) + offset;

    x = st_point_collocation(npanels, nbasis, curve);
    y = st_point_quadrature(npanels, nnodes, curve);
    [x, y] = st_mesh_geometry(x, y);

    % }}}

    % {{{ check centroid

    xc = st_mesh_centroid(x, y);
    xd = offset;

    zx = st_array_ax(x.x, true);

    fig = figure();
    plot(real(zx), imag(zx));
    plot(real(xc), imag(xc), 'k.', 'MarkerSize', 24);
    plot(real(xd), imag(xd), 'x', 'MarkerSize', 24);
    xlabel('$x$');
    ylabel('$\rho$');
    st_print(fig, cache.filename(cache, 'x', 'png'));

    testCase.verifyLessThan(norm(xc - xd), 1.0e-10);

    % }}}
end

% }}}

% {{{ test_mesh_equidistance

function test_mesh_equidistance(~)
    %
    % Test :func:`st_mesh_equidistance` by plotting.
    %

    npanels = 256;
    nnodes = 4;
    nbasis = 4;
    shape = @(xi) st_mesh_curve(xi, 'ufo');

    % initial geometry
    x = st_point_collocation(npanels, nbasis, shape);
    y = st_point_quadrature(npanels, nnodes, shape);
    [x, y] = st_mesh_geometry(x, y);

    % equidistance points
    ds = st_mesh_arclength(x, y);
    volume = st_mesh_volume(x, y);
    fprintf('before:    ds %.5e volume %.5e\n', ...
        norm(diff(ds), Inf), volume);

    [xe, ye] = st_mesh_equidistance(x, y, 'interp', 'pfilter', -1.0e-5);

    ds = st_mesh_arclength(xe, ye);
    volume_e = st_mesh_volume(xe, ye);
    fprintf('after:     ds %.5e volume %.5e\n', ...
        norm(diff(ds), Inf), abs(volume_e - volume) / abs(volume));

    % {{{ plot

    cache = unittest_create_cache('mesh_equidistance');

    zx = st_array_ax(x.vertices, true);
    ze = st_array_ax(xe.vertices, true);

    fig = figure();
    axis('equal');
    plot(real(zx), imag(zx), 'o-');
    plot(real(ze), imag(ze), 'o-');
    st_print(fig, cache.filename(cache, 'geometry', 'png'));

    fig = figure();
    plot(x.xi, x.kappa, '-');
    plot(x.xi, xe.kappa, '-');
    st_print(fig, cache.filename(cache, 'curvature', 'png'));

    % }}}
end

% }}}

% {{{ test_mesh_interp

function test_mesh_interp(~)
    %
    % Test :func:`st_mesh_interp` between a base mesh and equidstant mesh.
    %

    npanels = 32;
    nnodes = 4;
    nbasis = 4;
    shape = @(xi) st_mesh_curve(xi, 'ufo');

    % initial geometry
    x = st_point_collocation(npanels, nbasis, shape);
    y = st_point_quadrature(npanels, nnodes, shape);
    [x_i, y_i] = st_mesh_geometry(x, y);
    % equidistant geometry
    [x_e, ~] = st_mesh_equidistance(x_i, y_i, 'interp');

    % interpolation
    theta_i = angle(x_i.x);
    theta_e = angle(x_e.x);

    f_i = cos(2.0 * pi * sin(theta_i));
    f_e = cos(2.0 * pi * sin(theta_e));
    f_interp = st_mesh_interp(x_i, x_e, f_i);

    fprintf('error: %.5e, %.5e\n', ...
        st_mesh_h_max(x_i, y_i), ...
        norm(f_e - f_interp) / norm(f_interp));

    % {{{ plot

    cache = unittest_create_cache('mesh_interp');

    fig = figure();
    plot(theta_i, f_i);
    plot(theta_e, f_interp);
    st_print(fig, cache.filename(cache, 'theta', 'png'));
    cla();

    plot(x_i.xi, f_i);
    plot(x_i.xi, f_interp);
    st_print(fig, cache.filename(cache, 'xi', 'png'));
    cla();

    plot(x_e.xi, log10(abs(f_e - f_interp) + 1.0e-16));
    st_print(fig, cache.filename(cache, 'error', 'png'));

    % }}}
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
