% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_layer_electric_convergence
    tests = functiontests({@setup, @teardown, ...
                            @test_electric_potential_1, ...
                            @test_electric_potential_5, ...
                            @test_electric_field_1, ...
                            @test_electric_field_5});
end

% {{{ helpers

function r = util_convergence_evaluate(problem, x, y, method)
    param = problem.bc.param;

    switch method
    case 'ex'
        r = problem.user.inner_evaluate(problem, x, y, [], 'ex');
    case 'ap'
        q = st_repr_electric(x, y, param.S, problem.bc);
        r = problem.user.inner_evaluate(problem, x, y, q, 'ap');
    end
end

function util_layer_convergence(testCase, variables, problem)
    % {{{ exact solutions

    param.R = 1.0;
    param.S = 1.0;
    param.Q = 1.0;
    problem.param = st_struct_merge(problem.param, param, false);
    problem.user = st_struct_field(problem, 'user', struct());

    problem.prefix = sprintf('layer_%s_%.2f', problem.prefix, problem.param.S);
    problem.user.inner_evaluate = problem.evaluate;
    problem.evaluate = @util_convergence_evaluate;

    % }}}


    [eoc, convergence] = st_unittest_convergence(variables, problem);
    orders = convergence.expected_order;

    % verify convergence
    for m = 1:length(variables)
        v_or = eoc.global_orders.(variables{m});

        if max(eoc.errors.(variables{m})) > 1.0e-13
            testCase.verifyGreaterThan(v_or, orders(m));
        end
    end
end

% }}}

% {{{ test_electric_potential

function test_electric_potential(testCase, S)
    % FIXME: this is getting first order instead of high-order

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    s_log = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng, 'log', s_log);
    problem.geometry = geometry;

    % boundary conditions
    param.S = S;
    problem.param = param;

    bc.fn = 'electric';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'potential';
    problem.labels = {'\phi'};
    problem.resolutions = 2.^(1:6);
    problem.expected_order = 0.9;
    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'phi'}, problem);

    function [r] = fn_evaluate(p, x, y, q, method)
        bc = p.bc;

        switch method
        case 'ex'
            phi = bc.fn.phi_ext(x);
        case 'ap'
            phi = st_repr_electric_potential(x, y, q, bc);
        end

        r.phi = phi;
    end
end

function test_electric_potential_1(testCase)
    test_electric_potential(testCase, 1.0);
end

function test_electric_potential_5(testCase)
    test_electric_potential(testCase, 5.0);
end

% }}}

% {{{ test_electric_field

function test_electric_field(testCase, S)
    % FIXME: this is getting first order instead of high-order

    % {{{

    % geometry
    geometry.nnodes = 8;
    geometry.nbasis = 4;
    geometry.curve = 'circle';

    % singular quadrature
    s_sng = struct('nnodes', 2 * geometry.nbasis, ...
                   'npoly', 1 * geometry.nbasis);
    geometry.s_quad = struct('reg', s_sng);
    problem.geometry = geometry;

    % boundary conditions
    param.S = S;
    problem.param = param;

    bc.fn = 'electric';
    problem.boundary_condition = bc;

    % output
    problem.prefix = 'field';
    problem.labels = {'E^+_n', 'E^-_n'};
    problem.resolutions = 2.^(1:6);
    problem.expected_order = 0.9;
    problem.evaluate = @fn_evaluate;

    % }}}

    util_layer_convergence(testCase, {'en_ext', 'en_int'}, problem);

    function [r] = fn_evaluate(p, x, y, q, method)
        bc = p.bc;

        switch method
        case 'ex'
            % NOTE: on a sphere of radius R, `n = x / R`
            en_ext = st_dot(bc.fn.e_ext(x), x.x);
            en_int = st_dot(bc.fn.e_int(x), x.x);
        case 'ap'
            [en_ext, en_int] = st_repr_electric_field(x, y, q, bc);
        end

        r.en_ext = en_ext;
        r.en_int = en_int;
    end
end

function test_electric_field_1(testCase)
    test_electric_field(testCase, 1.0);
end

function test_electric_field_5(testCase)
    test_electric_field(testCase, 5.0);
end


% }}}

% {{{ test_maxwell_stress_tensor
% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:foldmethod=marker:
