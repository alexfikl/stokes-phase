% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function tests = test_optim
  tests = functiontests({@setup, @teardown, ...
                         @test_cg_quadratic, ...
                         @test_cg_rosenbrock});
end

% {{{ test_cg_quadratic

function test_cg_quadratic(testCase)
    %
    % Test convergence of a simple quadratic function.
    %

    % {{{

    % https://en.wikipedia.org/wiki/Conjugate_gradient_method#Numerical_example
    % exact solution
    x_optim = [1.0 / 11.0, 7.0 / 11.0];

    % problem setup
    A = [4.0, 1.0; 1.0, 3.0];
    b = [1.0, 2.0];

    % optimize
    cg.x0 = [2.0, 1.0];
    cg.fn = @fn_quadratic;
    cg.display = testCase.TestData.verbose;
    cg.beta = 'cgdescent';
    cg.gtol = 1.0e-8;
    cg.alpha_max = 1.0;

    % }}}

    % {{{

    x_cg = st_optim_cg(cg);

    % optimize
    fminunc_opts = optimoptions('fminunc', ...
        'SpecifyObjectiveGradient', true, ...
        'StepTolerance', cg.gtol, ...
        'FunctionTolerance', cg.gtol, ...
        'OptimalityTolerance', 1.0e-4 * cg.gtol, ...
        'Display', 'iter');
    x_fminunc = fminunc(cg.fn, cg.x0, fminunc_opts);

    % }}}

    [~, grad] = cg.fn(cg.x0);
    gtol = norm(grad) * cg.gtol;

    testCase.verifyLessThan(norm(x_cg - x_optim, Inf), gtol);
    testCase.verifyLessThan(norm(x_fminunc - x_optim, Inf), gtol);

    function [f, grad] = fn_quadratic(x)
        f = 0.5 * dot(A * x', x) - dot(b, x);
        if nargout > 1
            grad = (A * x')' - b;
        end
    end
end

% }}}

% {{{ test_cg_rosenbrock

function test_cg_rosenbrock(testCase)
    %
    % Test convergence of the Rosenbrock function.
    %

    % {{{

    % https://en.wikipedia.org/wiki/Rosenbrock_function
    % problem setup
    a = 100.0;
    b = 1.0;

    % optimize
    cg.x0 = [-2.0, 1.0];
    cg.fn = @fn_quadratic;
    cg.display = testCase.TestData.verbose;
    cg.beta = 'cgdescent';
    cg.gtol = 1.0e-10;
    cg.alpha_max = 1.0;

    % }}}

    % {{{

    x_cg = st_optim_cg(cg);

    % optimize
    fminunc_opts = optimoptions('fminunc', ...
        'SpecifyObjectiveGradient', true, ...
        'StepTolerance', 1.0e-4 * cg.gtol, ...
        'FunctionTolerance', 1.0e-4 * cg.gtol, ...
        'OptimalityTolerance', 1.0e-4 * cg.gtol, ...
        'Display', 'iter');
    x_fminunc = fminunc(cg.fn, cg.x0, fminunc_opts);

    % }}}

    [~, grad] = cg.fn(cg.x0);
    gtol = norm(grad) * cg.gtol;

    testCase.verifyLessThan(norm(x_cg - x_fminunc, Inf), gtol);

    function [f, grad] = fn_quadratic(x)
        f = a * (x(2) - x(1)^2)^2 + b * (x(1) - 1.0)^2;

        if nargout > 1
            grad = 2.0 * [...
                2.0 * a * x(1) * (x(1)^2 - x(2)) + b * (x(1) - 1.0), ...
                a * (x(2) - x(1)^2)];
        end
    end
end

% }}}

function setup(testCase)
    testCase.TestData = st_unittest_setup();
end

function teardown(testCase)
    st_unittest_setup(testCase.TestData, true);
end

% vim:ts=4:sw=4:sts=4:foldmethod=marker:foldlevel=0:
