// SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "st.h"
#include <mex.h>

#define USAGE_MESSAGE "usage: [Mxx, Mxy, Myx, Myy] = st_knl_ax_velocity_c(x, x0)"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if (nrhs != 4 || nlhs != 4) {
        mexErrMsgTxt(USAGE_MESSAGE);
    }

    knl_velocity_t M;
    point_t s, t;

    s.x = (double64_t) mxGetScalar(prhs[0]);
    s.y = (double64_t) mxGetScalar(prhs[1]);
    t.x = (double64_t) mxGetScalar(prhs[2]);
    t.y = (double64_t) mxGetScalar(prhs[3]);

    M = st_knl_ax_velocity(s, t);
    plhs[0] = mxCreateDoubleScalar((double) M.xx);
    plhs[1] = mxCreateDoubleScalar((double) M.xy);
    plhs[2] = mxCreateDoubleScalar((double) M.yx);
    plhs[3] = mxCreateDoubleScalar((double) M.yy);
}
