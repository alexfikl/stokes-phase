// SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "st.h"
#include <mex.h>

#define MAX_IT 32
#define USAGE_MESSAGE "usage: [Px, Py, Rx, Ry] = st_knl_ax_pressure_c(x, x0, n, n0)"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    if (nrhs != 8 || nlhs != 4) {
        mexErrMsgTxt(USAGE_MESSAGE);
    }

    knl_pressure_t M;
    point_t s, t;

    s.x = (double64_t) mxGetScalar(prhs[0]);
    s.y = (double64_t) mxGetScalar(prhs[1]);
    t.x = (double64_t) mxGetScalar(prhs[2]);
    t.y = (double64_t) mxGetScalar(prhs[3]);
    s.nx = (double64_t) mxGetScalar(prhs[4]);
    s.ny = (double64_t) mxGetScalar(prhs[5]);
    t.nx = (double64_t) mxGetScalar(prhs[6]);
    t.ny = (double64_t) mxGetScalar(prhs[7]);

    M = st_knl_ax_pressure(s, t);
    plhs[0] = mxCreateDoubleScalar((double) M.px);
    plhs[1] = mxCreateDoubleScalar((double) M.py);
    plhs[2] = mxCreateDoubleScalar((double) M.rx);
    plhs[3] = mxCreateDoubleScalar((double) M.ry);
}

