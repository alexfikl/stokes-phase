// SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "st.h"

#define MAX_IT 256

ellipke_t st_ellipke(double64_t K) {
    ellipke_t r;
    double64_t F = M_PI_2f64;
    double64_t E = 1.0;
    double64_t G = 1.0;
    double64_t D = 1.0;
    double64_t B = sqrt(K);
    double64_t C = 0.0;
    int it = 0;

    while (fabs(D) > 1.0e-32 && it < MAX_IT) {
        C = sqrt(1.0 - B * B);
        B = (B * B) / ((1.0 + C) * (1.0 + C));
        D = F * B;
        F = F + D;
        G = G * B / 2.0;
        E = E + G;

        it += 1;
    }

    r.F = F;
    r.E = F * (1.0 - K * E / 2.0);

    return r;
}

ellint_t st_ellint_v(double64_t x, double64_t y, double64_t x0, double64_t y0) {
    ellint_t r;
    ellipke_t k;
    double64_t R = 1.0 / ((x - x0) * (x - x0) + (y + y0) * (y + y0));
    double64_t K2 = 4.0 * y * y0 * R;

    k = st_ellipke(K2);

    if (fabsl(y0) > 1.0e-8) {
        double64_t K4 = K2 * K2;
        double64_t K21 = 1.0 / (1.0 - K2);
        double64_t C1 = 4.0 * sqrt(R);
        double64_t C3 = C1 * R;

        r.I10 = C1 * k.F;
        r.I11 = C1 * ((2.0 - K2) * k.F - 2.0 * k.E) / K2;

        r.I30 = C3 * K21 * k.E;
        r.I31 = C3 * ((K21 + 1.0) * k.E - 2.0 * k.F) / K2;
        r.I32 = C3 * ((K21 + (7.0 - K2)) * k.E -
                      4.0 * (2.0 - K2) * k.F) / K4;
    } else {
        R = sqrt(R);
        r.I10 = 2.0 * M_PIf64 * R;
        r.I11 = 0.0;
        r.I30 = 2.0 * M_PIf64 * R * R * R;
        r.I31 = 0.0;
        r.I32 = M_PIf64 * R * R * R;
    }

    return r;
}

knl_velocity_t st_knl_ax_velocity(point_t s, point_t t) {
    knl_velocity_t r;
    ellint_t k;

    k = st_ellint_v(s.x, s.y, t.x, t.y);
    r.xx = s.y * k.I10 + (s.x - t.x) * (s.x - t.x) * s.y * k.I30;
    r.xy = (s.x - t.x) * (s.y * s.y * k.I30 - s.y * t.y * k.I31);
    r.yx = (s.x - t.x) * (s.y * s.y * k.I31 - s.y * t.y * k.I30);
    r.yy = (s.y * k.I11 + s.y * (s.y * s.y + t.y * t.y) * k.I31 -
            s.y * s.y * t.y * (k.I30 + k.I32));

    return r;
}

ellint_t st_ellint_p(double64_t x, double64_t y, double64_t x0, double64_t y0) {
    ellint_t r;
    ellipke_t k;
    double64_t R = 1.0 / ((x - x0) * (x - x0) + (y + y0) * (y + y0));
    double64_t K2 = 4.0 * y * y0 * R;

    k = st_ellipke(K2);
    if (fabsl(y0) > 1.0e-8) {
        double64_t K21 = 1.0 / (1.0 - K2);
        double64_t C3 = 4.0 * sqrt(R) * R;

        r.I30 = C3 * K21 * k.E;
        r.I31 = C3 * ((K21 + 1.0) * k.E - 2.0 * k.F) / K2;
    } else {
        R = sqrt(R);
        r.I30 = 2.0 * M_PIf64 * R * R * R;
        r.I31 = 0.0;
    }

    return r;
}

knl_pressure_t st_knl_ax_pressure(point_t s, point_t t) {
    knl_pressure_t r;
    ellint_t k;
    double64_t Rxxx, Rxxy, Rxyx, Rxyy;

    k = st_ellint_p(s.x, s.y, t.x, t.y);
    r.px = 2.0 * s.y * (s.x - t.x) * k.I30;
    r.py = 2.0 * s.y * (s.y * k.I30 - t.y * k.I31);

    Rxxx = 2.0 * s.y * (s.x - t.x) * k.I30;
    Rxxy = 2.0 * s.y * (t.y * k.I30 - s.y * k.I31);
    Rxyx = 2.0 * s.y * (s.y * k.I30 - t.y * k.I31);
    Rxyy = 2.0 * s.y * (s.x - t.x) * k.I31;

    r.rx = ((Rxxx * t.nx + Rxxy * t.ny) * s.nx +
            (Rxyx * t.nx + Rxyy * t.ny) * s.ny);
    r.ry = ((-Rxxy * t.nx + Rxxx * t.ny) * s.nx +
            (-Rxyy * t.nx + Rxyx * t.ny) * s.ny);

    return r;
}
