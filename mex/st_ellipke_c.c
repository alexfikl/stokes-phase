// SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "st.h"
#include <mex.h>

#define USAGE_MESSAGE "usage: [F, E] = st_ellipke_c(K)"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    ellipke_t r;
    double64_t K;

    if (nrhs != 1 || nlhs != 2) {
        mexErrMsgTxt(USAGE_MESSAGE);
    }

    K = (double64_t) mxGetScalar(prhs[0]);
    r = st_ellipke(K);

    plhs[0] = mxCreateDoubleScalar((double) r.F);
    plhs[1] = mxCreateDoubleScalar((double) r.E);
}
