// SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ST_HEADER
#define ST_HEADER

#include <stdlib.h>
#include <math.h>

/* #ifndef USE_DOUBLE */
typedef long double double64_t;
/* #else */
/* typedef double double64_t; */
/* #endif */

typedef struct {
    double64_t x;
    double64_t y;
    double64_t nx;
    double64_t ny;
} point_t;

typedef struct {
    double64_t F;
    double64_t E;
} ellipke_t;

typedef struct {
    double64_t I10;
    double64_t I11;
    double64_t I30;
    double64_t I31;
    double64_t I32;
} ellint_t;


typedef struct {
    double64_t xx;
    double64_t xy;
    double64_t yx;
    double64_t yy;
} knl_velocity_t;

typedef struct {
    double64_t px;
    double64_t py;
    double64_t rx;
    double64_t ry;
} knl_pressure_t;

/***
 * Computes elliptic integrals of the first and second kind.
 */
ellipke_t st_ellipke(double64_t K);

/***
 * Evaluates velocity kernel.
 */
knl_velocity_t st_knl_ax_velocity(point_t s, point_t t);

/***
 * Evaluates pressure kernel.
 */
knl_pressure_t st_knl_ax_pressure(point_t s, point_t t);

#endif
