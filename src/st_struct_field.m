% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function f = st_struct_field(s, name, default_value)
    % Get the field from a struct.
    %
    % This functions similarly to Python's
    % `getter <https://docs.python.org/3/library/stdtypes.html#dict.get>`_
    % on dictionaries.
    %
    % :arg s: struct.
    % :arg name: field name.
    % :arg default_value: default value, if the field does not exist.
    %
    % :returns: field value.

    if isfield(s, name)
        f = s.(name);
    else
        if ~exist('default_value', 'var')
            error('Reference to non-existent field: %s', name);
        else
            f = default_value;
        end
    end
end

% vim:foldmethod=marker:
