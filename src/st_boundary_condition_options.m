% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [bc] = st_boundary_condition_options(varargin)
    % Default values for boundary conditions.
    %
    % Supported fields are:
    %
    % * ``fn``: name for the boundary conditions passed to
    %   :func:`st_sym_boundary` or a struct with the required
    %   boundary conditions. The naming and usage is described in
    %   :func:`st_sym_boundary`.
    % * ``param``: boundary condition parameters. The Capillary number
    %   ``Ca`` and the viscosity ratio ``lambda`` are pretty much always
    %   required.
    % * ``user``: additional user defined options.
    %
    % Unsupported fields are silently ignored.
    %
    % :arg varargin: key-value pairs of options or an existing struct with the
    %   required fields.
    % :returns: a struct with the mentioned fields.

    % {{{ default options

    % boundary conditions
    d.fn = [];
    d.param = struct();
    % user options
    d.user = struct();

    % }}}

    bc = st_struct_update(d, st_struct_varargin(varargin{:}));

    name = bc.fn;
    if ischar(name) || isstring(name)
        bc.fn = st_sym_boundary(char(name), bc.param);
        bc.param = st_struct_merge(bc.param, bc.fn.param, true);
    elseif isstruct(name)
        if isfield(bc.fn, 'param')
            bc.param = st_struct_merge(bc.param, bc.fn.param);
        end
    elseif isempty(name)
        error('boundary conditions not provided');
    else
        error('unknown type of boundary conditions: %s', class(name));
    end
end
