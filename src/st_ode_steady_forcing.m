% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [f] = st_ode_steady_forcing(forcing, ~, x, ~, st)
    % Compute the right-hand side of the kinematic equation.
    %
    % This function provides a right-hand side for :func:`st_ode_steady` and
    % :func:`st_ode_unsteady`. It is given by:
    %
    % .. math::
    %
    %     f(\mathbf{u}, \mathbf{X}) =
    %         (\mathbf{u} \cdot \mathbf{n}) \mathbf{n} +
    %         w \mathbf{t} +
    %         m \kappa \mathbf{n}
    %
    % where :math:`w` is described in Equation 2.2 from [Loewenberg1996]_
    % and :math:`m \kappa` is a mean curvature flow-type term meant to
    % smooth out the interface. The following fields in `options` are
    % used (defaults defined by :func:`st_ode_options`):
    %
    % * ``tangential_forcing``: enables the tangential term.
    % * ``curvature_forcing``: enables the mean curvature term.
    % * ``mobility``: coefficient for the mean curvature flow. A small
    %   coefficient is desired, so that the droplet doesn't collapse to a point.
    %
    % :arg t: current time.
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg st: Stokes solution, containing a field ``u``.
    %
    % :returns: the forcing term for the kinematic steady state equation.

    % References:
    %   [1] M. Loewenberg, E. J. Hinch, Numerical Simulation of a
    %   Concentrated Emulsion in Shear Flow, Journal of Fluid Mechanics,
    %   Vol. 321, 1996.

    % number of points
    npoints = length(x.x);

    % compute normal component
    fn = st_dot(st.u, x.n) .* x.n;

    % compute tangential component
    ft = 0.0;
    if forcing.user.tangential_forcing
        % grid size
        dxi = 0.5 / (npoints - 1);
        % factor
        c = npoints^1.5 / (1.0 + st.param.lambda);

        % compute tangential velocity
        % NOTE: the idea here is that we have two tangent vectors
        % on the left and right that point in opposite directions, i.e.
        % wl will point towards i + 1 and wr will point towards i - 1.
        % however, they are weighted by the curvature, so the sum will be
        % in the direction of the one with larger curvature.

        w = zeros(1, npoints, 'like', x.x);
        for i = 2:npoints - 1
            % contribution from the left
            j = i + 1;
            wl = (1.0 + abs(x.kappa(j))^1.5) * (x.x(j) - x.x(i)) * x.J(j) * dxi;
            % contribution from the right
            j = i - 1;
            wr = (1.0 + abs(x.kappa(j))^1.5) * (x.x(j) - x.x(i)) * x.J(j) * dxi;

            w(i) = c * (wl + wr);
        end

        ft = st_ax_filter(st_dot(w, x.t), 'fft', 5.0e-3);
        ft(1) = 0.0;
        ft(end) = 0.0;

        ft = ft .* x.t;
    end

    % add additional mean curvature
    fk = 0.0;
    if forcing.user.curvature_forcing
        fk = forcing.user.mobility * x.kappa_x .* x.n;
    end

    % add all contributions
    f = fn + ft + fk;
end

% vim:foldmethod=marker:
