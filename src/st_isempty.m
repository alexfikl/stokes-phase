% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = st_isempty(s)
    % Check if a container is empty.
    %
    % Extends the default :func:`isempty` to also work for ``struct``.
    %
    % :returns: boolean.

    if isstruct(s)
        r = isempty(fieldnames(s));
    else
        r = isempty(s);
    end
end

% vim:foldmethod=marker:
