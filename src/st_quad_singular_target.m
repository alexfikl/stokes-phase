% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xi, w] = st_quad_singular_target(x, y, quad)
    % Construct composite quadrature rule for a given target point.
    %
    % This function uses the regular quadrature on all panels except the panel
    % containing ``x``, which gets replaced by the singular quadrature rule
    % defined by ``quad``.
    %
    % :arg x: target point.
    % :arg y: source point information.
    % :arg quad: quadrature rule for singular integration, containing the
    %   ``xi`` and ``w`` fields, for the quadrature nodes and weights on
    %   :math:`[-1, 1]`.
    %
    % :returns: a tuple ``(xi, w)`` for the whole geometry.

    % adjacent panels
    iprev = discretize(x, y.panels, 'IncludedEdge', 'right');
    inext = discretize(x, y.panels, 'IncludedEdge', 'left');
end
