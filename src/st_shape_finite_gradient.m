% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [gradJ, st] = st_shape_finite_gradient(cost, x0, y0, bc, verbose)
    % Compute the finite difference gradient of a cost functional.
    %
    % This function computes:
    %
    % .. math::
    %     \nabla \mathcal{J} \cdot \mathbf{h}_i \approx
    %         \frac{1}{\epsilon}
    %         (\mathcal{J}(\mathbf{X} + \epsilon \mathbf{h}_i) -
    %         \mathcal{J}(\mathbf{X}))
    %
    % where :math:`\mathbf{h}_i \equiv h_i \mathbf{n}` and :math:`h_i` is a
    % bump function.
    %
    % The `cost` argument should be a struct containing information about the
    % cost functional evaluation procedure, as described in
    % :func:`st_shape_adjoint_gradient`. On top of that, it also requires the
    % fields
    %
    % * `eps` (default ``1.0e-7``): finite difference step size.
    % * `bump(x, y, i)`: a function handle that computes the bump
    %   :math:`h_i` at the target position ``i``, see for example
    %   :func:`st_shape_finite_bump`.
    %
    % :arg cost: cost functional information.
    % :arg x0: target point information.
    % :arg y0: source point information.
    % :arg bc: boundary conditions for the Stokes problem.
    % :arg verbose: print debug information.
    % :type verbose: default ``false``
    %
    % :returns: a tuple ``(gradJ, st)``, where ``gradJ`` is a vector of size
    %   ``ntargets`` and ``st`` contains the Stokes solution on the
    %   unperturbed mesh.

    if ~exist('bc', 'var') || isempty(bc)
        bc = struct();
    end

    if ~exist('verbose', 'var')
        verbose = false;
    end

    % get default parameters
    cost.eps = st_struct_field(cost, 'eps', 1.0e-7);
    cost.variables = st_struct_field(cost, 'variables', {});

    % number of target points
    ntargets = length(x0.x);

    % compute unperturbed cost functional
    st = st_repr_density_solution(x0, y0, bc, cost.variables);
    J0 = cost.fn.evaluate(cost, 0.0, x0, y0, st);

    gradJ = zeros(length(J0), ntargets);
    for i = 1:ntargets
        % perturbation bump at current position
        bump = cost.bump(cost, x0, y0, i);

        % construct new geometry
        [x, y] = st_mesh_geometry_update(x0, y0, ...
            x0.x + cost.eps * bump .* x0.n);

        % compute perturbed cost functional
        st_eps = st_repr_density_solution(x, y, bc, cost.variables);
        Jeps = cost.fn.evaluate(cost, 0.0, x, y, st_eps);

        % compute gradient
        gradJ(:, i) = (Jeps - J0) / cost.eps;

        if verbose && length(J0) == 1
            fprintf('[%05d / %05d] gradJeps = %+.5e gradJ = %+.5e\n', ...
                i, ntargets, Jeps - J0, gradJ(i));
        end
    end
end

% vim:foldmethod=marker:
