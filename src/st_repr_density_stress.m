% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [s_ext, s_int, cache] = st_repr_density_stress(x, y, q, bc)
    % Evaluate the on-surface Stokes stress tensor.
    %
    % The complete surface stress tensor is tricky to evaluate on the surface,
    % since all the integral equations relate to the traction, pressure or
    % velocity. However, using those quantities, we can reconstruct the
    % stress itself.
    %
    % We know we have 3 unknown stress components on each side, so we
    % require 3 equations to have a unique solution:
    %
    % * from the traction, we have that the normal component is
    %
    % .. math::
    %
    %     (\mathbf{n} \cdot \sigma^\pm) \cdot \mathbf{n} =
    %         \mathbf{f}^\pm \cdot \mathbf{n}.
    %
    % * we also have that the tangential component is
    %
    % .. math::
    %
    %     (\mathbf{n} \cdot \sigma^\pm) \cdot \mathbf{n} =
    %         \mathbf{f}^\pm \cdot \mathbf{t}.
    %
    % * finally, we can use the definition of the stress tensor to obtain
    %   a third equation:
    %
    % .. math::
    %
    %     (\mathbf{t} \cdot \sigma^\pm) \cdot \mathbf{t} =
    %         \mathbf{f}^\pm \cdot \mathbf{n} +
    %         2 (\mathbf{t} \cdot \nabla \mathbf{u} \mathbf{t} -
    %            \mathbf{n} \cdot \nabla \mathbf{u} \mathbf{n}).
    %
    % We can see that for these steps we require the traction and velocity
    % gradients on both sides of the interface. This boils down to 3 layer
    % potential evaluations.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density at target points.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: a tuple ``(s_ext, s_int)`` containing the on-surface stress
    %   tensor. Each one is an array of size ``(3, ntargets)``, containing
    %   the :math:`\sigma_{xx}, \sigma_{xy}` and :math:`\sigma_{yy}`
    %   components of the stress.

    % traction
    if ~isfield(bc.cache, 'fn_ext')
        [cache.fn_ext, cache.fn_int] = st_repr_density_traction(x, y, q, bc);
    else
        cache.fn_ext = bc.cache.fn_ext;
        cache.fn_int = bc.cache.fn_int;
    end

    % normal velocity gradient
    if ~isfield(bc.cache, 'dudn_ext')
        [cache.dudn_ext, cache.p_int] = st_repr_density_gradu(x, y, q, bc);
    else
        cache.dudn_ext = bc.cache.dudn_ext;
        cache.dudn_int = bc.cache.dudn_int;
    end
    dudn = 0.5 * st_dot(cache.dudn_ext + cache.dudn_int, x.n);

    % tangential velocity gradient
    if isfield(bc.cache, 'u')
        cache.u = st_repr_density_velocity(x, y, q, bc);
    else
        cache.u = bc.cache.u;
    end
    dudt = st_dot(st_derivative(x, cache.u, x.xi, 'fft'), x.t);

    % components in the (t, n) basis
    s_nn_ext = st_dot(cache.fn_ext, x.n);
    s_nn_int = st_dot(cache.fn_int, x.n);

    s_nt_ext = st_dot(cache.fn_ext, x.t);
    s_nt_int = st_dot(cache.fn_int, x.t);

    % NOTE: s_nn_ext - 2.0 * dudn == -p_ext
    s_tt_ext = (s_nn_ext - 2.0 * dudn) + 2.0 * dudt;
    s_tt_int = (s_nn_int - 2.0 * dudn) + 2.0 * dudt;

    % solve
    stress = st_sym_symmetric_tensor_solve();
    s_ext = stress(s_nn_ext, s_nt_ext, s_tt_ext, real(x.n), imag(x.n));
    s_int = stress(s_nn_int, s_nt_int, s_tt_int, real(x.n), imag(x.n));

    if nargout == 1
        s_ext = struct('sigma_ext', s_ext, ...
                       'sigma_int', s_int, ...
                       'cache', cache);
    end
end

% vim:foldmethod=marker:
