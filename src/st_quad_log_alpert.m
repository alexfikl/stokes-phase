% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xi, w] = st_quad_log_alpert(nnodes, y, order, a, b, corra, corrb)
    % Compute Alpert quadrature rules for log-singular integrals.
    %
    % This only contains the quadrature rules provided in
    % [Alpert1999]_. The order of the quadrature rule is :math:`h^p \log h`
    % and we provide the following orders :math:`2, 3, 4, 5, 6, 8, 10, 12, 14`
    % and :math:`16`.
    %
    % :arg nnodes: number of quadrature nodes.
    % :arg y: position of the singularity.
    % :type y: default ``-1``
    % :arg order: order of the quadratur rule.
    % :type order: default ``4``
    % :arg a: domain interval.
    % :type a: default ``-1``
    % :arg b: domain interval.
    % :type b: default ``1``
    % :arg corra: type of correction on left endpoint: ``0`` no correction,
    %   ``1`` regular correction, ``2`` log correction.
    % :type corra: default ``2``
    % :arg corrb: type of correction on right endpoint.
    % :type corrb: default ``1``
    %
    % :returns: a tuple ``(xi, w)`` of quadrature nodes and weights.

    % get parameters
    if ~exist('y', 'var') || isempty(y)
        y = -1.0;
    end

    if ~exist('order', 'var') || isempty(order)
        order = 4;
    end

    if ~exist('a', 'var') || isempty(a)
        a = -1.0;
    end

    if ~exist('b', 'var') || isempty(b)
        b = 1.0;
    end

    if ~exist('corra', 'var') || isempty(corra)
        corra = 2;
    end

    if ~exist('corrb', 'var') || isempty(corrb)
        corrb = 1;
    end

    % check parameters
    if a > b
        error('a > b: %g > %g', a, b)
    end

    if y < a || y > b
        error('singularity not in domain: %g not in [%g, %g]', y, a, b);
    end

    % get endpoint quadrature rules for the desired order
    [x0, w0, c0] = st_quad_corr(corra, order);
    [x1, w1, c1] = st_quad_corr(corrb, order);

    % construct quadrature rules for the whole domain
    h = 1.0 / (nnodes + c0 + c1 - 1.0);
    xc = c0 + (0:nnodes - 1);
    wc = ones(1, nnodes);

    if abs(y - a) < 1.0e-14       % left endpoint
        [xi, w] = st_quad_ab(x0, xc, x1, w0, wc, w1, a, b, h);
    elseif abs(y - b) < 1.0e-14   % right endpoint
        [xi, w] = st_quad_ab(x1, xc, x0, w1, wc, w0, a, b, h);
    else                            % interior
        [xi_l, w_l] = st_quad_ab(x1, xc, x0, w1, wc, w0, a, y, h);
        [xi_r, w_r] = st_quad_ab(x0, xc, x1, w0, wc, w1, y, b, h);
        xi = [xi_l, xi_r];
        w = [w_l, w_r];
    end

    if nargout == 1
        xi = struct('xi', xi, 'w', w);
    end
end

function [xi, w, c] = st_quad_corr(corr, order)
    switch corr
    case 0  % no correction
        xi = 0.0; w = 0.5; c = 1;
    case 1  % regular correction
        [xi, w, c] = st_quad_log_alpert_reg(order);
    case 2  % log correction
        [xi, w, c] = st_quad_log_alpert_log(order);
    otherwise
        error('unknown correction type: %d', corr);
    end
end

function [xi, w] = st_quad_ab(x0, xc, x1, w0, wc, w1, a, b, h)
    xi = [h * x0, h * xc, fliplr(1.0 - h * x1)];
    w = h * [w0, wc, fliplr(w1)];

    xi = a + (b - a) * xi;
    w = (b - a) * w;
end

function [x, w, a] = st_quad_log_alpert_log(order)
    switch order
    case 2
        a = 1.0;
    case 3
        a = 2.0;
    case 4
        a = 2.0;
    case 5
        a = 3.0;
    case 6
        a = 3.0;
    case 8
        a = 5.0;
    case 10
        a = 6.0;
    case 12
        a = 7.0;
    case 14
        a = 9.0;
    case 16
        a = 10.0;
    otherwise
        error('unsupported quadrature order: %d', order);
    end

    persistent alpert;
    if isempty(alpert)
        alpert = st_quad_log_alpert_load('alpert-log-quadrature.mat');
    end

    x = alpert(order).x';
    w = alpert(order).w';
end

function [x, w, a] = st_quad_log_alpert_reg(order)
    switch order
    case {2, 3}
        a = 1.0;
        order = 3;
    case 4
        a = 2.0;
    case 5
        a = 2.0;
    case 6
        a = 3.0;
    case 8
        a = 4.0;
    case {10, 12}
        a = 5.0;
        order = 12;
    case {14, 16}
        a = 7.0;
        order = 16;
    otherwise
        error('unsupported quadrature order: %d', order);
    end

    persistent alpert;
    if isempty(alpert)
        alpert = st_quad_log_alpert_load('alpert-reg-quadrature.mat');
    end

    x = alpert(order).x';
    w = alpert(order).w';
end

function [rules] = st_quad_log_alpert_load(filename)
    cwd = fileparts(mfilename('fullpath'));
    filename = fullfile(fileparts(cwd), 'data', filename);
    if ~exist(filename, 'file') == 2
        error('use `alpert_to_mat` to construct quadrature file: %s', ...
            filename);
    end

    rules = load(filename);
    rules = rules.rules;
end

% vim:foldmethod=marker:
