% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [jump] = st_jump_options(varargin)
    % Constructs a default struct for defining jump conditions for the traction.
    %
    % The main fields accepted are
    %
    % * ``param``: a structure containing flow parameters. The mandatory
    %   parameters are the Capillary number ``Ca`` and the viscosity
    %   ratio ``lambda``.
    % * ``user``: a struct containing user defined options, which is empty
    %   by default.
    % * ``optype``: can be ``'scalar'`` or ``'vector'``, for the normal
    %   component or the full shape gradient, respectively
    %
    % It can also contain a struct ``fn`` of function handles of the type
    % ``f(jump, t, x, y, st)``, where ``st`` is a struct containing the
    % Stokes solution (forward and adjoint, as necessary). The defined fields
    % are the same as for :func:`st_cost_options`.
    %
    % :returns: a struct with the fields initialized to their defaults.

    % {{{ default options

    % flow parameters
    d.param = struct();
    % user parameters
    d.user = struct();
    % operator type
    d.optype = 'scalar';

    % evaluation
    d.fn = jump_default_functions();

    % }}}

    jump = st_struct_update(d, st_struct_varargin(varargin{:}));

    if ~any(contains({'scalar', 'vector'}, jump.optype))
        error('unsupported operator type: `%s`', jump.optype);
    end
end

function [fn] = jump_default_functions()
    fn.evaluate = @jump_not_defined;
    fn.gradient_x = @jump_not_defined;
    fn.gradient_u = @jump_gradient_zero;
    fn.gradient_s = @jump_gradient_zero;
    fn.gradient_ca = @jump_not_defined;

    fn.operator_shape_x = @jump_not_defined;
    fn.operator_x = @jump_not_defined;
end

function [r] = jump_not_defined(~, ~, ~, ~, ~)      %#ok
    error('NotImplementedError: this function needs to be user defined.');
end

function [r] = jump_gradient_zero(~, ~, ~, ~, ~)
    r = 0.0;
end
