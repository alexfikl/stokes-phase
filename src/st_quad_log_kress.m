% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [x, w] = st_quad_log_kress(nnodes, weight, p, y, a, b)
    % Quadrature nodes and weights for singular integrals.
    %
    % Can be used for integrals of the type:
    %
    % .. math::
    %
    %     \int_{-1}^1 f(x)\, \mathrm{d} x \approx \sum f(x_i) w_i,
    %
    % where :math:`f(x)` has potential singularities at the endpoints. The
    % work is achieved by a change of variables with an appropriate function.
    % The weight functions can be:
    %
    %
    % * ``kress``: Equation 1.33 in [Chunrungsikul2001]_ with a parameter
    %   `p`. Larger `p` generally makes the points cluster more towards the
    %   extremities.
    % * ``chun``: Equation 1.32 in [Chunrungsikul2001]_ with a parameter
    %   `p`. Larger `p` generally makes the points cluster more towards the
    %   extremities, even more so than for `kress`.
    % * ``sidi``: Equation 9.46 in [Kress1998]_ or Equation 2.2 in [Sidi1993]_
    %   with a parameter `p`.
    %
    % :arg nnodes: number of quadrature nodes.
    % :arg weight: weight function.
    % :type weight: default ``'sidi'``.
    % :arg p: weight function parameters.
    % :type p: default `4`.
    % :arg a: domain starting point.
    % :type a: default ``-1``.
    % :arg b: domain endpoint.
    % :type b: default ``1``.
    %
    % :returns: a tuple ``(x, w)`` of quadrature nodes and weights.

    % REFERENCES:
    %   [1] R. Kress, Numerical Analysis, 1998, Springer.
    %   [2] S. Chunrungsikul, Numerical Quadrature Methods for Singular
    %   and Nearly Singular Integrals, 2001.
    %   [3] A. Sidi, A New Variable Transformation for Numerical Integration,
    %   1993.

    if ~exist('weight', 'var') || isempty(weight)
        weight = 'sidi';
    end

    if ~exist('p', 'var') || isempty(p)
        p = 4;
    end

    if p > 10
        % right after [1, Equation 9.47]
        warning('large p will likely cause underflow errors.');
    end

    if ~exist('a', 'var') || isempty(a)
        a = -1.0;
    end

    if ~exist('b', 'var') || isempty(b)
        b = 1.0;
    end

    if ~exist('y', 'var') || isempty(y)
        y = b;
    end

    % uniform nodes
    N = floor((nnodes - 1) / 2);
    if mod(nnodes, 2) == 0
        k = [-N - 1:-1, 1:N + 1];
    else
        k = -N:N;
    end

    % nodes and weights [1, Equation 9.36]
    weight = lower(weight);
    switch weight
    case 'kress'
        w = st_kress(p);
    case 'chun'
        w = st_chun(p);
    case 'sidi'
        w = st_sidi(p);
        % NOTE: because of the trigonometric functions, the weights and
        % nodes are defined on [0, 1] and translated to [-1, 1];
        k = 1:nnodes;
    otherwise
        error('unknown weight function: %s', weight);
    end
    assert(length(k) == nnodes);

    x = sym('x', 'real');
    w0 = matlabFunction(w);
    wp = matlabFunction(diff(w, x));

    N = max(k);
    x = w0(k / (N + 1));
    w = wp(k / (N + 1)) / (N + 1);

    [x, w] = st_quad_translate(x, w, a, b, y);
    if nargout == 1
        x = struct('xi', x, 'w', w);
    end
end

function [w] = st_kress(p)
    syms x real;

    % [2, Equation 1.33]
    vp = ((1 / 2 - 1 / p) * x^3 + 1 / p * x + 1 / 2)^p;
    vm = subs(vp, x, -x);
    w = (vp - vm) / (vp + vm);
end

function [w] = st_chun(p)
    syms x real;

    % [2, Equation 1.32]
    vp = (1 + x)^p;
    vm = subs(vp, x, -x);
    w = (vp - vm) / (vp + vm);
end

function [w] = st_sidi(p)
    syms x t real;

    % [1, Equation 9.46]
    w = int(sin(pi * t)^p, t, 0, x) / int(sin(pi * t)^p, t, 0, 1);
    % switch to [-1, 1]
    w = (w - 0.5) / 0.5;
end

% vim:foldmethod=marker:
