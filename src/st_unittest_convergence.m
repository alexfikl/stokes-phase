% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [eoc, convergence, problem] = st_unittest_convergence(variables, varargin)
    % Test convergence for a given problem setup.
    %
    % :arg variables: cell array with the variable names, used in file names
    %   and such.
    % :arg varargin: additional arguments passed to
    %   :func:`st_unittest_convergence_options` and :func:`st_unittest_problem`
    %
    % :return: a struct with the errors and orders at each step, as described
    %   in :func:`st_util_eoc`.

    % {{{ handle options

    options = st_struct_varargin(varargin{:}, 'variables', variables);
    convergence = st_unittest_convergence_options(options);

    if isfield(options, 'user')
        options = rmfield(options, 'user');
    end

    problem = st_unittest_problem(options);
    problem.user = convergence.user;
    geometry = problem.geometry;

    if isempty(convergence.evaluate)
        error('`evaluate` not provided');
    end

    % }}}

    % {{{ setup

    nvariables = numel(variables);

    % cache
    cache = convergence.cache;

    % errors
    eoc = st_util_eoc(variables, length(convergence.resolutions));

    % plotting
    plt = struct();
    if convergence.enable_plotting
        plt.variables = convergence.variables;
        plt.labels = convergence.labels;

        plt.fig = gobjects(1, 2 * nvariables);
        plt.fax = gobjects(1, 2 * nvariables);

        for m = 1:length(plt.fig)
            plt.fig(m) = figure();
            plt.fax(m) = gca();
        end

        plt.max_ax_ylim = -flintmax;
        plt.min_ax_ylim = flintmax;
    end

    % }}}

    % {{{ loop

    for n = 1:length(convergence.resolutions)
        geometry = convergence.geometry_update(...
            rmfield(geometry, {'x', 'y'}), ...
            convergence.resolutions(n));

        % unroll
        x = geometry.x;
        y = geometry.y;

        % evaluate solutions
        ex = st_evaluate_cached(convergence, problem, x, y, 'ex');
        ap = st_evaluate_cached(convergence, problem, x, y, 'ap');

        % compute errors
        for m = 1:nvariables
            v_ap = ap.(variables{m});
            v_ex = ex.(variables{m});

            v_er = convergence.norm(x, y, v_ap, v_ex) + 1.0e-16;
            eoc.errors.(variables{m})(n) = v_er;
        end
        eoc.npoints(n) = x.npanels;
        eoc.h(n) = st_mesh_h_max(x, y);

        eoc = st_util_eoc(eoc);

        % plot
        plt = st_plot_variables(plt, x, y, ap, ex, ...
            n == length(convergence.resolutions));
    end

    % }}}

    % {{{ plot

    st_print_variables(plt, cache);

    if convergence.enable_geometry_plot
        zx = st_array_ax(x.x, true);
        % zv = st_array_ax(x.vertices, true);

        fig = figure();
        plot(real(zx), imag(zx), '-');
        % plot(real(zv), imag(zv), 'ko');
        xlabel('$x$');
        ylabel('$\rho$');
        axis('equal');
        st_print(fig, cache.filename(cache, 'geometry', 'png'));
        close(fig);
    end

    % }}}

    % {{{ errors

    % compute and plot convergence
    if convergence.enable_plotting
        filename = cache.filename(cache, 'order', 'png');
    else
        filename = [];
    end

    eoc.global_orders = st_util_eoc_show(eoc, filename, convergence.user, ...
        'display', ~convergence.enable_latex_eoc, ...
        'expected_order', convergence.expected_order, ...
        'legend_labels', convergence.labels);

    % print a latex table
    if convergence.enable_latex_eoc
        st_convergence_table(eoc, convergence.labels, convergence.variables);
    end

    % }}}

    if convergence.enable_caching
        save(cache.filename(cache, 'setup'), ...
            'convergence', 'problem', 'eoc', ...
            '-v7.3');
    end
end

% {{{ evaluation

function [r] = st_evaluate_cached(convergence, problem, x, y, method)
    cache = convergence.cache;

    % check if cache exist
    if convergence.enable_caching
        filename = cache.filename(cache, ...
            sprintf('variables_%s_%05d', method, x.npanels));
        from_cache = exist(filename, 'file') == 2;
    else
        from_cache = false;
    end

    % get data
    if from_cache
        r = load(filename);
    else
        r = convergence.evaluate(problem, x, y, method);
        if convergence.enable_caching
            save(filename, '-struct', 'r', '-v7.3');
        end
    end
end

% }}}

% {{{ plotting

function [plt] = st_print_variables(plt, cache)
    if st_isempty(plt)
        return;
    end

    nvariables = length(plt.variables);
    min_ax_ylim = floor(plt.min_ax_ylim);
    max_ax_ylim = ceil(plt.max_ax_ylim);

    for m = 1:nvariables
        n = m;
        name = plt.variables{m};

        % pointwise errors
        set(plt.fax(n), 'YMinorTick', 'on');
        set(get(plt.fax(n), 'YAxis'), 'TickDirection', 'both');

        xlabel(plt.fax(n), '$\xi$');
        ylabel(plt.fax(n), sprintf('$\\log_{10} %s$', plt.labels{m}));
        yticks(plt.fax(n), min_ax_ylim:max_ax_ylim);
        if abs(max_ax_ylim - min_ax_ylim) > 1.0e-14
            ylim(plt.fax(n), [min_ax_ylim, max_ax_ylim]);
        end
        xticks(plt.fax(n), linspace(0.0, 0.5, 6));
        xlim(plt.fax(n), [0.0, 0.5]);
        st_print(plt.fig(n), ...
            cache.filename(cache, sprintf('%s_error', name), 'png'));

        % solutions
        n = nvariables + m;
        xlabel(plt.fax(n), '$\xi$');
        ylabel(plt.fax(n), sprintf('$%s$', plt.labels{m}));
        xlim(plt.fax(n), [0.0, 0.5]);
        st_print(plt.fig(n), ...
            cache.filename(cache, sprintf('%s_value', name), 'png'));

        close(plt.fig(n));
    end
end

function [plt] = st_plot_variables(plt, x, y, ap, ex, is_last)
    if st_isempty(plt)
        return;
    end

    nvariables = numel(plt.variables);
    for m = 1:nvariables
        name = plt.variables{m};

        % errors
        dv = ex.(name) - ap.(name);
        dv = log10(abs(dv) + 1.0e-16);

        % limits
        plt.max_ax_ylim = max(plt.max_ax_ylim, max(dv));
        plt.min_ax_ylim = min(plt.min_ax_ylim, min(dv));

        % abcissa
        if isfield(ap, 'xi') && isfield(ap.xi, name)
            xi = ap.xi.(name);
        else
            if isfield(x, 'xi') && length(dv) == length(x.xi)
                xi = x.xi;
            elseif isfield(y, 'xi') && length(dv) == length(y.xi)
                xi = y.xi;
            else
                xi = linspace(0, 1, length(dv));
            end
        end

        % plot
        plot(plt.fax(m), xi, dv, '-');
        plot(plt.fax(nvariables + m), xi, ap.(name), '--');
        if is_last
            plot(plt.fax(nvariables + m), xi, ex.(name), 'k-');
        end
    end
end

% }}}

% {{{ latex convergence

function st_convergence_table(eoc, labels, variables)
    table = struct();
    table.fmt = 'l';
    table.h = struct('values', eoc.npoints, ...
                     'header', 'M', ...
                     'fmt', '%5d');
    for m = 1:length(variables)
        name = variables{m};
        table.(name) = struct('values', eoc.errors.(variables{m}), ...
                              'header', labels{m}, ...
                              'fmt', '%.6e');
        name = sprintf('%s_eoc', name);
        table.(name) = struct('values', eoc.orders.(variables{m}), ...
                              'header', 'EOC', ...
                              'fmt', '%.2f');

        table.fmt = strcat(table.fmt, '|ll');
    end

    st_util_latex_table(table);
end

% }}}

% vim:foldmethod=marker:
