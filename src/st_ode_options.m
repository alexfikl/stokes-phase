% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [o] = st_ode_options(varargin)
    % Options for the kinematic condition time stepping.
    %
    % The admissible fields are
    %
    % * ``display``: display additional information per timestep.
    % * ``stepper``: time stepping method: ``'euler'``, ``'imex'``, ``'ssprk3'``.
    % * ``tmax``: final time for the simulation.
    % * ``maxit``: maximum number of timesteps.
    % * ``dt``: time step. If negative, then it will be determined at runtime.
    % * ``theta``: Courant number used in a CFL condition if the time step
    %   is not fixed.
    % * ``utol``: tolerance on the max norm of the normal velocity field
    %   for steady state computations.
    % * ``xtol``: tolerance on the relative interface deformation for
    %   steady state computations.
    % * ``odefreq``: output frequency based on the time step.
    % * ``odeplot(t, x, y, st)``: output function handle that takes the current
    %   time, the geometry information and the solution at the current time.
    % * ``solver``: integral representation used for solving for the velocity
    %   field and other variables. This is only used in :func:`st_ode_steady`.
    % * ``filter_strength``: used to smooth the interface at each time step
    %   using :func:`st_ax_filter`.
    % * ``checkpoints``: a cell array of files that can be used to retrieve
    %   forward solutions when solving the adjoint equations in
    %   :func:`st_ode_unsteady_adjoint`.
    %
    % Additional parameters can be passed in an unchecked ``'user'`` field.
    % The default values can be obtained by calling this function without
    % any parameters.
    %
    % :returns: options struct will missing fields filled in with their
    %   default values.

    % {{{ default options

    % print info each iteration
    global verbosity;
    d.display = ~isempty(verbosity) && verbosity;
    % filter strength
    d.filter_strength = -1;
    d.filter_freq = 0;

    % time stepping method
    d.stepper = 'euler';
    % maximum evolution time
    d.tmax = Inf;
    % maximum number of time steps
    d.maxit = Inf;
    % time step (if negative, determined at runtime)
    d.dt = -1.0;
    % CFL condition
    d.theta = 0.1;
    d.timestep = @default_ode_timestep;

    % steady state tolerance
    d.utol = 1.0e-4;
    d.xtol = [];
    % plotting tolerance
    d.odefreq = 1;
    % plotting function
    d.odeplot = [];

    % equidistancing
    d.enable_equidistance = false;
    d.equidistance = struct('freq', 0);

    % solver
    d.solver = 'density';
    % checkpoints
    d.checkpoints = {};

    % user defined parameters
    d.param = struct();
    d.user = struct();

    % }}}

    % {{{ customize

    o = st_struct_update(d, st_struct_varargin(varargin{:}));

    % turn into an integer
    if islogical(o.display)
        if o.display
            o.display = Inf;
        else
            o.display = 0;
        end
    end

    if ~isfield(o.equidistance, 'pfilter')
        o.equidistance.pfilter = o.filter_strength;
    end

    % tolerance
    if isempty(o.xtol)
        o.xtol = o.utol^2;
    end

    % stepper
    if ~any(strcmp(o.stepper, {'euler', 'imex', 'ssprk3', 'ssprk3a'}))
        error('unknown time stepping method: %s', o.stepper);
    end

    % if the time step is fixed, we can deduce the number of iterations
    if o.dt > 0
        if ~isinf(o.tmax) && isinf(o.maxit)
            o.maxit = floor(o.tmax / o.dt);
            o.dt = o.tmax / o.maxit + 1.0e-15;
        elseif isinf(o.tmax) && ~isinf(o.maxit)
            o.tmax = o.dt * o.maxit;
        else
            tmax = o.dt * o.maxit;
            if abs(tmax - o.tmax) > 1.0e-11
                error('`tmax`, `maxit` and `dt` are inconsistent');
            end
        end
    end

    % }}}
end

function [dt] = default_ode_timestep(ode, t, x, y, st, ~)
    if ode.dt > 0.0
        dt = ode.dt;
    else
        % get grid scaling
        h_min = st_mesh_h_min(x, y);

        % get velocity scaling
        if isstruct(st)
            if isfield(st, 'fn');
                U_min = min(abs(st.fn.uinf(x)));
            else
                U_min = min(abs(st.u));
            end
        else
            U_min = 1.0
        end

        if abs(U_min) < 1.0e-10
            U_min = 1.0;
        end
        U_min = 1.0;

        % get surface tension scaling
        if isfield(ode.param, 'gamma')
            gamma_max = max(abs(ode.param.gamma));
        else
            gamma_max = 1.0;
        end

        if abs(gamma_max) < 1.0e-10
            gamma_max = 1.0;
        end

        % from [Denner2015], Equation 4
        Ca = ode.param.Ca * (U_min * h_min / gamma_max);
        dt = 0.25 * ode.theta * sqrt(Ca) * h_min;
    end

    dt = min(dt, ode.tmax - t);
end

% vim:foldmethod=marker:
