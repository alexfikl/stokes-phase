% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [S] = st_layer_slp_complete(x, y, q)
    % Evaluate the completion to a single-layer potential.
    %
    % Evaluates the regular layer-potential
    %
    % .. math::
    %
    %     \mathcal{N}[\mathbf{q}](\mathbf{x}_0) =
    %     n_i(\mathbf{x}_0) \int_\Sigma n_j(\mathbf{x}) q_j(\mathbf{x})
    %     \,\mathrm{d}\mathbf{x}
    %
    % This can be used to complete the single-layer potential, which has
    % normal vectors in its nullspace.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: if the density is given, then we evaluate the layer potential,
    %   otherwise, a matrix operator is returned.

    if exist('q', 'var') && ~isempty(q)
        S = st_layer_slp_complete_apply(x, y, q);
    else
        % NOTE: we are not using st_layer_evaluate here because this is a regular
        % integral and does not require special treatment
        S = st_layer_slp_complete_matrix(x, y);
    end
end

function [S] = st_layer_slp_complete_apply(x, y, q)
    % interpolate density
    q = st_interpolate(x, q, y.xi);
    % integrate density
    S = st_ax_integral(y, st_dot(q, y.n));

    % compute layer
    S = S * x.n;
end

function [S] = st_layer_slp_complete_matrix(x, y)
    % number of target collocation points
    ntargets = length(x.x);
    % number of source panels
    npanels = length(x.panels) - 1;
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);
    % number of basis functions
    nbasis = length(x.nodes.basis);

    % evaluate basis at source points
    b_s = st_interp_evaluate_basis(x, y);

    % compute the operator
    Sxx = zeros(ntargets);
    Sxy = zeros(ntargets);
    Syx = zeros(ntargets);
    Syy = zeros(ntargets);

    for i = 1:ntargets
        for j = 1:npanels
            k = ((j - 1) * (nbasis - 1) + 1):(j * (nbasis - 1) + 1);

            for p = ((j - 1) * nnodes + 1):(j * nnodes)
                ds = 2.0 * pi * imag(y.x(p)) * y.J(p) * y.w(p);

                Sxx(i, k) = Sxx(i, k) + ...
                    real(x.n(i)) * real(y.n(p)) * b_s(p, :) .* ds;
                Sxy(i, k) = Sxy(i, k) + ...
                    real(x.n(i)) * imag(y.n(p)) * b_s(p, :) .* ds;
                Syx(i, k) = Syx(i, k) + ...
                    imag(x.n(i)) * real(y.n(p)) * b_s(p, :) .* ds;
                Syy(i, k) = Syy(i, k) + ...
                    imag(x.n(i)) * imag(y.n(p)) * b_s(p, :) .* ds;
            end
        end
    end

    % construct full operator
    S = [Sxx, Sxy; ...
         Syx, Syy];
end

% vim:foldmethod=marker:
