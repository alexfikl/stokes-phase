% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [intf] = st_ax_integral(y, f)
    % Compute the axisymmetric integral over a surface.
    %
    % The integral is given by:
    %
    % .. math::
    %
    %     \int_S f(x) \,\mathrm{d}x = 2 \pi \int_P f(r(s), s) r(s) \,\mathrm{d}s,
    %
    % where :math:`S = [0, 2 \pi] \times P` is the surface and
    % :math:`P` is the slice in the top :math:`x-y` plane, for `f` not depending
    % on :math:`\phi`, the toroidal direction.
    %
    % :arg y: source geometry information.
    % :arg f: variable prescribed at all the source points (see
    %   :func:`st_interpolate`). The variable can also be a function handle
    %   that takes the source geometry ``y`` as input.
    %
    % :returns: surface integral.

    % make sure our variable is at the source points
    if isa(f, 'function_handle')
        f = f(y);
    elseif length(f) == 1
        % a constant is fine
    elseif length(f) ~= length(y.x)
        error('f must be prescribed at source nodes. see st_interpolate.')
    end

    % compute integral
    intf = 2.0 * pi * sum(f .* imag(y.x) .* y.J .* y.w);
end

% vim:foldmethod=marker:
