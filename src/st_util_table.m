% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function st_util_table(data, fid, sep, eol)
    if ~exist('fid', 'var') || isempty(fid)
        fid = 1;
    end

    if ~exist('sep', 'var') || isempty(sep)
        sep = '  ';
    end
    sep = sprintf(' %s ', sep);

    if ~exist('eol', 'var') || isempty(eol)
        eol = '';
    end
    eol = sprintf('%s\n', eol);

    persistent allowed_variable_classes;
    if isempty(allowed_variable_classes)
        allowed_variable_classes = {'int16', 'int32', 'int64', ...
                                    'single', 'double', ...
                                    'char', 'string'};
    end

    % fields
    fields = fieldnames(data);
    % number of columns
    ncolumns = length(fields);
    % cleanup data
    for m = 1:ncolumns
        variable = data.(fields{m});
        if ~isstruct(variable)
            variable = struct('values', variable);
        end

        % check data type
        if iscell(variable.values)
            c_class = class(variable.values{1});
        else
            c_class = class(variable.values);
        end
        if ~any(strcmp(c_class, allowed_variable_classes))
            error('unknow data type for variable "%s": %s', fields{m}, c_class);
        end

        % update data
        variable.header = st_struct_field(variable, 'header', fields{m});
        variable.fmt = st_struct_field(variable, 'fmt', st_display_fmt(c_class));

        data.(fields{m}) = variable;
    end

    % find maximum number of rows
    nrows = intmin;
    for n = 1:ncolumns
        nrows = max(nrows, length(data.(fields{m}).values));
    end

    % headers
    c_table = cell(nrows + 1, ncolumns);
    for m = 1:ncolumns
        c_table{1, m} = data.(fields{m}).header;
    end

    % print everything to strings and find max column width
    for n = 1:nrows
        for m = 1:ncolumns
            variable = data.(fields{m});
            c_value = nan;

            if length(variable.values) >= n
                fmt = variable.fmt;
                if iscell(variable.values)
                    c_value = variable.values{n};
                else
                    c_value = variable.values(n);
                end
            end

            if isnan(c_value)
                fmt = '%s';
                c_value = '---';
            end

            c_table{n + 1, m} = sprintf(fmt, c_value);
        end
    end

    fmt = cell(1, ncolumns);
    for m = 1:ncolumns
        width = max(arrayfun(@(n) length(c_table{n, m}), 1:nrows + 1));
        fmt{m} = sprintf('%%+%ds', width);
    end

    % print to file
    c_row = cell(1, ncolumns);
    for n = 1:nrows + 1
        for m = 1:ncolumns
            c_row{m} = sprintf(fmt{m}, c_table{n, m});
        end
        fprintf(fid, '%s%s', strjoin(c_row, sep), eol);
    end
end

function fmt = st_display_fmt(c_class)
    switch c_class
    case { 'int16', 'int32', 'int64' }
        fmt = '%4d';
    case 'single'
        fmt = '%5.2f';
    case 'double'
        fmt = '%.5e';
    case { 'char', 'string' }
        fmt = '%s';
    otherwise
        error('unknown data type: %s', c_class);
    end
end

% vim:foldmethod=marker:
