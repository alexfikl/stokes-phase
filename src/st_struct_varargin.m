% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [s] = st_struct_varargin(varargin)
    % Convert a cell array to a struct.
    %
    % The first argument can be a struct, whose fields will be overwritten
    % and the remaining parameters must be pairs of ``("Name", value)``, in the
    % usual MATLAB way. The same value can appear multiple times, but the last
    % occurence will overwrite any previous ones.
    %
    % :returns: a struct with appropriate fields.

    s = struct();
    if isempty(varargin)
        return;
    end

    c = varargin;
    if isstruct(c{1})
        s = c{1};
        c = c(2:end);
    end

    if isempty(c)
        return;
    end

    if mod(numel(c), 2) ~= 0
        error('fields must come in pairs.');
    end

    pairs = reshape(c, 2, []);
    s0 = struct();
    for n = 1:size(pairs, 2)
        s0.(pairs{1, n}) = pairs{2, n};
    end
    s = st_struct_merge(s, s0, true);
end

% vim:foldmethod=marker:
