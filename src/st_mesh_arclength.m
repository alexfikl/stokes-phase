% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [s] = st_mesh_arclength(x, y)
    % Compute arclength of each panel.
    %
    % :arg x: target point information.
    % :arg y: source point information.
    %
    % :return: panel arclength.

    s = sum(reshape(y.J .* y.w, length(y.quad.unit_xi), []), 1);
end

