% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function r = st_shape_finite_bump(cost, x, y, f)
    % Smooth bump function for finite differencing shapes.
    %
    % The bump function we use is a modulated exponential. The exact
    % parameters can be attached to the `cost` struct. Namely, we have
    %
    % * `bump_type`: can be ``'x'`` or ``'xi'``, so the
    %   bump is parametrized by the physical coordinates or the
    %   computational domain coordinates, respectively.
    % * `sigma`: gives the variance of the exponential. If not provided,
    %   a default is computed using :func:`st_shape_finite_bump_sigma`.
    %
    % :arg cost: struct containing information about the bump function.
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg f: if it is a single integer, the bump function is evaluted at
    %   that target point. If it is an array, then it is convoluted with
    %   the bump function.

    sigma = st_shape_finite_bump_sigma(cost, x, y);

    switch cost.bump_type
    case 'x'
        bump = @(z, n) exp(-abs(z.x - x.x(n)).^2 / (2.0 * sigma)) .* ...
            sin(2.0 * pi * cos(2.0 * pi * z.xi));
    case 'xi'
        bump = @(z, n) exp(-abs(z.xi - x.xi(n)).^2 / (2.0 * sigma)) .* ...
            sin(2.0 * pi * cos(2.0 * pi * z.xi));
    otherwise
        error('unknown `bump_type`: %s', cost.bump_type);
    end

    if cost.bump_normalized
        bump_fn = @(z, n) bump(z, n) / st_ax_norm(y, bump(y, n), 2);
    else
        bump_fn = bump;
    end

    if numel(f) == 1 && (isinteger(f) || floor(f) == f)
        r = bump_fn(x, f);
    else
        r = st_bump_convolve(x, y, f, bump_fn);
    end
end

function [r] = st_bump_convolve(x, y, f, bump)
    ntargets = length(x.x);
    nsources = length(y.x);

    if numel(f) == ntargets
        f = st_interpolate(x, reshape(f, size(x.x)), y.xi);
    elseif numel(f) == nsources
        % nothing to do
    elseif numel(f) == 1
        % nothing to do
    else
        error('f size does not match source or target: %d', length(f));
    end

    r = zeros(1, ntargets);
    for i = 1:ntargets
        r(i) = st_ax_integral(y, f .* bump(y, i));
    end
end

% vim:foldmethod=marker:
