% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function g = st_mesh_target_geometry(x)
    % Compute geometry using Fourier-based interpolation.
    %
    % The Fourier coefficients for the parametrization are also returned.
    % Note that they describe the whole curve, not just the top half-plane,
    % since they are required to be periodic. We can use them to sample
    % different points in the following way:
    %
    % .. code-block:: matlab
    %
    %     n = length(zhat);
    %     xi_new = sort(rand(0.5, n + 10));
    %     x_new = zhat * exp(-zk' * xi_new) / n;
    %
    % The resulting geometry struct contains the following fields
    %
    % * ``x``: points: complex array ``(npoints,)``.
    % * ``n``: normal: complex array ``(npoints,)``.
    % * ``t``: tangent: complex array ``(npoints,)``.
    % * ``kappa``: mean curvature: real array``(npoints,)``.
    % * ``kappa_x``: first principal curvature (xy plane):
    %   real array ``(npoints,)``.
    % * ``kappa_p``: second principal curvature: real array ``(npoints,)``.
    % * ``J``: Jacobian of the transformation :math:`s(\xi)`, from arc length
    %   to a uniform parametrization :math:`\xi \in [0, 0.5]`.
    % * ``zk``: wavenumber corresponding to the Fourier coefficients.
    % * ``zhat``: Fourier coefficients for the coordinates.
    % * any fields copied from `x` that are not in the above list.
    %
    % :arg x: point coordinates in counter-clockwise order. Can be an array of
    %   size ``(1, npoints)`` of complex numbers (which transform to 2D
    %   coordinates in the usual way :math:`(\Re\{x\}, \Im\{x\})`) or a struct
    %   containing a field `x`, which is such a complex array.
    %
    % :returns: a struct containing geometry information.

    % construct points on the whole domain
    if isstruct(x)
        y = x.x;
    else
        y = x;
    end

    if abs(imag(y(1))) > 1.0e-13
        warning('right point not on axis: %.5e', abs(imag(y(1))));
    end
    if abs(imag(y(end))) > 1.0e-13
        warning('left point not on axis: %.5e', abs(imag(y(end))));
    end
    if min(imag(y)) < -1.0e-13
        warning('points not in top half-plane: %.5e', min(imag(y)));
    end

    % sizes
    n = length(y);
    m = 2 * (n - 1);

    % fill in the bottom half of the geometry
    z = st_array_ax(y);
    % sampling frequency on the [0, 1] domain
    zk = 1.0j * st_fftfreq(m);

    % compute derivatives
    zhat = fft(z);
    dx = ifft(zk .* zhat);
    ddx = ifft(zk.^2 .* zhat);

    % cut off bottom half
    z = z(1:n); dx = dx(1:n); ddx = ddx(1:n);

    % compute metric term
    J = abs(dx);
    % compute normal
    normal = -1.0j * dx ./ J;
    % compute curvature in the (x, y) plane
    kappa_x = -st_dot(normal, ddx) ./ J.^2;
    % compute mean curvature
    kappa_p = kappa_x;
    kappa_p(2:end - 1) = imag(normal(2:end - 1)) ./ imag(z(2:end - 1));

    % put it all together
    g.n = normal;
    g.t = 1.0j * normal;
    g.kappa = kappa_x + kappa_p;
    g.kappa_x = kappa_x;
    g.kappa_p = kappa_p;
    g.J = J;

    g.x = z;
    g.zk = zk;
    g.zhat = zhat;

    % add any additional parameters
    if isstruct(x)
        g = st_struct_merge(x, g, true);
    end

    % update panel endpoints
    % NOTE: convenience when x was obtained from st_point_collocation
    if isfield(g, 'nbasis')
        nbasis = g.nbasis;
    elseif isfield(g, 'nodes')
        nbasis = length(g.nodes.basis);
    else
        nbasis = -1;
    end

    if nbasis > 0
        g.vertices = g.x(1:nbasis - 1:end);
    end
end

% vim:foldmethod=marker:
