% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [jump] = st_jump_zero(varargin)
    % Defines a uniformly zero jump condition.
    %
    % This jump is meant for the zero surface tension (or infinite Capillary
    % number) case.
    %
    % :returns: a struct compatible with :func:`st_jump_options`.

    jump = st_jump_options(varargin{:});

    jump.fn.evaluate = @jump_zero_evaluate;
    jump.fn.gradient_x = @jump_zero_evaluate;
    jump.fn.gradient_ca = @jump_zero_evaluate;
    jump.fn.operator_x = @jump_zero_evaluate;
end

function [r] = jump_zero_evaluate(~, ~, ~, ~, ~)
    r = 0.0;
end
