% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [problem] = st_unittest_problem(varargin)
    % Setup a discretization and analytic solution for testing.
    %
    % The input is a set of key-value pairs, where the value is a struct
    % of options for the component defined by the key. They supported
    % components are:
    %
    %   * ``boundary_condition``: see :func:`st_boundary_condition_options`.
    %     Note that this is based on :func:`st_sym_boundary`, so some choices
    %     can also have a full analytic solution, not just boundary conditions.
    %   * ``adjoint_boundary_condition``: also constructs an adjoint
    %     solution.
    %   * ``cost``: see :func:`st_cost_options`.
    %   * ``jump``: see :func:`st_jump_options`.
    %   * ``forcing``: see :func:`st_forcing_options`.
    %   * ``geometry``: see :func:`st_geometry_options`.
    %   * ``param``: flow parameters, which are overwritten by the ones
    %     defined for ``boundary_condition``. They are also propageted to
    %     all the other components, as needed.
    %
    % :arg varargin: a struct or key-value pairs with the options.
    % :returns: a struct containing the geometry, solution and parameters.

    options = st_struct_varargin(varargin{:});
    options.param = st_struct_field(options, 'param', struct());

    problem = struct();

    % {{{ boundary conditions

    % get boundary conditions
    if isfield(options, 'boundary_condition')
        bc_options = get_component_options(options, 'boundary_condition');
        bc = st_boundary_condition_options(bc_options);

        problem.bc = bc;
        options.param = bc.param;
    end

    if isfield(options, 'adjoint_boundary_condition')
        adjoint_options = get_component_options(options, 'adjoint_boundary_condition');
        problem.adjoint_bc = st_boundary_condition_options(adjoint_options);
    end

    % }}}

    % {{{ cost

    geometry_options = get_component_options(options, 'geometry');
    s_singularity = st_struct_field(geometry_options, 's_singularity', {});

    if isfield(options, 'cost') ...
            && ~isempty(options.cost) ...
            && ~isempty(options.cost.cost_type)
        cost_options = get_component_options(options, 'cost');

        switch cost_options.cost_type
        case 'surface_area'
            cost = st_cost_surface_area(cost_options);
        case 'centroid'
            cost = st_cost_centroid(cost_options);
        case 'geometry'
            cost = st_cost_geometry(cost_options);
        case 'normal_velocity'
            cost = st_cost_normal_velocity(cost_options);
        case 'exterior_traction'
            cost = st_cost_exterior_traction(cost_options);
        otherwise
            warn_type_not_available(cost_options, 'cost');
            cost = st_cost_options(cost_options);
        end

        s_singularity = unique([s_singularity, ...
            cost.variables, ...
            cost.forward_variables, ...
            cost.adjoint_variables]);

        problem.cost = cost;
    end

    % }}}

    % {{{ jump conditions

    if isfield(options, 'jump') && ~isempty(options.jump)
        jump_options = get_component_options(options, 'jump');

        switch jump_options.jump_type
        case 'angle'
            jump = st_jump_angle(jump_options);
        case 'capillary'
            jump = st_jump_capillary(jump_options);
        case 'hadamard'
            jump = st_jump_hadamard(jump_options);
        case 'capillary_variable'
            jump = st_jump_capillary_variable(jump_options);
        case 'gravity'
            jump = st_jump_gravity(jump_options);
        case 'gravity_capillary'
            jump_c = st_jump_capillary(jump_options);
            jump_g = st_jump_gravity(jump_options);
            jump = st_gradient_combine('jump_c', jump_c, 'jump_g', jump_g);
        case 'gravity_capillary_variable'
            jump_c = st_jump_capillary_variable(jump_options);
            jump_g = st_jump_gravity(jump_options);
            jump = st_gradient_combine('jump_c', jump_c, 'jump_g', jump_g);
        case 'zero'
            jump = st_jump_zero(jump_options);
        otherwise
            warn_type_not_available(jump_options, 'jump');
            jump = st_jump_options(jump_options);
        end

        % reset problem jump to the requested one
        if isfield(problem, 'bc') && isfield(problem.bc.fn, 'jump')
            problem.bc.fn.jump = @(x) jump.fn.evaluate(jump, 0.0, x, x, jump);
            problem.bc.jump = jump;
        end

        problem.jump = jump;
    end

    % }}}

    % {{{ forcing

    if isfield(options, 'forcing') && ~isempty(options.forcing)
        forcing_options = get_component_options(options, 'forcing');

        switch forcing_options.forcing_type
        case 'normal_velocity'
            forcing = st_forcing_normal_velocity(forcing_options);
        otherwise
            warn_type_not_available(forcing_options, 'forcing');
            forcing = st_forcing_options(forcing_options);
        end

        problem.forcing = forcing;
        s_singularity = unique([s_singularity, forcing.variables]);
    end

    % }}}

    % {{{ optimization

    if isfield(options, 'optim') && ~isempty(options.optim)
        optim_options = get_component_options(options, 'optim');
        problem.optim = st_optim_options(optim_options);
    end

    % }}}

    % {{{ ode

    if isfield(options, 'ode') && ~isempty(options.ode)
        ode_options = get_component_options(options, 'ode');
        problem.ode = st_ode_options(ode_options);
    end

    % }}}

    % {{{ geometry

    if isfield(options, 'geometry')
        geometry_options.s_singularity = {};

        geometry = st_geometry(geometry_options);

        if isfield(problem, 'cost') && ~isempty(cost.desired_curve)
            x = geometry.x;
            x.x = cost.desired_curve(x.xi);

            if length(x.x) ~= length(x.xi)
                error('`desired_curve` not evaluated at required points');
            end

            [problem.cost.x, problem.cost.y] = st_mesh_geometry(x, geometry.y);
        end

        v = get_gradient_variables(problem, geometry.x, geometry.y);
        if isfield(problem, 'cost')
            problem.cost.forward_variables = v.forward_variables;
            problem.cost.adjoint_variables = v.adjoint_variables;
        end

        s_singularity = unique([s_singularity, ...
            v.forward_variables, ...
            v.adjoint_variables]);

        if numel(s_singularity) > 0
            quad = st_layer_quadrature(geometry.x, geometry.y, ...
                s_singularity, geometry.s_quad);

            geometry.y.quad = st_struct_merge(geometry.y.quad, quad, true);
        end

        problem.geometry = geometry;
    end

    % }}}
end

function [o] = get_component_options(options, name)
    o = st_struct_field(options, name, struct());
    if isempty(o)
        o = struct();
    end

    o.param = st_struct_field(o, 'param', struct());
    o.param = st_struct_merge(o.param, options.param, true);

    name_type = sprintf('%s_type', name);
    o.(name_type) = lower(st_struct_field(o, name_type, 'custom'));
end

function warn_type_not_available(options, name)
    name_type = sprintf('%s_type', name);
    name_type = options.(name_type);

    if strcmp(name_type, 'custom') == 0
        warning('falling back to `st_%s_options`. unknown `%s_type`: %s', ...
            name, name, name_type);
    else
        warning('falling back to `st_%s_options`. custom `%s_type` provided.', ...
            name, name);
    end
end

function [v] = get_gradient_variables(problem, x, y)
    v.forward_variables = {};
    v.adjoint_variables = {};

    if ~isfield(problem, 'cost')
        return;
    end

    cost = problem.cost;
    if isfield(problem, 'jump')
        lambda = problem.jump.param.lambda;

        with_jumps = false;
        if abs(lambda - 1.0) > 1.0e-14
            with_jumps = true;
        else
            j = problem.jump.fn.evaluate(problem.jump, 0.0, x, y, struct());
            j_dot_t = st_dot(j, x.t);

            if norm(j_dot_t, Inf) > 1.0e-13
                with_jumps = true;
            end
        end

        if with_jumps
            v.forward_variables = {'two_phase', 'dudn', 'eps', 'fn'};
            v.adjoint_variables = {'two_phase', 'dudn', 'eps', 'fn'};
        end

        if isfield(problem, 'forcing')
            v.forward_variables = unique([...
                v.forward_variables, problem.forcing.variables]);
        end

        v.forward_variables = unique([...
            v.forward_variables, cost.forward_variables, cost.variables]);
        v.adjoint_variables = unique([...
            v.adjoint_variables, cost.adjoint_variables, 'u']);
    else
        % handle exterior problem case?
    end
end

% vim:foldmethod=marker:
