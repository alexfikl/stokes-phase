% SPDX-FileCopyrightText: 1999 C. Pozrikidis
% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
%
% SPDX-License-Identifier: LGPL-2.1-or-later

function [F, E, flag]= st_ellipke(x, x0)
    % Compute elliptic integrals.
    %
    % Matches the interface and results of the default MATLAB ``ellipke``,
    % but is a lot faster (and sufficient for our needs). The function
    % essentially computes:
    %
    % .. code-block:: matlab
    %
    %     K2 = 4 * Y * Y0 / ((X - X0)^2 + (Y + Y0)^2);
    %     [F, E] = ellipke(K2);
    %
    % :arg x: source point.
    % :arg x0: target point.
    %
    % :returns: a tuple ``(F, E)`` of elliptic integrals of the first and
    %   second kind.
    %
    % :seealso: the ``05_stokes/drop_ax/ell_int.m`` function in the
    %   `BEMLIB <http://dehesa.freeshell.org/BEMLIB/>`__ sources.

    K = 4.0 * imag(x) * imag(x0) / ((real(x) - real(x0))^2 + (imag(x) + imag(x0))^2);
    % [F, E] = ellipke(K); return;
    % [F, E] = st_ellipke_c(K); return;

    F = 0.5 * pi;
    E = 1.0;
    G = 1.0;
    B = sqrt(K);
    D = 1.0;

    % NOTE: this will hit the limit for K = 1
    it = 0;
    maxit = 32;
    while abs(D) > 1.0e-12 && it < maxit
        C = sqrt(1 - B^2);
        B = (B / (1 + C))^2;
        D = F * B;
        F = F + D;
        G = G * B / 2;
        E = E + G;

        it = it + 1;
    end
    flag = it == maxit;

    E = F * (1 - K * E / 2);
end

% vim:foldmethod=marker:
