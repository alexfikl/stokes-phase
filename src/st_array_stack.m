% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [v] = st_array_stack(w)
    % Transform a complex array into a stacked real array.
    %
    % We represent two-dimensional vector fields as complex one component arrays.
    % This function can be used to stack the real and imaginary component so
    % as to make a real array of size :math:`2 n` of all the DOFs.
    %
    % Note that if a real array is passed in, the imaginary component is
    % assumed to be zero and the stacking proceeds in the same way.
    %
    % :arg w: complex representation of a vector field.
    % :arg shape: shape of the resulting array.
    % :type shape: If not given, the result will be a row or column vector
    %   the same as *w*.
    %
    % :returns: stacked array.

    % don't stack constants
    if length(w) == 1
        v = w;
        return;
    end

    v = st_array_reshape_like([real(w), imag(w)], w);
end

% vim:foldmethod=marker:
