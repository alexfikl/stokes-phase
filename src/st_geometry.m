% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [geometry] = st_geometry(varargin)
    % Setup geometry information.
    %
    % :returns: a struct compatible with :func:`st_geometry_options`.

    geometry = st_geometry_options(varargin{:});

    % {{{ construct computational meshes

    if isempty(geometry.x)
        if geometry.nbasis > 0
            geometry.x = st_point_collocation(geometry.npanels, ...
                geometry.nbasis, geometry.curve);
        else
            geometry.x = struct('npanels', geometry.npanels);
        end
    end

    if isempty(geometry.y)
        if geometry.nnodes > 0
            geometry.y = st_point_quadrature(geometry.npanels, ...
                geometry.nnodes, geometry.curve, geometry.quadtype);

            if numel(geometry.s_singularity) > 0
                quad = st_layer_quadrature(geometry.x, geometry.y, ...
                    geometry.s_singularity, geometry.s_quad);

                geometry.y.quad = st_struct_merge(geometry.y.quad, quad, true);
            end
        else
            geometry.y = struct('npanels', geometry.npanels);
        end
    end

    % }}}

    % {{{ compute normals, curvatures, etc.

    if isfield(geometry.x, 'x')
        geometry.x = st_mesh_target_geometry(geometry.x);

        if isfield(geometry.y, 'xi')
            geometry.y = st_mesh_source_geometry(geometry.x, geometry.y);
        end
    end

    % }}}
end

% vim:foldmethod=marker:
