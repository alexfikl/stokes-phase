% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [grad] = st_gradient_combine(varargin)
    % Gathers all the gradients from the input structures.
    %
    % The inputs are given as key-value pairs. The values must be structs
    % (see :func:`st_cost_options`) with the structure ``obj.fn.gradient_{VAR}``.
    % This function returns a similar struct with an ``fn`` field that sums
    % up all the values from the various other inputs.
    %
    % The intended usage is as follows
    %
    % .. code:: matlab
    %
    %     param = struct('Ca', 0.1, 'lambda', 1.0);
    %     jump_g = st_jump_gravity('param', param);
    %     jump_c = st_jump_capillary('param', param);
    %     jump = st_gradient_combine(...
    %         'gravity', jump_g, 'capillary', jump_c, ...
    %         'weights', struct('jump_c', 1.0, 'jump_g', 0.5));
    %
    % A similar setup can be used for structures conforming to
    % :func:`st_cost_options` or :func:`st_forcing_options`.
    %
    % :returns: a struct with the structure ``grad.fn.gradient_{VAR}``.

    % construct parts
    members = st_struct_varargin(varargin{:});
    if isfield(members, 'weights')
        weights = members.weights;
        members = rmfield(members, 'weights');
    else
        fields = fieldnames(members);
        weights = repmat({1}, numel(fields), 1);
        weights = cell2struct(weights, fields);
    end
    grad.weights = weights;
    grad.members = members;

    % merge user and param structs
    grad.user = struct();
    grad.param = struct();
    grad.optype = [];
    for field = fieldnames(grad.members)'
        member = grad.members.(field{1});
        grad.user = st_struct_merge(grad.user, member.user, true);
        grad.param = st_struct_merge(grad.param, member.param, true);

        if isfield(member, 'optype')
            if isempty(grad.optype)
                grad.optype = member.optype;
            else
                assert(strcmp(grad.optype, member.optype));
            end
        end
    end

    % set up all functions
    grad.fn.evaluate = @(g, t, x, y, st) grads_evaluate(g, t, x, y, st, 'evaluate');
    grad.fn.gradient_x = @(g, t, x, y, st) grads_evaluate(g, t, x, y, st, 'gradient_x');
    grad.fn.gradient_u = @(g, t, x, y, st) grads_evaluate(g, t, x, y, st, 'gradient_u');
    grad.fn.gradient_s = @(g, t, x, y, st) grads_evaluate(g, t, x, y, st, 'gradient_s');
    grad.fn.gradient_ca = @(g, t, x, y, st) grads_evaluate(g, t, x, y, st, 'gradient_ca');

    grad.fn.operator_x = @(g, t, x, y, st) grads_evaluate(g, t, x, y, st, 'operator_x');
end

function [r] = grads_evaluate(grad, t, x, y, st, name)
    if startsWith(name, 'gradient') || startsWith(name, 'evaluate')
        expected_size = size(x.x);
    elseif startsWith(name, 'operator')
        expected_size = length(st_array_stack(x.x));
        expected_size = [expected_size, expected_size];
    else
        error('unknown type: %s', name);
    end

    r = 0.0;
    for field = fieldnames(grad.members)'
        obj = grad.members.(field{1});
        if ~isfield(obj.fn, name)
            error('`%s` does not have a `%s`', field{1}, name);
        end
        fn = obj.fn.(name);

        r_update = fn(obj, t, x, y, st);
        if length(r_update) > 1
            if ~all(size(r_update) == expected_size)
                error('`%s` for field `%s` gives inconsistent size.', ...
                    name, field{1});
            end
        end

        r = r + r_update;
    end
end
