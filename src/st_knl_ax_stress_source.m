% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function Q = st_knl_ax_stress_source(Q, x, x0, I50, I51, I52, I53)
    % Axisymmetric free space Stresslet.
    %
    % The layer potential corresponding to this kernel is computed in
    % :func:`st_layer_dlp`.
    %
    % This function evaluates the axisymmetric Stresslet corresponding to
    % the velocity representation in :func:`st_layer_dlp`. This has the normal
    % vector evaluated at the source point, leading to slightly different
    % formulae.
    %
    % The exact axisymmetric form can be retrieved from the :math:`q`
    % coefficients in Appendix B of [Pozrikidis1990]_.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg I: elliptic integrals, see :func:`st_ellint_order5`.
    %
    % :returns: an array of size ``(2, 2, 2)`` containing the kernel components.
    % :see also: :func:`st_ellipke`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.
    %   [2] C. Pozrikidis, The Instability of a Moving Viscous Drop,
    %   Journal of Fluid Mechanics, 1990.

    X  = real(x);   Y  = imag(x);
    X0 = real(x0);  Y0 = imag(x0);
    Y2 = Y * Y;     Y02 = Y0 * Y0;
    Y3 = Y2 * Y;    Y03 = Y02 * Y0;
    XX0 = (X - X0)^2;

    % compute coefficients
    % see [1, Equation 2.4.6] q coefficients
    YX = -6.0 * Y * (X - X0);
    Q(1, 1, 1) = YX * XX0 * I50;
    Q(1, 1, 2) = YX * (X - X0) * (Y * I50 - Y0 * I51);
    Q(1, 2, 1) = Q(1, 1, 2);
    Q(1, 2, 2) = YX * (Y02 * I52 + Y2 * I50 - 2.0 * Y * Y0 * I51);

    YX = -6.0 * Y;
    Q(2, 1, 1) = YX * XX0 * (Y * I51 - Y0 * I50);
    Q(2, 1, 2) = YX * (X - X0) * ((Y2 + Y02) * I51 - Y * Y0 * (I50 + I52));
    Q(2, 2, 1) = Q(2, 1, 2);
    Q(2, 2, 2) = YX * (Y3 * I51 - Y2 * Y0 * (I50 + 2.0 * I52) + ...
                       Y * Y02 * (I53 + 2.0 * I51) - Y03 * I52);
end

% vim:foldmethod=marker:
