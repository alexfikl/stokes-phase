% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [g] = st_sym_geometry(name, opts, sym_only)
    % Symbolic representation of curves in :func:`st_mesh_curve`.
    %
    % :arg name: see :func:`st_mesh_curve`.
    % :arg opts: see :func:`st_mesh_curve`.
    % :returns: a struct with function handles for all the variables that
    %   are returned by :func:`st_mesh_target_geometry`.

    if ~exist('opts', 'var') || isempty(opts)
        opts = struct();
    end

    if ~exist('sym_only', 'var') || isempty(sym_only)
        sym_only = false;
    end

    % define parametrization parameter
    xi = sym('xi', 'real');

    name = lower(name);
    switch name
    case 'circle'
        r = st_struct_field(opts, 'R', 1.0);
    case 'ellipse'
        a = st_struct_field(opts, 'a', 1.0);
        b = st_struct_field(opts, 'b', 2.0);
        r = a * b ./ sqrt((a * cos(2.0 * pi * xi)).^2 + (b * sin(2.0 * pi * xi)).^2);
    case 'flower'
        R = st_struct_field(opts, 'R', 1.0);
        r = R * (1.0 + 6.0 / 10.0 * cos(12 * pi * xi)) * ...
                (1.0 + 4.0 / 10.0 * cos(2 * pi * xi));
    case 'ufo'
        R = st_struct_field(opts, 'R', 1.0);
        k = st_struct_field(opts, 'k', 4.0);
        r = R * (sqrt(9.0 * cos(2.0 * pi * xi).^2 + sin(2.0 * pi * xi).^2) + ...
                 cos(2.0 * pi * k * xi).^2);
    otherwise
        error('unknown geometry name: \"%s\"', name);
    end

    %
    % symbolic
    %

    % geometry
    x = r * cos(2.0 * pi * xi);
    y = r * sin(2.0 * pi * xi);
    % derivatives
    dx = diff(x, xi); ddx = diff(x, xi, 2);
    dy = diff(y, xi); ddy = diff(y, xi, 2);

    J = sqrt(dx^2 + dy^2);
    tx = dx / J; ty = dy / J;
    nx = ty; ny = -tx;
    kappa_x = (ddy * dx - ddx * dy) / J^3;
    kappa_p = ny / y;
    kappa = kappa_x + kappa_p;

    g.xi = xi;
    g.x = x;
    g.y = y;
    g.J = J;
    g.tx = tx; g.ty = ty;
    g.nx = nx; g.ny = ny;
    g.kappa_x = kappa_x;
    g.kappa_p = kappa_p;
    g.kappa = kappa;

    %
    % function handles
    %

    if sym_only
        return
    end

    g = struct('sym', g);
    g.curve = matlabFunction(simplify(x + 1j * y), 'Vars', xi);

    x0 = sym('x', 'real'); x1 = sym('y', 'real');
    g.J = to_handle(J);
    g.t = to_handle(tx + 1j * ty);
    g.n = to_handle(nx + 1j * ny);
    g.kappa_x = to_handle(kappa_x);
    g.kappa_p = to_handle(kappa_p);
    g.kappa = to_handle(kappa);

    function [g] = to_handle(fn)
        g = subs(simplify(fn), xi, atan2(x1, x0) / (2.0 * pi));
        g = subs(g, abs(x0 + 1j * x1), sqrt(x0^2 + x1^2));

        g = st_sym_handle(simplify(g), [x0, x1]);
    end
end

% vim:foldmethod=marker:
