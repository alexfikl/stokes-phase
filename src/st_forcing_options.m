% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [forcing] = st_forcing_options(varargin)
    % Constructs a default struct for defining the forcing condition.
    %
    % The main fields accepted are
    %
    % * ``param``: a structure containing flow parameters. The mandatory
    %   parameters are the Capillary number ``Ca`` and the viscosity
    %   ratio ``lambda``.
    % * ``user``: a struct containing user defined options, which is empty
    %   by default.
    % * ``variables``: a cell array definings the variables required to
    %   compute the forcing term, passed to :func:`st_repr_density_solution`.
    %
    % It must also contain a struct ``fn`` of function handles of the type
    % ``f(forcing, t, x, y, st)``, where ``st`` is a struct containing the
    % Stokes solution (forward and adjoint, as necessary). The defined fields
    % are are the same as for :func:`st_cost_options`.
    %
    % :returns: a struct with the fields initialized to their defaults.

    % {{{ default options

    % flow parameters
    d.param = struct();
    % user parameters
    d.user = struct();

    % variables required to compute the forcing term
    d.variables = {};
    % evaluation
    d.fn = forcing_default_functions();

    % }}}

    forcing = st_struct_update(d, st_struct_varargin(varargin{:}));
end

function [fn] = forcing_default_functions()
    fn.evaluate = @forcing_not_defined;
    fn.gradient_x = @forcing_not_defined;
    fn.gradient_u = @forcing_not_defined;
    fn.gradient_s = @forcing_gradient_zero;
    fn.gradient_Ca = @forcing_gradient_zero;

    fn.operator_x = @forcing_not_defined;
end

function [r] = forcing_not_defined(~, ~, ~, ~, ~)   %#ok
    error('NotImplementedError: this function needs to be user defined.');
end

function [r] = forcing_gradient_zero(~, ~, ~, ~, ~)
    r = 0.0;
end
