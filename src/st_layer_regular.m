% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [S] = st_layer_regular(x, y, q, ~)
    % Evaluate a regular kernel.
    %
    % This function is meant solely for testing the quadrature rules in a
    % realistic scenario, similar to the case where singularity subtraction is
    % used to regularize the kernel. It uses the following layer potential
    %
    %   .. math::
    %
    %       \int_\Sigma K(x, y) \,\mathrm{d}S
    %
    % where the kernel is :math:`\mathcal{C}^0` continuous and given by
    %
    %   .. math::
    %
    %       K(x, y) = |\Re\{x\} - \Re\{y\}|.
    %
    % :arg x: target point information.
    % :arg y: source point information.
    % :arg q: density (not used).

    knl.quad = y.quad.reg;

    if exist('q', 'var') && ~isempty(q)
        knl.S = zeros(size(x.x));
        knl.evaluate = @st_layer_regular_apply_kernel;

        knl = st_layer_evaluate(x, y, knl, q);
        S = knl.S;
    else
        error('NotImplementedError');
    end
end

function [knl] = st_layer_regular_apply_kernel(knl, target, source)
    % target index
    i = target.i;
    % source and target points
    y = source.x;
    x = target.x;

    S0 = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        phi = atan2(imag(x(i)), real(x(i)));
        theta = atan2(imag(y(n)), real(y(n)));
        K = 5 ./ (theta + 1) .* abs(cos(theta) - cos(phi));

        S0 = S0 + K * dS;
    end

    knl.S(i) = knl.S(i) + S0;
end

% vim:foldmethod=marker:
