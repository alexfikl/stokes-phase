% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cost] = st_cost_centroid(varargin)
    % Defines a cost functional that maximizes the centroid movement.
    %
    % It is given by
    %
    % .. math::
    %
    %     \mathcal{J} = \frac{\beta}{2} \|X_c(T) - X_0\|^2
    %         + \frac{\alpha}{2} \|\gamma\|_2^2,
    %
    % where :math:`X_c(T)` is the :math:`x` component of the centroid at the
    % final time and :math:`X_0` is a desired value.
    %
    % Additional user parameters:
    %
    % * ``'x0'``: desired centroid position.
    % * ``'beta'``: weighting for the first term in :math:`[-\infty, \infty]`.
    %   If negative, this is a maximization problem instead.
    % * ``'alpha'``: weighting for the second term in :math:`[0, \infty]`.
    %   If negative, the second term is disabled.
    % * ``'tmax'``: final time.
    %
    % :returns: a struct compatible with :func:`st_cost_options`.

    % {{{ defaults

    d.user.x0 = 0.0;
    d.user.alpha = -1.0;
    d.user.beta = -1.0;
    d.param.tmax = 0.0;
    d.variables = {'two_phase'};

    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    % }}}

    cost = st_cost_options(d);
    if ~isfield(cost.param, 'gamma')
        cost.user.alpha = -1.0;
    end

    cost.fn.evaluate = @cost_centroid_evaluate;
    cost.fn.gradient_x = @cost_centroid_gradient_x;
    cost.fn.gradient_u = @cost_centroid_gradient_zero;
    cost.fn.gradient_s = @cost_centroid_gradient_zero;
    cost.fn.gradient_ca = @cost_centroid_gradient_ca;

    cost.fn.operator_x = @cost_centroid_operator_x;
end

% {{{ gradients

function [r] = cost_centroid_evaluate(cost, t, x, y, ~)
    r = 0.0;
    alpha = cost.user.alpha;
    beta = cost.user.beta;

    if abs(t - cost.param.tmax) < 1.0e-14
        xc = st_mesh_centroid(x, y);
        r = beta / 2.0 * abs(xc - cost.user.x0)^2;

        if alpha > 0
            gamma = st_interpolate(x, cost.param.gamma, y.xi);
            r = r + alpha / 2.0 * st_ax_norm(y, gamma, 2).^2;
        end
    end
end

function [r] = cost_centroid_gradient_x(cost, t, x, y, ~)
    r = zeros(size(x.x));
    alpha = cost.user.alpha;
    beta = cost.user.beta;

    if abs(t - cost.param.tmax) < 1.0e-14
        xc = st_mesh_centroid(x, y);
        volume = st_mesh_volume(x, y);

        r = beta * st_dot(xc - cost.user.x0, x.x - xc) / volume;
        if alpha > 0
            r = r + alpha / 2.0 * x.kappa .* cost.param.gamma.^2;
        end

        switch cost.optype
        case 'scalar'
            M = st_ops_mass(x, y);
            r = (M * r')';
        case 'vector'
            r = r .* x.n;
        end
    end
end

function [r] = cost_centroid_gradient_zero(~, ~, ~, ~, ~)
    r = 0.0;
end

function [r] = cost_centroid_gradient_ca(cost, t, ~, ~, ~)
    r = 0.0;
    alpha = cost.user.alpha;

    if abs(t - cost.param.tmax) < 1.0e-14 && alpha > 0
        r = alpha * cost.param.gamma;
    end
end

% }}}

% {{{ operators

function [op] = cost_centroid_operator_x(cost, t, ~, ~, ~)
    if abs(t - cost.param.tmax) < 1.0e-14
        error('operator not available at tmax.');
    end

    op = 0.0;
end

% }}}
