% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function M = st_knl_ax_velocity(M, x, x0, I10, I11, I30, I31, I32)
    % Axisymmetric free space Stokeslet.
    %
    % The layer potential corresponding to this kernel is computed in
    % :func:`st_layer_slp`.
    %
    % The Stokeslet is given by Equation 2.2.8 in [Pozrikidis1992]_. In
    % axisymmetic coordinates, it can be found in Equation 2.4.5.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg I: elliptic integrals, see :func:`st_ellint_order3`.
    %
    % :returns: an array of size ``(2, 2)`` containing the kernel components.
    % :see also: :func:`st_ellipke`.

    X  = real(x);  Y  = imag(x);
    X0 = real(x0); Y0 = imag(x0);

    % kernel
    M(1, 1) = Y * I10 + (X - X0)^2 * (Y * I30);
    M(1, 2) = (X - X0) * (Y * Y * I30 - Y * Y0 * I31);
    M(2, 1) = (X - X0) * (Y * Y * I31 - Y * Y0 * I30);
    M(2, 2) = Y * I11 + (Y * (Y^2 + Y0^2)) * I31 - Y^2 * Y0 * (I30 + I32);
end

% vim:foldmethod=marker:
