% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [q] = st_repr_electric(x, y, S, bc)
    % Solve a two-phase electric problem.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg S: electric conductivity ratio.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: density on the surface.
    % :see also: :func:`st_layer_electric_dlp`.

    % compute freestream boundary condition
    einf = st_dot(bc.fn.einf(x), x.n);
    % coefficient
    kappa = (1 - S) / (1 + S);
    % compute right-hand side
    q = kappa / (2.0 * pi) * einf;

    if abs(S - 1.0) < 1.0e-14
        return;
    end

    % compute operator
    D = st_layer_electric_dlp(x, y, [], 'target');
    D = eye(size(D)) - kappa / (2.0 * pi) * D;

    % solve system
    linopts.RECT = true;
    q = linsolve(D, q', linopts)';
end
