% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xi, w] = st_quad_fejer(nnodes)
    % Fejer's second quadrature nodes and weights.
    %
    % The quadrature rule is of order :math:`n`, see [Waldvogel2006]_.
    %
    % :arg nnodes: number of quadrature nodes.
    % :returns: a tuple ``(x, w)`` of quadrature nodes and weights.

    % quadrature nodes: Chebychev points
    theta = (1:nnodes) * pi / (nnodes + 1);
    xi = cos(theta);

    % weights
    w = zeros(size(xi));
    j = 1:floor((nnodes + 1) / 2);
    for k = 1:nnodes
        w(k) = 4 / (nnodes + 1) * sin(theta(k)) * ...
            sum(sin((2 * j - 1) * theta(k)) ./ (2 * j - 1));
    end

    if nargout == 1
        xi = struct('xi', xi, 'w', w);
    end
end

% vim:foldmethod=marker:
