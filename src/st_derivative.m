% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [df] = st_derivative(x, f, xi, method)
    % Compute the derivative with respect to arclength of a given function.
    %
    % The available methods to compute derivatives are:
    %
    % * ``'fft'`` and ``'fft_xi'``, which use the FFT algorithm and compute
    %   derivatives with respect to arclength and :math:`\xi`, respectively.
    % * ``'lagrange'`` and ``'lagrange_xi'``, which use the Lagrange
    %   basis on `x`. Note that this cannot be used to compute derivatives at
    %   panel endpoints.
    %
    % :arg x: collocation point information.
    % :arg f: function to differentiate. We assume the function
    %   values are given at the collocation points.
    % :arg xi: points at which to compute the derivative.
    % :arg method: method used to compute derivatives.
    % :type method: default ``'lagrange'``.
    %
    % :return: an array `df` of the same size as `f`.

    if ~exist('method', 'var')
        method = 'lagrange';
    end
    method = lower(method);

    if isstruct(xi)
        xi = xi.xi;
    end

    if isa(f, 'function_handle')
        f = f(x);
    end

    df = 0.0;
    if length(f) == 1
        return;
    end

    ntargets = length(x.xi);
    if length(f) ~= ntargets
        error('incorrect size: f must be given at collocation points.');
    end

    switch method
    case 'fft'
        df = st_derivative_fft(x, f, xi, true);
    case 'fft_xi'
        df = st_derivative_fft(x, f, xi, false);
    case 'lagrange'
        df = st_derivative_lgr(x, f, xi, true);
    case 'lagrange_xi'
        df = st_derivative_lgr(x, f, xi, false);
    otherwise
        error('unknown differentiation method: %s', method);
    end

    if isreal(f)
        df = real(df);
    end
end

function [df] = st_derivative_fft(x, f, xi, arclength)
    % make periodic
    f = st_array_ax(f);
    % compute transform
    zk = 1.0j * st_fftfreq(length(f));
    zhat = fft(f);

    % compute derivative
    df = (zk .* zhat) * exp(-zk' * xi) / length(zk);

    % divide by arclength
    if arclength
        if length(x.xi) == length(xi) && norm(x.xi - xi) < 1.0e-14
            ds = x.J;
        else
            ds = abs((x.zk .* x.zhat) * exp(-x.zk' * xi) / length(x.zk));
        end
        df = df ./ ds;
    end
end

function [df] = st_derivative_lgr(x, f, xi, arclength)
    df = st_interp_evaluate(x, f, xi, 1);

    % compute arclength derivative, if possible
    if ~arclength
        return;
    end

    if isfield(x, 'J')
        ds = st_interpolate(x, x.J, xi);
        df = df ./ ds;
    else
        error('need metric to compute arclength derivative');
    end
end

% vim:foldmethod=marker:
