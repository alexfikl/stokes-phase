% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xstar, state] = st_ode_unsteady_adjoint(xf, yf, ...
        bc, forcing, cost, checkpoint, varargin)
    % Solve adjoint system.
    %
    % This function is a companion to :func:`st_ode_unsteady` and takes
    % very similar inputs.
    %
    % :arg xf: final target point information.
    % :arg yf: final source point information.
    % :arg bc: boundary conditions for the forward problem used to compute
    %   variables required in the adjoint gradient. They should follow the
    %   interface defined by :func:`st_boundary_condition_options`.
    % :arg forcing: source term for the forward problem. It should follow the
    %   interface defined by :func:`st_forcing_options`.
    % :arg cost: cost function information. It should follow the interface
    %   defined by :func:`st_cost_options`.
    % :arg fw: forward state information, as returned by :func:`st_ode_unsteady`.
    %   This is mainly used to retrieve the appropriate checkpoints.
    % :arg checkpoint: a cache created using :func:`st_cache_create`. This
    %   is used to save the adjoint solution.
    % :arg varargin: any additional time stepper ode that are passed in
    %   to :func:`st_ode_options`.
    %
    % :returns: adjoint-based gradient.

    % {{{ ode

    % check checkpointing
    enable_checkpointing = ~isempty(checkpoint);
    enable_iteration_checkpointing = false;
    if enable_checkpointing
        enable_iteration_checkpointing = isempty(checkpoint.file_series_id);
    end

    if ~enable_iteration_checkpointing
        warning('iterations are not checkpointed');
    end

    % parse all options
    ode = st_ode_options(varargin{:});
    ode.variables = unique([forcing.variables, cost.variables, cost.forward_variables]);

    % match with forward, if available
    if isfield(ode.user, 'forward')
        forward = ode.user.forward;

        % check that the information in the forward / adjoint matches
        if isinf(ode.maxit)
            ode.maxit = forward.maxit;
        else
            assert(forward.maxit == ode.maxit);
        end
        assert(abs(forward.tmax - ode.tmax) < 1.0e-14);
    end

    if isinf(ode.maxit)
        ode.maxit = length(ode.checkpoints);
    end
    assert(length(ode.checkpoints) == ode.maxit + 1);

    % check jump
    assert(strcmp(bc.jump.optype, 'vector'));
    assert(strcmp(cost.optype, 'vector'));

    if ode.display >= 2
        disp(ode);
        disp(ode.param);
        disp(cost);
    end

    % }}}

    % {{{ initialize variables

    t = ode.tmax;

    % compute "initial" condition
    % NOTE: initial condition can only come from the cost functional, which
    % does not depend on the adjoint variables, so this is sufficient
    st.forward = st_repr_density_solution(xf, yf, bc, cost.forward_variables);
    xstar = cost.fn.gradient_x(cost, ode.tmax, xf, yf, st);
    st.adjoint.x = xstar;

    % initialize variables
    state.times = ode.tmax;
    state.grad = cost.fn.gradient_ca(cost, ode.tmax, xf, xf, st);
    state.xstar_norm = norm(real(xstar), Inf) + 1.0j * norm(imag(xstar), Inf);
    state.fstar_norm = [];
    state.checkpoints = {};

    state.timing.geometry = [];
    state.timing.forward = [];
    state.timing.adjoint = [];
    state.timing.update = [];
    state.timing.total = [];

    grads = st_gradient_combine(...
        'cost', cost, 'jump', bc.jump, 'forcing', forcing);

    switch ode.stepper
    case 'euler'
        explicit_grads = grads;
    case 'imex'
        explicit_grads = st_gradient_combine(...
            'cost', cost, 'jump', bc.jump);
    case {'ssprk3', 'ssprk3a'}
        explicit_grads = grads;
    otherwise
        error('unknown method: %s', ode.stepper);
    end

    forcingstar = st_forcing_adjoint(...
        'param', bc.param, ...
        'user', struct('grads', explicit_grads));

    % load forward solution and check that everything matches
    state_f = load(ode.checkpoints{end});
    assert(abs(state_f.t - ode.tmax) < 1.0e-14);
    assert(norm(state_f.stages.k0 - xf.x) < 1.0e-14);

    % save all variables to pass to `update`
    problem.x = xf;
    problem.y = yf;
    problem.ode = ode;
    problem.bc = bc;
    problem.cost = cost;
    problem.grads = grads;
    problem.forcing = forcing;
    problem.forcingstar = forcingstar;

    % }}}

    % {{{ loop

    % at each iteration (going from t^{n + 1} -> t^{n})
    %
    %   1. compute forward solution at time t^n
    %      using the geometry at time t^n
    %   2. compute adjoint solution at time t^{n + 1}
    %      using geometry at time t^n and xstar at time t^{n + 1}
    %   3. compute right-hand side of the adjoint equation
    %   4. update xstart
    %   5. compute gradient with respect to Ca
    %      using the solutions at time t^{n + 1} (it just depends on ustar).

    if enable_checkpointing
        [checkpoint, filename] = checkpoint.fileseries(checkpoint, 'evolution');
        save_checkpoint(filename, st, ode.maxit, ode.tmax);
    end

    x = xf;
    y = yf;

    progress = st_util_progress('nitems', ode.maxit);
    last_progress_line_size = -1;

    for n = ode.maxit:-1:1
        if ode.display >= 1
            if ode.display == 1 && last_progress_line_size > 0
                fprintf(repmat('\b', 1, last_progress_line_size));
            end

            progress = st_util_progress(progress, 'action', 'tic');
        end

        t_total = tic();
        state.timing.geometry(end + 1) = 0.0;
        state.timing.forward(end + 1) = 0.0;
        state.timing.adjoint(end + 1) = 0.0;

        % {{{ load forward problem

        fw = load(ode.checkpoints{n});
        dt = t - fw.t;
        t = fw.t;

        % interpolate solution to new grid
        if isfield(fw.stages, 'kp')
            [x, y] = st_mesh_geometry_update(x, y, fw.stages.k0);
            [xp, yp] = st_mesh_geometry_update(x, y, fw.stages.kp);

            xc = st_mesh_centroid(x, y);
            xstar = st_mesh_interp(xp, x, xstar, 'fft', xc);
        end

        % FIXME: this is a hack for `st_repr_density_adjoint_solution` so that
        % it has the proper time. should just pass it as an argument.
        problem.bc.param.t = t;

        % }}}

        t_update = tic();
        switch ode.stepper
        case 'euler'
            [st, state] = update(t, xstar, x, y, problem, state);

            xstar = xstar + dt * st.adjoint.forcing;
        case 'imex'
            % explicit
            [st, state] = update(t, xstar, x, y, problem, state);
            b = st_array_stack(xstar + dt * st.adjoint.forcing);
            % implicit
            F = forcing.fn.operator_x(forcing, t, x, y, st, false);
            A = eye(size(F)) - dt * F;

            xstar = st_array_unstack(A \ b);
        case 'ssprk3'
            k2 = xstar;

            % stage 1
            [st_, state] = update(t, k2, fw.stages.k1, [], problem, state);
            k1 = k2 + dt * st.adjoint.forcing;

            % stage 2
            [st, state] = update(t, k1, x, y, problem, state);
            k0 = 3.0 / 4.0 * k2 + 1.0 / 4.0 * (k1 + dt * st.adjoint.forcing);

            % stage 3
            [st_, state] = update(t, k0, fw.stages.k2, [], problem, state);

            xstar = 1.0 / 3.0 * k2 + 2.0 / 3.0 * (k0 + dt * st_.adjoint.forcing);
        case 'ssprk3a'
            k2 = xstar;

            % stage 1
            [st_, state] = update(t, k2, fw.stages.k2, [], problem, state);
            k1 = k2 + dt * st_.adjoint.forcing;

            % stage 2
            [st_, state] = update(t, k1, fw.stages.k1, [], problem, state);
            k0 = k1 + dt * st_.adjoint.forcing;

            % stage 3
            [st, state] = update(t, k0, x, y, problem, state);

            xstar = k2 / 3.0 + k1 / 2.0 + (k0 + dt * st.adjoint.forcing) / 6.0;
        otherwise
            error('unknown method: %s', ode.stepper);
        end
        state.grad = state.grad + dt * grads.fn.gradient_ca(grads, t, x, y, st);

        state.timing.update(end + 1) = toc(t_update);

        % {{{ checkpoint

        if mod(n - 1, ode.odefreq) == 0
            try
                if ~isempty(ode.odeplot)
                    ode.odeplot(t, x, y, st);
                end
            catch
                warning('Error occured during plotting. Disabling ...');
                ode.odeplot = [];
                ode.odefreq = Inf;
            end
        end

        if enable_iteration_checkpointing
            [checkpoint, filename] = checkpoint.fileseries(checkpoint, 'evolution');
            save_checkpoint(filename, st, n, t);
        end

        % }}}

        % {{{ iteration dump

        fstar = st.adjoint.forcing;
        state.times(end + 1) = t;
        state.xstar_norm(end + 1) = norm(real(xstar), Inf) + 1.0j * norm(imag(xstar), Inf);
        state.fstar_norm(end + 1) = norm(real(fstar), Inf) + 1.0j * norm(imag(fstar), Inf);

        state.timing.total(end + 1) = toc(t_total);

        if ode.display >= 1
            progress = st_util_progress(progress, 'action', 'toc');

            if ode.display == 1
                if ~isinf(ode.maxit)
                    last_progress_line_size = fprintf('[%05d/%05d] %s', ...
                        n, ode.maxit, progress.strfmt(progress));
                else
                    last_progress_line_size = fprintf('[%05d] %s', ...
                        n, progress.strfmt(progress));
                end
            end
        end

        if ode.display >= 2
            display_timestep(ode, progress, state, st, x);
        end

        % }}}
    end

    if ode.display == 1
        fprintf('\n');
    end

    state.tmax = ode.tmax;
    state.maxit = ode.maxit;
    state.Ca = ode.param.Ca;
    state.lambda = ode.param.lambda;
    state.xstar = xstar;

    if enable_checkpointing
        filename = checkpoint.filename(checkpoint, 'state');

        state.checkpoints = checkpoint.file_series_cache;
        save(filename, '-struct', 'state');
    end
end

function save_checkpoint(filename, st, it, t)
    save(filename, 'st', 'it', 't');
end

function [st, state] = update(t, xstar, x, y, problem, state)
    % unroll
    st = struct();
    forcing = problem.forcing;
    forcingstar = problem.forcingstar;

    % update geometry
    tic();
    if isempty(y)
        [x, y] = st_mesh_geometry_update(problem.x, problem.y, x);
    end
    st.x = x.x;
    state.timing.geometry(end) = state.timing.geometry(end) + toc();

    % solve forward
    tic();
    st.forward = st_repr_density_solution(x, y, problem.bc, problem.ode.variables);
    st.forward.forcing = forcing.fn.evaluate(forcing, t, x, y, st.forward);
    state.timing.forward(end) = state.timing.forward(end) + toc();

    % solve adjoint
    tic();
    st.adjoint.x = xstar;
    st.adjoint = st_repr_density_adjoint_solution(x, y, problem.grads, st, ...
        problem.cost.adjoint_variables);
    fstar = forcingstar.fn.evaluate(forcingstar, t, x, y, st);
    state.timing.adjoint(end) = state.timing.adjoint(end) + toc();

    st.adjoint.forcing = st_dot(fstar, x.n) .* x.n;
end

function display_timestep(ode, progress, state, st, x)
    if ode.display < 2
        return;
    end

    n = ode.maxit - length(state.times) + 1;
    dt = state.times(end - 1) - state.times(end);
    t = state.times(end);
    fstar_dot_t = norm(st_dot(st.adjoint.forcing, x.t), Inf);

    fprintf('[%05d/%05d] t = %.5e / %.5e dt = %.5e\n', n, ode.maxit, t, ode.tmax, dt);

    fprintf('   xstar:      %.5e %.5e\n', ...
        real(state.xstar_norm(end)), imag(state.xstar_norm(end)));
    fprintf('   fstar:      %.5e %.5e %.5e\n', ...
        real(state.fstar_norm(end)), imag(state.fstar_norm(end)), fstar_dot_t);

    if isfield(ode.user, 'grad')
        fd_grad = norm(ode.user.grad, Inf);
        fprintf('   grad:       %.5e (%.5e)\n', norm(state.grad, Inf), fd_grad);
    else
        fprintf('   grad:       %.5e\n', norm(state.grad, Inf));
    end

    fprintf('   timing:     %s\n', progress.strfmt(progress));
    fprintf('               upd %.2fs fwd %.2fs adj %.2fs geo %.2fs\n', ...
        state.timing.update(end), ...
        state.timing.forward(end), ...
        state.timing.adjoint(end), ...
        state.timing.geometry(end));
end

% vim:foldmethod=marker:
