% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [prev] = st_graphics_defaults(varargin)
    % Preferred default graphics options.
    %
    % A list of possible options can be retrieved with ``get(0, 'factory')``.
    % The options passed are the same without the ``factory`` prefix.
    % Additional options controlling graphics output are controlled by
    % global variables:
    %
    %   * `PaperQualityPlots`: makes all the plots grayscale in the EPS format
    %     with thicker line widths and other similar options.
    %   * `PrintResolution`: default DPI for plots.
    %   * `PrintFormat`: default format for plots.
    %
    % The function is meant to be used over a block of code in the following
    % way to correctly set and reset the graphics:
    %
    %   .. code-block:: matlab
    %
    %       orig = st_graphics_defaults('Opt1', v1, ...);
    %       % ... code ...
    %       st_graphics_defaults(orig);
    %
    %
    % :arg varargin: list of graphics options.
    %
    % :returns: a struct with all the previous options, which can be used
    %   to reset the defaults by calling the functions again.

    opts = st_struct_varargin(varargin{:});
    prev = struct();

    global PaperQualityPlots;
    if ~isempty(PaperQualityPlots)
        opts.PrintFormat = st_struct_field(opts, 'PrintFormat', 'eps');
        opts.AxesColorOrder = st_struct_field(opts, 'AxesColorOrder', [0, 0, 0]);
        opts.LineMarkerSize = st_struct_field(opts, 'LineMarkerSize', 12);
    end

    % figure defaults
    st_set('LegendFontSizeMode', 'manual');
    st_set('LegendInterpreter', 'latex');
    st_set('LegendFontSize', 38);

    st_set('TextInterpreter', 'latex');
    st_set('LineLineWidth', 2);
    % st_set('LineMarkerSize', 12);

    st_set('AxesTickLabelInterpreter', 'latex');
    st_set('AxesNextPlot', 'add');
    st_set('AxesFontSize', 34);
    st_set('AxesXLimSpec', 'tight');
    st_set('AxesYLimSpec', 'tight');
    st_set('AxesXGrid', 'on');
    st_set('AxesYGrid', 'on');
    st_set('AxesBox', 'on');

    st_set('FigurePaperPositionMode', 'manual');
    st_set('FigureVisible', 'off');
    st_set('FigurePaperUnits', 'inches');
    st_set('FigurePaperPosition', [0, 0, 10, 10]);
    st_set('FigurePaperSize', [10, 10]);

    global AxesColorOrder;
    if ~isempty(AxesColorOrder) || isfield(opts, 'AxesColorOrder')
        st_set('AxesColorOrder', AxesColorOrder);
        new_order = get(0, 'defaultAxesColorOrder');
        if size(new_order, 1) == 1
            st_set('AxesLineStyleOrder', ...
                {'+-', 'o-', 's-', 'd-', 'x-', '^-', 'v-', '>-', '<-'});
        end
    end

    st_setg('PrintResolution', 300);
    st_setg('PrintFormat', 'png');

    % additional fields
    for f = fieldnames(opts)'
        st_set(f{1}, opts.(f{1}));
    end

    function st_set(field, value)
        [opts, prev] = st_set_default(opts, prev, field, value);
    end

    function st_setg(field, value)
        [opts, prev] = st_set_global(opts, prev, field, value);
    end
end

function [opts, prev] = st_set_default(opts, prev, field, value)
    if isfield(opts, field)
        value = opts.(field);
        opts = rmfield(opts, field);
    end
    opt_name = sprintf('default%s', field);

    try
        prev_value = get(0, opt_name);
        assert(~isstruct(prev_value));
    catch
        return;
    end
    prev.(field) = prev_value;

    set(0, opt_name, value);
end

function [opts, prev] = st_set_global(opts, prev, field, value)
    eval(sprintf('global %s; value = %s;', field, field));

    if ~isempty(value)
        prev.(field) = value;
    end

    value = st_struct_field(opts, field, value);        %#ok
    eval(sprintf('%s = value;', field))

    if isfield(opts, field)
        opts = rmfield(opts, field);
    end
end

% vim:foldmethod=marker:
