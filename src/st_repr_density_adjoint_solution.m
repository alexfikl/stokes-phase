% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [ad] = st_repr_density_adjoint_solution(x, y, grads, st, vars)
    % Compute adjoint Stokes solution.
    %
    % The main difference between the forward Stokes and the adjoint
    % stokes are the boundary conditions. The adjoint problem has a specific
    % set of boundary conditions that are determined by the various
    % derivatives of the user-provided data.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg grads: a sum of all the gradients around gathered using
    %   :func:`st_gradient_combine` or equivalent. In particular,
    %   for the adjoint solution, we use the gradient with respect to the
    %   velocity field and traction.
    % :arg st: forward Stokes solution.
    % :arg vars: desired variables, see :func:`st_repr_density_solution`.
    %
    % :return: a struct containing all the desired variables.

    if ~exist('vars', 'var') || isempty(vars)
        ad = struct();
        return;
    end

    % copy parameters
    if isfield(st, 'param')
        bc.param = st.param;
    end

    if isfield(st, 'forward') && isfield(st.forward, 'param')
        bc.param = st.forward.param;
    end
    t = st_struct_field(bc.param, 't', 0.0);

    % farfield velocity boundary condition
    fn.uinf = @(z) zeros(1, length(z.x), 'like', z.x);
    % farfield pressure boundary conditions
    fn.pinf = @(z) zeros(1, length(z.x));
    % farfield stress boundary condition
    fn.finf = @(z) zeros(1, length(z.x), 'like', z.x);
    fn.tinf = @(z) zeros(1, length(z.x), 'like', z.x);
    fn.sinf = @(z) zeros(3, length(z.x));
    % farfield normal ang tangential velocity gradient boundary condition
    fn.dudninf = @(z) zeros(1, length(z.x), 'like', z.x);
    fn.dudtinf = @(z) zeros(1, length(z.x), 'like', z.x);

    % surface boundary
    fn.usurf = @(z) grads.fn.gradient_s(grads, t, z, y, st);
    % jump condition
    fn.jump = @(z) grads.fn.gradient_u(grads, t, z, y, st);
    bc.fn = fn;

    ad = st_repr_density_solution(x, y, bc, vars);
    if isfield(st, 'adjoint')
        ad = st_struct_merge(st.adjoint, ad, true);
    end
end

% vim:foldmethod=marker:
