% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [jump] = st_jump_angle(varargin)
    % Defines an artificial traction jump.
    %
    % The jump is given in its non-dimensional form as
    %
    % .. math::
    %
    %     [\![\mathbf{n} \cdot \sigma]\!] =
    %         6 \alpha \frac{2 + 3 \lambda}{R^4 \lambda} \cos \theta \mathbf{n}.
    %
    % :returns: a struct compatible with :func:`st_jump_options`.

    jump = st_jump_options(varargin{:});

    % set function handles
    jump.fn.evaluate = @jump_angle_evaluate;
    jump.fn.gradient_x = @st_jump_shape_gradient;
    jump.fn.gradient_ca = @jump_angle_gradient_ca;

    jump.fn.operator_shape_x = @jump_angle_operator_shape_x;
    jump.fn.operator_x = @st_jump_shape_operator;
end

% {{{ gradient function handles

function [r] = jump_angle_evaluate(jump, ~, x, ~, ~)
    c_hr = st_jump_angle_c(jump);
    r = c_hr * cos(angle(x.x)) .* x.n;
end

function [r] = jump_angle_gradient_ca(jump, t, x, y, st)
    j = jump.fn.evaluate(jump, t, x, y, st);

    integrand = st_dot(st.adjoint.u, j);
    integrand = st_interpolate(x, integrand, y.xi);
    r = -1.0 / jump.param.Ca * st_ax_integral(y, integrand);
end

% }}}

% {{{ shape gradient operators

function [r] = jump_angle_operator_shape_x(jump, ~, x, y, ~)
    switch jump.optype
    case 'scalar'
        r = st_jump_angle_operator_shape_scalar(jump, x, y);
    case 'vector'
        r = st_jump_angle_operator_shape_vector(jump, x, y);
    otherwise
        error('unknown operator type');
    end
end

function [G] = st_jump_angle_operator_shape_scalar(jump, x, y)
    c_hr = st_jump_angle_c(jump);

    % jump derivatives
    Sn = c_hr * cos(angle(x.x));
    ds = c_hr * (imag(x.x) - 1.0j * real(x.x)) .* imag(x.x) ./ abs(x.x).^3;
    ds = st_dot(ds, x.n);

    % operator
    Gx = op(ds .* imag(x.n), Sn .* imag(x.t));
    Gy = op(ds .* real(x.n), Sn .* real(x.t));
    G = [Gx, Gy];

    function [O] = op(a, b)
        Cstar = st_ops_mass(x, y, a);
        Nstar = st_ops_adjoint_normal(x, y, b);
        O = Nstar + Cstar;
    end
end

function [G] = st_jump_angle_operator_shape_vector(jump, x, y)
    c_hr = st_jump_angle_c(jump);

    % jump derivatives
    Sn = c_hr * cos(angle(x.x));
    ds = c_hr * (imag(x.x) - 1.0j * real(x.x)) .* imag(x.x) ./ abs(x.x).^3;
    ds = st_dot(ds, x.n);

    % operator
    N = st_ops_adjoint_vector_normal(x, y, Sn .* x.t);
    Mxx = st_ops_mass(x, y, ds .* real(x.n) .* real(x.n));
    Mxy = st_ops_mass(x, y, ds .* real(x.n) .* imag(x.n));
    Myx = st_ops_mass(x, y, ds .* imag(x.n) .* real(x.n));
    Myy = st_ops_mass(x, y, ds .* imag(x.n) .* imag(x.n));
    M = [Mxx, Mxy; Myx, Myy];

    G = N + M;
end

function [c_hr] = st_jump_angle_c(jump)
    R = jump.param.R;
    lambda = jump.param.lambda;
    alpha = jump.param.alpha;
    c_hr = 6.0 * alpha * (2.0 + 3.0 * lambda) / (R^4 * lambda);
end

% }}}
