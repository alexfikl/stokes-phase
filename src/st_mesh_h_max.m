% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [hmax] = st_mesh_h_max(x, y)
    % Compute a maximum panel size in the mesh.
    %
    % :arg x: target point information.
    % :arg y: source point information.

    if ~isfield(y, 'J')
        hmax = 1.0 / x.npanels;
        return;
    end

    npanels = length(x.panels) - 1;
    nnodes = length(y.quad.unit_xi);

    % compute arclength of each panel
    hmax = -1.0;
    for j = 1:npanels
        k = ((j - 1) * nnodes + 1):(j * nnodes);

        ds = sum(y.J(k) .* y.w(k));
        hmax = max(hmax, ds);
    end
end

% vim:foldmethod=marker:
