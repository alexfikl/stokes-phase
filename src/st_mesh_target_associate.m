% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [panel_ids, xi] = st_mesh_target_associate(x, y)
    % Associate targets to source panels.
    %
    % Target association is done in the following way:
    %
    % * first we construct an array of source points and panel endpoints
    %   from the information in `Y`. By construction, we know in which
    %   panel each of these source points resides.
    % * then we compute the minimum distance from each target point to the
    %   points in the constructed array.
    % * using the index of the closest source point, we associate a
    %   panel to each target.
    %
    % Note that target points that are closest to a panel endpoint, get
    % associated to its two neighboring panels. Otherwise, each target is
    % associated with a single panel.
    %
    % :arg x: target point information.
    % :arg y: source point information.
    %
    % :returns: array of size ``(2, ntargets)`` of panel ids containing the
    %   indices of the two nearest panels to the target point.
    % :return: an array of source point closest to the target point.

    if isstruct(x)
        % NOTE: this means we are most likely doing self-evaluation, so we
        % can simplify the whole association process quite a bit
        if isfield(x, 'xi')
            if isfield(x, 'panels')
                panels = x.panels;
            elseif isfield(y, 'panels')
                panels = y.panels;
            else
                panels = [];
            end

            if ~isempty(panels)
                panel_ids = [discretize(x.xi, panels, 'IncludedEdge', 'left'); ...
                             discretize(x.xi, panels, 'IncludedEdge', 'right')];
                xi = x.xi;
                return;
            end
        end
    end

    if isstruct(x)
        x0 = x.x;
    else
        x0 = x;
    end

    % number of target collocation points
    ntargets = length(x0);
    % number of source quadrature points
    nsources = length(y.x);
    % number of panels
    npanels = length(y.vertices) - 1;
    % number of quadrature nodes
    nnodes = nsources / npanels;

    % length of combined array of source points + panel endpoints
    npoints = nsources + npanels + 1;
    % indices for panel endpoints
    point_end = 1:(nnodes + 1):npoints;
    point_int = setdiff(1:npoints, point_end);
    % construct an array
    xi = zeros(1, npoints, 'like', y.x);
    xi(point_end) = y.vertices;
    xi(point_int) = y.x;

    % find target closest to each point
    % TODO: this can be vectorized
    point_ids = -1.0 * ones(size(x0));
    for i = 1:ntargets
        [~, point_ids(i)] = min(abs(xi - x0(i)));
    end

    % find to which panels each source point actually belongs
    xi = zeros(1, npoints);
    xi(point_end) = y.panels;
    xi(point_int) = y.xi;
    % associate each point to a panel
    % TODO: these can be constructed directly with repmat or something.
    source_panel_left = discretize(xi, y.panels, 'IncludedEdge', 'left');
    source_panel_right = discretize(xi, y.panels, 'IncludedEdge', 'right');

    % match panels
    panel_ids = [source_panel_left(point_ids); ...
                 source_panel_right(point_ids)];
    xi = xi(point_ids);
end

% vim:foldmethod=marker:
