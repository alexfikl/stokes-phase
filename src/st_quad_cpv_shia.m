% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xi, w] = st_quad_cpv_shia(nnodes, y, a, b, method)
    % Compute Cauchy Princial Value quadrature rules.
    %
    % This constructs quadrature rules based on the work of
    % [Hui1999]_.
    %
    % :arg nnodes: number of quadrature nodes.
    % :arg y: position of the singularity in :math:`[a, b]`.
    % :type y: default: :math:`a + b / 2`
    % :arg a: domain starting point.
    % :type a: default ``-1``.
    % :arg b: domain endpoint.
    % :type b: default ``1``.
    %
    % :returns: a quadrature rule ``(xi, w)``.

    % get parameters
    if ~exist('a', 'var') || isempty(a)
        a = -1.0;
    end

    if ~exist('b', 'var') || isempty(b)
        b = +1.0;
    end

    if ~exist('y', 'var') || isempty(y)
        y = (a + b) / 2.0;
    end

    if ~exist('method', 'var') || isempty(method)
        method = 'legendre';
    end
    method = lower(method);

    switch method
    case 'legendre'
        [xi, w] = st_quad_shia_legendre(nnodes, y);
    case 'chebyshev'
        [xi, w] = st_quad_shia_chebychev(nnodes, y);
    otherwise
        error('unknown quadrature rule: %s', method);
    end

    if nargout == 1
        xi = struct('xi', xi, 'w', w);
    end
end

function [xi, w] = st_quad_shia_legendre(nnodes, y)
    % get Gauss-Legendre quadrature nodes and weights
    [xi, w] = st_quad_leggauss(nnodes);
    assert(all(abs(xi - y) > 1.0e-12));

    % evaluate Legendre polynomials
    Py0 = 1.0; Py1 = y;
    Qy0 = 0.5 * log((1 + y) / (1 - y));
    Qy1 = Py1 * Qy0 - 1;
    for n = 2:nnodes
        Py0 = ((2 * n - 1) * y * Py1 - (n - 1) * Py0) / n;
        [Py0, Py1] = deal(Py1, Py0);

        Qy0 = (2 * n - 1) / n * Qy1 - (n - 1) / n * Qy0;
        [Qy0, Qy1] = deal(Qy1, Qy0);
    end

    % add to quadrature rule
    xi = [xi, y];
    w = [w, -2.0 * Qy1 / Py1];
end

function [xi, w] = st_quad_shia_chebychev(nnodes, y, a, b)      %#ok
    error('NotImplementedError');
end
