% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function q = st_repr_density(x, y, lambda, bc, method)
    % Solve a two-phase Stokes equation for a given domain.
    %
    % This function uses the density representation from Problem 5.3.2
    % in [Pozrikidis1992]_. The iterative solver to be used can be
    % prescribed with the `method` argument
    %
    % * ``'gmres'``: uses ``gmres``.
    % * ``'direct'``: uses ``mldivide``, i.e. ``A \ b``.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg lambda: viscosity ratio.
    % :arg bc: see :func:`st_boundary_condition_options`.
    % :arg method: method used to solve the system:
    % :type method: default ``'direct'``
    %
    % :returns: density field on the surface.
    % :see also: :func:`st_layer_slp`, :func:`st_layer_dlp`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.

    if ~exist('method', 'var')
        method = 'direct';
    end

    % number of points
    npoints = length(x.x);
    % compute jump in normal stress
    deltaf = bc.fn.jump(x);
    % compute freestream boundary condition
    finf = bc.fn.finf(x);
    % compute right-hand side
    q = -1.0 / (4.0 * pi) * (deltaf / (1.0 + lambda) - ...
                            (1.0 - lambda) / (1.0 + lambda) * finf);

    if abs(lambda - 1.0) < 1.0e-14
        return;
    end

    % compute double-layer potential
    D = st_layer_dlp(x, y);

    D = eye(size(D)) + ...
        (1.0 - lambda) / (1.0 + lambda) / (4.0 * pi) * D;

    % solve system
    q = [real(q), imag(q)]';
    if strcmp(method, 'gmres')
        [q, flag] = gmres(D, q, floor(npoints / 2), 1.0e-12);
        if flag ~= 0
            fprintf('GMRES did not converge! Error: %d\n', flag);
        end
    else
        linopts.RECT = true;
        q = linsolve(D, q, linopts);
    end

    q = q(1:npoints)' + 1.0j * q(npoints + 1:end)';
end

% vim:foldmethod=marker:
