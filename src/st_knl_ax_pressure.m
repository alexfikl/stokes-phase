% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function P = st_knl_ax_pressure(P, x, x0, I30, I31)
    % Pressure Green function for Stokes flow in free space.
    %
    % The layer potential corresponding to this kernel is computed in
    % :func:`st_layer_pressure`.
    %
    % The greenlet is given by Equation 2.2.10 in [Pozrikidis1992]_.
    % This function computes :math:`p_j` at the given points in an axisymmetric
    % setting.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg I: elliptic integrals, see :func:`st_ellint_order3`.
    %
    % :returns: an array of size ``(1, 2)`` containing the kernel components.
    % :see also: :func:`st_ellipke`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.

    P = st_knl_ax_electric_dlp_source(P, x, x0, 2 * I30, 2 * I31);
end

% vim:foldmethod=marker:
