% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function S = st_layer_slp(x, y, q, repr)
    % Evaluate the axisymmetric Stokes single-layer potential.
    %
    % This function computes the Stokes single-layer potential, as given in
    % Equation 4.1.1 of [Pozrikidis1992]_:
    %
    % .. math::
    %
    %     \mathcal{S}[\mathbf{q}](\mathbf{x}_0) = \int_\Sigma
    %         G_{ij}(\mathbf{x}, \mathbf{x}_0) q_j(\mathbf{x}) \,\mathrm{d}\mathbf{x},
    %
    % where the kernel :math:`G_{ij}` is given in Equation 2.2.8. The kernel
    % itself is computed using :func:`st_knl_ax_velocity`. There are two
    % different representations that this function provides
    %
    % * ``'density'``: implements the standard single-layer potential.
    % * ``'normal'``: assumes that the density is of the form :math:`q \mathbf{n}`,
    %   where :math:`\mathbf{n}` is the normal vector, and integrates the
    %   normal into the kernel formulation.
    %
    % The two are only meaningfully different when the matrix operator is
    % constructed.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    % :arg repr: desired form of the single-layer potential.
    % :type repr: default ``'density'``
    %
    % :returns: if the density is given, then we evaluate the single-layer,
    %   otherwise, a matrix operator is returned.

    % REFERENCES:
    %   [1] C. Pozrikidis, The Instability of a Moving Viscous Drop,
    %   Journal of Fluid Mechanics, 1990.

    if ~exist('repr', 'var') || isempty(repr)
        repr = 'density';
    end
    repr = lower(repr);

    if ~any(contains({'density', 'normal'}, repr))
        error('unknown representation: %s', repr);
    end

    if isfield(y.quad, 'u')
        knl.quad = y.quad.u;
    else
        knl.quad = y.quad.log;
    end

    % allocate these here so we don't have do to it in the kernel
    knl.M_c = zeros(2, 2);
    knl.S_c = zeros(4, x.nbasis);

    if exist('q', 'var') && ~isempty(q)
        % NOTE: the density can always have a zero imaginary part, so we need
        % to check that the user requested the normal representation.
        if strcmp(repr, 'normal')
            if isreal(q)
                q = q .* x.n;
            else
                error('normal representation requires a real density.');
            end
        end

        knl.S = zeros(1, length(x.x), 'like', x.x);
        knl.evaluate = @st_layer_slp_apply_kernel;

        knl = st_layer_evaluate(x, y, knl, q);
        S = knl.S;
    else
        knl.Sxx = zeros(length(x.x));
        knl.Sxy = zeros(length(x.x));
        knl.Syx = zeros(length(x.x));
        knl.Syy = zeros(length(x.x));
        knl.evaluate = @st_layer_slp_matrix_kernel;

        knl = st_layer_evaluate(x, y, knl);

        if strcmp(repr, 'normal')
            S = [knl.Sxx * diag(real(x.n)) + knl.Sxy * diag(imag(x.n)); ...
                 knl.Syx * diag(real(x.n)) + knl.Syy * diag(imag(x.n))];
        else
            S = [knl.Sxx, knl.Sxy; ...
                 knl.Syx, knl.Syy];
        end
    end
end

function [knl] = st_layer_slp_apply_kernel(knl, target, source)
    % target index
    i = target.i;
    % sources and targets
    y = source.x;
    x = target.x;

    M = knl.M_c;
    ellint = @st_ellint_order3;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % integrate Stokeslet on interval
    Sx = 0.0; Sy = 0.0;

    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I10, I11, I30, I31, I32] = ellint(y(n), x(i), F, E);
        % compute kernel
        M = st_knl_ax_velocity(M, y(n), x(i), I10, I11, I30, I31, I32);
        % [M(1, 1), M(1, 2), M(2, 1), M(2, 2)] = st_knl_ax_velocity_c(...
        %     real(y(n)), imag(y(n)), real(x(i)), imag(x(i)));

        Sx = Sx + (M(1, 1) * real(source.q(n)) + M(1, 2) * imag(source.q(n))) * dS;
        Sy = Sy + (M(2, 1) * real(source.q(n)) + M(2, 2) * imag(source.q(n))) * dS;
    end

    knl.S(i) = knl.S(i) + complex(Sx, Sy);
end

function [knl] = st_layer_slp_matrix_kernel(knl, target, source)
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % sources and targets
    y = source.x;
    x = target.x;

    M = knl.M_c;
    ellint = @st_ellint_order3;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    S0 = knl.S_c;
    S0(:, :) = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I10, I11, I30, I31, I32] = ellint(y(n), x(i), F, E);
        % compute kernel
        M = st_knl_ax_velocity(M, y(n), x(i), I10, I11, I30, I31, I32);

        % add interactions
        for k = 1:nbasis
            bnk = dS * basis(n, k);
            S0(1, k) = S0(1, k) + M(1, 1) * bnk;
            S0(2, k) = S0(2, k) + M(1, 2) * bnk;
            S0(3, k) = S0(3, k) + M(2, 1) * bnk;
            S0(4, k) = S0(4, k) + M(2, 2) * bnk;
        end
    end

    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.Sxx(i, j) = knl.Sxx(i, j) + S0(1, k);
        knl.Sxy(i, j) = knl.Sxy(i, j) + S0(2, k);
        knl.Syx(i, j) = knl.Syx(i, j) + S0(3, k);
        knl.Syy(i, j) = knl.Syy(i, j) + S0(4, k);
    end
end

% vim:foldmethod=marker:
