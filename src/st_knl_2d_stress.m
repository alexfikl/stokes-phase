% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [T] = st_knl_2d_stress(T, x, x0, varargin)
    % 2D free-space stress kernel.
    %
    % Given in Equation 2.6.20 in [Pozrikidis1992]_.
    %
    % :arg x: source point.
    % :arg x0: target point.
    %
    % :returns: an array of size ``(2, 2)`` containing the kernel components.

    RX = real(x) - real(x0);
    RY = imag(x) - imag(x0);
    R = sqrt(RX^2 + RY^2);

    % kernel
    T(1, 1, 1) = -4.0 * RX^3 / R^4;
    T(1, 1, 2) = -4.0 * RX^2 * RY / R^4;
    T(1, 2, 1) = -4.0 * RX^2 * RY / R^4;
    T(1, 2, 2) = -4.0 * RX * RY^2 / R^4;

    T(2, 1, 1) = -4.0 * RY * RX^2 / R^4;
    T(2, 1, 2) = -4.0 * RY^2 * RX / R^4;
    T(2, 2, 1) = -4.0 * RY^2 * RX / R^4;
    T(2, 2, 2) = -4.0 * RY^3 / R^4;
end

% vim:foldmethod=marker:
