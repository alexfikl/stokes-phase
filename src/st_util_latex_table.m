% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function st_util_latex_table(data, fid)
    % Print a table in latex format.
    %
    % The printed table depends on the `booktabs` package. The
    % `data` argument contains a set of fields, that corresponds to columns
    % in the resulting table. Each field can be an array or a struct with
    % the following fields:
    %
    % * `data`: data array to be written out.
    % * `header`: column name.
    % * `fmt`: formatting string for the column. By default, numbers are
    %   printed in a fixed precision and strings printed as is.
    %
    % Not all columns need to have the same number of rows and can contain
    % any type of information. The only requirement is that each column
    % contains the same type of data, i.e. they are not cell arrays.
    %
    % The `data` struct can also contain a ``fmt`` field that describes the
    % column layout, passed as an argument to the ``tabular`` environment.
    %
    % :arg data: structured data to write out.
    % :arg fid: file identifier, from `fopen`.
    % :type fid: default ``stdout``.


    if ~exist('fid', 'var')
        fid = 1;
    end

    % preamble
    if isfield(data, 'fmt')
        table_fmt = data.fmt;
        data = rmfield(data, 'fmt');
    else
        ncolumns = length(fieldnames(data));
        table_fmt = repmat('l', 1, ncolumns);
    end

    % fields
    fields = fieldnames(data);
    % number of columns
    ncolumns = length(fields);

    % make sure headers are wrapped in math $header$
    for m = 1:ncolumns
        variable = data.(fields{m});
        if ~isstruct(variable)
            variable = struct('values', variable);
        end

        if ~isfield(variable, 'header')
            variable.header = fields{m};
        end
        variable.header = sprintf('$%s$', variable.header);

        data.(fields{m}) = variable;
    end
    fprintf(fid, '\\begin{table}\n');
    fprintf(fid, '\\begin{tabular}{%s} \\toprule\n', table_fmt);

    st_util_table(data, fid, '&', ' \\');

    % footer
    fprintf(fid, '\\bottomrule\n');
    fprintf(fid, '\\end{tabular}\n');
    fprintf(fid, '\\end{table}\n');
end

% vim:foldmethod=marker:
