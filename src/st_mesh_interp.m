% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [g] = st_mesh_interp(xfrom, xto, f, method, xc)
    % Interpolate function between two meshes of the same geometry.
    %
    % :arg xfrom: target point information of the incoming mesh.
    % :arg xto: target point information of the outgoing mesh.
    % :arg f: function on *xfrom* to interpolate to *xto*.
    % :arg method: see :func:`st_interpolate`.
    %
    % :arg g:

    g = f;
    if norm(xfrom.x - xto.x) < 1.0e-14
        return;
    end

    if ~exist('method', 'var') || isempty(method)
        method = 'lagrange';
    end
    method = lower(method);

    if ~exist('xc', 'var') || isempty(xc)
        xc = 0.0;
    end

    xi = abs(angle(xto.x - xc) / (2.0 * pi));
    g = st_interpolate(xfrom, f, xi, method);
end
