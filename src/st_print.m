% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function st_print(fig, filename, varargin)
    % Wrapper around the default ``print`` command.
    %
    % Default values can be set by the global variables:
    %
    % * `PrintResolution`
    % * `PrintFormat`
    %
    % :arg fig: figure handle.
    % :arg filename: filename.
    % :arg varargin: additional arguments.

    % determine resolution
    global PrintResolution;
    if isempty(PrintResolution)
        p_resolution = 300;
    else
        p_resolution = PrintResolution;
    end

    % determine format
    global PrintFormat;
    [dirname, basename, ext] = fileparts(char(filename));
    if isempty(PrintFormat)
        if isempty(ext)
            ext = 'png';
        else
            ext = lower(ext(2:end));
        end
        p_format = st_ext2fmt(ext);
    else
        p_format = lower(PrintFormat);
        ext = st_fmt2ext(p_format);
    end
    filename = fullfile(dirname, sprintf('%s.%s', basename, ext));

    % print
    % TODO: there's a new exportgraphics in R2020a that seems worth looking into
    p_format = sprintf('-d%s', p_format);
    p_resolution = sprintf('-r%d', p_resolution);
    print(fig, filename, p_format, p_resolution, varargin);

    global verbosity;
    if verbosity
        fprintf('output: %s\n', filename);
    end
end

function [fmt] = st_ext2fmt(ext)
    switch ext
    case 'jpg'
        fmt = 'jpeg';
    case 'tif'
        fmt = 'tiff';
    case 'pcx'
        fmt = 'pcx24b';
    case 'eps'
        fmt = 'epsc';
    case 'ps'
        fmt = 'psc';
    case 'svg'
        fmt = 'svg';
    otherwise
        fmt = ext;
    end
end

function [ext] = st_fmt2ext(fmt)
    switch fmt
    case {'jpeg'}
        ext = 'jpg';
    case {'png'}
        ext = 'png';
    case {'tiff', 'tiffn'}
        ext = 'tif';
    case {'meta'}
        ext = 'emf';
    case {'bmpmono', 'bmp', 'bmp16m', 'bmp256'}
        ext = 'bmp';
    case {'hdf'}
        ext = 'hdf';
    case {'pbm', 'pbmraw'}
        ext = 'pbm';
    case {'pcxmono', 'pcx24b', 'pcx256', 'pcx16'}
        ext = 'pcx';
    case {'pgm', 'pgmraw'}
        ext = 'pgm';
    case {'ppm', 'ppmraw'}
        ext = 'ppm';
    case {'pdf'}
        ext = 'pdf';
    case {'eps', 'epsc', 'eps2', 'epsc2'}
        ext = 'eps';
    case {'svg'}
        ext = 'svg';
    case {'ps', 'psc', 'ps2', 'psc2'}
        ext = 'ps';
    otherwise
        error('unknown format: %s', fmt);
    end
end

% vim:foldmethod=marker:
