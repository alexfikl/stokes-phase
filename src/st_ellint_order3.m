% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [I10, I11, I30, I31, I32] = st_ellint_order3(x, x0, F, E)
    % Compute elliptic integrals in the Stokeslet.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg F: elliptic integral of the first kind, see :func:`st_ellipke`.
    % :arg E: elliptic integral of the second kind.
    %
    % :returns: elliptic integrals described in Section 2.4 of
    %   [Pozrikidis1992]_ used to compute the Stokeslet. Namely, it computes
    %   :math:`I_{10}, I_{11}, I_{30}, I_{31}` and :math:`I_{32}`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.
    %   [2] C. Pozrikidis, The Instability of a Moving Viscous Drop,
    %   Journal of Fluid Mechanics, 1990.

    R = 1.0 / ((real(x) - real(x0))^2 + (imag(x) + imag(x0))^2);

    % compute K
    % see [2, Equation 2.12.12]
    K2 = 4.0 * imag(x) * imag(x0) * R;
    K4 = K2 * K2;
    % NOTE: this can cause divisions by zero
    K21 = 1.0 / (1.0 - K2);
    % compute coefficient I_{mn} coefficient
    % see [2, Equation 2.12.10]
    C1 = 4.0 * sqrt(R);
    C3 = C1 * R;

    % compute single-layer potential matrix coefficients
    % see [2, Equation 2.12.10]
    I10 = C1 * F;
    I11 = C1 * ((2.0 - K2) * F - 2.0 * E) / K2;

    I30 = C3 * K21 * E;
    I31 = C3 * ((K21 + 1.0) * E - 2.0 * F) / K2;
    I32 = C3 * ((K21 + (7 - K2)) * E - ...
                4.0 * (2.0 - K2) * F) / K4;
end

% vim:foldmethod=marker:
