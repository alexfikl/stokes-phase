% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [e] = st_ax_error(x, y, uhat, u, p, relative)
    % Compute errors between two values using :func:`st_ax_norm`.
    %
    % The error norm is given by
    %
    % .. math::
    %
    %     e = \frac{\|\hat{u} - u\|_p}{\|u\|_p},
    %
    % where the numerator is omitted if *relative* is ``false``. By default,
    % we expect that *uhat* and *u* are given at the quadrature nodes. If
    % this is not the case, an attempt is made to interpolate them using
    % :func:`st_interpolate`.
    %
    % :arg x: target point information.
    % :arg y: source point information.
    % :arg uhat: first vector (approximation).
    % :arg u: second vector (exact).
    % :arg p: norm order.
    % :type p: default :math:`2`
    % :arg relative: if true, compute the relative error.
    % :type relative: default ``true``
    %
    % :returns: error between the two vectors.

    if ~exist('p', 'var') || isempty(p)
        p = 2;
    end

    if ~exist('relative', 'var') || isempty(relative)
        relative = true;
    end

    % interpolate to source points
    if length(uhat) ~= length(y.xi)
        uhat = st_interpolate(x, uhat, y);
    end

    if length(u) ~= length(y.xi)
        u = st_interpolate(x, u, y);
    end

    u_norm = 1.0;
    if relative
        u_norm = st_ax_norm(y, u, p);
        if u_norm < 1.0e-14
            u_norm = 1.0;
        end
    end

    e = st_ax_norm(y, uhat - u, p) / u_norm;
end

% vim:foldmethod=marker:
