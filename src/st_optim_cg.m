% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [x, stats] = st_optim_cg(varargin)
    % Nonlinear conjugate gradient.
    %
    % The algorithm uses usual methods (for the notation see [Hager2006]_).
    % The line search is a naive backtracking algorithm that just
    % check the Armijo conditions, not the full Wolfe conditions.
    %
    % :arg cg: a struct containing the problem definitions.
    % :return: an array ``x`` with the solution.
    % :return: a struct with information about the iterations.

    % {{{ default options

    cg = st_optim_options(varargin{:});
    assert(~isempty(cg.x0));
    assert(~isempty(cg.fn));

    cg.stats = struct();

    % }}}

    % {{{ initialize

    [f, grad] = cg.fn(cg.x0);

    state.irestart = 0;
    state.iteration = 0;

    state.x_curr = cg.x0;
    state.x_prev = cg.x0;
    state.f_prev = f;
    state.f_curr = f;
    state.grad_prev = grad;
    state.grad_curr = grad;
    state.d = -grad;

    state.f_increase = 0;
    state.user_stop = false;

    % make relative tolerance
    cg.gtol = norm(grad, Inf) * cg.gtol;
    cg.ftol = abs(f) * cg.ftol;
    cg.xtol = norm(cg.x0) * cg.xtol;

    if isempty(cg.alpha_max)
        cg.alpha_max = 1.0e-2 * 1.0 / norm(grad);
    end

    if cg.display >= 1
        disp(cg);

        fprintf('%-9s%-15s %-15s %-15s %-10s\n', ...
            'Iter', 'f(x)', '|grad|', 'alpha', 'restart');
        fprintf('%4d %15.5e %15.5e %15.5e %5d\n', ...
            state.iteration, ...
            state.f_curr, norm(state.grad_curr), nan, state.irestart);
    end

    cg.stats.fn_call = 1;
    cg.stats.gn_call = 1;

    if length(cg.x0) == 1
        cg.stats.x = cg.x0;
    else
        cg.stats.x = norm(cg.x0, Inf);
    end
    cg.stats.func = f;
    cg.stats.grad = norm(grad, inf);

    cg.stats.ddir = [];
    cg.stats.alpha = [];

    % }}}

    % {{{ loop

    if ~isempty(cg.output)
        data.x = state.x_curr;
        data.f = state.f_curr;
        data.g = state.grad_curr;
        data.d = state.d;
        data.iteration = state.iteration;

        cg.output(data);
    end

    flag = 0;
    while state.iteration < cg.maxiter
        % {{{ update solution

        [cg, alpha, alpha_min] = st_optim_armijo(cg, state, state.x_curr);
        if state.iteration > 0
            if abs(alpha - alpha_min) < 1.0e-14 && ...
                abs(state.alpha_prev - state.alpha_min_prev) < 1.0e-14
                flag = 10;
                break
            end
        end

        if state.iteration > 0
            % make sure alpha doesn't jump too much
            alpha = min(alpha, 1.2 * state.alpha_prev);
            % smooth it out a bit anyway
            alpha = min(alpha, 0.5 * (alpha + state.alpha_prev));
        end

        state.alpha_min_prev = alpha_min;
        state.alpha_prev = alpha;

        state.x_prev = state.x_curr;
        state.x_curr = cg.project(state.x_prev + alpha * state.d, state.d);

        % }}}

        % {{{ compute new function and gradient

        [f, grad] = cg.fn(state.x_curr);

        state.f_prev = state.f_curr;
        state.f_curr = f;
        state.grad_prev = state.grad_curr;
        state.grad_curr = grad;

        % }}}

        % {{{ update descent direction

        beta = st_optim_update(cg, state);
        state.d = -state.grad_curr + beta * state.d;

        state.irestart = state.irestart + 1;
        if state.irestart == cg.restart_maxit || sum(st_dot(state.grad_curr, state.d)) > 0.0
            state.d = -state.grad_curr;
            state.irestart = 0;
        end

        % }}}

        % {{{ output

        % update stats
        cg.stats.fn_call = cg.stats.fn_call + 1;
        cg.stats.gn_call = cg.stats.gn_call + 1;

        if length(state.x_curr) == 1
            cg.stats.x(end + 1) = state.x_curr;
        else
            cg.stats.x(end + 1) = norm(state.x_curr, Inf);
        end
        cg.stats.func(end + 1) = f;
        cg.stats.grad(end + 1) = norm(grad, Inf);

        cg.stats.ddir(end + 1) = norm(state.d, Inf);
        cg.stats.alpha(end + 1) = alpha;

        % let the user print things out
        if ~isempty(cg.output) && mod(state.iteration + 1, cg.outputfreq) == 0
            data.x = state.x_curr;
            data.f = state.f_curr;
            data.g = state.grad_curr;
            data.d = state.d;
            data.iteration = state.iteration + 1;

            state.user_stop = cg.output(data);
        end

        % }}}

        % {{{ check stoping conditions

        % function has been increasing for a while now
        if state.f_curr - state.f_prev > 0
            state.f_increase = state.f_increase + 1;

            if state.f_increase == 3
                flag = 11;
                break;
            end
        else
            state.f_increase = 0;
        end

        % user requested stop
        if state.user_stop
            flag = 2;
            break;
        end

        % gradient finally small enough
        if norm(state.grad_curr, Inf) < cg.gtol
            flag = 1;
            break;
        end

        % }}}

        % {{{ next iteration

        state.iteration = state.iteration + 1;

        if cg.display >= 1
            fprintf('%4d %15.5e %15.5e %15.5e %5d\n', ...
                state.iteration, ...
                state.f_curr, norm(state.grad_curr, Inf), alpha, state.irestart);
        end

        % }}}
    end

    % }}}

    if cg.display >= 1
        switch flag
        case 0
            fprintf('\nStopping condition: reached maxit.\n');
        case 1
            fprintf('\nStopping condition: reached gtol.\n');
            fprintf('>> norm(grad) = %.5e < %.5e\n', ...
                norm(state.grad_curr, Inf), cg.gtol);
        case 2
            fprintf('\nStopping condition: user requested.\n');
        case 10
            fprintf('\nStopping condition: alpha stalled.\n');
        case 11
            fprintf('\nStopping condition: increase in f.\n');
        end
    end

    x = state.x_curr;
    stats = cg.stats;
end

% {{{ armijo

function [cg, alpha, alpha_min] = st_optim_armijo(cg, state, x)
    %
    % find bounds for alpha
    %

    if isa(cg.alpha_max, 'function_handle')
        alpha_max = cg.alpha_max(x, state.d);
    else
        alpha_max = cg.alpha_max;
    end

    if isfield(cg, 'stats')
        % NOTE: this takes the last few alphas and averages them. should help
        % if the actual step size is a lot smaller than the given one
        % (e.g. if the gradient is big)
        alpha_avg = mean(cg.stats.alpha(max(1, end - 5):end));
        alpha_max = min(alpha_max, 1.2 * alpha_avg);
    end

    if isa(cg.alpha_min, 'function_handle')
        alpha_min = cg.alpha_min(x, state.d);
    else
        alpha_min = cg.alpha_min;
    end

    %
    % backtracking
    %

    f = state.f_curr;
    dg = sum(st_dot(state.grad_curr, state.d));

    it = 0;

    alpha = alpha_max;
    while alpha > alpha_min && it < cg.alpha_maxit
        fa = cg.fn(cg.project(x + alpha * state.d, state.d));
        cg.stats.fn_call = cg.stats.fn_call + 1;

        if (fa - f) < alpha * cg.armijo_beta * dg
            break;
        end

        if cg.display >= 1
            fprintf('[%04d/%04d] Armijo rule with %.5e < %.5e < %.5e with ', ...
                it, cg.alpha_maxit, alpha_min, alpha, alpha_max);
            fprintf('fa = %.5e f = %.5e dg = %.5e\n', ...
                fa, f, alpha * cg.armijo_beta * dg);
        end

        alpha = max(cg.armijo_ratio * alpha, alpha_min);
        it = it + 1;
    end
end

% }}}

% {{{ step

function [beta] = st_optim_update(cg, state)
    switch cg.beta_update
    case 'steepest'
        beta = 0.0;
    case 'hestenesstiefl'
        beta = st_optim_hestenes_stiefl(cg, state);
    case 'fletcherreeves'
        beta = st_optim_flecher_reeves(cg, state);
    case 'polakribiere'
        beta = st_optim_polak_ribiere(cg, state);
    case 'hagerzhang'
        beta = st_optim_hager_zhang(cg, state);
    case 'cgdescent'
        beta = st_optim_cg_descent(cg, state);
    otherwise
        error('unknown update parameter: %s', cg.beta_update);
    end
end

function [beta] = st_optim_hestenes_stiefl(~, state)
    y = state.grad_curr - state.grad_prev;
    beta = sum(st_dot(state.grad_curr, y)) / ...
           sum(st_dot(state.d, y));
end

function [beta] = st_optim_flecher_reeves(~, state)
    beta = sum(st_dot(state.grad_curr, state.grad_curr)) / ...
           sum(st_dot(state.grad_prev, state.grad_prev));
end

function [beta] = st_optim_polak_ribiere(~, state)
    y = state.grad_curr - state.grad_prev;
    beta = sum(st_dot(state.grad_curr, y)) / ...
           sum(st_dot(state.grad_prev, state.grad_prev));
    beta = max(beta, 0.0);
end

function [beta] = st_optim_hager_zhang(~, state)
    y = state.grad_curr - state.grad_prev;
    dy = sum(st_dot(state.d, y));
    ynorm = sum(st_dot(y, y));

    beta = sum(st_dot(y - 2.0 * ynorm / dy * state.d, state.grad_curr / dy));
end

function [beta] = st_optim_cg_descent(cg, state)
    y = state.grad_curr - state.grad_prev;
    dy = sum(st_dot(state.d, y));

    ynorm = sum(st_dot(y, y));
    dnorm = sqrt(sum(st_dot(state.d, state.d)));
    gnorm = sqrt(sum(st_dot(state.grad_prev, state.grad_prev)));

    y = y - cg.hz_theta * ynorm / dy * state.d;
    beta = sum(st_dot(y, state.grad_curr)) / dy;

    eta = -1.0 / (dnorm * min(cg.hz_eta, gnorm));
    beta = max(beta, eta);
end

% }}}

% vim:foldmethod=marker:
