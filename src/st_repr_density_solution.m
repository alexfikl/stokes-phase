% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [st] = st_repr_density_solution(x, y, bc, vars)
    % Compute Stokes solution.
    %
    % The `vars` argument is a cell array defining which variables are to be
    % computed. The required elements in the array are
    %
    % * `exterior`: solve exterior problem using :func:`st_repr_density_exterior`.
    % * `two_phase`: solve two-phase problem using :func:`st_repr_density`.
    %
    % Then additional optional values define the desired flow variables
    %
    % * ``'u'``: velocity field
    % * ``'p'``: pressure field.
    % * ``'fn'`` and ``'ft'``: normal and tangent components of the stress tensor.
    % * ``'dudn'`` and ``'dudt'``: normal and tagent components of the velocity gradient.
    % * ``'eps'``: strain rate tensor.
    % * ``'sigma'``: stress tensor.
    % * ``'grads'``: normal component of the stress gradient.
    %
    % The *bc* argument should contain all the boundary conditions and
    % problem parameters. The boundary conditions depend on the requested
    % variables in `vars`, while the problem parameters should be passed in
    % `bc.param`. For example, when solving a two-phase problem, we expect
    % `bc.param.lambda` to be present.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg bc: see :func:`st_boundary_condition_options`.
    % :arg vars: desired variables to be computed.
    %
    % :return: a struct containing all the desired variables. The names of
    %   the fields match those of the input `vars`, with a suffix of
    %   `_ext` or `_int` if the variable has a jump at the interface.

    persistent StokesVariableNames;
    if isempty(StokesVariableNames)
        StokesVariableNames = struct(...
            'u',    {{'u'}}, ...
            'p',    {{'p_ext', 'p_int'}}, ...
            'fn',   {{'fn_ext', 'fn_int'}}, ...
            'ft',   {{'ft_ext', 'ft_int'}}, ...
            'dudn', {{'dudn_ext', 'dudn_int'}}, ...
            'dudt', {{'dudt_ext', 'dudt_int'}}, ...
            'eps',  {{'eps_ext', 'eps_int'}}, ...
            'sigma',{{'sigma_ext', 'sigma_int'}}, ...
            'grads',{{'grads_ext', 'grads_int'}});
    end

    st = struct();
    if ~exist('vars', 'var') || isempty(vars)
        return;
    end

    vars = unique(vars);
    variables = struct();
    for n = 1:length(vars)
        if isfield(StokesVariableNames, vars{n})
            variables.(vars{n}) = StokesVariableNames.(vars{n});
        else
            variables.(vars{n}) = vars{n};
        end
    end

    % solve for the density
    st = struct();
    if isfield(variables, 'exterior')
        st.q = st_repr_density_exterior(x, y, bc);
        variables = rmfield(variables, 'exterior');
    elseif isfield(variables, 'two_phase')
        st.q = st_repr_density(x, y, bc.param.lambda, bc);
        variables = rmfield(variables, 'two_phase');
    else
        return;
    end
    st.param = bc.param;

    % compute variables
    % TODO: just make sure that `eps` or `sigma` are first in the variable
    % list. We compute them first because they require
    st.cache = struct();
    if isfield(variables, 'eps')
        st = st_solution(st, x, y, bc, variables.eps, 'eps');
        variables = rmfield(variables, 'eps');
    end

    if isfield(variables, 'sigma')
        st = st_solution(st, x, y, bc, variables.sigma, 'sigma');
        variables = rmfield(variables, 'sigma');
    end

    fields = fieldnames(variables);
    remaining = variables;
    for n = 1:length(fields)
        if ~isfield(StokesVariableNames, fields{n})
            continue;
        end

        st = st_solution(st, x, y, bc, variables.(fields{n}), fields{n});
        remaining = rmfield(remaining, fields{n});
    end

    st = rmfield(st, 'cache');
    if length(fieldnames(remaining)) >= 1
        fields = join(fieldnames(remaining), ', ', 1);
        warning('remaining unknown fields: %s', fields{1});
    end
end

function [st] = st_solution(st, x, y, bc, vars, name)
    if all(isfield(st.cache, vars))
        % fprintf('st_repr_density_solution: hitting cache for `%s`\n', name);
        result = st.cache;
    else
        % fprintf('st_repr_density_solution: missing cache for `%s`\n', name);
        bc.cache = st.cache;
        result = st_repr_density_layer(x, y, st.q, bc, name);

        if isfield(result, 'cache')
            st.cache = st_struct_merge(st.cache, result.cache, true);
        end
    end

    for n = 1:length(vars)
        st.(vars{n}) = result.(vars{n});
    end
end

function [r] = st_repr_density_layer(x, y, q, bc, name)
    name = lower(name);
    switch name
    case 'u'
        r.u = st_repr_density_velocity(x, y, q, bc);
    case 'p'
        r = st_repr_density_pressure(x, y, q, bc);
    case 'dudn'
        r = st_repr_density_gradu(x, y, q, bc, 'normal');
    case 'dudt'
        r = st_repr_density_gradu(x, y, q, bc, 'tangentfft');
    case 'fn'
        r = st_repr_density_traction(x, y, q, bc, 'normal');
    case 'ft'
        r = st_repr_density_traction(x, y, q, bc, 'tangent');
    case 'sigma'
        r = st_repr_density_stress(x, y, q, bc);
    case 'eps'
        r = st_repr_density_strain(x, y, q, bc);
    case 'grads'
        r = st_repr_density_grads(x, y, q, bc);
    end
end

% vim:foldmethod=marker:
