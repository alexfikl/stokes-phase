% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [o] = st_unittest_convergence_options(varargin)
    % Get a struct of options for :func:`st_unittest_convergence`.
    %
    % Available options are
    %
    %   * ``variables``: cell array of variable names, used for output.
    %   * ``labels``: a LaTex label for each variable for plotting.
    %   * ``expected_order``: expected convergence order for each variable.
    %   * ``resolutions``: an array of resolutions to iterate over for testing
    %     convergence.
    %   * ``enable_plotting``: flag to enable plotting.
    %   * ``enable_geometry_plot``: plot geometry at the finest resolution.
    %   * ``enable_latex_eoc``: output convergence in a LaTeX table instead of
    %     a simple ASCII table.
    %   * ``enable_caching``: creates a separate cache directory to capture
    %     all the output from the run.
    %   * ``cache_id``: passed to :func:`st_cache_create`.
    %   * ``prefix``: passed to :func:`st_cache_create` for output.
    %   * ``cache``: a cache from :func:`st_cache_create` that is automatically
    %     created if not given.
    %   * ``user``: additional user controlled options.
    %
    % Additionally, a few function handles need to be provided
    %
    %   * ``evaluate(problem, x, y, method)``: evaluates the exact solution
    %     when ``method='ex'`` and approximate solution when ``method='ap'``.
    %     The first argument contains the problem parameters. The function should
    %     return a struct with field names matching the given ``variables``.
    %   * ``norm(x, y, f, g)``: compuates an error norm on the current geometry.
    %   * ``geometry_update(options, resolution)``: constructs the geometry
    %     compatible with :func:`st_geometry_options` for a given resolution.
    %
    % The only required parameters are the ``variables`` and the
    % ``evaluate`` function handle.
    %
    % :returns: a struct with default options.

    % {{{ default options

    % covergence
    d.resolutions = 2.^(1:6);
    d.evaluate = [];
    d.expected_order = [];
    d.norm = @(x, y, f, g) st_ax_error(x, y, f, g, 2, true);
    d.geometry_update = @(o, r) geometry_update(o, r);

    % input/output
    d.variables = {};
    d.labels = {};
    d.enable_plotting = true;
    d.enable_geometry_plot = false;
    d.enable_latex_eoc = false;

    % caching
    d.enable_caching = false;
    d.cache_id = datestr(now, 'yyyy_mm_dd');
    d.prefix = 'unittest';
    d.cache = struct();

    d.user = struct();

    % }}}

    o = st_struct_update(d, st_struct_varargin(varargin{:}));

    o.enable_geometry_plot = o.enable_geometry_plot && o.enable_plotting;

    % set up labels
    if isempty(o.labels)
        o.labels = o.variables;
    else
        assert(length(o.variables) == length(o.labels));
    end

    % set up orders
    if isempty(o.expected_order)
        % nothing to do
    elseif length(o.expected_order) == 1
        o.expected_order = o.expected_order * ones(size(o.variables));
    else
        assert(length(o.variables) == length(o.expected_order));
    end

    % setup io
    global verbosity;
    if ~isempty(verbosity) && verbosity
        o.cache = st_cache_create([], o.cache_id, o.prefix);
    else
        if o.enable_caching
            o.cache = st_cache_create([], o.cache_id, o.prefix);
        end

        o.enable_plotting = false;
        o.enable_geometry_plot = false;
        o.enable_latex_eoc = false;
    end
end

function [g] = geometry_update(options, npanels)
    options.npanels = npanels;
    g = st_geometry(options);
end

% vim:foldmethod=marker:
