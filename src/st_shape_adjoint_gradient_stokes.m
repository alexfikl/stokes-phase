% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [grad, terms] = st_shape_adjoint_gradient_stokes(x, ~, st)
    % Helper for :func:`st_shape_adjoint_gradient`.
    %
    % In a Lagrangian for a Stokes problem, we have terms coming
    % from the cost functional, terms coming from the boundary conditions
    % (in particular the jump at the interface) and terms coming from the
    % Stokes equations. This function computes the terms in the adjoint-based
    % gradient coming from the last of these groups.
    %
    % This is an internal function and not meant to be used.
    %
    % :arg x: target geometry struct.
    % :arg y: source geometry struct.
    % :arg st: solutions to the forward and adjoint Stokes problems.
    %
    % :returns: a struct of terms from the adjoint gradient.

    terms = struct();

    % solutions
    sf = st.forward;
    sa = st.adjoint;
    lambda = sf.param.lambda;

    % 2. gradient of the left-hand side of ``[[n sigma]] = jump``
    dd = @(i) (...
        sa.eps_ext(i, :) .* sf.eps_ext(i, :) - ...
        lambda * sa.eps_int(i, :) .* sf.eps_int(i, :));

    % add axisymmetric source term ur / r from gradient
    urr = (1.0 - lambda) * imag(sa.u) .* imag(sf.u) ./ imag(x.x).^2;
    % handle singular on axis points
    dudn_a = st_dot(sa.dudn_ext([1, end]), x.n([1, end]));
    dudn_f = st_dot(sf.dudn_ext([1, end]), x.n([1, end]));
    urr([1, end]) = (1 - lambda) * dudn_a .* dudn_f;

    terms.T3 = -2.0 * (dd(1) + 2.0 * dd(2) + dd(3) + urr);

    % 3. gradient for ``[[u]] = 0``
    fnstar = 0.5 * (sa.fn_ext + lambda * sa.fn_int);
    terms.T4 = st_dot(fnstar, sf.dudn_ext - sf.dudn_int);
    fn = 0.5 * (sf.fn_ext + lambda * sf.fn_int);
    terms.T5 = st_dot(fn, sa.dudn_ext - sa.dudn_int);

    grad = terms.T3 + terms.T4 + terms.T5;
end
