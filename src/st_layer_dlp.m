% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function D = st_layer_dlp(x, y, q, repr)
    % Evaluate the axisymmetric double-layer potential.
    %
    % This function computes the Stokes double-layer potential, as given in
    % Equation 4.3.1 of [Pozrikidis1992]_:
    %
    % .. math::
    %
    %     \mathcal{D}[\mathbf{q}](\mathbf{x}_0) = \int_\Sigma
    %         T_{ijk}(\mathbf{x}, \mathbf{x}_0)
    %         q_j(\mathbf{x}) n_k(\mathbf{x}) \,\mathrm{d}\mathbf{x},
    %
    % or in Equation 5.3.4:
    %
    % .. math::
    %
    %     \mathcal{D}_0[\mathbf{q}](\mathbf{x}_0) = n_k(\mathbf{x}_0) \int_\Sigma
    %         T_{ijk}(\mathbf{x}, \mathbf{x}_0)
    %         q_j(\mathbf{x}) \,\mathrm{d}\mathbf{x},
    %
    % where the kernel :math:`T_{ijk}` is given in Equation 2.2.11.
    % Taking the limit to the surface, both kernels have jump conditions,
    % which are given by Equations 4.1.7 and 4.1.8. These two representations
    % are called
    %
    % * ``'velocity'`` for :math:`\mathcal{D}`, since this layer potential
    %   appears in the representation with the velocity as the unknown,
    %   based on the Lorentz Reciprocal Theorem, as given in Equation 5.2.9.
    % * ``'density'`` for :math:`\mathcal{D}_0`, since this layer potential
    %   appears in the representation where a density is the unknown,
    %   as given by in Equation 5.3.9.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    % :arg repr: desired form of the double-layer potential.
    % :type repr: default ``'density'``
    %
    % :returns: if the density is given, then we evaluate the double-layer,
    %   otherwise, a matrix operator is returned.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.

    if ~exist('repr', 'var') || isempty(repr)
        repr = 'density';
    end
    repr = lower(repr);

    if ~any(contains({'density', 'velocity'}, repr))
        error('unknown representation: %s', repr);
    end

    if isfield(y.quad, 'fn')
        knl.quad = y.quad.fn;
    else
        knl.quad = y.quad.reg;
    end
    % allocate these here so we don't have do to it in the kernel
    knl.Q_c = zeros(2, 2, 2);
    knl.P_c = zeros(2, 2, 2);
    knl.V_c = zeros(4, x.nbasis);
    knl.W_c = zeros(2, 2);

    if exist('q', 'var') && ~isempty(q)
        knl.D = -4.0 * pi * q;
        knl.evaluate = str2func(sprintf('st_layer_dlp_%s_apply_kernel', repr));

        knl = st_layer_evaluate(x, y, knl, q);
        D = knl.D;
    else
        knl.Dxx = -4.0 * pi * eye(length(x.x));
        knl.Dxy = zeros(length(x.x));
        knl.Dyx = zeros(length(x.x));
        knl.Dyy = -4.0 * pi * eye(length(x.x));
        knl.evaluate = str2func(sprintf('st_layer_dlp_%s_matrix_kernel', repr));

        knl = st_layer_evaluate(x, y, knl);
        D = [knl.Dxx, knl.Dxy; ...
             knl.Dyx, knl.Dyy];
    end
end

function [knl] = st_layer_dlp_density_apply_kernel(knl, target, source) %#ok
    % target index
    i = target.i;
    % sources and targets
    y = source.x;
    x = target.x;

    Q = knl.Q_c;
    ellint = @st_ellint_order5;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order5_axis;
    end

    % retrieve target variables
    nx_i = real(target.n(i)); ny_i = imag(target.n(i));
    qx_i = real(target.q(i)); qy_i = imag(target.q(i));

    % integrate kernel over segment
    Dx = 0.0; Dy = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);
        nx_k = real(source.n(n)); ny_k = imag(source.n(n));
        qx_k = real(source.q(n)); qy_k = imag(source.q(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I50, I51, I52, I53] = ellint(y(n), x(i), F, E);
        % compute kernel
        Q = st_knl_ax_stress_target(Q, y(n), x(i), I50, I51, I52, I53);

        % add interactions
        % NOTE: subtract singularity (see [1, Equation 2.22, 2.26])
        Dx = Dx + ...
            Q(1, 1, 1) * (qx_k * nx_i - qx_i * nx_k) * dS + ...
            Q(1, 1, 2) * (qx_k * ny_i - qy_i * nx_k) * dS + ...
            Q(1, 2, 1) * (qy_k * nx_i - qx_i * ny_k) * dS + ...
            Q(1, 2, 2) * (qy_k * ny_i - qy_i * ny_k) * dS;
        Dy = Dy + ...
            Q(2, 1, 1) * (qx_k * nx_i - qx_i * nx_k) * dS + ...
            Q(2, 1, 2) * (qx_k * ny_i - qy_i * nx_k) * dS + ...
            Q(2, 2, 1) * (qy_k * nx_i - qx_i * ny_k) * dS + ...
            Q(2, 2, 2) * (qy_k * ny_i - qy_i * ny_k) * dS;
    end

    knl.D(i) = knl.D(i) + complex(Dx, Dy);
end

function [knl] = st_layer_dlp_velocity_apply_kernel(knl, target, source) %#ok
    % target index
    i = target.i;
    % target density
    qx_i = real(target.q(i)); qy_i = imag(target.q(i));
    % sources and targets
    y = source.x;
    x = target.x;

    Q = knl.Q_c;
    P = knl.P_c;
    ellint = @st_ellint_order5;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order5_axis;
    end

    Dx = 0.0; Dy = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);
        nx_k = real(source.n(n)); ny_k = imag(source.n(n));
        qx_k = real(source.q(n)); qy_k = imag(source.q(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I50, I51, I52, I53] = ellint(y(n), x(i), F, E);
        % compute kernels
        Q = st_knl_ax_stress_source(Q, y(n), x(i), I50, I51, I52, I53);
        P = st_knl_ax_stress_target(P, y(n), x(i), I50, I51, I52, I53);

        % add interactions
        % NOTE: subtract singularity (see [1, Equationm 2.24b])
        Dx = Dx + ...
            ((Q(1, 1, 1) * nx_k + Q(1, 1, 2) * ny_k) * qx_k - ...
             (P(1, 1, 1) * nx_k + P(1, 2, 1) * ny_k) * qx_i) * dS + ...
            ((Q(1, 2, 1) * nx_k + Q(1, 2, 2) * ny_k) * qy_k - ...
             (P(1, 1, 2) * nx_k + P(1, 2, 2) * ny_k) * qy_i) * dS;
        Dy = Dy + ...
            ((Q(2, 1, 1) * nx_k + Q(2, 1, 2) * ny_k) * qx_k - ...
             (P(2, 1, 1) * nx_k + P(2, 2, 1) * ny_k) * qx_i) * dS + ...
            ((Q(2, 2, 1) * nx_k + Q(2, 2, 2) * ny_k) * qy_k - ...
             (P(2, 1, 2) * nx_k + P(2, 2, 2) * ny_k) * qy_i) * dS;
    end

    knl.D(i) = knl.D(i) + complex(Dx, Dy);
end

function [knl] = st_layer_dlp_density_matrix_kernel(knl, target, source) %#ok
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % target normal
    nx_i = real(target.n(i)); ny_i = imag(target.n(i));
    % sources and targets
    y = source.x;
    x = target.x;

    Q = knl.Q_c;
    ellint = @st_ellint_order5;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order5_axis;
    end

    % compute operators
    D0 = knl.V_c; D0(:, :) = 0.0;
    D1 = knl.W_c; D1(:, :) = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);
        nx_k = real(source.n(n)); ny_k = imag(source.n(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I50, I51, I52, I53] = ellint(y(n), x(i), F, E);
        % compute kernel
        Q = st_knl_ax_stress_target(Q, y(n), x(i), I50, I51, I52, I53);

        % add source interactions
        for k = 1:nbasis
            bnk = dS * basis(n, k);

            D0(1, k) = D0(1, k) + ...
                (Q(1, 1, 1) * nx_i + Q(1, 1, 2) * ny_i) * bnk;
            D0(2, k) = D0(2, k) + ...
                (Q(1, 2, 1) * nx_i + Q(1, 2, 2) * ny_i) * bnk;
            D0(3, k) = D0(3, k) + ...
                (Q(2, 1, 1) * nx_i + Q(2, 1, 2) * ny_i) * bnk;
            D0(4, k) = D0(4, k) + ...
                (Q(2, 2, 1) * nx_i + Q(2, 2, 2) * ny_i) * bnk;
        end

        % add target interactions
        D1(1, 1) = D1(1, 1) + (Q(1, 1, 1) * nx_k + Q(1, 2, 1) * ny_k) * dS;
        D1(1, 2) = D1(1, 2) + (Q(1, 1, 2) * nx_k + Q(1, 2, 2) * ny_k) * dS;
        D1(2, 1) = D1(2, 1) + (Q(2, 1, 1) * nx_k + Q(2, 2, 1) * ny_k) * dS;
        D1(2, 2) = D1(2, 2) + (Q(2, 1, 2) * nx_k + Q(2, 2, 2) * ny_k) * dS;
    end

    % add source interactions in panel
    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.Dxx(i, j) = knl.Dxx(i, j) + D0(1, k);
        knl.Dxy(i, j) = knl.Dxy(i, j) + D0(2, k);
        knl.Dyx(i, j) = knl.Dyx(i, j) + D0(3, k);
        knl.Dyy(i, j) = knl.Dyy(i, j) + D0(4, k);
    end

    % add target interactions in panel
    knl.Dxx(i, i) = knl.Dxx(i, i) - D1(1, 1);
    knl.Dxy(i, i) = knl.Dxy(i, i) - D1(1, 2);
    knl.Dyx(i, i) = knl.Dyx(i, i) - D1(2, 1);
    knl.Dyy(i, i) = knl.Dyy(i, i) - D1(2, 2);
end

function [knl] = st_layer_dlp_velocity_matrix_kernel(knl, target, source) %#ok
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % sources and targets
    y = source.x;
    x = target.x;

    Q = knl.Q_c;
    P = knl.P_c;
    ellint = @st_ellint_order5;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order5_axis;
    end

    % compute layer operator
    D0 = knl.V_c; D0(:, :) = 0.0;
    D1 = knl.W_c; D1(:, :) = 0.0;

    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);
        nx_k = real(source.n(n)); ny_k = imag(source.n(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I50, I51, I52, I53] = ellint(y(n), x(i), F, E);

        % compute kernels
        Q = st_knl_ax_stress_source(Q, y(n), x(i), I50, I51, I52, I53);
        P = st_knl_ax_stress_target(P, y(n), x(i), I50, I51, I52, I53);

        % add source interactions
        for k = 1:nbasis
            bnk = dS * basis(n, k);

            D0(1, k) = D0(1, k) + ...
                (Q(1, 1, 1) * nx_k + Q(1, 1, 2) * ny_k) * bnk;
            D0(2, k) = D0(2, k) + ...
                (Q(1, 2, 1) * nx_k + Q(1, 2, 2) * ny_k) * bnk;
            D0(3, k) = D0(3, k) + ...
                (Q(2, 1, 1) * nx_k + Q(2, 1, 2) * ny_k) * bnk;
            D0(4, k) = D0(4, k) + ...
                (Q(2, 2, 1) * nx_k + Q(2, 2, 2) * ny_k) * bnk;
        end

        % add target interactions
        D1(1, 1) = D1(1, 1) + (P(1, 1, 1) * nx_k + P(1, 2, 1) * ny_k) * dS;
        D1(1, 2) = D1(1, 2) + (P(1, 1, 2) * nx_k + P(1, 2, 2) * ny_k) * dS;
        D1(2, 1) = D1(2, 1) + (P(2, 1, 1) * nx_k + P(2, 2, 1) * ny_k) * dS;
        D1(2, 2) = D1(2, 2) + (P(2, 1, 2) * nx_k + P(2, 2, 2) * ny_k) * dS;
    end

    % add source interactions in panel
    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.Dxx(i, j) = knl.Dxx(i, j) + D0(1, k);
        knl.Dxy(i, j) = knl.Dxy(i, j) + D0(2, k);
        knl.Dyx(i, j) = knl.Dyx(i, j) + D0(3, k);
        knl.Dyy(i, j) = knl.Dyy(i, j) + D0(4, k);
    end

    % add target interactions in panel
    knl.Dxx(i, i) = knl.Dxx(i, i) - D1(1, 1);
    knl.Dxy(i, i) = knl.Dxy(i, i) - D1(1, 2);
    knl.Dyx(i, i) = knl.Dyx(i, i) - D1(2, 1);
    knl.Dyy(i, i) = knl.Dyy(i, i) - D1(2, 2);
end

% vim:foldmethod=marker:
