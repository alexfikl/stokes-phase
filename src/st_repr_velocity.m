% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function u = st_repr_velocity(x, y, lambda, bc, method, uguess)
    % Solve a two-phase Stokes equation for a given domain.
    %
    % This function uses the velocity representation from Problem 5.2.2
    % in [Pozrikidis1992]_. The iterative solver to be used can be prescribed
    % with the `method` argument
    %
    % * ``'gmres'``: uses ``gmres``.
    % * ``'direct'``: uses ``mldivide``, i.e. ``A \ b``.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg lambda: viscosity ratio.
    % :arg bc: see :func:`st_boundary_condition_options`.
    % :arg method: method used to solve the system.
    % :type method: default ``'direct'``
    % :arg uguess: initial guess if using ``gmres``.
    % :type uguest: default ``[]``
    %
    % :returns: velocity field on the surface.
    % :see also: :func:`st_layer_slp`, :func:`st_layer_dlp`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.
    %   [2] C. Pozrikidis, Introduction to Theoretical and Computational
    %   Fluid Dynamics, 2001.

    if ~exist('method', 'var') || isempty(method)
        % the system is well conditioned, so this is a good choice.
        method = 'direct';
    end

    if ~exist('uguess', 'var')
        uguess = [];
    end

    % number of points
    npoints = length(x.x);
    % compute jump in normal stress
    deltaf = bc.fn.jump(x);
    % compute freestream boundary condition
    uinf = bc.fn.uinf(x);

    % compute single-layer
    deltaf = st_layer_slp(x, y, deltaf);
    % compute right-hand side
    u = 2.0 / (1.0 + lambda) * uinf - ...
        1.0 / (4.0 * pi * (1.0 + lambda)) * deltaf;

    if abs(lambda - 1.0) < 1.0e-14
        return;
    end

    % compute double-layer potential
    D = st_layer_dlp(x, y, [], 'velocity');
    D = eye(size(D)) - ...
        (1.0 - lambda) / (4.0 * pi * (1.0 + lambda)) * D;

    % solve system
    u = [real(u), imag(u)]';
    if strcmp(method, 'gmres')
        [u, flag] = gmres(D, u, uguess);
        if flag ~= 0
            fprintf('GMRES did not converge! Error: %d\n', flag);
        end
    else
        u = D \ u;
    end

    u = u(1:npoints)' + 1.0j * u(npoints + 1:end)';
end

% vim:foldmethod=marker:
