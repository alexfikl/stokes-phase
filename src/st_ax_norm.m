% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [normf] = st_ax_norm(y, f, p)
    % Compute the axisymmetric :math:`L^p` norm over a surface.
    %
    % :arg y: source geometry information.
    % :arg f: variable prescribed at all the source points.
    % :arg p: a real scalar representing the norm order.
    % :type p: default :math:`2`.
    %
    % :returns: norm.

    if ~exist('p', 'var') || isempty(p)
        p = 2;
    end

    % make sure our variable is at the source points
    if isa(f, 'function_handle')
        f = f(y);
    elseif length(f) == 1
        % constants are fine
    elseif length(f) ~= length(y.x)
        error('f must be prescribed at source nodes. see `st_interpolate`.')
    end

    switch p
    case 1
        normf = st_ax_integral(y, abs(f));
    case 2
        normf = sqrt(st_ax_integral(y, abs(f).^2));
    case 'inf'
        normf = max(abs(f));
    otherwise
        error('unknow norm: %d', p);
    end
end

% vim:foldmethod=marker:
