% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function D = st_layer_tangent_stress(x, y, q)
    % Evaluate the axisymmetric tangent double-layer potential.
    %
    % This function computes the tangent Stokes double-layer potential, as
    % given by:
    %
    % .. math::
    %
    %     \mathcal{D}_t[\mathbf{q}](\mathbf{x}_0) = t_k(\mathbf{x}_0) \int_\Sigma
    %         T_{ijk}(\mathbf{x}, \mathbf{x}_0)
    %         q_j(\mathbf{x}) \,\mathrm{d}\mathbf{x},
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: if the density is given, then we evaluate the double-layer,
    %   otherwise, a matrix operator is returned.

    if isfield(y.quad, 'ft')
        knl.quad = y.quad.ft;
    else
        knl.quad = y.quad.cpv;
    end
    % allocate these here so we don't have do to it in the kernel
    knl.Q_c = zeros(2, 2, 2);
    knl.V_c = zeros(4, x.nbasis);

    if exist('q', 'var') && ~isempty(q)
        knl.D = zeros(1, length(x.x), 'like', x.x);
        knl.evaluate = @st_layer_tangent_stress_density_apply_kernel;

        knl = st_layer_evaluate(x, y, knl, q);
        D = knl.D;
    else
        knl.Dxx = zeros(length(x.x));
        knl.Dxy = zeros(length(x.x));
        knl.Dyx = zeros(length(x.x));
        knl.Dyy = zeros(length(x.x));
        knl.evaluate = @st_layer_tangent_stress_density_matrix_kernel;

        knl = st_layer_evaluate(x, y, knl);
        D = [knl.Dxx, knl.Dxy; ...
             knl.Dyx, knl.Dyy];
    end
end

function [knl] = st_layer_tangent_stress_density_apply_kernel(knl, target, source)
    % target index
    i = target.i;
    % target tangent
    tx_i = real(target.t(i)); ty_i = imag(target.t(i));
    % source and target points
    y = source.x;
    x = target.x;

    Q = knl.Q_c;
    ellint = @st_ellint_order5;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order5_axis;
    end

    Dx = 0.0; Dy = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);
        qx_k = real(source.q(n)); qy_k = imag(source.q(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I50, I51, I52, I53] = ellint(y(n), x(i), F, E);
        % compute kernel
        Q = st_knl_ax_stress_target(Q, y(n), x(i), I50, I51, I52, I53);

        % add interactions
        Dx = Dx + ...
            (Q(1, 1, 1) * qx_k * tx_i + ...
             Q(1, 1, 2) * qx_k * ty_i + ...
             Q(1, 2, 1) * qy_k * tx_i + ...
             Q(1, 2, 2) * qy_k * ty_i) * dS;
        Dy = Dy + ...
            (Q(2, 1, 1) * qx_k * tx_i + ...
             Q(2, 1, 2) * qx_k * ty_i + ...
             Q(2, 2, 1) * qy_k * tx_i + ...
             Q(2, 2, 2) * qy_k * ty_i) * dS;
    end

    knl.D(i) = knl.D(i) + complex(Dx, Dy);
end

function [knl] = st_layer_tangent_stress_density_matrix_kernel(knl, target, source)
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % target tangent
    tx_i = real(target.t(i)); ty_i = imag(target.t(i));
    % source and target points
    y = source.x;
    x = target.x;

    Q = knl.Q_c;
    ellint = @st_ellint_order5;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order5_axis;
    end

    % compute operators
    D0 = knl.V_c;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [I50, I51, I52, I53] = ellint(y(n), x(i), F, E);
        % compute kernel
        Q = st_knl_ax_stress_target(Q, y(n), x(i), I50, I51, I52, I53);

        % add source interactions
        for k = 1:nbasis
            bnk = dS * basis(n, k);

            D0(1, k) = D0(1, k) + ...
                (Q(1, 1, 1) * tx_i + Q(1, 1, 2) * ty_i) * bnk;
            D0(2, k) = D0(2, k) + ...
                (Q(1, 2, 1) * tx_i + Q(1, 2, 2) * ty_i) * bnk;
            D0(3, k) = D0(3, k) + ...
                (Q(2, 1, 1) * tx_i + Q(2, 1, 2) * ty_i) * bnk;
            D0(4, k) = D0(4, k) + ...
                (Q(2, 2, 1) * tx_i + Q(2, 2, 2) * ty_i) * bnk;
        end
    end

    % add source interactions in panel
    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.Dxx(i, j) = knl.Dxx(i, j) + D0(1, k);
        knl.Dxy(i, j) = knl.Dxy(i, j) + D0(2, k);
        knl.Dyx(i, j) = knl.Dyx(i, j) + D0(3, k);
        knl.Dyy(i, j) = knl.Dyy(i, j) + D0(4, k);
    end
end

% vim:foldmethod=marker:
