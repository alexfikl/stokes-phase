% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [geometry] = st_geometry_options(varargin)
    % Default options for :func:`st_geometry`.
    %
    % Accepted fields are
    %
    %   * ``npanels``: number of panels / elements.
    %   * ``nnodes``: number of quadrature nodes on the non-singular panels.
    %   * ``quadtype``: type of the quadrature rules for regular panels.
    %   * ``nbasis``: number of collocation nodes / basis functions.
    %   * ``curve(xi)``: a function handle that returns the axisymmetric
    %     curve, e.g. :func:`st_mesh_curve`.
    %   * ``x``: struct field by :func:`st_geometry` by calling
    %     :func:`st_point_collocation`.
    %   * ``y``: struct field by :func:`st_geometry` by calling
    %     :func:`st_point_quadrature`.
    %   * ``s_singularity`` and ``s_quad``: variables passed to
    %     :func:`st_layer_quadrature` to construct all the necessary
    %     quadrature rules for singular panels.
    %   * ``user``: a struct containing any user-defined options.
    %
    % :returns: options struct.

    % {{{ default options

    % geometry parameters
    d.npanels = 64;
    d.nnodes = 8;
    d.nbasis = 4;
    d.curve = [];

    % geometry
    d.x = [];
    d.y = [];

    % quadrature rules for regular panels
    d.quadtype = 'leggauss';
    d.s_quad = struct();
    d.s_singularity = {};

    % user defined options
    d.user = struct();

    % }}}

    % {{{ customizations

    geometry = st_struct_update(d, st_struct_varargin(varargin{:}));

    if ischar(geometry.curve) || isstring(geometry.curve)
        geometry.curve = @(xi) st_mesh_curve(xi, char(geometry.curve), geometry.user);
    end

    % unify s_singularity and s_quad
    if geometry.nnodes > 0
        s_quad_fields = fieldnames(geometry.s_quad);
        geometry.s_singularity = unique([geometry.s_singularity, s_quad_fields]);

        if length(s_quad_fields) > 1
            if length(geometry.s_singularity) ~= length(s_quad_fields)
                error('incompatible definition for required singular quadratures');
            end
        end
    else
        % NOTE: this is used in a few tests to disable the quadrature or
        % collocation parts of the geometry struct.
        geometry.s_quad = struct();
        geometry.s_singularity = {};
    end

    % }}}
end

% vim:foldmethod=marker:
