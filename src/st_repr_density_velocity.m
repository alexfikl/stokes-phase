% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [u] = st_repr_density_velocity(x, y, q, bc)
    % Compute surface velocity field.
    %
    % The surface velocity is computed using the single-layer potential from
    % :func:`st_layer_slp`. It has no jump at the interface, since the velocity
    % has no jump at the interface, so the solution is given by
    % Equation 5.3.1 in [Pozrikidis1992]_.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: velocity field on the interface.

    if ~isempty(q)
        u = bc.fn.uinf(x);
        if norm(q, Inf) < 1.0e-10
            return;
        end

        u = u + st_layer_slp(x, y, q);
    else
        u = st_layer_slp(x, y);
    end
end

% vim:foldmethod=marker:
