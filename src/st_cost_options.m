% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cost] = st_cost_options(varargin)
    % Constructs a default struct for defining cost functionals.
    %
    % The main fields accepted are
    %
    % * ``param``: a structure containing flow parameters. The mandatory
    %   parameters are the Capillary number ``Ca`` and the viscosity
    %   ratio ``lambda``. This should be consistent between various
    %   objects, e.g. like :func:`st_boundary_condition_options`.
    % * ``variables``: a cell array of variable names needed to compute the
    %   cost functional. These are passed to :func:`st_repr_density_solution`.
    % * ``forward_variables``: a cell array of variable names needed in the
    %   computation of the adjoint gradient. Only the variables explicitly
    %   in the cost functional are required.
    % * ``adjoint_variables``: a cell array that should be left empty.
    % * ``optype``: can be ``'scalar'`` or ``'vector'`` for the normal
    %   component of the shape gradient or the full gradient.
    % * ``user``: a struct containing user defined options, which is empty
    %   by default.
    %
    % It can also contain a struct ``fn`` of function handles of the type
    % ``f(cost, t, x, y, st)``, where ``st`` is a struct containing the
    % Stokes solution (forward and adjoint, as necessary). The defined fields
    % are:
    %
    % * ``evaluate``: evaluates the cost functional.
    % * ``gradient_x``: shape gradient of the cost functional. The
    %   shape gradient is given in a weak form.
    % * ``gradient_u``: gradient with respect to the velocity field.
    % * ``gradient_s``: gradient with respect to the traction field.
    % * ``gradient_ca``: gradient with respect to the surface tension.
    % * ``operator_x``: matrix operator representing the shape derivative
    %   as an action on the adjoint variables.
    %
    % Additional fields used in finite difference calculations involving the
    % cost functional (see :func:`st_shape_finite_gradient`) are
    %
    % * ``eps``: a step size for finite difference
    % * ``bump(cost, x, y, f)``: a function handle that constructs an
    %   appropriate bump function. By default, this is defined by
    %   :func:`st_shape_finite_bump`.
    % * ``bump_type``, ``bump_cells``, ``bump_eps`` and ``sigma`` are all
    %   parameters that are passed to :func:`st_shape_finite_bump`.
    %
    % This function is mostly meant for custom cost functionals. Several
    % examples, e.g. :func:`st_cost_normal_velocity`, are already included.
    %
    % :arg varargin: key-value pairs, where additional keys are ignored.
    % :returns: a struct with the above fields initialized to their defaults.

    % {{{ default parameters

    % flow parameters
    d.param = struct();

    % variables in cost functional
    d.variables = {};
    % variables in adjoint gradient
    d.forward_variables = {};
    d.adjoint_variables = {};

    d.optype = 'scalar';

    % geometry
    % FIXME: this should be something in `user` or `param`
    d.desired_curve = [];
    d.x = [];
    d.y = [];

    % evaluation
    d.fn = cost_default_functions();
    % allow user defined options
    d.user = struct();

    % finite difference
    d.eps = 1.0e-6;
    % finite difference bump
    % NOTE: see `st_shape_finite_bump` and `st_shape_finite_bump_sigma`
    d.bump_type = 'xi';
    d.bump_cells = 4;
    d.bump_eps = 1.0e-2;
    d.bump_normalized = true;
    d.sigma = 1.0e-2;
    d.bump = @st_shape_finite_bump;

    % }}}

    % {{{ customizations

    cost = st_struct_update(d, st_struct_varargin(varargin{:}));

    if ~strcmp(cost.optype, {'scalar', 'vector'})
        error('unknown operator type: %s', cost.optype);
    end

    % add variables for adjoint based gradients
    variables = unique([cost.variables, ...
                        cost.forward_variables, ...
                        cost.adjoint_variables]);
    if any(contains(variables, 'exterior'))
        assert(~any(contains(variables, 'two_phase')));

        vars_cost = {'exterior'};
        vars_forward = {'exterior', 'dudn'};
        vars_adjoint = {'exterior', 'fn'};
    elseif any(contains(variables, 'two_phase'))
        assert(~any(contains(variables, 'exterior')));

        vars_cost = {'two_phase'};
        if abs(cost.param.lambda - 1.0) < 1.0e-14
            vars_forward = {'two_phase', 'dudn'};
            vars_adjoint = {'two_phase', 'dudn', 'u'};
        else
            vars_forward = {'two_phase', 'eps', 'dudn', 'fn'};
            vars_adjoint = {'two_phase', 'eps', 'dudn', 'fn', 'u'};
        end
    else
        vars_cost = {};
        vars_forward = {};
        vars_adjoint = {};
    end

    cost.variables = unique([cost.variables, vars_cost]);
    cost.forward_variables = unique([cost.forward_variables, vars_forward]);
    cost.adjoint_variables = unique([cost.adjoint_variables, vars_adjoint]);

    % }}}
end

function [fn] = cost_default_functions()
    fn.evaluate = @cost_not_defined;
    fn.gradient_x = @cost_not_defined;
    fn.gradient_u = @cost_not_defined;
    fn.gradient_s = @cost_not_defined;
    fn.gradient_ca = @cost_not_defined;

    fn.operator_x = @cost_not_defined;
end

function [r] = cost_not_defined(~, ~, ~, ~, ~)  %#ok
    error('NotImplementedError: this function needs to be user defined.');
end
