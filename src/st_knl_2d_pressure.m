% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [P] = st_knl_2d_pressure(P, x, x0, varargin)
    % 2D free space pressure kernel.
    %
    % Given in Equation 2.6.19 in [Pozrikidis1992]_.
    %
    % :arg x: source point.
    % :arg x0: target point.
    %
    % :returns: an array of size ``(2, 2)`` containing the kernel components.

    RX = real(x) - real(x0);
    RY = imag(x) - imag(x0);
    R = sqrt(RX^2 + RY^2);

    % kernel
    P(1) = 2 * RX / R^2;
    P(2) = 2 * RY / R^2;
end

% vim:foldmethod=marker:
