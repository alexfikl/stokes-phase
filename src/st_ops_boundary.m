% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [A, b] = st_ops_boundary(x, A, b, boundary_type, bc)
    % Modify operator and right-hand side for boundary conditions
    %
    % :arg A: matrix operator.
    % :arg b: right-hand side vector.
    % :arg boundary_type: type of the boundary: `none`, `dirichlet` or
    %   `neumann`.
    % :arg bc: array with two values for the left and right boundary
    %   conditions.
    %
    % :return: a tuple ``(A, b)`` with the modified matrix and right-hand
    %   side vector.


    from the traction, we have that the normal component is

    if ~exist('boundary_type', 'var') || isempty(boundary_type)
        boundary_type = 'none';
    end
    boundary_type = lower(boundary_type);

    if ~exist('bc', 'var') || isempty(bc)
        bc = [0.0, 0.0];
    end

    if length(bc) == 1
        bc = [bc, bc];
    end

    % number of basis functions
    nbasis = length(x.nodes.basis);
    dxi = x.panels(2) - x.panels(1);

    % set boundary conditions
    switch boundary_type
    case 'none'
        % nothing to do
    case 'dirichlet'
        A(1, :) = 0.0;
        A(1, 1) = 1.0;
        A(end, :) = 0.0;
        A(end, end) = 1.0;

        b(1) = bc(1);
        b(end) = bc(2);
    case 'neumann'
        % get basis function derivatives evaluated at the boundary points
        dphi = zeros(2, nbasis);
        for p = 1:nbasis
            dphi(1, p) = double(x.nodes.dbasis{p}(-1.0));
            dphi(2, p) = double(x.nodes.dbasis{p}(+1.0));
        end

        A(1, :) = 0.0;
        A(1, 1:nbasis) = dphi(1, :);
        A(end, :) = 0.0;
        A(end, end - nbasis + 1:end) = dphi(end, :);

        b(1) = bc(1) * dxi;
        b(end) = bc(2) * dxi;
    otherwise
        error('unknown boundary type: %s', boundary_type);
    end
end

% vim:foldmethod=marker:
