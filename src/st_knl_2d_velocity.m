% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [M] = st_knl_2d_velocity(M, x, x0, varargin)
    % 2D free space Stokeslet kernel.
    %
    % Given in Equation 2.6.17 in [Pozrikidis1992]_.
    %
    % :arg x: source point.
    % :arg x0: target point.
    %
    % :returns: an array of size ``(2, 2)`` containing the kernel components.

    RX = real(x) - real(x0);
    RY = imag(x) - imag(x0);
    R = sqrt(RX^2 + RY^2);

    % kernel
    M(1, 1) = -log(R) + RX^2 / R^2;
    M(1, 2) = RX * RY / R^2;
    M(2, 1) = RY * RX / R^2;
    M(2, 2) = -log(R) + RY^2 / R^2;
end

% vim:foldmethod=marker:
