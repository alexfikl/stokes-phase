% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function grad = st_jump_shape_gradient(jump, t, x, y, st)
    % Computes the shape gradient components of a jump term.
    %
    % :arg jump: jump information compatible with :func:`st_jump_options`.
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg st: forward and adjoint solutions to the Stokes problem.
    %
    % :returns: the gradient.

    % adjoint variables
    u = st.adjoint.u;
    dudn = 0.5 * (st.adjoint.dudn_ext + st.adjoint.dudn_int);

    % jumps
    jmp = jump.fn.evaluate(jump, t, x, y, st);
    G = jump.fn.operator_shape_x(jump, t, x, y, st);

    switch jump.optype
    case 'scalar'
        % compute terms
        T0 = G * st_array_stack(u)';
        T1 = x.kappa .* st_dot(u, jmp);
        T2 = st_dot(dudn, jmp);

        % add up to get gradient
        M = st_ops_mass(x, y);
        grad = (T0 + M * (T1 + T2).').';
    case 'vector'
        % compute terms
        T0 = st_array_unstack(G * st_array_stack(u));
        T1 = x.kappa .* st_dot(u, jmp);
        T2 = st_dot(dudn, jmp) .* x.n;
        T3 = T1 + T2;

        % add up to get gradient
        M = st_ops_mass(x, y);
        grad = T0 + (M * T3.').';
    otherwise
        error('unknown operator type: `%s`', jump.optype);
    end

    % terms.T0 = T0;
    % terms.T1 = T1;
    % terms.T2 = T2;
end
