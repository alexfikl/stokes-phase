% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [jump] = st_jump_capillary_variable(varargin)
    % Defines a variable surface tension traction jump.
    %
    % The jump is given it its non-dimensional form by the Young-Laplace law
    %
    % .. math::
    %
    %    [\![\mathbf{n} \cdot \sigma]\!] =
    %        \frac{1}{\mathrm{Ca}_0} (\gamma \kappa \mathbf{n} + \nabla_\Sigma \gamma),
    %
    % where :math:`\gamma` is the surface tension and :math:`\nabla_\Sigma` is
    % the tangential gradient.
    %
    % :returns: a struct compatible with :func:`st_jump_options`.

    jump = st_jump_options(varargin{:});
    if ~isfield(jump.param, 'gamma')
        error('jump is missing surface tension');
    end

    jump.fn.evaluate = @jump_capillary_variable_evaluate;
    jump.fn.gradient_x = @st_jump_shape_gradient;
    jump.fn.gradient_ca = @jump_capillary_variable_gradient_ca;

    jump.fn.operator_shape_x = @jump_capillary_variable_operator_u;
    jump.fn.operator_x = @st_jump_shape_operator;
end

% {{{ gradient function handles

function [r] = jump_capillary_variable_evaluate(jump, ~, x, ~, ~)
    Ca = jump.param.Ca;
    gamma = jump.param.gamma;
    nabla_gamma = st_derivative(x, gamma, x.xi, 'fft');

    r = 1.0 / Ca .* (gamma .* x.kappa .* x.n + nabla_gamma .* x.t);
end

function [r] = jump_capillary_variable_gradient_ca(jump, ~, x, ~, st)
    Ca = jump.param.Ca;

    u_dot_n = st_dot(st.adjoint.u, x.n);
    dudn = 0.5 * st_dot(st.forward.dudn_ext + st.forward.dudn_int, x.n);
    r = 1.0 / Ca * (2.0 * x.kappa .* u_dot_n + dudn);
end

% }}}

% {{{ shape gradient operators

function [G] = jump_capillary_variable_operator_u(jump, ~, x, y, ~)
    switch jump.optype
    case 'scalar'
        G = st_jump_capillary_variable_operator_scalar(jump, x, y);
    case 'vector'
        G = st_jump_capillary_variable_operator_vector(jump, x, y);
    end
end

function [G] = st_jump_capillary_variable_operator_scalar(jump, x, y)
    Ca = jump.param.Ca;
    gamma = jump.param.gamma;

    Gx = build_op(gamma .* kappa .* real(x.t), gamma .* real(x.n));
    Gy = build_op(gamma .* kappa .* imag(x.t), gamma .* imag(x.n));
    G = [Gx, Gy];

    function [O] = build_op(a, b)
        Nstar = st_ops_adjoint_normal(x, y, a);
        [KXstar, KPstar] = st_ops_adjoint_curvature(x, y, b);
        O = 1.0 / Ca * (Nstar + KXstar + KPstar);
    end
end

function [G] = st_jump_capillary_variable_operator_vector(jump, x, y)
    Ca = jump.param.Ca;
    gamma = jump.param.gamma;

    N = st_ops_adjoint_vector_normal(x, y, gamma .* x.kappa .* x.t);
    [KX, KP] = st_ops_adjoint_vector_curvature(x, y, gamma .* x.n);

    G = 1.0 / Ca * (N + KX + KP);
end

% }}}
