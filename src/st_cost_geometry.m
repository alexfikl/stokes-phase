% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cost] = st_cost_geometry(varargin)
    % Defines a geometry matching cost functional.
    %
    % It is given by
    %
    % .. math::
    %
    %     \mathcal{J} = \frac{1}{2} \int_{\Sigma}
    %         \|\mathbf{X}(T) - \mathbf{X}_d \|^2 \,\mathrm{d}S.
    %
    % This cost functional expects `param.tmax` to be present.
    %
    % :returns: a struct compatible with :func:`st_cost_options`.

    cost = st_cost_options(varargin{:});

    cost.fn.evaluate = @cost_geometry_evaluate;
    cost.fn.gradient_x = @cost_geometry_gradient_x;
    cost.fn.gradient_u = @cost_geometry_gradient_u;
    cost.fn.gradient_s = @cost_geometry_gradient_s;
    cost.fn.gradient_ca = @cost_geometry_gradient_ca;

    cost.fn.operator_x = @cost_geometry_operator_x;
end

function [r] = cost_geometry_evaluate(cost, t, ~, y, ~)
    r = 0.0;

    if abs(t - cost.param.tmax) < 1.0e-14
        r = st_ax_integral(y, 0.5 * abs(y.x - cost.y.x).^2);
    end
end

function [r] = cost_geometry_gradient_x(cost, t, x, y, ~)
    r = zeros(size(x.x));

    if abs(t - cost.param.tmax) < 1.0e-14
        dx = x.x - cost.x.x;
        r = st_dot(dx, x.n) + 0.5 * x.kappa .* abs(dx).^2;

        switch cost.optype
        case 'scalar'
            M = st_ops_mass(x, y);
            r = (M * r')';
        case 'vector'
            r = r .* x.n;
        end
    end
end

function [r] = cost_geometry_gradient_u(~, ~, ~, ~, ~)
    r = 0.0;
end

function [r] = cost_geometry_gradient_s(~, ~, ~, ~, ~)
    r = 0.0;
end

function [r] = cost_geometry_gradient_ca(~, ~, ~, ~, ~)
    r = 0.0;
end

% }}}

% {{{ operators

function [op] = cost_geometry_operator_x(cost, t, ~, ~, ~)
    if abs(t - cost.param.tmax) < 1.0e-14
        error('operator not available at tmax.');
    end

    op = 0.0;
end

% }}}
