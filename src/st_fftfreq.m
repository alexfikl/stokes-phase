% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function f = st_fftfreq(m, dx)
    % Compute FFT sample frequencies like
    % `numpy <https://docs.scipy.org/doc/numpy/reference/generated/numpy.fft.fftfreq.html#numpy.fft.fftfreq>`_.
    %
    % :arg m: window length.
    % :arg dx: domain size.
    %
    % :returns: array containing sample frequencies.

    if ~exist('dx', 'var') || isempty(dx)
        dx = 1.0;
    end
    dx = dx / (2.0 * pi);

    n1 = floor((m - 1) / 2);
    n2 = floor(m / 2);
    f = ifftshift(-n2:n1) / dx;
end

% vim:foldmethod=marker:
