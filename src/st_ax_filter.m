% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function fs = st_ax_filter(f, method, p)
    % Apply a filter to some data.
    %
    % There are a few implemented filters at the moment. They are
    %
    % * ``'fft'``: smoothing by convolving with a Gaussian in Fourier space.
    %   Note that in this case, `f` must be periodic. Its smoothing parameter
    %   is :math:`p`, with a smaller :math:`p` denoting less smoothing.
    % * ``'lele'``: spectral-like filter based on [Lele1992]_ formulas
    %   (C.2.4) and (C.2.11). The strength of the smoothing is determined by
    %   the number of smoothing passes :math:`p`.
    % * ``'lsq'``: least-squares method from [Eilers2003]_. The parameter
    %   determines the smoothing strength.
    %
    % :arg f: real or complex array.
    % :arg method: smoothing method.
    % :arg p: smoothing parameter.
    %
    % :returns: filtered data.

    % References:
    %   [1] S. K. Lele, Compact Finite Difference Schemes with Spectral-like
    %   Resolution, JCP, Vol. 103, 1992.

    if ~exist('method', 'var') || isempty(method)
        method = 'fft';
    end
    method = lower(method);

    if ~exist('p', 'var')
        p = [];
    end

    fs = f;
    if ~isempty(p) && p <= 0
        return;
    end

    fs = reshape(f, [1, length(f)]);
    switch method
    case 'fft'
        fs = st_ax_filter_fft(fs, p);
    case 'lele'
        fs = st_ax_filter_lele(fs, p);
    case 'lsq'
        fs = st_ax_filter_lsq(fs, p);
    otherwise
        error('unknown method: %s', method);
    end

    fs = st_array_reshape_like(fs(1:length(f)), f);
end

function [fs] = st_ax_filter_fft(f, p)
    if isempty(p)
        p = 1.0e-3;
    end

    % make periodic
    f = st_array_ax(f);
    % construct filter
    m = length(f);
    gk = exp(-(p * st_fftfreq(m)).^2);

    % filter data
    fs = ifft(fft(f) .* gk);
    if isreal(f)
        fs = real(fs);
    end
end

function [f] = st_ax_filter_lele(f, p)
    % define coefficients:
    %   * first row: see [1, Equation C.2.4] with alpha = beta = d = 0.
    %   * rest: see [1, Equation C.2.11]
    persistent a1 a2 a3;
    if isempty(a1)
        a1 = [-1.0 / 16.0, 4.0 / 16.0, 10.0 / 16.0, 4.0 / 16.0, -1.0 / 16.0];
        a2 = [15.0 / 16.0, 4.0 / 16.0, -6.0 / 16.0, 4.0 / 16.0, -1.0 / 16.0];
        a3 = [1.0 / 16.0, 12.0 / 16.0, 6.0 / 16.0, -4.0 / 16.0, 1.0 / 16.0];
    end

    if isempty(p)
        p = 1;
    end

    % smoothing passes
    n = length(f);
    fn = zeros(size(f), 'like', f);

    for k = 1:round(p)
        % handle inner points
        for i = 3:(n - 2)
            fn(i) = dot(a1, f(i - 2:i + 2));
        end

        % handle boundary terms
        fn(1) = dot(a2, f(1:5));
        fn(2) = dot(a3, f(1:5));

        fn(end - 1) = dot(fliplr(a3), f(end - 4:end));
        fn(end - 0) = dot(fliplr(a2), f(end - 4:end));

        % update values for next pass
        f = fn;
    end
end

function [fs] = st_ax_filter_lsq(f, p)
    % construct laplacian matrix
    persistent D;

    f = st_array_ax(f);
    if isempty(D) || length(f) ~= size(D, 1)
        % discrete laplacian
        D = ones(size(f));
        D = spdiags([D', -2.0 * D', D'], [-1, 0, 1], length(f), length(f));
        D(1, end) = 1; D(end, 1) = 1;

        D = D' * D;
    end

    if isempty(p)
        p = 0.1;
    end

    % add l2 regularization
    R = (speye(length(f)) + p * D);
    % solve least-squares problem
    fs = R \ f';
end

% vim:foldmethod=marker:
