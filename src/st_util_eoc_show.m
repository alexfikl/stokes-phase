% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [orders, fig] = st_util_eoc_show(eoc, filename, varargin)
    % Plot errors and print orders of convergence
    %
    % :arg filename: name of output file.
    % :arg errors: a struct with fields containing errors arrays.
    % :arg h: mesh sizes corresponding to each errors level.
    %
    % :return: convergence orders.

    % {{{ handle inputs

    global verbosity;
    is_verbose = ~isempty(verbosity) && verbosity;

    d.show_table = is_verbose;
    d.markers = [];
    d.xlabel = 'h_{\mathrm{max}}';
    d.ylabel = 'Error';
    d.legend_location = 'northeast';
    d.legend_labels = {};
    d.expected_order = [];

    d = st_struct_update(d, st_struct_varargin(varargin{:}));

    variables = eoc.variables;
    nvariables = length(variables);

    d.show_table = is_verbose && d.show_table;

    if isempty(d.expected_order)
        % nothing to do
    elseif length(d.expected_order) == 1
        d.expected_order = d.expected_order * ones(1, nvariables);
    else
        assert(length(d.expected_order) == nvariables);
    end

    if isempty(d.legend_labels)
        d.legend_labels = variables;
    end

    % }}}

    % {{{ compute orders

    h = eoc.h;
    errors = eoc.errors;

    orders = struct();
    for n = 1:length(variables)
        o = polyfit(log(h), log(errors.(variables{n})), 1);
        if isnan(o(1))
            % assuming the error was floating point zero for multiple steps,
            % which would give a NaN in polyfit
            orders.(variables{n}) = Inf;
        else
            orders.(variables{n}) = o(1);
        end
    end

    % }}}

    % {{{ print results

    if d.show_table
        t_orders = zeros(1, length(variables));
        t_errors = zeros(size(t_orders));
        t_expected = d.expected_order;

        for n = 1:length(variables)
            t_orders(n) = orders.(variables{n});
            t_errors(n) = min(errors.(variables{n}));
        end

        ttable = struct();
        ttable.variables = struct('values', {variables}, ...
                                  'header', 'Value');
        ttable.orders = struct('values', t_orders, ...
                               'header', 'Orders', ...
                               'fmt', '%.2f');
        if ~isempty(t_expected)
            ttable.expected = struct('values', t_expected, ...
                                     'header', 'Expected', ...
                                     'fmt', '> %.2f');
        end
        ttable.errors = struct('values', t_errors, ...
                              'header', 'MinError', ...
                              'fmt', '%.6e');
        st_util_table(ttable);
    end

    % }}}

    % {{{ plot errors

    if isempty(filename)
        return;
    end

    fig = figure('visible', 'off');
    fig_handle = gobjects(1, length(variables));
    fig_legend = cell(1, length(variables));

    set(gca, 'XScale', 'log', 'YScale', 'log');
    for n = 1:length(variables)
        fig_handle(n) = loglog(h, errors.(variables{n}), 'o--');

        if ~isnan(orders.(variables{n}))
            fig_legend{n} = sprintf('$%s: %.2f$', ...
                d.legend_labels{n}, orders.(variables{n}));
        else
            fig_legend{n} = sprintf('$%s$', ...
                d.legend_labels{n});
        end
    end

    set(gca, 'YMinorTick', 'on');
    set(get(gca, 'YAxis'), 'TickDirection', 'both');

    xlabel(sprintf('$%s$', d.xlabel));
    ylabel(sprintf('$%s$', d.ylabel));

    if ~isempty(d.legend_location)
        if ischar(d.legend_location)
            switch d.legend_location
            case 'none'
                % nothing to show
            otherwise
                legend(fig_handle, fig_legend, 'Location', d.legend_location);
            end
        elseif isa(d.legend_location, 'function_handle')
            d.legend_location(fig, gca, fig_handle, fig_legend);
        end
    end

    if nargout <= 1
        % only print if the caller doesn't want to add more stuff
        st_print(fig, filename);
        close(fig);
    end

    % }}}
end

% vim:foldmethod=marker:
