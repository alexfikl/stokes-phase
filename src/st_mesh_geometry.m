% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [t, s] = st_mesh_geometry(x, y)
    % Compute geometry.
    %
    % The resulting structs ``t`` and ``s`` contain all the fields normally
    % added by :func:`st_mesh_target_geometry` and all the fields of `x` and
    % `y`, if they are structs as well (uses :func:`st_struct_update`).
    %
    % Generally, the user would want to generate all the required geometry
    % information as follows:
    %
    % .. code-block:: matlab
    %
    %     shape_fn = @(xi) exp(2.0j * pi * xi);
    %     x = st_point_collocation(npanels, nbasis, shape_fn);
    %     y = st_point_quadrature(npanels, nnodes);
    %     [x, y] = st_mesh_geometry(x, y);
    %
    % to get all fields required in most functions.
    %
    % :arg x: target node information. It can be a complex array of
    %   collocation points or a struct with an `x` field (as
    %   returned by :func:`st_point_collocation`).
    % :arg y: source node information. It can be a real array of points
    %   in :math:`[0, 0.5]`, where the source geometry is evaluated, or a
    %   struct with a `xi` field (as returned by :func:`st_point_quadrature`).
    %
    % :returns: target geometry ``t``, as returned by :func:`st_mesh_target_geometry` and
    %   evaluated at the collocation points, with any additional fields from
    %   the input `x`.
    % :returns: source geometry ``s``, evaluated at the quadrature points, if
    %   provided. The source geometry is computed by interpolating
    %   from the target geometry in :func:`st_mesh_source_geometry`.

    % compute target (uniformly spaced) geometry information
    t = st_mesh_target_geometry(x);

    % compute source point geometry by interpolating the target points
    if exist('y', 'var') && ~isempty(y)
        s = st_mesh_source_geometry(t, y, 'fft');
    else
        s = struct();
    end
end

% vim:foldmethod=marker:
