% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [g] = st_interp_evaluate(x, f, xi, basis_id)
    % Evaluate basis functions on a new set of points.
    %
    % This is a slightly more general version of :func:`st_interpolate`
    % that can evaluate the basis functions or their derivatives up to second
    % order.
    %
    % :arg x: collocation point information.
    % :arg f: array to interpolate. We assume that the array is already
    %   given at the collocation points.
    % :arg xi: new set of points at which to evaluate `f`.
    % :arg basis_id: index of the basis functions. This basically corresponds
    %   to the order of the derivative, i.e. `0` is the basis function itself,
    %   which is equivalent to :func:`st_interpolate`, `1` is the first
    %   derivative, which is equivalent to :func:`st_derivative`, etc.
    %
    % :returns: expansion evaluated with the coefficients in `f`.

    if ~exist('basis_id', 'var') || isempty(basis_id)
        basis_id = 0;
    end

    % number of basis functions
    nbasis = x.nbasis;
    % basis functions
    switch basis_id
    case 0
        basis = x.nodes.basis;
    case 1
        basis = x.nodes.dbasis;
    case 2
        basis = x.nodes.ddbasis;
    otherwise
        error('unknown basis: %d', basis_id);
    end

    % check points
    if basis_id > 0
        if length(x.xi) == length(xi) && norm(x.xi - xi) < 1.0e-14
            error('cannot evaluate derivatives as collocation points');
        end
    end

    % check size of the given array
    ntargets = length(x.xi);
    if isa(f, 'function_handle')
        f = f(x);
    elseif length(f) ~= ntargets
        error('f must have the same size as the collocation points: %d ~= %d.', ...
              length(f), ntargets);
    end

    % find buckets for new points
    j = discretize(xi, x.panels);

    % translate coordinates to panel local
    xim = 0.5 * (x.panels(j + 1) + x.panels(j));
    dxi = 0.5 * (x.panels(j + 1) - x.panels(j));
    eta = (xi - xim) ./ dxi;

    % evaluate at each new point
    g = 0.0;
    for p = 1:nbasis
        g = g + double(basis{p}(eta)) .* f((j - 1) * (nbasis - 1) + p);
    end
end

% vim:foldmethod=marker:
