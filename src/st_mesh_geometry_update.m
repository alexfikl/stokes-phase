% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xout, yout] = st_mesh_geometry_update(x, y, xnew, with_geometry)
    % Update a given geometry to a new set of collocation points.
    %
    % This function updates the new points and calls :func:`st_mesh_geometry` to
    % recompute all the geometrical quantities on the new curve.
    %
    % :arg x: existing target geometry information.
    % :arg y: existing source geometry information.
    % :arg xnew: a new set of node coordinates with the same size as ``x.xi``.
    %   This can also be a function handle, in which case it is just evaluated
    %   on the computational grid ``x.xi``.
    %
    % :returns: a tuple ``(x, y)`` with the target and source geometries.

    if ~exist('y', 'var')
        y = [];
    end

    if ~exist('with_geometry', 'var') || isempty(with_geometry)
        with_geometry = true;
    end

    xout = x;
    yout = y;

    if isa(xnew, 'function_handle')
        xout.x = xnew(x.xi);
    else
        assert(length(xnew) == length(x.x))
        xout.x = xnew;
    end

    if with_geometry
        [xout, yout] = st_mesh_geometry(xout, yout);
    else
        % NOTE: keep in sync with `st_mesh_target_geometry`
        geometry_fields = {'n', 't', 'kappa', 'kappa_x', 'kappa_p', 'J', 's'};
        xout = rmfield(xout, [geometry_fields, 'zk', 'zhat']);
        yout = rmfield(yout, geometry_fields);

        yout.x = st_interpolate(xout, xout.x, yout.xi);
        yout.vertices = xout.vertices;
    end
end
