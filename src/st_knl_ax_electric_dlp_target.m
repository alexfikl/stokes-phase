% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function D = st_knl_ax_electric_dlp_target(D, x, x0, I30, I31)
    % Adjoint double-layer potential kernel for the Laplace equation.
    %
    % The kernel is not normalized by :math:`4 \pi` to match the Stokes
    % kernels, which are also not normalized.
    %
    % This kernel is meant to be dotted with the normal vector at the target.
    % The corresponding layer-potential is computed in :func:`st_layer_electric_dlp`.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg I: elliptic integrals, see :func:`st_ellint_order3`.
    %
    % :returns: an array of size ``(1, 2)`` containing the kernel components.
    % :see also: :func:`st_ellipke`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.

    X  = real(x);  Y  = imag(x);
    X0 = real(x0); Y0 = imag(x0);

    % build Green's function
    D(1) = Y * (X - X0) * I30;
    D(2) = Y * (Y * I31 - Y0 * I30);
end

% vim:foldmethod=marker:
