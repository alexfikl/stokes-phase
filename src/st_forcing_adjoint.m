% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [forcing] = st_forcing_adjoint(varargin)
    % Forcing for the adjoint evolution equation.
    %
    % This should not be used as is, but is mostly meant as a nice wrapper
    % for use in :func:`st_ode_unsteady_adjoint`.
    %
    % :returns: a struct compatible with :func:`st_forcing_options`.

    forcing = st_forcing_options(varargin{:});
    if ~isfield(forcing.user, 'grads')
        error('forcing function requires `grads`');
    end

    forcing.fn.evaluate = @forcing_adjoint_evaluate;
    forcing.fn.operator_x = @forcing_adjoint_operator_x;
end

% {{{ forcing_adjoint_evaluate

function [r] = forcing_adjoint_evaluate(forcing, t, x, y, st, is_weak)
    if ~exist('is_weak', 'var')
        is_weak = false;
    end

    grads = forcing.user.grads;
    M = st_ops_mass(x, y);

    % 1. gradient term from the problem data
    T1 = grads.fn.gradient_x(grads, t, x, y, st);
    % 2. gradient term from the Stokes equations
    if has_stokes_terms(forcing, t, x, y, st)
        T = st_shape_adjoint_gradient_stokes(x, y, st);
        T2 = M * (T .* x.n).';
    else
        T2 = 0.0;
    end
    % 3. put them all together
    r = T1 + T2.';

    % invert to get strong form
    if ~is_weak
        rx = st_ops_solve(x, M, real(r), 'neumann');
        ry = st_ops_solve(x, M, imag(r), 'dirichlet');
        r = rx + 1j * ry;
    end
end

% }}}

% {{{ forcing_adjoint_operator_x

function [op] = forcing_adjoint_operator_x(forcing, t, x, y, st)
    if abs(forcing.param.lambda - 1.0) > 1.0e-14
        error('only `lambda = 1` supported at the moment.');
    end
    grads = forcing.user.grads;

    % 1. gradient terms from the problem data
    T1 = grads.fn.operator_x(grads, t, x, y, st);
    % 2. gradient terms from the Stokes equations
    if has_stokes_terms(forcing, t, x, y, st)
        T2 = forcing_adjoint_stokes_operator_x(forcing, t, x, y, st);
    else
        T2 = 0.0;
    end

    % sum up operators and leave them in a weak form
    op = T1 + T2;
end

function [op] = forcing_adjoint_stokes_operator_x(~, ~, x, y, st)
    % NOTE: this matches st_shape_adjoint_gradient_stokes
    lambda = st.forward.param.lambda;
    if abs(lambda - 1.0) > 1.0e-14
        error('only `lambda = 1` supported at the moment.');
    end

    sf = st.forward;
    fn_avg = 0.5 * (sf.fn_ext + lambda * sf.fn_int);
    du_jmp = sf.dudn_ext - sf.dudn_int;
    ft_jmp = st_dot(sf.fn_ext - lambda * sf.fn_int, x.t);

    % weak operator
    b = -8.0 * pi * (fn_avg - st_dot(fn_avg, x.n) .* x.n);
    W1 = st_ops_vector_mass_product(x, y, x.n, b);
    b = 0.5 * ((1.0 + lambda) * du_jmp - 2.0 * ft_jmp .* x.t);
    W2 = st_ops_vector_mass_product(x, y, x.n, b);
    % layer potentials
    K1 = st_layer_dlp(x, y, [], 'density');
    % density operator
    Q = -1.0 / (4.0 * pi) / (1.0 + lambda);
    Q = st_array_tensor_product(x.n, Q * x.n);

    % put it all together
    op = (W1 - W2 * K1) * Q;
end

% }}}

% {{{ has_stokes_terms

function [r] = has_stokes_terms(forcing, t, x, y, st)
    jump = st_struct_field(forcing.user.grads.user, 'jump', []);
    if isempty(jump)
        r = false;
        return;
    end

    lambda = forcing.param.lambda;
    if abs(lambda - 1.0) > 1.0e-14
        r = true;
        return;
    end

    j = jump.fn.evaluate(jump, t, x, y, st);
    j_dot_t = norm(st_dot(j, x.t), Inf);

    r = j_dot_t > 1.0e-10;
end

% }}}
