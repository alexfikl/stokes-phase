% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [w] = st_array_unstack(v)
    % Transform a real array into a complex array half the size.
    %
    % This is a counterpart to :func:`st_array_stack` and satisfies
    %
    % .. code::
    %
    %     st_array_unstack(st_array_stack(x)) == x;
    %
    % :arg v: real array.
    % :type shape: default ``(1, length(v) / 2)``
    %
    % :returns: a complex array.

    m = length(v);
    if m == 1
        w = v;
        return;
    end

    if mod(m, 2) ~= 0
        error('array length is required to be even');
    end

    m = floor(m / 2);
    w = st_array_reshape_like(v(1:m) + 1.0j * v(m + 1:end), v);
end

% vim:foldmethod=marker:
