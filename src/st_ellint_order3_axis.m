% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [I10, I11, I30, I31, I32] = st_ellint_order3_axis(x, x0, ~, ~)
    % Compute elliptic integrals in Stokeslet for an on-axis point.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg F: elliptic integral of the first kind, see :func:`st_ellipke`.
    % :arg E: elliptic integral of the second kind.
    %
    % :returns: elliptic integrals described in Section 2.4 of
    %   [Pozrikidis1992]_ used to compute the Stokeslet. Namely, it computes
    %   :math:`I_{10}, I_{11}, I_{30}, I_{31}` and :math:`I_{32}`.

    R = 1.0 / abs(x - x0);

    I10 = 2.0 * pi * R;
    I11 = 0.0;
    I30 = 2.0 * pi * R^3;
    I31 = 0.0;
    I32 = 1.0 * pi * R^3;
end

% vim:foldmethod=marker:
