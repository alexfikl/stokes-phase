% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [quad] = st_layer_quadrature(x, y, singularity, opts)
    % Construct quadrature rules for singular layer potential integration.
    %
    % The *singularity* is a cell array of one of ``'reg'``, ``'log'``,
    % ``'cpv'``, ``'hfp'`` or a layer-potential identifiers accepted by
    % :func:`st_repr_density_solution`. If an identifier is given, they get
    % mapped to the actual type of the singularity.
    %
    % The `opts` is a struct with keys that match the elements of the
    % `singularity` cell array. Each field is then another struct that
    % specifices the fields:
    %
    % * `method`: one of the quadrature methods, e.g. :func:`st_quad_carley`.
    % * `nnodes`: number of quadrature nodes.
    % * other arguments accepted by the respective quadrature rule.
    %
    % An example construction can be as follows:
    %
    % .. code:: matlab
    %
    %     s_log = struct('nnodes', 4, 'npoly', 4, 'method', 'carley');
    %     s_fn = struct('nnodes', 4, 'method', 'leggauss');
    %     o = struct('log', s_log, 'fn', s_fn);
    %     quad = st_layer_quadrature(x, y, {'log', 'fn', 'u'}, o);
    %
    % We can see that the exact options for the ``'log'`` and ``'fn'`` fields
    % are specified, but the quadrature rules for ``'u'`` are going to be
    % constructed with default values.
    %
    % :arg x: target geometry struct.
    % :arg y: source geometry struct.
    % :arg singularity: cell array of singularity types.
    % :type singularity: default ``{'reg'}``
    % :arg opts: options to construct quadrature rules for each type of
    %   singularity.
    %
    % :return: a struct with one field per sigularity type containing the
    %   quadrature rules in a format that is used by :func:`st_layer_evaluate`.

    persistent s_var_quadrature_rules;      %#ok
    if isempty(s_var_quadrature_rules)
        s_layer_quadrature_types = struct(...
            'p', 'reg', ...
            'u', 'log', ...
            'fn', 'reg', ...
            'ft', 'cpv', ...
            'dudn', 'log', ...
            'dudt', 'log', ...
            'grads', 'hfp');
    end

    if ~exist('singularity', 'var') || isempty(singularity)
        singularity = 'reg';
    end

    if ~exist('opts', 'var') || isempty(opts)
        opts = struct();
    end

    if ~iscell(singularity)
        singularity = {singularity};
    end

    % clean up cell array
    singularity = unique(singularity);
    singularity = st_cell_replace(singularity, 'exterior', {'log'});
    singularity = st_cell_replace(singularity, 'two_phase', {'reg'});
    singularity = st_cell_replace(singularity, 'eps', {'fn', 'p', 'u'});
    singularity = st_cell_replace(singularity, 'sigma', {'fn', 'p', 'u'});

    % construct options for each element of the cell array
    if st_isempty(opts) || any(isfield(opts, {'nnodes', 'npoly', 'method'}))
        quad = struct();
        for m = 1:numel(singularity)
            quad.(singularity{m}) = opts;
        end
        opts = quad;
    end

    quad = struct();
    for m = 1:numel(singularity)
        name = singularity{m};

        stype = st_struct_field(s_layer_quadrature_types, name, name);
        sopt = st_stype_options(x, y, opts, name, stype);

        switch stype
        case 'reg'
            r = st_layer_quadrature_regular(x, y, sopt);
        case 'log'
            r = st_layer_quadrature_log(x, y, sopt);
        case 'cpv'
            r = st_layer_quadrature_cpv(x, y, sopt);
        case 'hfp'
            r = st_layer_quadrature_hfp(x, y, sopt);
        otherwise
            error('unkown singularity type: %s', stype);
        end

        quad.(name) = r;
    end

    quad = st_struct_merge(y.quad, quad);
end

% {{{ helpers

function [c] = st_cell_replace(orig, value, new_values)
    c = orig;

    mask = contains(c, value);
    if any(mask)
        c = c(~mask);
        c = unique([c, new_values]);
    end
end

function [o] = st_stype_options(x, ~, opts, name, stype)
    if isfield(opts, name) && ~st_isempty(opts.(name))
        o = opts.(name);
        return;
    end

    if isfield(opts, stype) && ~st_isempty(opts.(stype))
        o = opts.(stype);
        return;
    end

    % NOTE: these defaults are taken from `test_layer_convergence`
    nbasis = length(x.nodes.basis);
    switch name
    case {'p', 'u'}
        o = struct('nnodes', 4 * nbasis, 'npoly', nbasis);
    case {'fn', 'ft'}
        o = struct('nnodes', 2 * nbasis, 'npoly', nbasis);
    case {'dudn', 'dudt'}
        o = struct('nnodes', 6 * nbasis, 'npoly', nbasis, 'method', 'carley');
    otherwise
        o = struct();
    end
end

% }}}

% {{{ quadrature rules

function [q] = st_layer_quadrature_regular(x, y, opts)
    method = lower(st_struct_field(opts, 'method', 'leggauss'));
    q = st_quadrature_build(x, y, opts, @st_quad);

    function [xi, w] = st_quad(~, s_nnodes, yi, a, b)
        switch method
        case 'leggauss'
            [xi, w] = st_quad_leggauss(s_nnodes);
        case 'fejer'
            [xi, w] = st_quad_fejer(s_nnodes);
        otherwise
            error('NotImplementedError: %s', method);
        end

        [xi, w] = st_quadrature_patch(xi, w, yi, a, b);
    end
end

function [q] = st_layer_quadrature_log(x, y, opts)
    method = lower(st_struct_field(opts, 'method', 'alpert'));

    switch method
    case 'alpert'
        q = st_layer_quadrature_alpert(x, y, opts);
    case 'kress'
        q = st_layer_quadrature_kress(x, y, opts);
    case 'carley'
        q = st_layer_quadrature_carley(x, y, 1, opts);
    otherwise
        error('NotImplementedError: %s', method);
    end
end

function [q] = st_layer_quadrature_cpv(x, y, opts)
    method = lower(st_struct_field(opts, 'method', 'carley'));

    switch method
    case 'carley'
        q = st_layer_quadrature_carley(x, y, 2, opts);
    otherwise
        error('NotImplementedError: %s', method);
    end
end

function [q] = st_layer_quadrature_hfp(x, y, opts)
    method = lower(st_struct_field(opts, 'method', 'carley'));
    opts.solver = st_struct_field(opts, 'solver', 'qr');

    switch method
    case 'carley'
        q = st_layer_quadrature_carley(x, y, 3, opts);
    otherwise
        error('NotImplementedError: %s', method);
    end
end

function [q] = st_layer_quadrature_carley(x, y, order, opts)
    % polynomial order
    opts.nodes = st_struct_field(opts, 'nnodes', length(y.quad.unit_xi));
    s_npoly = st_struct_field(opts, 'npoly', floor(opts.nodes / 2));
    % quadrature rule type
    s_sgn = st_struct_field(opts, 'sign', 1);
    order = s_sgn * order;
    % linear system solver
    solver = st_struct_field(opts, 'solver', 'qr');

    q = st_quadrature_build(x, y, opts, @st_quad);

    function [xi, w] = st_quad(s_ovsmp, s_nnodes, yi, a, b)
        [xi, w] = st_quad_carley(s_ovsmp * s_nnodes, yi, order, ...
                                  s_ovsmp * s_npoly, a, b, solver);
    end
end

function [q] = st_layer_quadrature_alpert(x, y, opts)
    % order
    s_order = st_struct_field(opts, 'npoly', 4);

    q = st_quadrature_build(x, y, opts, @st_quad);

    function [xi, w] = st_quad(s_ovsmp, s_nnodes, yi, a, b)
        [xi, w] = st_quad_log_alpert(s_ovsmp * s_nnodes, yi, s_order, a, b);
    end
end

function [q] = st_layer_quadrature_kress(x, y, opts)
    % weight-type
    s_weight = st_struct_field(opts, 'weight', 'sidi');
    % power
    s_power = st_struct_field(opts, 'npoly', 4);

    q = st_quadrature_build(x, y, opts, @st_quad);

    function [xi, w] = st_quad(~, s_nnodes, yi, a, b)
        [xi, w] = st_quad_log_kress(s_nnodes, s_weight, s_power);
        [xi, w] = st_quadrature_patch(xi, w, yi, a, b);
    end
end

% }}}

% {{{ build rules

function [q] = st_quadrature_build(x, y, opts, quad)
    % number of nodes
    s_nnodes = st_struct_field(opts, 'nnodes', length(y.quad.unit_xi));
    % panel endpoint oversampling
    s_ovsmp = st_struct_field(opts, 'ovsmp', 2);

    % number of collocation points
    nbasis = x.nbasis;
    % allocate
    q.unit_xi = cell(1, nbasis + 2);
    q.unit_w = cell(1, nbasis + 2);

    % domain endpoints
    a = x.panels(1); b = x.panels(2);
    [xi, w] = quad(1, s_nnodes, a, a, b);
    q.unit_xi{end - 1} = xi;
    q.unit_w{end - 1} = w;

    a = x.panels(end - 1); b = x.panels(end);
    [xi, w] = quad(1, s_nnodes, b, a, b);
    q.unit_xi{end} = xi;
    q.unit_w{end} = w;

    % panel endpoints
    a = -1.0; b = 1.0;
    [xi, w] = quad(s_ovsmp, s_nnodes, 0.0, a, b);

    q.unit_xi{1} = xi;      q.unit_w{1} = w;
    q.unit_xi{nbasis} = xi; q.unit_w{nbasis} = w;

    % panel interior points
    for n = 2:nbasis - 1
        [xi, w] = quad(1, s_nnodes, x.nodes.unit_xi(n), a, b);
        q.unit_xi{n} = xi;
        q.unit_w{n} = w;
    end

    q = st_quadrature_translate(x, q);
end

function [xi, w] = st_quadrature_patch(xi, w, yi, a, b)
    if abs(yi - a) < 1.0e-14 || abs(yi - b) < 1.0e-14
        xm = (b + a) / 2.0; dx = (b - a) / 2.0;
        xi = xm + dx * xi;
        w = dx * w;
    else
        xm = (yi + a) / 2.0; dx = (yi - a) / 2.0;
        xi_l = xm + dx * xi;
        w_l = dx * w;

        xm = (b + yi) / 2.0; dx = (b - yi) / 2.0;
        xi_r = xm + dx * xi;
        w_r = dx * w;

        xi = [xi_l, xi_r];
        w = [w_l, w_r];
    end
end

function [q] = st_quadrature_translate(x, q)
    % number of targets
    ntargets = length(x.xi);
    % number of panels
    npanels = length(x.panels) - 1;
    % number of basis functions per panel
    nbasis = length(x.nodes.basis);

    % map targets to panels
    panel_ids = st_mesh_target_associate(x);
    % panel_ids(2, 1) = 0; panel_ids(1, end) = npanels + 1;

    % construct interpolation points
    nsources = length(q.unit_xi{end - 1}) + ...
               length(q.unit_xi{end}) + ...
               length(q.unit_xi{1}) * (npanels - 1);
    for k = 2:nbasis - 1
        nsources = nsources + length(q.unit_xi{k}) * npanels;
    end
    xi = zeros(1, nsources);
    wi = zeros(1, nsources);

    % right domain endpoint
    js = 1;
    je = length(q.unit_xi{end - 1});
    xi(js:je) = q.unit_xi{end - 1};
    wi(js:je) = q.unit_w{end - 1};

    for i = 2:ntargets - 1
        % get surrounding panel ids;
        iprev = panel_ids(2, i);
        inext = panel_ids(1, i);
        % get target point index in panel
        k = mod(i - 1, nbasis - 1) + 1;
        % indices
        js = je + 1;
        je = je + length(q.unit_xi{k});

        a = x.panels(iprev); b = x.panels(inext + 1);
        xi(js:je) = 0.5 * (b + a) + 0.5 * (b - a) * q.unit_xi{k};
        wi(js:je) = 0.5 * (b - a) * q.unit_w{k};
    end

    % left domain endpoint
    js = je + 1;
    je = je + length(q.unit_xi{end});
    xi(js:je) = q.unit_xi{end};
    wi(js:je) = q.unit_w{end};

    q.xi = xi;
    q.w = wi;
end

% }}}

% vim:foldmethod=marker:
