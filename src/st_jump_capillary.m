% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [jump] = st_jump_capillary(varargin)
    % Defines a traction jump based on the Young-Laplace law with constant surface tension.
    %
    % The jump is given it its non-dimensional form by the Young-Laplace law
    %
    % .. math::
    %
    %     [\![\mathbf{n} \cdot \sigma]\!] = \frac{1}{\mathrm{Ca}} \kappa \mathbf{n}.
    %
    % :returns: a struct compatible with :func:`st_jump_options`.

    d.user.c_kappa_x = 1.0;
    d.user.c_kappa_p = 1.0;
    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    jump = st_jump_options(d);

    % set function handles
    jump.fn.evaluate = @jump_capillary_evaluate;
    jump.fn.gradient_x = @(j, t, x, y, s) st_jump_shape_gradient(j, t, x, y, s);
    jump.fn.gradient_ca = @jump_capillary_gradient_ca;

    jump.fn.operator_shape_x = @jump_capillary_operator_u;
    jump.fn.operator_x = @st_jump_shape_operator;
end

% {{{ gradient function handles

function [r] = jump_capillary_evaluate(jump, ~, x, ~, ~)
    kappa = st_jump_capillary_kappa(jump, x);
    r = 1.0 / jump.param.Ca * kappa .* x.n;
end

function [r] = jump_capillary_gradient_ca(jump, t, x, y, st)
    jmp = jump.fn.evaluate(jump, t, x, y, st);

    integrand = st_dot(st.adjoint.u, jmp);
    integrand = st_interpolate(x, integrand, y.xi);
    r = -1.0 / jump.param.Ca * st_ax_integral(y, integrand);
end

% }}}

% {{{ shape gradient operators

function [G] = jump_capillary_operator_u(jump, ~, x, y, ~)
    switch jump.optype
    case 'scalar'
        G = st_jump_capillary_operator_scalar(jump, x, y);
    case 'vector'
        G = st_jump_capillary_operator_vector(jump, x, y);
    end
end

function [G] = st_jump_capillary_operator_scalar(jump, x, y)
    cx = jump.user.c_kappa_x;
    cp = jump.user.c_kappa_p;
    kappa = st_jump_capillary_kappa(jump, x);

    Gx = build_op(kappa .* real(x.t), real(x.n));
    Gy = build_op(kappa .* imag(x.t), imag(x.n));
    G = [Gx, Gy];

    function [O] = build_op(a, b)
        Nstar = st_ops_adjoint_normal(x, y, a);
        [KXstar, KPstar] = st_ops_adjoint_curvature(x, y, b);
        O = 1.0 / jump.param.Ca * (Nstar + cx * KXstar + cp * KPstar);
    end
end

function [G] = st_jump_capillary_operator_vector(jump, x, y)
    cx = jump.user.c_kappa_x;
    cp = jump.user.c_kappa_p;
    kappa = st_jump_capillary_kappa(jump, x);

    N = st_ops_adjoint_vector_normal(x, y, kappa .* x.t);
    [KX, KP] = st_ops_adjoint_vector_curvature(x, y, x.n);

    G = 1.0 / jump.param.Ca * (N + cx * KX + cp * KP);
end

function [kappa] = st_jump_capillary_kappa(jump, x)
    kappa = jump.user.c_kappa_x * x.kappa_x ...
        + jump.user.c_kappa_p * x.kappa_p;
end

% }}}
