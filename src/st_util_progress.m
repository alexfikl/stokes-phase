% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [p] = st_util_progress(p, varargin)
    if ischar(p)
        varargin = [p, varargin];
        p = struct();
    end

    % get default options
    d.nitems = -1;
    d.action = 'init';
    d.strfmt = [];

    o = st_struct_update(d, st_struct_varargin(varargin{:}));
    if isinf(o.nitems) || isnan(o.nitems)
        o.nitems = -1;
    end

    switch o.action
    case 'init'
        p.current_item = 0;
        p.nitems = o.nitems;

        p.t_progress = -1;
        p.t_elapsed = 0.0;
        p.t_avg = 0.0;
        p.t_total = 0.0;
        p.strfmt = o.strfmt;

        if isempty(p.strfmt)
            p.strfmt = @progress_format;
        end
    case 'tic'
        p.t_progress = tic();
        p.current_item = p.current_item + 1;
    case 'toc'
        p.t_elapsed = toc(p.t_progress);

        m = p.current_item - 1;
        p.t_avg = (m * p.t_avg + p.t_elapsed) / (m + 1);
        p.t_total = p.t_total + p.t_elapsed;
    case 'reset'
        toc(p.t_progress);
        p.t_progress = -1;
    otherwise
        error('unrecognized action: %s', action);
    end
end

function [s] = progress_format(p)
    if p.nitems < 0
        t_final = p.t_total;
    else
        t_final = p.t_avg * (p.nitems - p.current_item + 1);
    end

    t_hours = floor(t_final / 3600);
    t_mins = floor((t_final - t_hours * 3600) / 60);
    t_secs = floor(t_final - t_hours * 3600 - t_mins * 60);

    if p.nitems < 0
        s = sprintf('curr %.2fs avg %.2fs elapsed %02dh%02dm%02ds', ...
            p.t_elapsed, p.t_avg, t_hours, t_mins, t_secs);
    else
        s = sprintf('curr %.2fs avg %.2fs estimated %02dh%02dm%02ds', ...
            p.t_elapsed, p.t_avg, t_hours, t_mins, t_secs);
    end
end
