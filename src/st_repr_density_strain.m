% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [eps_ext, eps_int, cache] = st_repr_density_strain(x, y, q, bc)
    % Evaluate the on-surface Stokes strain rate tensor.
    %
    % The strain rate tensor :math:`\varepsilon` is given by
    %
    % .. math::
    %
    %     \varepsilon_{ij} = \frac{1}{2} \left(
    %     \nabla \mathbf{u} + \nabla \mathbf{u}^T
    %     \right).
    %
    % See :func:`st_repr_density_stress` for an explanation of how the strain
    % can also be computed.
    %
    % Note that the strain tensor is in cylindrical coordinates and has an
    % additional component in the :math:`\phi` direction (see vector gradients)
    % that is not computed by this function.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density at target points.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :return: a tuple ``(eps_ext, eps_int)`` with the strain tensor components.

    % traction
    cache = struct();
    if ~isfield(bc.cache, 'fn_ext')
        [cache.fn_ext, cache.fn_int] = st_repr_density_traction(x, y, q, bc);
    else
        cache.fn_ext = bc.cache.fn_ext;
        cache.fn_int = bc.cache.fn_int;
    end

    % normal velocity gradient
    if ~isfield(bc.cache, 'dudn_ext')
        [cache.dudn_ext, cache.dudn_int] = st_repr_density_gradu(x, y, q, bc);
    else
        cache.dudn_ext = bc.cache.dudn_ext;
        cache.dudn_int = bc.cache.dudn_int;
    end

    % tangential velocity gradient
    if ~isfield(bc.cache, 'u')
        cache.u = st_repr_density_velocity(x, y, q, bc);
    else
        cache.u = bc.cache.u;
    end

    % components in the (t, n) basis
    eps_nn = 0.5 * st_dot(cache.dudn_ext + cache.dudn_int, x.n);
    eps_nt_ext = 0.5 * st_dot(cache.fn_ext, x.t);
    eps_nt_int = 0.5 * st_dot(cache.fn_int, x.t);
    eps_tt = st_dot(st_derivative(x, cache.u, x.xi, 'fft'), x.t);

    % solve
    strain = st_sym_symmetric_tensor_solve();
    eps_ext = strain(eps_nn, eps_nt_ext, eps_tt, real(x.n), imag(x.n));
    eps_int = strain(eps_nn, eps_nt_int, eps_tt, real(x.n), imag(x.n));

    if nargout == 1
        eps_ext = struct('eps_ext', eps_ext, ...
                         'eps_int', eps_int, ...
                         'cache', cache);
    end
end

% vim:foldmethod=marker:
