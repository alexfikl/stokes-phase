% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [op] = st_jump_shape_operator(jump, t, x, y, st)
    % Construct an operator corresponding to :func:`st_jump_shape_gradient`.
    %
    % :arg jump: jump information compatible with :func:`st_jump_options`.
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg st: forward and adjoint solutions to the Stokes problem.
    %
    % :returns: a dense matrix corresponding to the operator.

    if strcmp(jump.optype, 'vector') == 0
        error('only available for vector equation');
    end

    if abs(jump.param.lambda - 1.0) > 1.0e-14
        error('only `lambda = 1` supported at the moment.');
    end

    j = jump.fn.evaluate(jump, t, x, y, st);

    % weak operators
    W1 = jump.fn.operator_shape_x(jump, t, x, y, st);
    W2 = st_ops_vector_mass_product(x, y, x.n, x.kappa .* j);
    W3 = st_ops_vector_mass_product(x, y, x.n, j);
    % layer potentials
    K1 = st_layer_slp(x, y, [], 'density');
    K2 = st_layer_normal_gradu(x, y, []);
    % density operator
    Q = -1.0 / (4.0 * pi) / (1.0 + jump.param.lambda);
    Q = st_array_tensor_product(x.n, Q * x.n);

    % put it all together
    op = ((W1 + W2) * K1 - W3 * K2) * Q;
end

