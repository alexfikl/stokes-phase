% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cost] = st_cost_surface_area(varargin)
    % Defines a cost functional that maximizes the surface area.
    %
    % It is given by
    %
    % .. math::
    %
    %     \mathcal{J} = -\int_\Sigma \,\mathrm{d}S.
    %
    % :returns: a struct compatible with :func:`st_cost_options`.

    % {{{ defaults

    d.user.alpha = -1.0;
    d.param.tmax = 0.0;
    d.variables = {'two_phase'};

    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    % }}}

    cost = st_cost_options(d);
    if ~isfield(cost.param, 'gamma')
        cost.user.alpha = -1.0;
    end

    cost.fn.evaluate = @cost_surface_area_evaluate;
    cost.fn.gradient_x = @cost_surface_area_gradient_x;
    cost.fn.gradient_u = @cost_surface_area_gradient_zero;
    cost.fn.gradient_s = @cost_surface_area_gradient_zero;
    cost.fn.gradient_ca = @cost_surface_area_gradient_ca;

    cost.fn.operator_x = @cost_surface_area_operator_x;
end

% {{{ gradients

function [r] = cost_surface_area_evaluate(cost, t, ~, y, ~)
    r = 0.0;
    alpha = cost.user.alpha;

    if abs(t - cost.param.tmax) < 1.0e-14
        r = -st_ax_integral(y, 1.0);

        if alpha > 0
            gamma = st_interpolate(x, cost.param.gamma, y.xi);
            r = r + alpha / 2.0 * st_ax_norm(y, gamma, 2);
        end
    end
end

function [r] = cost_surface_area_gradient_x(cost, t, x, ~, ~)
    r = zeros(size(x.x));
    alpha = cost.user.alpha;

    if abs(t - cost.param.tmax) < 1.0e-14
        r = -x.kappa;

        if alpha > 0
            r = r + alpha / 2.0 * x.kappa .* cost.param.gamma.^2;
        end

        switch cost.optype
        case 'scalar'
            M = st_ops_mass(x, y);
            r = (M * r')';
        case 'vector'
            r = r .* x.n;
        end
    end
end

function [r] = cost_surface_area_gradient_zero(~, ~, ~, ~, ~)
    r = 0.0;
end

function [r] = cost_surface_area_gradient_ca(cost, t, ~, ~, ~)
    r = 0.0;
    alpha = cost.user.alpha;

    if abs(t - cost.param.tmax) < 1.0e-14 && alpha > 0
        r = alpha * cost.param.gamma;
    end
end

% }}}

% {{{ operators

function [op] = cost_surface_area_operator_x(cost, t, ~, ~, ~)
    if abs(t - cost.param.tmax) < 1.0e-14
        error('operator not available at tmax.');
    end

    op = 0.0;
end

% }}}
