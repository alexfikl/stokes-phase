% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function S = st_layer_normal_grads(x, y, q)
    % Evaluate the axisymmetric stress normal gradient layer potential.
    %
    % The layer potential is given by:
    %
    % .. math::
    %
    %     \mathcal{R}[\mathbf{q}](\mathbf{x}_0) =
    %         \int_\Sigma R_{ijkl}(\mathbf{x}, \mathbf{x}_0)
    %                     n_i(\mathbf{x}_0)
    %                     n_k(\mathbf{x}_0)
    %                     n_l(\mathbf{x}_0)
    %                     q_j(\mathbf{x}) \,\mathrm{d}\mathbf{x},
    %
    % where the axisymmetric kernel :math:`R_{ijkl}` is computed by
    % :func:`st_knl_ax_stress_gradient`. The general form of the kernel is given
    % by Equation 3.8 in [Gonzalez2009]_. It has a jump given by:
    %
    % .. math::
    %
    %     \mathcal{R}[\mathbf{q}](\mathbf{x}_0) =
    %         \pm 4 \pi \kappa_x \mathbf{q} \cdot \mathbf{n}
    %         \mp 4 \pi \mathbf{t} \frac{\partial \mathbf{q}}{\partial s}
    %         + \mathcal{R}[\mathbf{q}](\mathbf{x}_0).
    %
    % Using these equations, we can compute the normal stress gradient on
    % both sides of the interface.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: stress gradient normal layer potential.

    % REFERENCES:
    %   [1] O. Gonzalez, On Stable, Complete, and Singularity-Free Boundary
    %   Integral Formulations of Exterior Stokes Flow, SIAM Journal of
    %   Applied Mathematics, 2009.

    if isfield(y.quad, 'grads')
        knl.quad = y.quad.grads;
    else
        knl.quad = y.quad.hfp;
    end

    % preallocate
    knl.R_c = zeros(2, 1);

    if exist('q', 'var') && ~isempty(q)
        knl.S = zeros(1, length(x.x));
        knl.evaluate = @st_layer_normal_grads_kernel;

        knl = st_layer_evaluate(x, y, knl, q);
        S = knl.S;
    else
        S = zeros(length(x.x));
    end
end

function [knl] = st_layer_normal_grads_kernel(knl, target, source)
    % get elliptic integral evaluators
    ellint5 = @st_ellint_order5;
    ellint7 = @st_ellint_order7;
    if abs(imag(target.x(target.i))) < 1.0e-8
        ellint5 = @st_ellint_order5_axis;
        ellint7 = @st_ellint_order7_axis;
    end
    knl.kernel = @fn_kernel;

    % add up layer potential
    ntargets = length(target.x);
    if false && target.singular && target.i ~= 1 && target.i ~= ntargets
        % determine target panel
        i = target.i;
        nbasis = length(target.nodes.basis);

        p = floor((i - 1) / (nbasis - 1)) + 1;
        iprev = p - (mod(i - 1, nbasis - 1) == 0);
        inext = p + 1;

        target.a = target.panels(iprev);
        target.b = target.panels(inext);
        assert(target.a < source.xi(source.jstart));
        assert(source.xi(source.jend) < target.b);

        knl = st_layer_normal_grads_kernel_hfp(knl, target, source);
    else
        knl = st_layer_normal_grads_kernel_reg(knl, target, source);
    end

    function R = fn_kernel(R, x, x0, n0)
        [F, E] = st_ellipke(x, x0);
        [I50, I51, I52, I53] = ellint5(x, x0, F, E);
        [I70, I71, I72, I73, I74] = ellint7(x, x0, F, E);
        R = st_knl_ax_stress_gradient(R, x, x0, n0, ...
            I50, I51, I52, I53, I70, I71, I72, I73, I74);
    end
end

function [knl] = st_layer_normal_grads_kernel_reg(knl, target, source)
    % target index
    i = target.i;
    % source and target points
    y = source.x;
    x = target.x;
    nx = target.n;

    % shortcuts
    R = knl.R_c;
    K = knl.kernel;

    % integrate Stresslet normal gradient on interval
    S0 = 0.0;
    for n = source.jstart:source.jend
        % evaluate kernel
        R = K(R, y(n), x(i), nx(i));
        % add up
        S0 = S0 + (R(1) * real(source.q(n)) + R(2) * imag(source.q(n))) * ...
                    source.J(n) * source.w(n);
    end

    knl.S(i) = knl.S(i) + S0;
end

function [knl] = st_layer_normal_grads_kernel_hfp(knl, target, source)
    % target index
    i = target.i;
    % source and target points
    y = source.x;
    x = target.x;
    nx = target.n;

    % shortcuts
    R = knl.R_c;
    K = knl.kernel;

    % evaluate kernel at source points
    Rs = zeros(1, source.jend - source.jstart + 1);
    for n = source.jstart:source.jend
        % evaluate kernel
        R = K(R, y(n), x(i), nx(i));
        % evaluate numerator
        j = n - source.jstart + 1;
        dxi = source.xi(n) - target.xi(i);
        Rs(j) = (dxi^3 * R(1)) * real(source.q(n)) * source.J(n) + ...
                (dxi^3 * R(2)) * imag(source.q(n)) * source.J(n);
    end

    % interpolate to target point
    Rx = interp1(source.xi(source.jstart:source.jend), ...
                 Rs, target.xi(i), 'linear');

    % sum up strongly singular part
    S0 = 0.0;
    for n = source.jstart:source.jend
        j = n - source.jstart + 1;

        dxi = source.xi(n) - target.xi(i);
        S0 = S0 + (Rs(j) - Rx) / dxi^3 * source.w(n);
    end
    % add additional hypersingular part
    xi = target.xi(i);
    if abs(xi - target.b) < 1.0e-15
        % Rx = -Rx / (target.b - target.a);
        Rx = 0.5 * Rx / (target.b - target.a)^2;
    elseif abs(xi - target.a) < 1.0e-15
        % Rx = -Rx / (target.b - target.a);
        Rx = 0.5 * Rx / (target.b - target.a)^2;
    else
        % Rx = -Rx / (target.b - xi) - Rx / (xi - target.a);
        Rx = 0.5 * Rx * (1.0 / (target.b - xi)^2 - 1.0 / (xi - target.a)^2);
    end
    S0 = S0 + Rx;

    knl.S(i) = knl.S(i) + S0;
end

% vim:ts=4:sw=4:sts=4:
