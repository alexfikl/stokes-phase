% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [options] = st_unittest_example_runner(runner, varargin)
    % Run an example.
    %
    % This function is tailor-made for running our examples and probably
    % not much else. It's meant to give everyone a stable naming scheme
    % and avoid some repeated code.
    %
    % There are a few required fields that are used to name caches and things
    % like that. They are
    %
    %   * ``example_prefix``: used to name caches.
    %   * ``example_id``: used to name caches.
    %   * ``cache_id``: see :func:`st_cache_create`.
    %   * ``cache_datadir``: see :func:`st_cache_create`.
    %   * ``cache_prefix``: a prefix for the cache folder, which is set to
    %     ``{example_prefix}_{example_id} by default.
    %   * ``reload_options``: if true, attempt to reload options from a cache
    %     e.g. from a previous run. Note that this will overwrite any existing
    %     options.
    %
    % Besides the input key-value pairs, the struct passed to ``runner`` will
    % also have
    %
    %   * ``problem_init(options)``: a function handle that is compatible with
    %    :func:`st_unitexample_problem`.
    %   * ``cache``: cache information from :func:`st_cache_create`.
    %
    % :arg runner: a function handle ``runner(options)``, where
    %   `options` is a struct constructed from the remaining arguments by
    %   passing them to :func:`st_struct_varargin`.
    % :arg varargin: remaining parameters as a struct or key-value pairs.
    %
    % :returns: the `options` struct with updated fields.

    % {{{ handle options

    global verbosity;
    options = st_struct_varargin(varargin{:});

    options.reload_options = st_struct_field(options, 'reload_options', false);
    options.verbose = st_struct_field(options, 'verbose', ...
            ~isempty(verbosity) && verbosity);

    options.cache_id = st_struct_field(options, 'cache_id', []);
    options.cache_datadir = st_struct_field(options, 'cache_datadir', []);

    % }}}

    % {{{ cache setup

    if options.reload_options
        if isempty(options.cache_datadir) ...
                && (isempty(options.cache_id) || strcmp(options.cache_id, 'now') == 1)
            error('must provide a cache directory or id on reload');
        end
    end

    if isfield(options, 'cache_prefix')
        if isa(options.cache_prefix, 'function_handle')
            prefix = options.cache_prefix(options);
        else
            prefix = options.cache_prefix;
        end
    else
        prefix = sprintf('%s_%s', options.example_prefix, options.example_id);
    end

    cache = st_cache_create(options.cache_datadir, options.cache_id, prefix);
    options.cache_datadir = cache.datadir;
    options.cache_id = cache.cache_id;
    options.cache_prefix = prefix;

    % }}}

    % {{{ save/reload problem setup

    filename = cache.filename(cache, 'setup');
    options.problem_init = str2func(sprintf('example_%s_data', options.example_id));

    if options.reload_options
        if exist(filename, 'file') ~= 2
            error('cannot reload options. file does not exist: %s', filename);
        end

        problem = load(filename);
        if isfield(problem, 'resolutions')
            options.resolutions = problem.resolutions;
        end

        if isfield(problem, 'epsilons')
            options.epsilons = problem.epsilons;
        end

        if isempty(options.cache_datadir)
            if strcmp(problem.example_id, options.example_id) == 0
                error('different `example_id` in saved file: %s', problem.example_id);
            end

            if strcmp(problem.example_prefix, options.example_prefix) == 0
                error('different `example_prefix` in saved file: %s', problem.example_prefix);
            end
        end
    else
        problem = options.problem_init(options);

        if isfield(problem, 'extra_options')
            options = st_struct_merge(options, problem.extra_options, true);
        end

        problem.example_prefix = options.example_prefix;
        problem.example_id = options.example_id;

        if isfield(options, 'resolutions')
            problem.resolutions = options.resolutions;
        end

        if isfield(options, 'epsilons')
            problem.epsilons = options.epsilons;
        end

        save(filename, '-struct', 'problem');
    end

    % }}}

    % {{{ run passed in function

    if options.verbose
        fprintf('options\n');
        disp(options);
        if isfield(problem, 'bc')
            fprintf('parameters\n');
            disp(problem.bc.param);
        end
    end

    options = runner(problem, cache, options);
    if options.verbose
        fprintf('cached: %s\n', cache.datadir);
    end

    % }}}
end

