% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function S = st_layer_pressure(x, y, q)
    % Evaluate the axisymmetric pressure layer potential.
    %
    % The pressure layer potential given by:
    %
    % .. math::
    %
    %     \mathcal{P}[\mathbf{q}](\mathbf{x}_0) =
    %         \int_\Sigma p_j(\mathbf{x}, \mathbf{x}_0)
    %         q_j(\mathbf{x}) \,\mathrm{d}\mathbf{x},
    %
    % where the kernel :math:`p_j` is given by Equation 2.2.10 of
    % [Pozrikidis1992]_.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: pressure layer potential. If the density is given, then we
    %   evaluate the layer potential, otherwise, a matrix operator is returned.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.

    if isfield(y.quad, 'p')
        knl.quad = y.quad.p;
    elseif isfield(y.quad, 'log')
        knl.quad = y.quad.log;
    else
        knl.quad = y.quad.reg;
    end

    % knl.quad = st_struct_field(y.quad, 'pressure', y.quad.cpv);
    % allocate these here so we don't have do to it in the kernel
    knl.P_c = zeros(2, 1);
    knl.R_c = zeros(2, 1);
    knl.V_c = zeros(2, x.nbasis);
    knl.W_c = zeros(2, 1);

    if exist('q', 'var') && ~isempty(q)
        knl.S = 4.0 * pi * st_dot(q, x.n);
        knl.evaluate = @st_layer_pressure_apply_kernel;
        % knl.S = zeros(size(x.x));
        % knl.evaluate = @st_layer_pressure_apply_kernel_cpv;

        knl = st_layer_evaluate(x, y, knl, q);
        S = knl.S;
    else
        knl.Sx = 4.0 * pi * diag(real(x.n));
        knl.Sy = 4.0 * pi * diag(imag(x.n));
        knl.evaluate = @st_layer_pressure_matrix_kernel;

        knl = st_layer_evaluate(x, y, knl);
        S = [knl.Sx, knl.Sy];
    end
end

function [knl] = st_layer_pressure_apply_kernel(knl, target, source)
    % target index
    i = target.i;
    % source and target points
    y = source.x;
    x = target.x;

    P = knl.P_c;
    R = knl.R_c;
    ellint = @st_ellint_order3;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % integrate Greenlet over segment
    S0 = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint(y(n), x(i), F, E);
        % compute kernels
        P = st_knl_ax_pressure(P, y(n), x(i), I30, I31);
        R = st_knl_ax_pressure_ss(R, y(n), x(i), ...
            source.n(n), target.n(i), I30, I31);
        % [P(1), P(2), R(1), R(2)] = st_knl_ax_pressure_c(...
        %     real(y(n)), imag(y(n)), real(x(i)), imag(x(i)), ...
        %     real(source.n(n)), imag(source.n(n)), ...
        %     real(target.n(i)), imag(target.n(i)));

        S0 = S0 + (P(1) * real(source.q(n)) - R(1) * real(target.q(i))) * dS + ...
                  (P(2) * imag(source.q(n)) - R(2) * imag(target.q(i))) * dS;
    end

    knl.S(i) = knl.S(i) + S0;
end

function [knl] = st_layer_pressure_apply_kernel_cpv(knl, target, source) %#ok
    % target index
    i = target.i;
    % source and target points
    y = source.x;
    x = target.x;

    P = knl.P_c;
    ellint = @st_ellint_order3;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % integrate Greenlet over segment
    S0 = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint(y(n), x(i), F, E);
        % compute kernels
        P = st_knl_ax_pressure(P, y(n), x(i), I30, I31);

        S0 = S0 + (P(1) * real(source.q(n)) + ...
                   P(2) * imag(source.q(n))) * dS;
    end

    knl.S(i) = knl.S(i) + S0;
end


function [knl] = st_layer_pressure_matrix_kernel(knl, target, source)
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % source and target points
    y = source.x;
    x = target.x;

    P = knl.P_c;
    R = knl.R_c;
    ellint = @st_ellint_order3;
    if abs(imag(x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % compute layer potential
    P0 = knl.V_c;
    R0 = knl.W_c;

    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint(y(n), x(i), F, E);
        % compute kernels
        P1 = st_knl_ax_pressure(P, y(n), x(i), I30, I31);
        R1 = st_knl_ax_pressure_ss(R, y(n), x(i), ...
                               source.n(n), target.n(i), ...
                               I30, I31);

        % add source interactions
        for k = 1:nbasis
            bnk = basis(n, k) * dS;
            P0(1, k) = P0(1, k) + P1(1) * bnk;
            P0(2, k) = P0(2, k) + P1(2) * bnk;
        end

        % add target interactions
        R0(1) = R0(1) + R1(1) * dS;
        R0(2) = R0(2) + R1(2) * dS;
    end

    % add source interactions in panel
    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.Sx(i, j) = knl.Sx(i, j) + P0(1, k);
        knl.Sy(i, j) = knl.Sy(i, j) + P0(2, k);
    end

    % add target interactions in panel
    knl.Sx(i, i) = knl.Sx(i, i) - R0(1);
    knl.Sy(i, i) = knl.Sy(i, i) - R0(2);
end

% vim:foldmethod=marker:
