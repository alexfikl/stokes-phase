% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [fn] = st_sym_handle(f, x, force_simplify)
    % Wrapper around ``matlabFunction``.
    %
    % This function creates function handles of the form ``f(z)`` where ``z``
    % is either the source or target geometry information. This function
    % constructs two types of handles
    %
    % * if *x* contains two symbolic variables, we assume they are the
    %   :math:`(x, y)` coordinates of the geometry.
    % * if *x* is a single symbolic variable, we assume it refers to the
    %   parametrization :math:`\xi` of the curve.
    %
    % Other cases should be handled by directly calling ``matlabFunction`` as
    %
    % .. code:: matlab
    %
    %     f = simplify(f, 'Steps', force_simplify);
    %     fn = matlabFunction(f, 'Vars', x);
    %
    % :arg f: symbolic expression for the function.
    % :arg x: symbolic function variables.
    % :arg force_simplify: simplify the given expressions. This is meant to
    %   be an integer denoting the number of simplification steps
    %   (see ``sym/simplify``).
    % :type force_simplify: default ``0``
    %
    % :return: a function handle that takes the geometry as an argument.

    if ~exist('x', 'var') || isempty(x)
        x = [sym('x', 'real'), sym('y', 'real')];
    end

    if ~exist('force_simplify', 'var') || isempty(force_simplify)
        force_simplify = 0;
    end
    force_simplify = floor(force_simplify);

    if isstruct(f)
        fn = struct();
        for field = fieldnames(f)'
            g = f.(field{1});
            if isfloat(g)
                % nothing to do
            elseif all(isSymType(g, 'number'))
                g = double(g);
            elseif isSymType(g, 'expression')
                g = st_sym_handle_function(g, x, force_simplify);
            else
                error('unknown type for field `%s`', field{1});
            end

            fn.(field{1}) = g;
        end
    else
        fn = st_sym_handle_function(f, x, force_simplify);
    end
end

function [fn] = st_sym_handle_function(f, x, force_simplify)
    if force_simplify > 0
        f = simplify(f, 'Steps', force_simplify);
    end

    f = matlabFunction(f, 'Vars', x);
    if length(x) == 1
        fn = @(x) f(x.xi) .* ones(size(x.x));
    elseif length(x) == 2
        fn = @(x) f(real(x.x), imag(x.x)) .* ones(size(x.x));
    else
        error('unsupported number of arguments');
    end
end

% vim:foldmethod=marker:
