% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [state] = st_ode_unsteady_load(cache, x0, y0, subfolder, prefix)
    % Load the final ODE state from a given cache.
    %
    % :arg cache: existing cache (see :func:`st_cache_create`) or a cache directory.
    % :arg x0: initial target point information used to reconstruct the geometry.
    % :arg y0: initial source point informaiton.
    % :arg subfolder: if given, a new cache is created at this subfolder
    %   with the same prefix as *cache*.
    % :arg prefix: prefix for the cache created in *subfolder*, if given.
    %
    % :returns: same state as :func:`st_ode_unsteady`.

    if ~exist('prefix', 'var') || isempty(prefix)
        prefix = cache.prefix;
    end

    if isstruct(cache)
        if ~exist('subfolder', 'var') || isempty(subfolder)
            checkpoint = cache;
        else
            checkpoint = st_cache_create(...
                fullfile(cache.datadir, subfolder), [], prefix);
        end
    else
        if ~exist('subfolder', 'var') || isempty(subfolder)
            datadir = cache;
        else
            datadir = fullfile(cache, subfolder);
        end

        checkpoint = st_cache_create(datadir, [], prefix);
    end

    % NOTE: keep in sync with `st_ode_unsteady`
    filename = checkpoint.filename(checkpoint, 'state');
    state = load(filename);

    % reconstruct geometry
    filename = checkpoint.filename(checkpoint, 'setup');
    if exist(filename, 'file') == 2
        r = load(filename, 'geometry');
        x0 = r.geometry.x;
        y0 = r.geometry.y;
    end

    [state.x, state.y] = st_mesh_geometry_update(x0, y0, state.x);

    % check if dirnames match
    if ~isempty(state.checkpoints)
        dirname = fileparts(state.checkpoints{1});

        % if the directories don't match, just relocate all the files
        if strcmp(dirname, checkpoint.datadir) == 0
            warning('checkpoints pointing to %s', dirname);

            ncheckpoints = length(state.checkpoints);
            for n = 1:ncheckpoints
                [~, basename, ext] = fileparts(state.checkpoints{n});
                filename = sprintf('%s.%s', basename, ext);

                state.checkpoints{n} = fullfile(checkpoint.datadir, filename);
            end
        end
    end
end
