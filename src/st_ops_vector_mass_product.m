% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [M] = st_ops_vector_mass_product(x, y, a, b)
    % Construct a vector mass operator with off-diagonal blocks.
    %
    % This constructs a mass operator for the tensor product
    %
    %   .. math::
    %
    %       M = \int_\Sigma \mathbf{a} \otimes \mathbf{b} \,\mathrm{d}S.
    %
    % :arg x: target geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg y: source geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg a: coefficient.
    % :arg b: coefficient.

    if ~exist('a', 'var') || isempty(a)
        a = 1.0 + 1.0j;
    end

    if ~exist('b', 'var') || isempty(b)
        b = 1.0 + 1.0j;
    end

    Mxx = st_ops_mass(x, y, real(a) .* real(b));
    Mxy = st_ops_mass(x, y, real(a) .* imag(b));
    Myx = st_ops_mass(x, y, imag(a) .* real(b));
    Myy = st_ops_mass(x, y, imag(a) .* imag(b));
    M = [Mxx, Mxy; Myx, Myy];
end
