% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cost] = st_cost_normal_velocity(varargin)
    % Defined a cost functional matching the normal velocity.
    %
    % It is given by
    %
    % .. math::
    %
    %     \mathcal{J} = \frac{1}{2} \int_{\Sigma}
    %         |\mathbf{u} \cdot \mathbf{n} - u_d|^2 \,\mathrm{d}S.
    %
    % :returns: a struct compatible with :func:`st_cost_options`.

    cost = st_cost_options(varargin{:}, ...
        'variables', {'two_phase', 'u'}, ...
        'forward_variables', {'two_phase', 'u', 'dudn'});

    cost.fn.evaluate = @cost_velocity_evaluate;
    cost.fn.gradient_x = @cost_velocity_gradient_x;
    cost.fn.gradient_u = @cost_velocity_gradient_u;
    cost.fn.gradient_s = @cost_velocity_gradient_s;
end

function [r] = cost_velocity_evaluate(cost, ~, x, y, st)
    u = st_interpolate(x, st.u, y.xi);
    du = st_dot(u, y.n) - cost.user.udy;

    r = st_ax_integral(y, 0.5 * abs(du).^2);
end

function [r] = cost_velocity_gradient_x(cost, ~, x, y, st)
    M = st_ops_mass(x, y);
    N = st_ops_adjoint_normal(x, y);

    u = st.forward.u;
    du = st_dot(u, x.n) - cost.user.udx;
    % NOTE: averaging just to average error
    dudn = 0.5 * st_dot(st.forward.dudn_ext + st.forward.dudn_int, x.n);

    T0 = N * (du .* st_dot(u, x.t))';
    T1 = du .* dudn;
    T2 = 0.5 * x.kappa .* abs(du).^2;

    r = (T0 + M * (T1 + T2).').';
end

function [r] = cost_velocity_gradient_u(cost, ~, x, ~, st)
    r = (st_dot(st.forward.u, x.n) - cost.user.udx) .* x.n;
end

function [r] = cost_velocity_gradient_s(~, ~, ~, ~, ~)
    r = 0.0;
end
