% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function V = st_layer_tangent_gradu(x, y, q)
    % Evaluate the axisymmetric tangent velocity gradient potential.
    %
    % This function computes the tangent velocity gradient with the kernel
    % given by Equation 3.8 in [Gonzalez2009]_:
    %
    % .. math::
    %
    %     \mathcal{U}_t[\mathbf{q}](\mathbf{x}_0) =
    %     t_k(\mathbf{x}_0) \int_\Sigma U_{ijk}(\mathbf{x}, \mathbf{x}_0)
    %         q_j(\mathbf{x}) \,\mathrm{d}\mathbf{x}.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: if the density is given, then we evaluate the layer potential,
    %   otherwise, a matrix operator is returned.

    if isfield(y.quad, 'dudt')
        knl.quad = y.quad.dudt;
    else
        knl.quad = y.quad.cpv;
    end

    % allocate these here so we don't have do to it in the kernel
    knl.U_c = zeros(2, 2, 2);
    knl.V_c = zeros(4, x.nbasis);

    if exist('q', 'var') && ~isempty(q)
        knl.V = zeros(1, length(x.x), 'like', x.x);
        knl.evaluate = @st_layer_tangent_gradu_apply_kernel;

        knl = st_layer_evaluate(x, y, knl, q);
        V = knl.V;
    else
        knl.Vxx = zeros(length(x.x));
        knl.Vxy = zeros(length(x.x));
        knl.Vyx = zeros(length(x.x));
        knl.Vyy = zeros(length(x.x));
        knl.evaluate = @st_layer_tangent_gradu_matrix_kernel;

        knl = st_layer_evaluate(x, y, knl);
        V = [knl.Vxx, knl.Vxy; ...
             knl.Vyx, knl.Vyy];
    end
end

function [knl] = st_layer_tangent_gradu_apply_kernel(knl, target, source)
    % target index
    i = target.i;
    % target tangent
    tx_i = real(target.t(i)); ty_i = imag(target.t(i));
    % source and target points
    y = source.x;
    x = target.x;

    U = knl.U_c;
    ellint3 = @st_ellint_order3;
    ellint5 = @st_ellint_order5;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint3 = @st_ellint_order3_axis;
        ellint5 = @st_ellint_order5_axis;
    end

    Vx = 0.0; Vy = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);
        qx_k = real(source.q(n)); qy_k = imag(source.q(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31] = ellint3(y(n), x(i), F, E);
        [I50, I51, I52, I53] = ellint5(y(n), x(i), F, E);

        % compute kernels
        U = st_knl_ax_gradu(U, y(n), x(i), ...
            I30, I31, I50, I51, I52, I53);

        % add interactions
        Vx = Vx + ...
            (U(1, 1, 1) * qx_k * tx_i + ...
             U(1, 1, 2) * qx_k * ty_i + ...
             U(1, 2, 1) * qy_k * tx_i + ...
             U(1, 2, 2) * qy_k * ty_i) * dS;
        Vy = Vy + ...
            (U(2, 1, 1) * qx_k * tx_i + ...
             U(2, 1, 2) * qx_k * ty_i + ...
             U(2, 2, 1) * qy_k * tx_i + ...
             U(2, 2, 2) * qy_k * ty_i) * dS;
    end

    knl.V(i) = knl.V(i) + complex(Vx, Vy);
end

function [knl] = st_layer_tangent_gradu_matrix_kernel(knl, target, source)
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % target tangent
    tx_i = real(target.t(i)); ty_i = imag(target.t(i));
    % source and target points
    y = source.x;
    x = target.x;

    U = knl.U_c;
    ellint3 = @st_ellint_order3;
    ellint5 = @st_ellint_order5;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint3 = @st_ellint_order3_axis;
        ellint5 = @st_ellint_order5_axis;
    end

    % compute operators
    V0 = knl.V_c;
    V0(:, :) = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31] = ellint3(y(n), x(i), F, E);
        [I50, I51, I52, I53] = ellint5(y(n), x(i), F, E);
        % compute kernels
        U = st_knl_ax_gradu(U, y(n), x(i), ...
            I30, I31, I50, I51, I52, I53);

        % add source interactions
        for k = 1:nbasis
            bnk = dS * basis(n, k);

            V0(1, k) = V0(1, k) + ...
                (U(1, 1, 1) * tx_i + U(1, 1, 2) * ty_i) * bnk;
            V0(2, k) = V0(2, k) + ...
                (U(1, 2, 1) * tx_i + U(1, 2, 2) * ty_i) * bnk;
            V0(3, k) = V0(3, k) + ...
                (U(2, 1, 1) * tx_i + U(2, 1, 2) * ty_i) * bnk;
            V0(4, k) = V0(4, k) + ...
                (U(2, 2, 1) * tx_i + U(2, 2, 2) * ty_i) * bnk;
        end
    end

    % add source interactions in panel
    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.Vxx(i, j) = knl.Vxx(i, j) + V0(1, k);
        knl.Vxy(i, j) = knl.Vxy(i, j) + V0(2, k);
        knl.Vyx(i, j) = knl.Vyx(i, j) + V0(3, k);
        knl.Vyy(i, j) = knl.Vyy(i, j) + V0(4, k);
    end
end

% vim:foldmethod=marker:
