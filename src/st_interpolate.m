% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [g] = st_interpolate(x, f, xi, method)
    % Interpolate a function to a new set of points.
    %
    % There are two methods available for interpolation:
    %
    % * ``'fft'``, which uses FFTs.
    % * ``'lagrange'``, which uses the Lagrange polynoamial basis on `x`.
    %
    % :arg x: collocation point information.
    % :arg f: array to interpolate. We assume that the array is already
    %   given at the collocation points.
    % :arg xi: new set of points at which to evaluate `f`.
    % :arg method: method used to interpolate.
    % :type method: default ``'lagrange'``.
    %
    % :returns: `f` evaluated at the new set of points.

    if ~exist('method', 'var')
        method = 'lagrange';
    end
    method = lower(method);

    if isstruct(xi)
        xi = xi.xi;
    end

    if numel(f) == 1
        g = f * ones(size(xi));
        return;
    end

    switch method
    case 'fft'
        interp = @st_interpolate_fft;
    case 'lagrange'
        interp = @st_interp_evaluate;
    otherwise
        error('unknown interpolation method: %s', method);
    end

    if isstruct(f)
        g = struct();

        fields = fieldnames(f);
        for n = 1:length(fields)
            g.(fields{n}) = f.(fields{n});
            if length(g.(fields{n})) == length(x.x)
                g.(fields{n}) = interp(x, g.(fields{n}), xi);
            end
        end
    else
        g = interp(x, f, xi);
    end
end

function [g] = st_interpolate_fft(~, f, xi)
    % sizes
    n = length(f);
    m = 2 * (n - 1);
    % sampling frequencies
    zk = 1.0j * st_fftfreq(m);

    % fill in bottom half due to axysymmetry
    g = st_array_ax(f);
    % compute fft
    ghat = fft(g);
    % interpolate
    g = ghat * exp(-zk' * xi) / length(zk);
    % reshape
    if isreal(f)
        g = real(g);
    end
    g = reshape(g, size(xi));
end

% vim:foldmethod=marker:
