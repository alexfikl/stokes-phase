% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [alpha] = st_optim_geometry_alpha_max(x, d, beta)
    % Compute maximum CG step size for shape optimization.
    %
    % We have two heuristic requirements for the step size:
    %
    % * first, it should be sufficiently small so that the resulting
    %   shape deformation is small.
    % * second, due to axisymmetry we need to ensure that :math:`\rho > 0`.
    %   This imposes the condition that
    %
    % .. math::
    %
    %     \rho + \alpha d_\rho > 0
    %     \implies \alpha < \min -\rho_i / d_{\rho, i}.
    %
    % :arg x: interface coordinates.
    % :arg d: descent direction.
    % :arg beta: additional factor to scale alpha.
    % :type beta: default ``1.0``.
    %
    % :returns: a maximum step size.

    if ~exist('beta', 'var') || isempty(beta)
        beta = 1;
    end

    % compute itersection with y = 0 axis along d direction
    %   x   +   alpha * d_1 = something
    %   y   +   alpha * d_2 = 0
    alpha = -imag(x(2:end - 1)) ./ imag(d(2:end - 1));
    alpha = min(alpha(alpha > 0.0));

    % make sure the step isn't too large anyway
    % beta = min(1.0, 0.001 * max(1.0, 1.0 / norm(d, Inf)));
    alpha = beta * min(alpha, 1.0);

    if isempty(alpha)
        alpha = beta;
    end
end
