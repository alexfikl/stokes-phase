% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [KX, KP] = st_ops_adjoint_curvature(x, y, sigma)
    % Construct the adjoint operator of the linearized mean curvature.
    %
    % See :func:`st_ops_adjoint_normal` for some details on how the operators
    % were derived.
    %
    % :arg x: target geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg y: source geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg sigma: coefficients evaluated at the target points. As for
    %   :func:`st_ops_adjoint_normal`, this can be a struct containing
    %   the fields ``g_in``, ``dg_in``, ``g_out`` and ``dg_out``, all of which
    %   are optional and default to :math:`1`.
    %
    % :returns: the operators ``(KX, KP)``, which are the adjoint operators
    %   of the first and second principal curvatures.

    if ~exist('sigma', 'var')
        sigma = struct();
    end

    if ~isstruct(sigma)
        sigma = struct('g_in', sigma);
    end

    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);
    % number of basis functions
    nbasis = length(x.nodes.basis);

    % evaluate basis on each point of the reference element
    phi = zeros(nbasis, nnodes);
    dphi = zeros(nbasis, nnodes);
    for p = 1:nbasis
        phi(p, :) = double(x.nodes.basis{p}(y.quad.unit_xi));
        dphi(p, :) = double(x.nodes.dbasis{p}(y.quad.unit_xi));
    end
    % get coefficients at source points
    sigma = st_get_coefficients(x, y, sigma);

    KX = st_adjoint_first_curvature(x, y, sigma, phi, dphi);
    KP = st_adjoint_second_curvature(x, y, sigma, phi, dphi);
end

function [K] = st_adjoint_first_curvature(x, y, sigma, phi, dphi)
    % number of targets
    ntargets = length(x.x);
    % number of panels
    npanels = length(x.panels) - 1;
    % number of basis functions
    nbasis = length(x.nodes.basis);
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);

    % area element
    dS = y.w ./ y.J;
    % coefficients
    g1 = y.J .* sigma.g_in .* real(y.n);
    g2 = imag(y.x) .* sigma.dg_in;
    g3 = imag(y.x) .* sigma.g_in;
    g4 = imag(y.x) .* y.kappa_x.^2 .* y.J.^2 .* sigma.g_out .* sigma.g_in;

    % compute operator
    K = zeros(ntargets, ntargets);
    for n = 1:npanels
        % source (quadrature) points in the current panel
        k = ((n - 1) * nnodes + 1):(n * nnodes);

        % evaluate block
        for p = 1:nbasis
            i = (n - 1) * (nbasis - 1) + p;
            for q = 1:nbasis
                j = (n - 1) * (nbasis - 1) + q;

                dgphi = sigma.g_out(k) .* dphi(p, :) + ...
                        sigma.dg_out(k) .* phi(p, :);
                K(i, j) = K(i, j) + ...
                    sum(g1(k) .* dgphi .* phi(q, :) .* dS(k)) + ...
                    sum(g2(k) .* dgphi .* phi(q, :) .* dS(k)) + ...
                    sum(g3(k) .* dgphi .* dphi(q, :) .* dS(k)) - ...
                    sum(g4(k) .* phi(p, :) .* phi(q, :) .* dS(k));
            end
        end
    end

    K(1, 1) = K(1, 1) - sigma.g_out(1) * sigma.g_in(1);
    K(end, end) = K(end, end) - sigma.g_out(end) .* sigma.g_in(end);
end

function [K] = st_adjoint_second_curvature(x, y, sigma, phi, dphi)
    % number of targets
    ntargets = length(x.x);
    % number of panels
    npanels = length(x.panels) - 1;
    % number of basis functions
    nbasis = length(x.nodes.basis);
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);

    % compute area element
    dS = y.J .* y.w;
    % compute coefficients
    g1 = real(y.n) ./ y.J .* sigma.g_out .* sigma.g_in;
    g2 = real(y.n) ./ y.J .* sigma.dg_out .* sigma.g_in;
    g3 = imag(y.x) .* y.kappa_p.^2 .* sigma.g_out .* sigma.g_in;

    % compute operator
    K = zeros(ntargets, ntargets);

    for n = 1:npanels
        % source (quadrature) points in the current panel
        k = ((n - 1) * nnodes + 1):(n * nnodes);

        % evaluate block
        for p = 1:nbasis
            i = (n - 1) * (nbasis - 1) + p;
            for q = 1:nbasis
                j = (n - 1) * (nbasis - 1) + q;

                K(i, j) = K(i, j) - ...
                    sum(g1(k) .* dphi(p, :) .* phi(q, :) .* dS(k)) - ...
                    sum(g2(k) .*  phi(p, :) .* phi(q, :) .* dS(k)) - ...
                    sum(g3(k) .*  phi(p, :) .* phi(q, :) .* dS(k));
            end
        end
    end

    K(1, 1) = K(1, 1) + sigma.g_out(1) * sigma.g_in(1);
    K(end, end) = K(end, end) + sigma.g_out(end) .* sigma.g_in(end);
end

function [r] = st_get_coefficients(x, y, sigma)
    % get coefficients
    r.g_in = st_struct_field(sigma, 'g_in', @() ones(size(y.x)));
    r.g_out = st_struct_field(sigma, 'g_out', @() ones(size(y.x)));
    r.dg_in = st_struct_field(sigma, 'dg_in', @() dg('g_in'));
    r.dg_out = st_struct_field(sigma, 'dg_out', @() dg('g_out'));

    % interpolate as necessary
    for f = fieldnames(r)'
        g = r.(f{1});

        if isa(g, 'function_handle')
            g = g();
        end
        if length(g) ~= length(y.x)
            g = st_interpolate(x, g, y.xi);
        end

        r.(f{1}) = g;
    end

    r.dg_in = y.J .* r.dg_in;
    r.dg_out = y.J .* r.dg_out;

    function [d] = dg(name)
        if isfield(sigma, name)
            d = st_derivative(x, sigma.(name), y.xi);
        else
            d = zeros(size(y.x));
        end
    end
end

% vim:foldmethod=marker:
