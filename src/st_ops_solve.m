% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = st_ops_solve(x, A, b, boundary_type, bc, method)
    % Solve a linear system.
    %
    % :arg x: target point information, as obtained from
    %   :func:`st_point_collocation`.
    % :arg A: matrix operator.
    % :arg b: right-hand side vector.
    % :arg boundary_type: see :func:`st_ops_boundary`.
    % :arg bc: see :func:`st_ops_boundary`.

    if ~exist('boundary_type', 'var') || isempty(boundary_type)
        boundary_type = 'none';
    end
    boundary_type = lower(boundary_type);

    if ~exist('bc', 'var') || isempty(bc)
        bc = [0.0, 0.0];
    end

    if length(bc) == 1
        bc = [bc, bc];
    end

    if ~exist('methd', 'var')
        method = 'direct';
    end
    method = lower(method);

    % apply boundary conditions
    [A0, b0] = st_ops_boundary(x, A, b, boundary_type, bc);
    % A0 = A; b0 = b;

    % solve system and get solution
    switch method
    case 'direct'
        linopts.RECT = true;
        r = linsolve(A0, reshape(b0, length(b0), 1), linopts);
    case 'gmres'
        [r, ~] = gmres(A0, reshape(b0, length(b0), 1), ...
            floor(length(b) / 2), 1.0e-12);
    otherwise
        error('unknown solver method: %s', method);
    end
    r = r';
end

% vim:foldmethod=marker:
