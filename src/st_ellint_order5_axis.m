% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [I50, I51, I52, I53] = st_ellint_order5_axis(x, x0, ~, ~)
    % Compute elliptic integrals in the Stresslet for on-axis points.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg F: elliptic integral of the first kind, see :func:`st_ellipke`.
    % :arg E: elliptic integral of the second kind.
    %
    % :returns: elliptic integrals described in Section 2.4 of
    %   [Pozrikidis1992]_ used to compute the Stresslet. Namely, it computes
    %   :math:`I_{50}, I_{51}, I_{52}` and :math:`I_{53}`.

    R = 1.0 / abs(x - x0)^5;

    I50 = 2.0 * pi * R;
    I51 = 0.0;
    I52 = pi * R;
    I53 = 0.0;
end

% vim:foldmethod=marker:
