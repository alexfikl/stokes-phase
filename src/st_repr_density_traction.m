% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [f_ext, f_int] = st_repr_density_traction(x, y, q, bc, repr)
    % Compute the surface traction.
    %
    % The surface traction is computed using the double-layer potential from
    % :func:`st_layer_dlp`. When taking the limit to the boundary, the layer
    % potential has a jump with the limits given by Equations 5.3.6 and
    % 5.3.7 from [Pozrikidis1992]_.
    %
    % This function has two modes of operation, where it computes the normal
    % and tangential tractions, i.e.
    %
    % .. math::
    %
    %     \mathbf{f}_n \triangleq \mathbf{n} \cdot \sigma
    %     \quad \text{or} \quad
    %     \mathbf{f}_t \triangleq \mathbf{t} \cdot \sigma
    %
    % for the axisymmetric tangent in the :math:`x-y` plane.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    % :arg bc: see :func:`st_boundary_condition_options`.
    % :arg repr: compute ``'tangential'`` or ``'normal'`` traction.
    % :type repr: default ``'normal'``.
    %
    % :returns: a tuple ``(fn_ext, fn_int)`` with the exterior and interior
    %   traction distributions on the surface. Note that the traction is
    %   non-dimensional.
    %
    % :seealso: :func:`st_layer_dlp`, :func:`st_layer_tangent_stress`.

    if ~exist('repr', 'var')
        repr = 'normal';
    end
    repr = lower(repr);

    if ~any(contains({'normal', 'tangent'}, repr))
        error('unknown representation: %s', repr);
    end

    if ~isempty(q)
        switch repr
        case 'normal'
            finf = bc.fn.finf(x);
        case 'tangent'
            finf = bc.fn.tinf(x);
        end

        f_ext = finf;
        f_int = finf;
        if norm(q, inf) < 1.0e-10
            return;
        end

        % compute layer potential
        switch repr
        case 'normal'
            dlp = st_layer_dlp(x, y, q);

            f_ext = finf - 4.0 * pi * q - dlp;
            f_int = finf + 4.0 * pi * q - dlp;
        case 'tangent'
            dlp = st_layer_tangent_stress(x, y, q);

            q = st_dot(q, x.n) .* x.t + st_dot(q, x.t) .* x.n;
            f_ext = finf - 4.0 * pi * q - dlp;
            f_int = finf + 4.0 * pi * q - dlp;
        end
    else
        ntargets = length(q);

        switch repr
        case 'normal'
            D = st_layer_dlp(x, y);

            J = 4.0 * pi * eye(2 * ntargets);
            f_ext = -J - D;
            f_int = +J - D;
        case 'tangent'
            D = st_layer_tangent_stress(x, y);

            J = real(x.n) .* imag(x.t) + imag(x.n) .* real(x.t);
            J = [...
                8.0 * pi * diag(real(x.n) .* real(x.t)), ...
                4.0 * pi * diag(J); ...
                4.0 * pi * diag(J), ...
                8.0 * pi * diag(imag(x.n) .* imag(x.t))];

            f_ext = -J - D;
            f_int = +J - D;
        end
    end

    if nargout == 1
        switch repr
        case 'normal'
            f_ext = struct('fn_ext', f_ext, 'fn_int', f_int);
        case 'tangent'
            f_ext = struct('ft_ext', f_ext, 'ft_int', f_int);
        end
    end
end

% vim:foldmethod=marker:
