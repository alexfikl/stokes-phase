% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [p_ext, p_int] = st_repr_density_pressure(x, y, q, bc)
    % Compute the surface pressure.
    %
    % The surface pressure is computed using the pressure layer potential
    % from :func:`st_layer_pressure`. Using these results and Equation
    % 5.3.2 from [Pozrikidis1992]_, we compute the pressure on both sides
    % of the interface.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: a tuple ``(p_ext, p_int)`` with the exterior and interior
    %   pressure distributions on the surface. Note that the pressure is
    %   non-dimensional.

    if ~isempty(q)
        pinf = bc.fn.pinf(x);

        p_ext = pinf;
        p_int = pinf;
        if norm(q, Inf) < 1.0e-10
            return;
        end

        % compute layer potential
        plp = st_layer_pressure(x, y, q);
        % dot density with the normal
        qn = st_dot(q, x.n);

        p_ext = p_ext + 4.0 * pi * qn - plp;
        p_int = p_int - 4.0 * pi * qn - plp;
    else
        J = [...
            4.0 * pi * diag(real(x.n)), ...
            4.0 * pi * diag(imag(x.n))];
        P = st_layer_pressure(x, y);

        p_ext = +J - P;
        p_int = -J - P;
    end

    if nargout == 1
        p_ext = struct('p_ext', p_ext, ...
                       'p_int', p_int);
    end
end

% vim:foldmethod=marker:
