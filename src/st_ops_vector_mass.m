% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [M] = st_ops_vector_mass(x, y, sigma)
    % Construct a vector mass operator.
    %
    % :arg x: target geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg y: source geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg sigma: weight.
    %
    % :returns: the mass operator ``M``.

    if ~exist('sigma', 'var')
        sigma = 1.0 + 1.0j;
    end

    % construct blocks
    Mxx = st_ops_mass(x, y, real(sigma));
    Mxy = zeros(length(sigma));
    Myy = st_ops_mass(x, y, imag(sigma));

    % construct final operator
    M = [Mxx, Mxy; Mxy, Myy];
end

% vim:foldmethod=marker:
