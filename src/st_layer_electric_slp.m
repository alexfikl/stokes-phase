% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function S = st_layer_electric_slp(x, y, q)
    % Evaluate axisymmetric Laplace single-layer potential.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: single-layer potential.

    if isfield(y.quad, 'phi')
        knl.quad = y.quad.phi;
    else
        knl.quad = y.quad.log;
    end

    knl.S = zeros(1, length(x.x));
    knl.evaluate = @st_layer_electric_slp_kernel;

    knl = st_layer_evaluate(x, y, knl, q);
    S = knl.S;
end

function [knl] = st_layer_electric_slp_kernel(knl, target, source)
    % target index
    i = target.i;
    % sources and targets
    y = source.x;
    x = target.x;

    ellint = @st_ellint_order3;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % integrate kernel
    S = 0.0; G = 0.0;

    for n = source.jstart:source.jend
        [F, E] = st_ellipke(y(n), x(i));
        I10 = ellint(y(n), x(i), F, E);
        G = st_knl_ax_electric_slp(G, y(n), x(i), I10);

        S = S + G * real(source.q(n)) * source.J(n) * source.w(n);
    end

    knl.S(i) = knl.S(i) + S;
end
