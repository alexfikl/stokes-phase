% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [jump] = st_jump_gravity(varargin)
    % Defines a traction jump for when gravity is present.
    %
    % This is meant to be used in conjunction with a surface tension-based
    % jump, e.g. :func:`st_jump_capillary`, where we have that
    %
    % .. math::
    %
    %     [\![\mathbf{n} \cdot \sigma]\!] =
    %         x \mathbf{n} +
    %         \frac{1}{\mathrm{Bo}} \kappa \mathbf{n},
    %
    % where :math:`\mathrm{Bo}` is the Bond number. It is given by
    %
    % .. math::
    %
    %     \mathrm{Bo} = \frac{\Delta \rho g R^2}{\gamma},
    %
    % where :math:`\Delta \rho` is the difference in density between the
    % fluids and :math:`g` is the gravitational acceleration. This function
    % implements the gravity term in the expression above.
    %
    % :returns: a struct compatible with :func:`st_jump_options`.

    jump = st_jump_options(varargin{:});

    jump.fn.evaluate = @jump_gravity_evaluate;
    jump.fn.gradient_x = @st_jump_shape_gradient;
    jump.fn.gradient_ca = @jump_gravity_gradient_ca;

    jump.fn.operator_shape_x = @jump_gravity_operator_shape_x;
    jump.fn.operator_x = @st_jump_shape_operator;
end

% {{{ gradient function handles

function [r] = jump_gravity_evaluate(~, ~, x, ~, ~)
    r = real(x.x) .* x.n;
end

function [r] = jump_gravity_gradient_ca(~, ~, ~, ~, ~)
    r = 0.0;
end

% }}}

% {{{ shape gradient operators

function [op] = jump_gravity_operator_shape_x(jump, ~, x, y, ~)
    switch jump.optype
    case 'scalar'
        op = jump_gravity_operator_shape_scalar(jump, x, y);
    case 'vector'
        op = jump_gravity_operator_shape_vector(jump, x, y);
    end
end

function [op] = jump_gravity_operator_shape_scalar(~, x, y)
    Gx = build_op(real(x.x) .* real(x.t), real(x.n));
    Gy = build_op(real(x.x) .* imag(x.t), imag(x.n));
    op = [Gx, Gy];

    function [O] = build_op(a, b)
        N = st_ops_adjoint_normal(x, y, a);
        M = st_ops_mass(x, y, b);
        O = M + N;
    end
end

function [op] = jump_gravity_operator_shape_vector(~, x, y)
    N = st_ops_adjoint_vector_normal(x, y, real(x.x) .* x.t);
    M = st_ops_vector_mass_product(x, y, x.n, x.n);
    op = N + M;
end

% }}}
