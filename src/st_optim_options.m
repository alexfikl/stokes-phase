% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [o] = st_optim_options(varargin)
    % Default options for :func:`st_optim_cg`.
    %
    % Required fields:
    %
    % * ``x0``: initial guess.
    % * ``[f, df] = fn(x)``: function handle that evaluates at a given ``x``.
    %   If two output arguments are requested, the second one should be the
    %   gradient at ``x``, of the same size and shape as ``x``, i.e.
    %   :math:`f: \mathbb{R}^n \to \mathbb{R}`.
    %
    % Optional fields include
    %
    % * ``display``: display per-iteration information, e.g. function values.
    % * ``maxiter``: maximum number of iterations.
    % * ``restart_maxit``: maximum number of iterations before a restart.
    %   A restart resets the descent direction to the one given by the gradient.
    %   This can also happen automatically if the descent direction points
    %   in an opposite direction to the gradient, i.e. it is no longer a
    %   descent direction.
    % * ``ftol``: relative tolerance between consecutive values of function
    %   evaluations.
    % * ``xtol``: relative tolerance between consecutive values of the
    %   argument.
    % * ``gtol``: relative tolerance for the gradient.
    % * ``beta``: method used to compute the cg update parameter.
    % * ``hz_eta``: threshold in Hager-Zhang algorithm, see Equation 7.1
    %   in [Hager2006]_.
    % * ``hz_theta``: weight in Hager-Zhang algorithm, see Equation 7.1
    %   in [Hager2006]_.
    % * ``alpha_max``: maximum allowable step size. Can also
    %   be a user defined function handle that takes ``(x, d)`` as arguments.
    % * ``alpha_min``: minimum allowable step size, which can also be a
    %   user defined function handle.
    % * ``armijo_beta``: parameter in the Armijo condition.
    % * ``armijo_ratio``: decrease in alpha at each iteration.
    % * ``output(o)``: user-defined function handle. Takes a struct with the
    %   fields `x`, `f`, `d`, `iteration`.
    % * ``outputfreq``: frequency of output per number of iterations.
    % * ``project(x, d)``: user-defined function that can be used to
    %   project the result of an update to an allowable region.
    %
    % Additional parameters, that remain unchecked, can be passed in a
    % separate ``'user'`` field. The default values can be obtained by
    % calling this function without any arguments.
    %
    % The CG update methods that are implemented are (see [Hager2006]_):
    %
    % * ``'Steepest'``: :math:`\beta \equiv 0`.
    % * ``'HestenesStiefl'``.
    % * ``'FletcherReeves'``.
    % * ``'PolakRibiere'``.
    % * ``'HagerZhang'``: like ``'CGDescent'`` with :math:`\eta = \infty` and
    %   :math:`\theta = 2`.
    % * ``'CGDescent'``: uses the parameters ``hz_eta`` and ``hz_theta``. This is
    %   not well-named, since it is not the algorithm from [Hager2006]_.
    %
    % :return: a struct with default and updated options.

    % {{{ default options

    global verbosity;
    d.display = ~isempty(verbosity) && verbosity;

    % initial condition
    d.x0 = [];
    % function to optimize
    d.fn = [];

    % iterations
    d.maxiter = [];
    d.restart_maxit = Inf;

    % tolerances
    d.gtol = 1.0e-6;
    d.ftol = 1.0e-6;
    d.xtol = 1.0e-6;

    % step size update formula
    d.beta_update = 'cgdescent';
    d.hz_eta = 0.01;
    d.hz_theta = 0.5;

    % step size bounds
    d.alpha_max = [];
    d.alpha_min = 1.0e-8;
    d.alpha_maxit = Inf;
    % armijo rule options
    d.armijo_beta = 1.0e-3;
    d.armijo_ratio = 0.75;

    % output
    d.output = [];
    d.outputfreq = 1;

    % projection
    d.project = @(x, d) x;

    % user defined parameters
    d.user = struct();

    % }}}

    % {{{ customize

    o = st_struct_update(d, st_struct_varargin(varargin{:}));
    o.beta_update = lower(o.beta_update);

    % turn into an integer
    if islogical(o.display)
        if o.display
            o.display = Inf;
        else
            o.display = 0;
        end
    end

    if isempty(o.maxiter)
        if ~isempty(o.x0)
            o.maxiter = 200 * length(o.x0);
        else
            o.maxiter = Inf;
        end
    end

    % }}}
end

% vim:foldmethod=marker:
