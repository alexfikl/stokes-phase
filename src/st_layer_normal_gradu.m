% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function V = st_layer_normal_gradu(x, y, q)
    % Evaluate the axisymmetric normal velocity gradient potential.
    %
    % This function computes the normal velocity gradient with the kernel
    % given by Equation 3.8 in [Gonzalez2009]_:
    %
    % .. math::
    %
    %     \mathcal{U}_n[\mathbf{q}](\mathbf{x}_0) =
    %     n_k(\mathbf{x}_0) \int_\Sigma U_{ijk}(\mathbf{x}, \mathbf{x}_0)
    %         q_j(\mathbf{x}) \,\mathrm{d}\mathbf{x}.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: if the density is given, then we evaluate the layer potential,
    %   otherwise, a matrix operator is returned.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.

    if isfield(y.quad, 'dudn')
        knl.quad = y.quad.dudn;
    elseif isfield(y.quad, 'log')
        knl.quad = y.quad.log;
    else
        knl.quad = y.quad.reg;
    end

    % allocate these here so we don't have do to it in the kernel
    knl.P_c = zeros(1, 2);
    knl.S_c = zeros(1, 2);
    knl.Q_c = zeros(2, 2, 2);
    knl.R_c = zeros(1, 2);
    knl.V_c = zeros(4, x.nbasis);
    knl.W_c = zeros(2, 2);

    % evaluate
    if exist('q', 'var') && ~isempty(q)
        knl.V = -4.0 * pi * st_dot(q, x.t) .* x.t;
        knl.evaluate = @st_layer_normal_gradu_apply_kernel;

        knl = st_layer_evaluate(x, y, knl, q);
        V = knl.V;
    else
        knl.Vxx = -4.0 * pi * diag(real(x.t) .* real(x.t));
        knl.Vxy = -4.0 * pi * diag(imag(x.t) .* real(x.t));
        knl.Vyx = -4.0 * pi * diag(real(x.t) .* imag(x.t));
        knl.Vyy = -4.0 * pi * diag(imag(x.t) .* imag(x.t));

        knl.evaluate = @st_layer_normal_gradu_matrix_kernel;

        knl = st_layer_evaluate(x, y, knl);
        V = [knl.Vxx, knl.Vxy; ...
             knl.Vyx, knl.Vyy];
    end
end

function [knl] = st_layer_normal_gradu_apply_kernel(knl, target, source)
    % target index
    i = target.i;
    % sources and targets
    x = target.x;
    y = source.x;

    P = knl.P_c;
    S = knl.S_c;
    Q = knl.Q_c;
    R = knl.R_c;
    ellint3 = @st_ellint_order3;
    ellint5 = @st_ellint_order5;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint3 = @st_ellint_order3_axis;
        ellint5 = @st_ellint_order5_axis;
    end

    % retrieve target variables
    nx_i = real(target.n(i)); ny_i = imag(target.n(i));
    tx_i = real(target.t(i)); ty_i = imag(target.t(i));
    qx_i = real(target.q(i)); qy_i = imag(target.q(i));

    % integrate kernel over segment
    P0 = 0.0; P1 = 0.0;
    S0 = 0.0; S1 = 0.0;

    for n = source.jstart:source.jend
        dS = 0.5 * source.J(n) * source.w(n);
        nx_k = real(source.n(n)); ny_k = imag(source.n(n));
        qx_k = real(source.q(n)); qy_k = imag(source.q(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint3(y(n), x(i), F, E);
        [I50, I51, I52, I53] = ellint5(y(n), x(i), F, E);
        % compute kernels
        P = st_knl_ax_pressure(P, y(n), x(i), I30, I31);
        S = st_knl_ax_pressure_t(S, y(n), x(i), I30, I31);
        Q = st_knl_ax_stress_target(Q, y(n), x(i), I50, I51, I52, I53);
        R = st_knl_ax_pressure_ss(R, y(n), x(i), ...
            source.n(n), target.n(i), I30, I31);

        % add pressure interactions
        PP = (P(1) * qx_k - R(1) * qx_i) + (P(2) * qy_k - R(2) * qy_i);
        SS = (S(1) * qx_k + R(1) * qy_i) - (S(2) * qy_k + R(2) * qx_i);

        P0 = P0 + (PP * nx_i + SS * tx_i) * dS;
        P1 = P1 + (PP * ny_i + SS * ty_i) * dS;

        % add stress interactions
        % NOTE: directly copied from st_layer_dlp::st_layer_dlp_density_apply
        S0 = S0 + ...
            Q(1, 1, 1) * (qx_k * nx_i - qx_i * nx_k) * dS + ...
            Q(1, 1, 2) * (qx_k * ny_i - qy_i * nx_k) * dS + ...
            Q(1, 2, 1) * (qy_k * nx_i - qx_i * ny_k) * dS + ...
            Q(1, 2, 2) * (qy_k * ny_i - qy_i * ny_k) * dS;
        S1 = S1 + ...
            Q(2, 1, 1) * (qx_k * nx_i - qx_i * nx_k) * dS + ...
            Q(2, 1, 2) * (qx_k * ny_i - qy_i * nx_k) * dS + ...
            Q(2, 2, 1) * (qy_k * nx_i - qx_i * ny_k) * dS + ...
            Q(2, 2, 2) * (qy_k * ny_i - qy_i * ny_k) * dS;
    end

    knl.V(i) = knl.V(i) + complex(P0 + S0, P1 + S1);
end

function [knl] = st_layer_normal_gradu_matrix_kernel(knl, target, source)
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % target variables
    nx_i = real(target.n(i)); ny_i = imag(target.n(i));
    tx_i = real(target.t(i)); ty_i = imag(target.t(i));
    % sources and targets
    x = target.x;
    y = source.x;

    P = knl.P_c;
    S = knl.S_c;
    Q = knl.Q_c;
    R = knl.R_c;
    ellint3 = @st_ellint_order3;
    ellint5 = @st_ellint_order5;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint3 = @st_ellint_order3_axis;
        ellint5 = @st_ellint_order5_axis;
    end

    % compute operators
    V0 = knl.V_c;
    V1 = knl.W_c;

    for n = source.jstart:source.jend
        dS = 0.5 * source.J(n) * source.w(n);
        nx_k = real(source.n(n)); ny_k = imag(source.n(n));

        % compute elliptic integrals
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint3(y(n), x(i), F, E);
        [I50, I51, I52, I53] = ellint5(y(n), x(i), F, E);

        % compute kernels
        P = st_knl_ax_pressure(P, y(n), x(i), I30, I31);
        S = st_knl_ax_pressure_t(S, y(n), x(i), I30, I31);
        Q = st_knl_ax_stress_target(Q, y(n), x(i), I50, I51, I52, I53);
        R = st_knl_ax_pressure_ss(R, y(n), x(i), ...
                              source.n(n), target.n(i), I30, I31);

        % add source interactions
        for k = 1:nbasis
            bnk = dS * basis(n, k);

            V0(1, k) = V0(1, k) + ...
                ((P(1) * nx_i + S(1) * tx_i) + ...
                 (Q(1, 1, 1) * nx_i + Q(1, 1, 2) * ny_i)) * bnk;
            V0(2, k) = V0(2, k) + ...
                ((P(2) * nx_i - S(2) * tx_i) + ...
                 (Q(1, 2, 1) * nx_i + Q(1, 2, 2) * ny_i)) * bnk;
            V0(3, k) = V0(3, k) + ...
                ((P(1) * ny_i + S(1) * ty_i) + ...
                 (Q(2, 1, 1) * nx_i + Q(2, 1, 2) * ny_i)) * bnk;
            V0(4, k) = V0(4, k) + ...
                ((P(2) * ny_i - S(2) * ty_i) + ...
                 (Q(2, 2, 1) * nx_i + Q(2, 2, 2) * ny_i)) * bnk;
        end

        % add target interactions
        V1(1, 1) = V1(1, 1) - ...
            ((R(1) * nx_i + R(2) * tx_i) + ...
             (Q(1, 1, 1) * nx_k + Q(1, 2, 1) * ny_k)) * dS;
        V1(1, 2) = V1(1, 2) - ...
            ((R(2) * nx_i - R(1) * tx_i) + ...
             (Q(1, 1, 2) * nx_k + Q(1, 2, 2) * ny_k)) * dS;
        V1(2, 1) = V1(2, 1) - ...
            ((R(1) * ny_i + R(2) * ty_i) + ...
             (Q(2, 1, 1) * nx_k + Q(2, 2, 1) * ny_k)) * dS;
        V1(2, 2) = V1(2, 2) - ...
            ((R(2) * ny_i - R(1) * ty_i) + ...
             (Q(2, 1, 2) * nx_k + Q(2, 2, 2) * ny_k)) * dS;
    end

    % add source interactions in panel
    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.Vxx(i, j) = knl.Vxx(i, j) + V0(1, k);
        knl.Vxy(i, j) = knl.Vxy(i, j) + V0(2, k);
        knl.Vyx(i, j) = knl.Vyx(i, j) + V0(3, k);
        knl.Vyy(i, j) = knl.Vyy(i, j) + V0(4, k);
    end

    % add target interactions in panel
    knl.Vxx(i, i) = knl.Vxx(i, i) + V1(1, 1);
    knl.Vxy(i, i) = knl.Vxy(i, i) + V1(1, 2);
    knl.Vyx(i, i) = knl.Vyx(i, i) + V1(2, 1);
    knl.Vyy(i, i) = knl.Vyy(i, i) + V1(2, 2);
end

function [P] = st_knl_ax_pressure_t(P, x, x0, I30, I31)
    X  = real(x);  Y  = imag(x);
    X0 = real(x0); Y0 = imag(x0);

    % build Green's function
    P(1) = 2.0 * Y * (Y * I31 - Y0 * I30);
    P(2) = 2.0 * Y * (X - X0) * I31;
end

% vim:foldmethod=marker:
