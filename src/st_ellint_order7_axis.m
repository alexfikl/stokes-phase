% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [I70, I71, I72, I73, I74] = st_ellint_order7_axis(x, x0, ~, ~)
    % Compute elliptic integrals for the Stresslet derivative for on-axis points.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg F: elliptic integral of the first kind, see :func:`st_ellipke`.
    % :arg E: elliptic integral of the second kind.
    %
    % :returns: elliptic integrals described in Section 2.4 of
    %   [Pozrikidis1992]_ used to compute the Stresslet derivative.
    %   Namely, it computes :math:`I_{70}, I_{71}, I_{72}, I_{73}` and
    %   :math:`I_{54}`.

    R = 1.0 / abs(x - x0)^7;

    I70 = 2.0 * pi * R;
    I71 = 0.0;
    I72 = pi * R;
    I73 = 0.0;
    I74 = 0.75 * pi * R;
end
