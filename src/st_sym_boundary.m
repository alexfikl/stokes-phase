% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [st] = st_sym_boundary(name, varargin)
    % Boundary conditions for Stokes flow.
    %
    % The returned struct will have function handles ``f(x)``, where ``x``
    % is the full collocation point information, as obtained from
    % :func:`st_mesh_target_geometry`. The following boundary conditions are
    % provided
    %
    % * ``uinf``: freestream velocity field.
    % * ``finf`` and ``tinf``: freestream normal and tangential traction.
    % * ``pinf``: freestream pressure.
    % * ``dudninf`` and ``dudtinf``: freestream normal and tangential velocity
    %   gradient.
    % * ``sinf``: freestream stress.
    % * ``usurf``: surface velocity field, for single-phase solutions.
    % * ``jump``: interfacial jump, for two-phase solutions. This should
    %   generally be replaced by structs as :func:`st_jump_options`.
    %
    % Known boundary conditions are
    %
    % * `HadamardRybczynski`: full solution, see :func:`st_sym_solutions`.
    % * `SolidSphere`: full solution, see :func:`st_sym_solutions`.
    % * `SolidQuadratic`: full solution, see :func:`st_sym_solutions`.
    % * `ExtensionalFlow`: freestream conditions only.
    % * `FourRoller`: freestream conditions only.
    % * `UniformFlow`: freestream conditions only.
    %
    % If the boundary conditions have an associated full solution (usually
    % on a sphere), then an additiona field ``sym`` is provided with the
    % solution obtained from :func:`st_sym_solutions`. Function handles for
    % each analytic solution are also given.
    %
    % :arg name: name of the solution.
    % :arg varargin: additional parameters for the boundary conditions.
    %
    % :returns: a struct with the above described fields.

    param.uinf = 1.0;
    param.pinf = 0.0;

    param = st_struct_merge(param, st_struct_varargin(varargin{:}), true);

    % get solution
    name = lower(name);
    switch name
    case {'uniformflow', 'extensionalflow', 'fourroller'}
        param.alpha = st_struct_field(param, 'alpha', 1.0);
        st = st_sym_boundary_farfield(name, param, struct());
    case {'hadamardrybczynski', 'solidsphere', 'solidquadratic', 'electric'}
        st.sym = st_sym_solutions(name, param);
        param = st_struct_merge(param, st.sym.param, true);

        st = st_struct_merge(st, st_sym_boundary_farfield(name, param, st.sym));
        st = st_struct_merge(st, st_sym_boundary_analytic(st));
    otherwise
        error('unknown boundary conditions: `%s`', name);
    end
end

% {{{ boundary conditions

function [st] = st_sym_boundary_farfield(name, opts, sym)
    switch name
    % {{{ two phase solutions

    case 'extensionalflow'
        st = st_sym_boundary_linear(opts.pinf, opts.alpha, -opts.alpha / 2.0);
        st.jump = @(x) x.kappa / opts.Ca .* x.n;
    case 'fourroller'
        st = st_sym_boundary_linear(opts.pinf, opts.alpha, -opts.alpha);
        st.jump = @(x) x.kappa / opts.Ca .* x.n;
    case 'uniformflow'
        st = st_sym_boundary_constant(opts.uinf, opts.pinf);
        st.jump = @(x) x.kappa / opts.Ca .* x.n;
    case 'hadamardrybczynski'
        st = st_sym_boundary_constant(opts.uinf, opts.pinf);

        st.jump = st_sym_boundary_analytical_jump(sym, opts.lambda, opts.Ca);
    case 'electric'
        st = st_sym_boundary_constant(opts.uinf, opts.pinf, opts.einf);
        if isfield(opts, 'Ca')
            st.jump = st_sym_boundary_analytical_jump(sym, opts.lambda, opts.Ca);
        end

    % }}}

    % {{{ single phase solutions

    case 'solidsphere'
        st = st_sym_boundary_constant(opts.uinf, opts.pinf);

        % u_theta = U sin(theta) => u = U sin(theta) t
        st.usurf = @(x) opts.alpha * sin(2.0 * pi * x.xi) .* x.t;
    case 'solidquadratic'
        st.uinf = @(x) imag(x.x).^2;
        st.pinf = @(x) (opts.pinf + 4.0 * real(x.x));
        st.sinf = @(x) [...
            -opts.pinf - 4.0 * real(x.x); ...
            2.0 * imag(x.x); ...
            -opts.pinf - 4.0 * real(x.x)];
        st.finf = @(x) st_dot(-(opts.pinf + 4.0 * real(x.x)) + 2.0j * imag(x.x), x.n) + ...
                1.0j * st_dot(2.0 * imag(x.x) - 1.0j * (opts.pinf + 4.0 * real(x.x)), x.n);
        st.tinf = @(x) st_dot(-(opts.pinf + 4.0 * real(x.x)) + 2.0j * imag(x.x), x.t) + ...
                1.0j * st_dot(2.0 * imag(x.x) - 1.0j * (opts.pinf + 4.0 * real(x.x)), x.t);
        st.dudninf = @(x) 2.0 * imag(x.x) .* imag(x.n);
        st.dudtinf = @(x) 2.0 * imag(x.x) .* imag(x.t);
        st.dsinf = @(x) -8.0 .* real(x.n).^3;

        % static no-slip boundary conditions
        st.usurf = @(x) zeros(size(x.x), 'like', x.x);

    % }}}
    end

    st.param = opts;
end

% {{{ Stokes boundary conditions

function [st] = st_sym_boundary_constant(uinf, pinf, einf)
    st.uinf = @(x) uinf * ones(1, length(x.x));
    st.finf = @(x) -pinf * x.n;
    st.tinf = @(x) -pinf * x.t;
    st.pinf = @(x) pinf * ones(1, length(x.x));
    st.sinf = @(x) repmat([-pinf, 0, -pinf]', 1, length(x.x));
    st.dudninf = @(x) zeros(1, length(x.x));
    st.dudtinf = @(x) zeros(1, length(x.x));
    st.dsinf = @(x) zeros(1, length(x.x));

    if exist('einf', 'var')
        st = st_struct_merge(st, st_sym_boundary_electric(einf));
    end
end

function [st] = st_sym_boundary_linear(pinf, alpha, beta)
    st.uinf = @(x) alpha * real(x.x) + 1j * beta * imag(x.x);
    st.pinf = @(x) pinf * ones(1, length(x.x));
    st.finf = @(x) -pinf * x.n + ...
        2 * (alpha * real(x.n) + 1j * beta * imag(x.n));
    st.tinf = @(x) -opts.pinf * x.t + ...
        2 * (alpha * real(x.t) + 1j * beta * imag(x.t));
    st.sinf = @(x) repmat(...
        [-pinf + 2 * alpha, 0, -pinf + 2 * beta]', ...
        1, length(x.x));
    st.dudninf = @(x) alpha * real(x.n) + 1j * beta * imag(x.n);
    st.dudtinf = @(x) alpha * real(x.t) + 1j * beta * imag(x.t);
end

% }}}

% {{{ analytical solutions

function [jump] = st_sym_boundary_analytical_jump(st, lambda, Ca)
    s_ext = st.sigma_ext * st.n';
    s_int = st.sigma_int * st.n';

    jump_sym = dot(s_ext - lambda * s_int, st.t);
    jump_sym = simplify(subs(jump_sym, st.x, st.xs));
    if ~isequaln(jump_sym, sym(0))
        warning('jump term has a tangential term component that is ignored');
    end

    jump_sym = simplify(dot(s_ext - lambda * s_int, st.n));
    jump_sym = simplify(subs(jump_sym, st.x, st.xs));
    jump_stress = matlabFunction(jump_sym, 'Vars', st.theta);
    jump = @(x) (jump_stress(angle(x.x)) + 1.0 / Ca * x.kappa) .* x.n;
end

function [st] = st_sym_boundary_analytic(st)
    s = st.sym;

    % velocity
    st.u_ext = st_sym_handle(s.u_ext(1) + 1.0j * s.u_ext(2), s.x);
    st.u_int = st_sym_handle(s.u_int(1) + 1.0j * s.u_int(2), s.x);

    % velocity gradient: normal and tangential
    gradu = s.gradu_ext * s.n';
    st.dudn_ext = st_sym_handle(gradu(1) + 1.0j * gradu(2), s.x);
    gradu = s.gradu_int * s.n';
    st.dudn_int = st_sym_handle(gradu(1) + 1.0j * gradu(2), s.x);

    gradu = s.gradu_ext * s.t';
    st.dudt_ext = st_sym_handle(gradu(1) + 1.0j * gradu(2), s.x);
    gradu = s.gradu_int * s.t';
    st.dudt_int = st_sym_handle(gradu(1) + 1.0j * gradu(2), s.x);

    % pressure
    st.p_ext = st_sym_handle(s.p_ext, s.x);
    st.p_int = st_sym_handle(s.p_int, s.x);

    % traction
    fn_ext = s.sigma_ext * s.n';
    fn_int = s.sigma_int * s.n';
    st.fn_ext = st_sym_handle(fn_ext(1) + 1.0j * fn_ext(2), s.x);
    st.fn_int = st_sym_handle(fn_int(1) + 1.0j * fn_int(2), s.x);

    ft_ext = s.sigma_ext * s.t';
    ft_int = s.sigma_int * s.t';
    st.ft_ext = st_sym_handle(ft_ext(1) + 1.0j * ft_ext(2), s.x);
    st.ft_int = st_sym_handle(ft_int(1) + 1.0j * ft_int(2), s.x);

    % stress
    st.sigma_ext = st_sym_tensor(s.sigma_ext, s.x);
    st.sigma_int = st_sym_tensor(s.sigma_int, s.x);

    % strain
    st.eps_ext = st_sym_tensor(s.tau_ext, s.x);
    st.eps_int = st_sym_tensor(s.tau_int, s.x);

    % stress gradient
    st.grads_ext = st_sym_handle(s.grads_ext, s.x);
    st.grads_int = st_sym_handle(s.grads_int, s.x);

    if isfield(s, 'e_ext')
        st = st_struct_merge(st, ...
            st_sym_boundary_analytic_electric(st));
    end
end

% }}}

% {{{ electric boundary conditions

function [ee] = st_sym_boundary_electric(einf)
    ee.phinf = @(x) einf * real(x.x);
    ee.einf = @(x) einf * ones(1, length(x.x));
end

function [ee] = st_sym_boundary_analytic_electric(ee)
    s = ee.sym;

    % electric potential
    ee.phi_ext = st_sym_handle(s.phi_ext, s.x);
    ee.phi_int = st_sym_handle(s.phi_int, s.x);

    % electric field
    ee.e_ext = st_sym_handle(s.e_ext(1) + 1.0j * s.e_ext(2), s.x);
    ee.e_int = st_sym_handle(s.e_int(1) + 1.0j * s.e_int(2), s.x);
    % compute Maxwell stress tensor
    sigma_ext = s.e_ext' * s.e_ext - 0.5 * dot(s.e_ext, s.e_ext);
    sigma_int = s.e_int' * s.e_int - 0.5 * dot(s.e_int, s.e_int);
    ee.sigma_e_ext = st_sym_tensor(sigma_ext, s.x);
    ee.sigma_e_int = st_sym_tensor(sigma_int, s.x);

    % compute surface traction
    fe_ext = sigma_ext .* s.n';
    fe_int = sigma_int .* s.n';
    ee.fn_e_ext = st_sym_handle(fe_ext(1) + 1.0j * fe_ext(2), s.x);
    ee.fn_e_int = st_sym_handle(fe_int(1) + 1.0j * fe_int(2), s.x);

    % compute surface jump
    jump = fe_ext - ee.param.Q * fe_int;
    ee.jump_e = st_sym_handle(jump(1) + 1.0j * jump(2), s.x);
end

% }}}

function [fn] = st_sym_tensor(T, x)
    Txx = st_sym_handle(T(1, 1), x);
    Txy = st_sym_handle(T(1, 2), x);
    Tyy = st_sym_handle(T(2, 2), x);

    fn = @(y) [Txx(y); Txy(y); Tyy(y)];
end

% }}}

% vim:foldmethod=marker:
