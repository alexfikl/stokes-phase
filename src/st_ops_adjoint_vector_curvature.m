% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [KX, KP] = st_ops_adjoint_vector_curvature(x, y, sigma)
    % Construct the adjoint curvature vector operator.
    %
    % The adjoint vector curvature operator is related to its scalar
    % version implemented in :func:`st_ops_adjoint_curvature` by
    %
    % .. math::
    %
    %     \mathbf{k}^*[\mathbf{X}^*] = k^*[\mathbf{X}^* \cdot \mathbf{s}] \mathbf{n}
    %
    % as is the case for :func:`st_ops_adjoint_vector_normal`.
    %
    % :arg x: target geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg y: source geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg sigma: coefficient multiplying :math:`\mathbf{X}^*`
    %
    % :returns: the operator ``KX`` and ``KP`` corresponding to the first
    %   and second principal curvatures.

    if ~exist('sigma', 'var')
        sigma = ones(size(x.x));
        sigma = sigma + 1.0j * sigma;
    end

    % construct blocks
    % xx
    sigma0 = struct('g_out',  real(x.n), ...
                    'dg_out', x.kappa_x .* real(x.t), ...
                    'g_in',   real(sigma));
    [Kxx, Pxx] = st_ops_adjoint_curvature(x, y, sigma0);
    % xy
    sigma0 = struct('g_out',  real(x.n), ...
                    'dg_out', x.kappa_x .* real(x.t), ...
                    'g_in',   imag(sigma));
    [Kxy, Pxy] = st_ops_adjoint_curvature(x, y, sigma0);
    % yx
    sigma0 = struct('g_out',  imag(x.n), ...
                    'dg_out', x.kappa_x .* imag(x.t), ...
                    'g_in',   real(sigma));
    [Kyx, Pyx] = st_ops_adjoint_curvature(x, y, sigma0);
    % yy
    sigma0 = struct('g_out',  imag(x.n), ...
                    'dg_out', x.kappa_x .* imag(x.t), ...
                    'g_in',   imag(sigma));
    [Kyy, Pyy] = st_ops_adjoint_curvature(x, y, sigma0);

    % construct final operator
    KX = [Kxx, Kxy; Kyx, Kyy];
    KP = [Pxx, Pxy; Pyx, Pyy];
end

% vim:foldmethod=marker:
