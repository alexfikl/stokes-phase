% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [gamma] = st_jump_surface_tension(name, xi, varargin)
    % Variable surface tension formulae from literature.
    %
    % Available formulae are
    %
    % * ``'Seric'``: from [Seric2018]_, Section 4.1.
    % * ``'Bump'``: similar to ``'Seric'``, but with a perturbation
    %   of :math:`\sin (\pi / 4 \cos (4 \pi k \xi))`.
    % * ``'LiLubkin'``: from [Li2001]_, Section 4.2.
    %
    % :arg name: name of the formula (case insensitive).
    % :arg xi: computational grid points.
    % :arg varargin: additional options.
    %
    % :return: surface tension evaluated at the given points.

    options = st_struct_varargin(varargin{:});

    name = lower(name);
    switch name
    case 'constant'
        gamma = ones(size(xi));
    case 'seric'
        h0 = st_struct_field(options, 'h0', 1.0);
        eps = st_struct_field(options, 'eps', 0.1);
        k = st_struct_field(options, 'k', 1.0);

        gamma = h0 + eps * cos(2.0 * pi * k * xi);
    case 'bump'
        h0 = st_struct_field(options, 'h0', 1.0);
        eps = st_struct_field(options, 'eps', 0.1);
        k = 2 * st_struct_field(options, 'k', 1.0);

        gamma = h0 + eps * sin(pi / 4 * cos(2 * pi * k * xi));
    case 'lilubkin'
        dxi = xi(2) - xi(1);

        h0 = st_struct_field(options, 'h0', 0.01);
        eps = st_struct_field(options, 'eps', 5.0 * dxi);
        w = st_struct_field(options, 'w', 0.125);

        xi = xi - 0.25;
        delta = (1.0 + cos(2.0 * pi * xi / w)) / 2.0;

        gamma = (h0 + eps * delta) * (abs(xi) < (w / 2.0));
    otherwise
        error('unknown surface tension: %s', name);
    end
end
