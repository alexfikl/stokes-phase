% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [sigma] = st_shape_finite_bump_sigma(cost, x, y)
    % Compute a grid-dependent spread of the bump function.
    %
    % Meant as a companion to :func:`st_shape_finite_bump` to ensure that
    % the bump is sufficiently spread out for the interpolation to work.
    % In particular, if the bump is too thin, it will resemble a delta
    % function and give bad results.
    %
    % :arg cost: struct containing information about the bump function.
    % :arg x: target point information.
    % :arg y: source point information.
    %
    % :return: spread :math:`\sigma`.

    if ~isempty(cost.sigma)
        sigma = cost.sigma;
        return;
    end

    switch cost.bump_type
    case 'x'
        h = st_mesh_h_max(x, y);
    case 'xi'
        h = x.panels(2) - x.panels(1);
    otherwise
        error('unknown `bump_type`: %s', cost.bump_type);
    end

    b_h = h * cost.b_cells / 2.0;
    sigma = -b_h / (2.0 * log(cost.b_eps));
end

% vim:foldmethod=marker:
