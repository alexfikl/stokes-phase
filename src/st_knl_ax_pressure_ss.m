% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function Rn = st_knl_ax_pressure_ss(Rn, x, x0, n, n0, I30, I31)
    % Axisymmetric kernel used for subtracting the singularity of the
    % pressure kernel given by :func:`st_knl_ax_pressure`.
    %
    % The layer potential corresponding to this kernel is computed in
    % :func:`st_layer_pressure`.
    %
    % The singularity subtraction should proceed as follows:
    %
    % .. math::
    %
    %     \int_\Sigma (p_j(\mathbf{x}, \mathbf{x}_0) q_j(\mathbf{x}) -
    %                  r_j(\mathbf{x}, \mathbf{x}_0)
    %                  q_j(\mathbf{x}_0)) \,\mathrm{d}\mathbf{x}.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg n: normal vector at source point.
    % :arg n0: normal vector at target point.
    % :arg I: elliptic integrals, see :func:`st_ellint_order3`.
    %
    % :returns: an array of size ``(2,)`` containing the kernel components.
    % :see also: :func:`st_ellipke`.

    X  = real(x);  Y  = imag(x);
    X0 = real(x0); Y0 = imag(x0);

    % build Green's function
    Rxxx = 2.0 * Y * (X - X0) * I30;
    Rxxy = 2.0 * Y * (Y0 * I30 - Y * I31);
    Rxyx = 2.0 * Y * (Y * I30 - Y0 * I31);
    Rxyy = 2.0 * Y * (X - X0) * I31;

    Ryxx = -Rxxy;
    Ryxy =  Rxxx;
    Ryyx = -Rxyy;
    Ryyy =  Rxyx;

    NX0 = real(n0); NY0 = imag(n0);
    NX  = real(n);  NY  = imag(n);

    Rn(1) = (Rxxx * NX0 + Rxxy * NY0) * NX + ...
            (Rxyx * NX0 + Rxyy * NY0) * NY;
    Rn(2) = (Ryxx * NX0 + Ryxy * NY0) * NX + ...
            (Ryyx * NX0 + Ryyy * NY0) * NY;
end

% vim:foldmethod=marker:
