% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [hmin] = st_mesh_h_min(x, y)
    % Compute a minimum panel size in the mesh.
    %
    % :arg x: target point information.
    % :arg y: source point information.

    % number of panels
    npanels = length(x.panels) - 1;
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);

    % compute arclength of each panel
    hmin = flintmax;
    for j = 1:npanels
        k = ((j - 1) * nnodes + 1):(j * nnodes);

        ds = sum(y.J(k) .* y.w(k));
        hmin = min(hmin, ds);
    end
end

% vim:foldmethod=marker:
