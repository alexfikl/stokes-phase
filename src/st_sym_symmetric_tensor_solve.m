% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [fn] = st_sym_symmetric_tensor_solve()
    % Determine the components of a tensor from its normal and tangential parts.
    %
    % Esentially performs a change of basis function the
    % :math:`(\mathbf{t}, \mathbf{n})` basis to the Cartesian
    % :math:`(\mathbf{e}_x, \mathbf{e}_y)`.
    %
    % There are only 3 unknowns in this system, so we only need 3 equations
    % for the components :math:`T_{nn}`, :math:`T_{nt}` and :math:`T_{tt}`.
    %
    % :return: a function handle ``fn(nn, nt, tt, real(normal), imag(normal))``, where
    %   ``nn`` and friends are the components of the tensor and
    %   ``normal`` is the surface normal, that returns the tensor components
    %   in Cartesian coordinates.

    persistent solution;
    if isempty(solution)
        syms nx ny real;
        syms b1 b2 b3 real;
        syms txx txy tyy real;

        tx = -ny;
        ty = nx;

        % dot(sigma * n, n) == dot(f, n)
        eq1 = nx^2 * txx + 2 * nx * ny * txy + ny^2 * tyy == b1;
        % dot(sigma * n, t) == dot(f, t)
        eq2 = nx * tx * txx + (nx * ty + ny * tx) * txy + ny * ty * tyy == b2;
        % dot(sigma * t, t) == dot(f, n) + 2 (dudt - dudn)
        eq3 = tx^2 * txx + 2 * tx * ty * txy + ty^2 * tyy == b3;

        [A, B] = equationsToMatrix([eq1, eq2, eq3], [txx, txy, tyy]);
        assume(nx^2 + ny^2 == 1)
        fn = simplify(linsolve(A, B));
        solution = matlabFunction(fn, 'Vars', [b1, b2, b3, nx, ny]);
    end

    fn = solution;
end
