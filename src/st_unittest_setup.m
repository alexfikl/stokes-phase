% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [settings] = st_unittest_setup(options, teardown)
    % Setup paths and plotting for tests.
    %
    % The function is meant to be called first without the *teardown* argument,
    % to set the default values. Then, it can be called again with the
    % returned *settings* and *teardown* to restore the workplace.
    %
    %   .. code:: matlab
    %
    %       config = st_unittest_setup();
    %       % .. coding ...
    %       st_unittest_setup(config, true);
    %
    % :arg options: options struct.
    % :arg teardown: if present, attempt to restore settings from *options*.
    % :returns: original settings data, before any applied changes, which
    %   can be used to restore the state by calling this function again.

    global verbosity use_cache;

    if ~exist('options', 'var')
        options = struct();
    end

    if ~isstruct(options)
        options = struct('folder', options);
    end

    if ~isfield(options, 'folder')
        options.folder = pwd();
    end

    if ~exist('teardown', 'var')
        teardown = false;
    end

    if teardown
        format;

        % restore graphics
        if isfield(options, 'graphics')
            st_graphics_defaults(options.graphics);
        end

        % reset paths
        if isfield(options, 'path')
            path(options.path);
        end

        % reset warnings
        if isfield(options, 'verbose') && ~options.verbose
            warning('on', 'MATLAB:dispatcher:nameConflict');
        end

        close all;
        rng('default');
    else
        format long g;
        % rng(42, 'philox');
        rng('shuffle');

        % add source directory to search path
        settings.path = path();

        curr_folder = options.folder;
        directory = 'xxx_unknown_xxx';
        while ~isfolder(directory)
            directory = sprintf('%s/src', curr_folder);
            curr_folder = fileparts(curr_folder);
        end
        settings.srcdir = directory;
        addpath(directory);
        directory = sprintf('%s/mex', fileparts(directory));
        settings.mexdir = directory;
        addpath(directory);

        % caching
        settings.use_cache = st_struct_field(options, 'use_cache', ...
            ~isempty(use_cache) && use_cache);

        % verbosity
        settings.verbose = st_struct_field(options, 'verbosity', ...
            ~isempty(verbosity) && verbosity);

        if ~settings.verbose
            warning('off', 'MATLAB:dispatcher:nameConflict');

            directory = sprintf('%s/silence', options.folder);
            if isfolder(directory)
                addpath(directory);
            end
        end

        % NOTE: some examples use this
        directory = sprintf('%s/data', options.folder);
        if isfolder(directory)
            addpath(directory);
        end

        % setup graphics
        settings.graphics = st_graphics_defaults(options);
    end
end

% vim:foldmethod=marker:
