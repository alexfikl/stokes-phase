% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [s] = st_struct_update(s, s0)
    % Update a struct with fields from a another struct.
    %
    % This function is function is meant to only update the existing fields
    % in *s* with fields from *s0*. Not matching fields are simply ignored.
    %
    % This functions similarly to Python's
    % `update <https://docs.python.org/3/library/stdtypes.html#dict.update>`_
    % on dictionaries.
    %
    % :arg s: struct.
    % :arg s0: other struct.
    %
    % :returns: new struct with updated fields.

    if ~isstruct(s) || ~isstruct(s0)
        error('both arguments must be structs.');
    end

    fields = fieldnames(s);
    for m = 1:numel(fields)
        if ~isfield(s0, fields{m})
            continue;
        end

        value = s0.(fields{m});
        if isstruct(s.(fields{m})) && isstruct(s0.(fields{m}))
            if ~st_isempty(s.(fields{m}))
                value = st_struct_update(s.(fields{m}), s0.(fields{m}));
            end
        end

        s.(fields{m}) = value;
    end
end

% vim:foldmethod=marker:
