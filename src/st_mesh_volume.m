% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [v] = st_mesh_volume(~, y)
    % Compute the volume of a shape.
    %
    % The volume is computed using the divergence theorem as follows:
    %
    % .. math::
    %
    %     \int_{\Omega} \,\mathrm{d}V =
    %     \int_{\Omega} \frac{1}{3} \nabla \cdot \mathbf{x} \,\mathrm{d}V =
    %     \int_{\Sigma} \frac{1}{3} \mathbf{x} \cdot \mathbf{n} \,\, \mathrm{d}S.
    %
    % :arg x: target point information.
    % :arg y: source point information.
    % :returns: the full 3D volume of the axisymmetric shape.

    v = st_ax_integral(y, 1.0 / 3.0 * st_dot(y.x, y.n));
end

% vim:foldmethod=marker:
