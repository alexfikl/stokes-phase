% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [s] = st_struct_merge(s, s0, overwrite)
    % Merge fields from two structs.
    %
    % This functions similarly to Python's
    % `update <https://docs.python.org/3/library/stdtypes.html#dict.update>`_
    % on dictionaries when *overwrite* is *true*.
    %
    % :arg s: struct.
    % :arg s0: other struct.
    % :arg overwrite: if *true*, the fields in *s* are overwritten with those
    %   in *s0*.
    % :type overwrite: default *false*
    %
    % :returns: new struct with fields in the union of the two structs.

    if ~isstruct(s) || ~isstruct(s0)
        error('both arguments must be structs.');
    end

    if ~exist('overwrite', 'var') || isempty(overwrite)
        overwrite = false;
    end

    fields = fieldnames(s0);
    for m = 1:numel(fields)
        if isfield(s, fields{m}) && ~overwrite
            continue;
        end

        value = s0.(fields{m});
        if isfield(s, fields{m})
            if isstruct(s.(fields{m})) && isstruct(s0.(fields{m}))
                value = st_struct_merge(s.(fields{m}), s0.(fields{m}), overwrite);
            end
        end

        s.(fields{m}) = value;
    end
end

% vim:foldmethod=marker:
