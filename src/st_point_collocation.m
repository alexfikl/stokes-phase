% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [targets] = st_point_collocation(npanels, nbasis, fn)
    % Generate collocation points.
    %
    % The struct that is returned contains the following fields
    %
    % * ``nodes.basis_sym``: symbolic representation of each
    %   (Lagrange) basis function.
    % * ``nodes.basis``: cell array of size ``(nbasis,)`` of Lagrange
    %   polynomial basis function handles. Each function handles takes
    %   as an argument nodes from a reference element.
    % * ``nodes.dbasis`` and ``nodes.ddbasis``: cell array of size
    %   ``(nbasis,)`` of the first- and second-order derivatives of each
    %   element in `basis`.
    % * ``nodes.unit_xi``: array of size ``(nbasis,)`` of collocation
    %   nodes on a reference :math:`[-1, 1]` element.
    % * ``npanels``.
    % * ``nbasis``.
    % * ``xi``: array of size ``(1, npanels * (nbasis - 1) + 1)`` of
    %   collocation nodes on a uniform :math:`[0, 0.5]` parametrization. The
    %   size is due to the fact that collocation points at panel
    %   endpoints appear only once in the array.
    % * ``panels``: real array of size ``(npanels + 1,)`` of panel endpoints
    %   in *xi*.
    % * ``x``: complex array of size ``(1, npanels * (nbasis - 1) + 1)``
    %   of points describing the curve in 2D. (only if *fn* is
    %   provided).
    % * ``vertices``: complex array of size ``(npanels + 1,)`` containing
    %   the panel endpoints on the 2D curve. (only if *fn* is provided)
    %
    % :arg npanels: number of panels (or elements).
    % :arg nbasis: number of collocation points in each
    %   panel. In the case of collocation points, the end points
    %   are shared between panels. Should be an even number so
    %   that the quadrature and collocation sets of points don't
    %   overlap.
    % :arg fn: ``@(xi)`` function handle defining the geometry that takes
    %   the parametrization :math:`\xi \in [0, 0.5]` and returns the
    %   :math:`x` coordinates of each point as a complex number.
    %
    % :returns: a struct describing the high-order surface.

    % generate panels
    panels = linspace(0.0, 0.5, npanels + 1);
    % panel midpoints
    xim = 0.5 * (panels(1:end - 1) + panels(2:end));
    % panels sizes
    dxi = diff(panels);

    % compute collocation nodes and basis
    nodes = st_basis_lagrange(nbasis, 0.25 / npanels);
    % translate nodes
    xi = repmat(nodes.unit_xi(1:end - 1)', 1, npanels);
    xi = [reshape(xim + 0.5 * dxi .* xi, 1, []), panels(end)];

    targets.nodes = nodes;
    targets.panels = xi(1:nbasis - 1:end);
    targets.xi = xi;
    targets.dxi = xi(2) - xi(1);

    targets.nbasis = length(nodes.basis);
    targets.npanels = npanels;

    if exist('fn', 'var')
        if ischar(fn)
            targets.x = st_mesh_curve(targets.xi, fn);
        elseif isa(fn, 'function_handle')
            targets.x = fn(targets.xi);
        elseif isa(fn, 'float') && length(fn) == length(xi)
            targets.x = fn;
        elseif isempty(fn)
            return;
        else
            error('unknown shape variable type.');
        end
        targets.vertices = targets.x(1:nbasis - 1:end);
    end
end

function nodes = st_basis_lagrange(nbasis, dxi)
    eta = sym('eta', 'real');

    % generate collocation points
    if nbasis == 1
        z = [0.0]; %#ok
    else
        z = linspace(-1.0, 1.0, nbasis);
    end

    % generate basis functions
    basis = cell(1, nbasis);
    dbasis = cell(1, nbasis);
    ddbasis = cell(1, nbasis);

    for p = 1:nbasis
        b = (eta - z) ./ (z(p) - z);
        b = b(~isinf(b));

        sym_basis = simplify(prod(b));
        sym_dbasis = simplify(diff(sym_basis, eta) / dxi);
        sym_ddbasis = simplify(diff(sym_basis, eta, 2) / dxi^2);

        basis{p} = matlabFunction(sym_basis, 'Vars', eta);
        dbasis{p} = matlabFunction(sym_dbasis, 'Vars', eta);
        ddbasis{p} = matlabFunction(sym_ddbasis, 'Vars', eta);
    end

    nodes.unit_xi = z;
    nodes.basis = basis;
    nodes.dbasis = dbasis;
    nodes.ddbasis = ddbasis;
end

% vim:foldmethod=marker:
