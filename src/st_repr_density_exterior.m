% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [q] = st_repr_density_exterior(x, y, bc)
    % Solve a single-phase Stokes equation for a given domain.
    %
    % This function uses the density representation from Section 4.1
    % in [Pozrikidis1992]_. The solver only supports ``mldivide`` with the
    % QR method, since the resulting matrix is generally badly conditioned.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: density field on the surface.
    % :see also: :func:`st_layer_slp`, :func:`st_layer_slp_complete`.

    % construct single-layer potential
    S = st_layer_slp(x, y);
    % construct completion matrix
    N = st_layer_slp_complete(x, y);

    % right-hand side
    q = bc.fn.usurf(x) - bc.fn.uinf(x);
    q = [real(q), imag(q)]';

    % solve system
    linopts.RECT = true;
    [q, ~] = linsolve(S + N, q, linopts);

    % put back into a complex number
    q = q(1:length(x.x))' + ...
        1.0j * q(length(x.x) + 1:end)';
end

% vim:foldmethod=marker:
