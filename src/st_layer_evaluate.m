% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function knl = st_layer_evaluate(x, y, knl, q)
    % Evaluate an axisymmetric layer potential.
    %
    % This function evaluates a layer potential of the general form
    %
    % .. math::
    %
    %     \mathcal{K}_{ik\dots}[\mathbf{q}](\mathbf{x}) =
    %     \int_\Sigma K_{ijk\dots}(\mathbf{x}, \mathbf{y})
    %         q_j(\mathbf{y})
    %     \,\mathrm{d}S.
    %
    % This is done either by directly applying it to a given density
    % :math:`\mathbf{q}`, or by constructing discrete matrix operators that
    % do this.
    %
    % The layer potential is defined by the ``knl`` argument, that describes
    % the kernel we are evaluating. The way this function works is that:
    %
    % * the ``knl`` is reponsible for computing the kernel and caching the
    %   fields where it is stored.
    % * the current function is responsible for telling the ``knl`` at
    %   which points to evaluate the kernel and with what quadrature rules.
    %
    % The ``knl`` struct has the following main fields
    %
    % * ``quad``: special quadrature rules to be used near singularities or
    %   cusps arising from singularity subtraction. The required quadrature
    %   rules can be constructed with :func:`st_layer_quadrature`.
    % * ``knl = evaluate(knl, target, source)``: a function handle that takes
    %   the source and target point information, evaluates the kernel and
    %   returns the updated ``knl`` struct.
    % * any additional fields are private and can be manipulated in the
    %   ``evaluate`` call.
    %
    % The input `source` and `target` structs have mainly the same fields as the
    % input `x` and `y` structs. On top of that, the `target` struct has
    % a ``i`` field denoting the current target point. The source and target
    % structs also have a ``jstart`` and ``jend`` fields corresponding to
    % the range of source points that should be computed. This range can be
    % used to index into other arrays such as the normal `source.n`.
    %
    % :arg x: target geometry struct.
    % :arg y: source geometry struct.
    % :arg knl: struct containing kernel information.
    % :arg q: density at the collocation points.
    %
    % :returns: `knl` struct with updated fields containing the layer
    %   potential.

    if exist('q', 'var') && ~isempty(q)
        knl = st_layer_evaluate_apply(x, y, q, knl);
    else
        knl = st_layer_evaluate_matrix(x, y, knl);
    end
end

function knl = st_layer_evaluate_apply(x, y, q, knl)
    % number of target collocation points
    ntargets = length(x.x);
    % number of source panels
    npanels = length(x.panels) - 1;
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);
    % number of basis functions
    nbasis = length(x.nodes.basis);

    % precompute geometry near singularities
    ys = st_interp_geometry(x, q, knl);
    % save data
    x.q = q;
    y.q = st_interpolate(x, q, y.xi);

    %
    % compute layer potential
    %

    knl_evaluate = knl.evaluate;
    ys.jend = 0;
    for i = 1:ntargets
        x.i = i;
        x.singular = false;

        %
        % compute farfield interactions
        %

        % compute interactions with panels to the right
        iprev = ys.panel_ids(2, i);
        for j = 1:iprev - 1
            y.jstart = (j - 1) * nnodes + 1;
            y.jend = j * nnodes;

            knl = knl_evaluate(knl, x, y);
        end

        % compute interactions with panels to the left
        inext = ys.panel_ids(1, i);
        for j = inext + 1:npanels
            y.jstart = (j - 1) * nnodes + 1;
            y.jend = j * nnodes;

            knl = knl_evaluate(knl, x, y);
        end

        %
        % compute nearfield interactions
        %

        % get index of current collocation point in the panel
        if i == 1
            k = length(knl.quad.unit_xi) - 1;
        elseif i == ntargets
            k = length(knl.quad.unit_xi);
        else
            k = mod(i - 1, nbasis - 1) + 1;
        end

        % evaluate
        x.singular = true;
        ys.jstart = ys.jend + 1;
        ys.jend = ys.jend + length(knl.quad.unit_xi{k});
        knl = knl_evaluate(knl, x, ys);
    end
end

function [knl] = st_layer_evaluate_matrix(x, y, knl)
    % number of target collocation points
    ntargets = length(x.x);
    % number of source panels
    npanels = x.npanels;
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);
    % number of basis functions
    nbasis = length(x.nodes.basis);

    % precompute geometry near singularities
    ys = st_interp_geometry(x, [], knl);
    % save data
    y.basis = st_interp_evaluate_basis(x, y);

    % compute layer potential
    knl_evaluate = knl.evaluate;
    ys.jend = 0;
    for i = 1:ntargets
        x.i = i;

        %
        % compute farfield interactions
        %

        % compute interactions with panels to the right
        iprev = ys.panel_ids(2, i);
        for p = 1:iprev - 1
            x.jstart = (p - 1) * (nbasis - 1) + 1;
            x.jend = x.jstart + nbasis - 1;
            y.jstart = (p - 1) * nnodes + 1;
            y.jend = y.jstart + nnodes - 1;

            knl = knl_evaluate(knl, x, y);
        end

        % compute interactions with panels to the left
        inext = ys.panel_ids(1, i);
        for p = inext + 1:npanels
            x.jstart = (p - 1) * (nbasis - 1) + 1;
            x.jend = x.jstart + nbasis - 1;
            y.jstart = (p - 1) * nnodes + 1;
            y.jend = y.jstart + nnodes - 1;

            knl = knl_evaluate(knl, x, y);
        end

        %
        % compute nearfield interactions
        %

        % get index of current collocation point in the panel
        if i == 1
            k = length(knl.quad.unit_xi) - 1;
        elseif i == ntargets
            k = length(knl.quad.unit_xi);
        else
            k = mod(i - 1, nbasis - 1) + 1;
        end

        % evaluate
        jstart = ys.jend + 1;
        jend = ys.jend + length(knl.quad.unit_xi{k});
        if i == 1
            n = 0;
        elseif i == ntargets
            n = length(knl.quad.unit_xi{k});
        else
            n = discretize(x.xi(i), ys.xi(jstart:jend));
        end

        % left
        if i > 1
            x.jstart = (iprev - 1) * (nbasis - 1) + 1;
            x.jend = x.jstart + nbasis - 1;
            ys.jstart = ys.jend + 1;
            ys.jend = ys.jend + n;

            knl = knl_evaluate(knl, x, ys);
        end

        % left
        if i < ntargets
            x.jstart = (inext - 1) * (nbasis - 1) + 1;
            x.jend = x.jstart + nbasis - 1;
            ys.jstart = ys.jend + 1;
            ys.jend = jend;

            knl = knl_evaluate(knl, x, ys);
        end
    end
end

function ys = st_interp_geometry(x, q, knl)
    % map targets to panels
    panel_ids = st_mesh_target_associate(x);
    % panel_ids(2, 1) = 0; panel_ids(1, end) = npanels + 1;

    % compute geometry
    % FIXME: This is a dense matvec and makes the code die at grid sizes
    % of ~1k panels or ~20k points.
    xhat = exp(-x.zk' * knl.quad.xi) / length(x.zk);
    dx = (x.zk .* x.zhat) * xhat;
    ys.x = x.zhat * xhat;

    % dx = st_derivative(x, x.x, knl.quad.xi, 'lagrange_xi');
    % ys.x = st_interpolate(x, x.x, knl.quad.xi);

    ys.J = abs(dx);
    ys.t = dx ./ ys.J;
    ys.n = -1.0j * ys.t;

    ys.xi = knl.quad.xi;
    ys.w = knl.quad.w;
    ys.panel_ids = panel_ids;
    if ~isempty(q)
        ys.q = st_interpolate(x, q, knl.quad.xi);
    else
        ys.basis = st_interp_evaluate_basis(x, knl.quad.xi);
    end
end

% vim:foldmethod=marker:
