% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [x, y, state] = st_ode_steady(x0, y0, forcing, bc, checkpoint, varargin)
    % Drive a drop to steady state.
    %
    % The kinematic equation for the drop is given by:
    %
    % .. math::
    %
    %     \frac{\partial \mathbf{X}}{\partial t} =
    %         \mathbf{f}(\mathbf{u}, \mathbf{X}).
    %
    % where :math:`f` is given by *forcing* (see also
    % :func:`st_forcing_options`). The equation above is solved using a
    % second-order Runge-Kutta time integrator, where each step proceeds as follows:
    %
    % * First, the right-hand side of Equation 5.2.9 from [Pozrikidis1992]_
    %   is evaluated for the current velocity. For the first time step, we
    %   initialize the velocity field to zero, so the double-layer is not
    %   evaluated.
    % * Once an intermediary velocity has been obtained, we advance the
    %   solution using the kinematic condition.
    % * At the end of the time step, the resulting geometry is smoothed,
    %   the points are equidistanced, the solution is plotted, etc., as
    %   requested by the user.
    % * If not given explicitly, at every iteration, the timestep is
    %   recomputed using the formula (inspired by [Denner2015]_):
    %
    %   .. math::
    %
    %       \Delta t = \theta \min_i \sqrt{Ca \Delta s_i^3}.
    %
    % * The time stepping is stopped if one of two conditions is satisfied:
    %
    %   1. if :math:`\|\mathbf{u} \cdot \mathbf{n}\|_\infty < \epsilon_u`,
    %      which is the physical steady state condition, or
    %   2. if :math:`\|x^{n + 1} - x^{n}\|_\infty < \epsilon_x`,
    %      which checks if the time stepping has stagnated. As a rule,
    %      :math:`\epsilon_x \ll \epsilon_u` to achieve desired results.
    %
    % This pseudo-time integration is effectively a fixed point iteration for
    % the pair :math:`(\mathbf{x}, \mathbf{u})`. Note that, since this is the
    % case, the velocity field :math:`\mathbf{u}` only solves the Stokes
    % equations once steady state was reached to a sufficiently small
    % tolerance. All intermediate values cannot be considered valid solutions.
    % The actual solution should be obtained only after steady state has been
    % reached using, e.g., :func:`st_repr_velocity`. Exact solutions at each
    % time step can be obtained by passing ``solver = 'density'``, but this
    % is significantly slower.
    %
    % :arg x0: initial target geometry information.
    % :arg y0: initial source geometry information.
    % :arg bc: boundary conditions for the Stokes equations. See
    %   :func:`st_boundary_condition_options` for the format.
    % :arg checkpoint: a cache created by :func:`st_cache_create`. We make
    %   use of its ``fileseries`` field to output the iterations.
    % :arg varargin: options passed to :func:`st_ode_options`. Options can
    %   be provided by a struct followed by additional pairs of options.
    %
    % :returns: a tuple ``(x, y, state)`` of the geometry at the final time
    %   and a state struct with additional information about the simulation.

    % References:
    %   [1] F. Denner, B. G. M. van Wachem, Numerical Timestep Restrictions
    %   as a Result of Capillary Waves, Journal of Computational Physics,
    %   Vol. 285, 2015.

    persistent volume_state;
    if isempty(volume_state)
        volume_state = {'shrinking', 'growing'};
    end

    % {{{ ode

    ode = st_ode_options(varargin{:});
    ode.forcing = forcing;

    enable_checkpointing = ~isempty(checkpoint);
    enable_iteration_checkpointing = false;
    if enable_checkpointing
        enable_iteration_checkpointing = isempty(checkpoint.file_series_id);
    end

    % }}}

    % {{{ initialize variables

    % compute geometry
    [x, y] = st_mesh_geometry(x0, y0);

    % compute time step
    fixed_time_step = ode.dt > 0;
    if ~fixed_time_step
        % from [1, Equation 4]
        h_min = st_mesh_h_min(x, y);
        ode.dt = ode.theta * sqrt(bc.param.Ca) * h_min^(3.0 / 2.0);
    end
    ode.dt = min(ode.dt, ode.tmax);

    % make tolerances relative
    uinf = bc.fn.uinf(x);
    ode.utol = ode.utol * norm(st_dot(uinf, x.n), Inf);
    ode.xtol = ode.xtol * norm(x.x, Inf);

    if ode.display
        disp(ode);
    end

    % }}}

    % {{{ initialize loop

    L = max(real(x.x));
    B = max(imag(x.x));

    % iteration
    t = 0.0;
    it = 0;

    [x, y] = st_wrap_equidistance(t, x, y, ode, bc);

    % statistics for each iteration
    state.t = t;
    state.u_dot_n = [];
    state.volume = st_mesh_volume(x, y);
    state.deformation = (L - B) / (L + B);
    state.u = [];

    state.checkpoints = {};

    state.timing.solver = [];
    state.timing.geometry = [];
    state.timing.forcing = [];

    % }}}

    % {{{ steady state evolution

    if enable_iteration_checkpointing
        st_wrap_checkpoint(checkpoint, it, t, x, 0.0);
    end

    while t < ode.tmax && it < ode.maxit
        state.timing.solver(end + 1) = 0.0;
        state.timing.geometry(end + 1) = 0.0;
        state.timing.forcing(end + 1) = 0.0;

        % {{{ update geometry
        xc = x.x;

        % stage 1
        [k1, state] = st_wrap_forcing(t, x, y, ode, bc, state);

        % stage 2
        x.x = xc + 0.5 * ode.dt * k1;
        [k2, state] = st_wrap_forcing(t + 0.5 * ode.dt, x, y, ode, bc, state);

        % update the geometry
        x.x = xc + ode.dt * k2;

        if mod(it - 1, 16) == 0
            [x, y, state] = st_wrap_equidistance(t, x, y, ode, bc, state);
        else
            [x, y, state] = st_wrap_geometry(t, x, y, ode, bc, state, true);
        end

        % }}}

        % {{{ checkpoint

        if mod(it - 1, ode.odefreq) == 0
            try
                if ~isempty(ode.odeplot)
                    ode.odeplot(t, x, y, state.u);
                end
            catch
                warning('Error occured during plotting. Disabling ...');
                ode.odeplot = [];
                ode.odefreq = Inf;
            end
        end

        if enable_iteration_checkpointing
            st_wrap_checkpoint(checkpoint, it, t, x, state.u);
        end

        % }}}

        % {{{ iteration dump

        L = max(real(x.x));
        B = max(imag(x.x));

        state.t(end + 1) = t + ode.dt;
        state.u_dot_n(end + 1) = norm(st_dot(state.u, x.n), Inf);
        state.volume(end + 1) = st_mesh_volume(x, y);
        state.deformation(end + 1) = (L - B) / (L + B);

        if ode.display
            fprintf('[%05d] dt = %.5e t = %.5e\n', it, ode.dt, t);

            % relative volume change
            dv = (state.volume(end) - state.volume(1)) / state.volume(1);
            m = (state.volume(end) > state.volume(end - 1)) + 1;

            fprintf('   udotn:          %.6e (%.6e)\n', ...
                state.u_dot_n(end), ode.utol);
            fprintf('   dx:             %.6e (%.6e)\n', ...
                norm(x.x - xc, Inf), ode.xtol);
            fprintf('   deformation:    %.6e\n', state.deformation(end));
            fprintf('   volume:         %.6e (%s)\n', dv, volume_state{m});
        end

        % }}}

        % {{{ check stopping criteria

        % check first stopping condition: residual
        delta = norm(x.x - xc, Inf);
        if delta < ode.xtol
            break;
        end

        % check second stopping condition: steady state
        if state.u_dot_n(end) < ode.utol
            break;
        end

        % }}}

        % {{{ update time step

        t = t + ode.dt;
        it = it + 1;

        if ~fixed_time_step
            h_min = st_mesh_h_min(x, y);
            ode.dt = ode.theta * sqrt(bc.param.Ca) * h_min^(3.0 / 2.0);
        end
        ode.dt = min(ode.dt, ode.tmax - t);

        % }}}
    end

    % }}}

    state.x = x;
    state.maxit = it;
    state.tmax = t;

    if enable_checkpointing
        filename = checkpoint.filename(checkpoint, 'state');

        state.checkpoints = checkpoint.file_series_cache;
        save(filename, '-struct', 'state');
    end
end

function [st, state] = st_wrap_solver(~, x, y, ode, bc, state)
    tic();
    switch ode.solver
    case 'density'
        st = st_repr_density_solution(x, y, bc, ode.variables);
    case 'velocity'
        st.u = st_repr_velocity(x, y, bc.param.lambda, bc, [], state.u);
        st.param = bc.param;
        state.u = st.u;
    case 'fixedpoint'
        lambda = bc.param.lambda;

        % boundary conditions
        deltaf = bc.fn.jump(x);
        uinf = bc.fn.uinf(x);

        % add farfield
        u = 2.0 / (1.0 + lambda) * uinf;
        % add single layer
        f = st_layer_slp(x, y, deltaf);
        u = u - 1.0 / (1.0 + lambda) / (4.0 * pi) * f;
        % add double layer
        if ~isempty(state.u) && abs(lambda - 1.0) < 1.0e-14
            f = st_layer_dlp(x, y, state.u);
            u = u + (1.0 - lambda) / (1.0 + lambda) / (4.0 * pi) * f;
        end

        state.u = u;
        st.u = u;
        st.param = bc.param;
    otherwise
        error('unknown solver: %s', ode.solver);
    end
    t_elapsed = toc();

    state.timing.solver(end) = state.timing.solver(end) + t_elapsed;
end

function [f, state] = st_wrap_forcing(t, x, y, ode, bc, state)
    % NOTE: save updating the geometry at the start of the iteration, when
    % it's still valid from the previous time step
    if abs(t - state.t(end)) > 1.0e-14
        [x, y, state] = st_wrap_geometry(t, x, y, ode, bc, state);
    end

    [st, state] = st_wrap_solver(t, x, y, ode, bc, state);

    % compute forcing term
    tic();
    f = ode.forcing.fn.evaluate(ode.forcing, t, x, y, st);
    t_elapsed = toc();

    state.timing.forcing(end) = state.timing.forcing(end) + t_elapsed;
end

function [x, y, state] = st_wrap_geometry(~, x, y, ode, ~, state, with_filter)
    if ~exist('with_filter', 'var')
        with_filter = false;
    end

    tic();
    if ode.filter_strength > 0
        x.x = st_ax_filter(x.x, 'fft', ode.filter_strength);
    end
    [x, y] = st_mesh_geometry(x, y);
    t_elapsed = toc();

    state.timing.geometry(end) = state.timing.geometry(end) + t_elapsed;
end

function [x, y, state] = st_wrap_equidistance(~, x, y, ode, ~, state)
    tic();
    [x, y] = st_mesh_equidistance(x, y, 'interp', ...
        'pfilter', ode.filter_strength);
    t_elapsed = toc();

    if exist('state', 'var')
        state.timing.geometry(end) = state.timing.geometry(end) + t_elapsed;
    end
end

function [checkpoint] = st_wrap_checkpoint(checkpoint, it, t, x, u)
    [checkpoint, filename] = checkpoint.fileseries(checkpoint, 'steady_state');
    save(filename, 'it', 't', 'x', 'u');
end

% vim:foldmethod=marker:
