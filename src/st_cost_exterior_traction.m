% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cost] = st_cost_exterior_traction(varargin)
    % Defines a traction matching cost functional.
    %
    % It is given by
    %
    % .. math::
    %
    %     \mathcal{J} = \frac{1}{2} \int_{\Sigma}
    %         \|\mathbf{f} - \mathbf{f}_d \|^2 \,\mathrm{d}S.
    %
    % :returns: a struct compatible with :func:`st_cost_options`.

    cost = st_cost_options(varargin{:}, ...
        'variables', {'exterior', 'fn'}, ...
        'forward_variables', {'exterior', 'fn', 'grads'});

    cost.fn.evaluate = @cost_exterior_traction_evaluate;
    cost.fn.gradient_x = @cost_exterior_traction_gradient_x;
    cost.fn.gradient_u = @cost_exterior_traction_gradient_u;
    cost.fn.gradient_s = @cost_exterior_traction_gradient_s;
    cost.fn.gradient_b = @cost_exterior_traction_gradient_b;
end

function [r] = cost_exterior_traction_evaluate(cost, ~, x, y, st)
    df = st_interpolate(x, st_dot(st.fn_ext, x.n) - cost.user.fdx, y.xi);
    r = st_ax_integral(y, 0.5 * abs(df).^2);
end

function [r] = cost_exterior_traction_gradient_x(cost, ~, x, y, st)
    M = st_ops_mass(x, y);
    N = st_ops_adjoint_normal(x, y, ones(size(x.x)));

    deltaf = st_dot(st.forward.fn_ext, x.n) - cost.user.fdx;
    if isfield(cost.user, 'sym')
        grads = sym_stress_gradient(cost.user.sym, x);
    else
        grads = deltaf .* st.forward.grads_ext;
    end

    T0 = N * (2.0 * deltaf .* st_dot(st.forward.fn_ext, x.t))';
    T1 = M * grads';
    T2 = M * (0.5 * x.kappa .* abs(deltaf).^2)';

    r = T0 + T1 + T2;
end

function [r] = cost_exterior_traction_gradient_u(~, ~, ~, ~, ~)
    r = 0.0;
end

function [r] = cost_exterior_traction_gradient_s(cost, ~, x, ~, st)
    r = -(st_dot(st.forward.fn_ext, x.n) - cost.user.fdx) .* x.n;
end

function [r] = cost_exterior_traction_gradient_b(~, ~, ~, ~, st)
    r = -st.adjoint.fn_ext;
end

% {{{

function [ds] = sym_stress_gradient(st, x)
    ds = 0;

    fn = 0.5 * dot(st.sigma_ext * st.n', st.n)^2;
    for i = 1:length(st.n)
        ds = ds + diff(fn, st.x(i)) * st.n(i);
    end

    g = matlabFunction(ds, 'Vars', st.x);
    ds = g(real(x.x), imag(x.x));
end

% }}}
