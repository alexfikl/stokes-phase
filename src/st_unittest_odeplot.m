% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [fn] = st_unittest_odeplot(x, mode, varargin)
    % {{{ default options

    d.xlim = [min(real(x.x)) - 0.5, max(real(x.x)) + 0.5];
    d.ylim = [min(imag(x.x)) - 0.5, max(imag(x.x)) + 0.5];
    d.klim = [min(x.kappa) - 0.5, max(x.kappa) + 0.5];

    d = st_struct_update(d, st_struct_varargin(varargin{:}));

    % }}}

    if ~exist('mode', 'var') || isempty(mode)
        mode = 'forward';
    end
    mode = lower(mode);

    switch mode
    case 'forward'
        fn = create_forward_odeplot(x, d);
    case 'adjoint'
        error('NotImplementedError');
    end
end

function [fn] = create_forward_odeplot(x, options)
    z0 = st_array_ax(x.x, true);
    v0 = st_array_ax(x.vertices, true);

    figure('visible', 'on');
    subplot(1, 2, 1);
    h1 = plot(real(z0), imag(z0));
    h2 = plot(real(v0), imag(v0), 'k.', 'MarkerSize', 8);
    xlabel('$x$');
    ylabel('$\rho$');
    if ~isempty(options.xlim)
        xlim(options.xlim);
    end
    if ~isempty(options.ylim)
        ylim(options.ylim);
    end
    axis('equal');

    subplot(1, 2, 2);
    h3 = plot(x.xi, x.kappa);
    xlabel('$\xi$');
    ylabel('$\kappa$');
    xlim([0.0, 0.5]);
    if ~isempty(options.klim)
        ylim(options.klim);
    end
    drawnow;

    fn = @(t, x, y, st) fn_odeplot(t, x, y, st, h1, h2, h3);

    function fn_odeplot(~, x, ~, ~, h1, h2, h3)
        z = st_array_ax(x.x, true);
        v = st_array_ax(x.vertices, true);

        set(h1, 'XData', real(z));
        set(h1, 'YData', imag(z));
        set(h2, 'XData', real(v));
        set(h2, 'YData', imag(v));
        set(h3, 'YData', x.kappa);
        drawnow;
        pause(0.01);
    end
end
