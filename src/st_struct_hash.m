% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [h] = st_struct_hash(varargin)
    % Hash a given struct.
    %
    % This function does not hash all the contents of a struct. We just use
    % the scalar quantities and fieldnames. There is also no randomness, so
    % the same struct will hash to the same value if no values were modified.
    %
    % :arg varargin: any number of structs.
    % :return: a string of the hexadecimal representation of the MD5 hash.

    engine = java.security.MessageDigest.getInstance('MD5');
    for n = 1:length(varargin)
        engine = hash_rec(varargin{n}, engine);
    end

    h = double(typecast(engine.digest, 'uint8'));
    h = sprintf('%.2x', h);
end

function engine = hash_rec(st, engine)
    engine.update(typecast(uint16(class(st)), 'uint8'));

    if isnumeric(st) && length(st) == 1
        if isreal(st)
            engine.update(typecast(st, 'uint8'));
        else
            engine.update(typecast(real(st), 'uint8'));
            engine.update(typecast(imag(st), 'uint8'));
        end
    elseif islogical(st) && length(st) == 1
        engine.update(typecast(uint8(st), 'uint8'));
    elseif ischar(st)
        engine.update(typecast(uint16(st), 'uint8'));
    elseif isstruct(st) && length(st) == 1
        fields = sort(fieldnames(st));

        for n = 1:length(fields)
            engine.update(typecast(uint16(fields{n}), 'uint8'));
            engine = hash_rec(st.(fields{n}), engine);
        end
    else
        % ignoring other values
    end
end

% vim:foldmethod=marker:
