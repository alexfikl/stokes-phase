% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function xdoty = st_dot(x, y)
    % Compute element-wise dot product of two complex arrays.
    %
    % We represent a 2D real array :math:`(u, v)` as a complex number
    % :math:`u + \imath v`. To compute the element-wise dot product of two
    % such complex arrays, we have:
    %
    % .. math::
    %
    %     x \cdot y \triangleq \Re\{x\} \cdot \Re\{y\} + \Im\{x\} \cdot \Im\{y\}
    %
    % :arg x: complex array.
    % :arg y: complex array.
    %
    % :returns: element-wise dot product (real array).

    if length(x) ~= 1 && length(y) ~= 1
        if ~all(size(x) == size(y))
            error('vectors have incompatible dimensions');
        end
    end

    xdoty = real(x) .* real(y) + imag(x) .* imag(y);
end

% vim:foldmethod=marker:
