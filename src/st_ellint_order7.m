% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [I70, I71, I72, I73, I74] = st_ellint_order7(x, x0, F, E)
    % Compute elliptic integrals for the Stresslet derivative.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg F: elliptic integral of the first kind, see :func:`st_ellipke`.
    % :arg E: elliptic integral of the second kind.
    %
    % :returns: elliptic integrals described in Section 2.4 of
    %   [Pozrikidis1992]_ used to compute the Stresslet derivative.
    %   Namely, it computes :math:`I_{70}, I_{71}, I_{72}, I_{73}` and
    %   :math:`I_{54}`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.
    %   [2] C. Pozrikidis, The Instability of a Moving Viscous Drop,
    %   Journal of Fluid Mechanics, 1990.

    R = 1.0 / ((real(x) - real(x0))^2 + (imag(x) + imag(x0))^2);

    % compute K
    % see [1, Equation 2.4.8]
    K2 = 4.0 * imag(x) * imag(x0) * R;
    K4 = K2 * K2;
    K6 = K2 * K4;
    K8 = K2 * K6;
    K10 = K2 * K8;
    K12 = K2 * K10;
    % NOTE: can cause division by zero lower down
    K22 = (1.0 - K2)^2;
    K23 = (1.0 - K2) * K22;
    % compute coefficient for I_{mn}
    % see [1, Equation 2.4.7]
    C7 = 4.0 * R^3 * sqrt(R);

    I70 =  C7 * (8.0 * K4 - 23.0 * K2 + 23) / (15.0 * K23) * E ...
          +C7 * (4.0 * K2 - 8.0) / (15.0 * K22) * F;
    I71 = -C7 * (8.0 * K6 - 19.0 * K4 + 9.0 * K2 - 6.0) / (15.0 * K2 * K23) * E ...
          -C7 * (4.0 * K4 - 6.0 * K2 + 6.0) / (15.0 * K2 * K22) * F;
    I72 =  C7 * (8.0 * K8 - 15.0 * K6 + 7.0 * K4 + 16.0 * K2 - 8.0) / (15.0 * K4 * K23) * E ...
          +C7 * (4.0 * K6 - 4.0 * K4 - 12.0 * K2 + 8.0) / (15.0 * K4 * K22) * F;
    I73 = -C7 * (8.0 * K10 - 11.0 * K8 + 17.0 * K6 - 118 * K4 + ...
                    160.0 * K2 -64.0) / (15.0 * K6 * K23) * E ...
          -C7 * (4.0 * K8 - 2.0 * K6 + 66.0 * K4 - ...
                    128.0 * K2 + 64.0) / (15.0 * K6 * K22) * F;
    I74 =  C7 * (8.0 * K12 - 7.0 * K10 + 39.0 * K8 - 832.0 * K6 + ...
                    2336.0 * K4 - 2304.0 * K2 + 768.0) / (15.0 * K8 * K23) * E ...
          +C7 * (4.0 * K10 + 360.0 * K6 - 1520.0 * K4 + ....
                1920.0 * K2 - 768.0) / (15.0 * K8 * K22) * F;
end

% vim:foldmethod=marker:
