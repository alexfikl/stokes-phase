% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [m] = st_ax_mean(y, f)
    % Compute the axisymmetric mean over a surface.
    %
    % The mean is given by (see :func:`st_ax_integral`):
    %
    % .. math::
    %
    %     \frac{1}{|S|} \int_S f(x) \,\mathrm{d}x,
    %
    % where :math:`|S|` is the area.
    %
    % :arg y: source geometry information.
    % :arg f: variable prescribed at all the source points
    %
    % :returns: mean.

    m = st_ax_integral(y, f) / st_ax_integral(y, 1.0);
end

% vim:foldmethod=marker:
