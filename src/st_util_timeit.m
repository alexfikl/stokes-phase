% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [t_avg, t_std, t_total] = st_util_timeit(fn, nruns, alpha)
    % Time a given function.
    %
    % :arg fn: function handle with no arguments.
    % :arg nruns: number of runs.
    % :type nruns: default :math:`128`
    % :arg alpha: confidence level.
    % :type alpha: default :math:`0.95`
    %
    % :return: a tuple ``(t_avg, t_std, t_total)``, with the average time,
    %   confidence interval and total time (all in seconds). The confidence
    %   interval is computed under the assumption that the timing samples
    %   have a Gaussian distribution.

    if ~exist('nruns', 'var')
        nruns = 128;
    end

    if ~exist('alpha', 'var')
        alpha = 0.95;
    end

    t = zeros(1, nruns);
    for k = 1:nruns
        tic();
        fn();
        t(k) = toc();
    end

    t_total = sum(t);
    t_avg = mean(t);
    t_std = std(t);
    t_inv = st_tinv(alpha, length(t) - 1);
    t_std = t_inv * t_std / sqrt(length(t));
end

function [p] = st_tinv(p, nu)
    % https://www.mathworks.com/matlabcentral/answers/260564-how-to-implement-tinv-manually-without-statistics-and-machine-learning-toolbox
    t_dist = @(t) (1.0 - betainc(nu / (nu + t^2), nu / 2.0, 0.5));
    t_dist_r = @(t) 1.0 - (1.0 - t_dist(t)) / 2.0;
    t_inv = @(alpha) fzero(@(s) (max(alpha, (1 - alpha)) - t_dist_r(s)), 5.0);

    p = t_inv(p);
end

% vim:foldmethod=marker:
