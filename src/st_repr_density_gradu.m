% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [du_ext, du_int] = st_repr_density_gradu(x, y, q, bc, repr)
    % Compute normal or tangential velocity gradient.
    %
    % The normal velocity gradient is computed by using the layer potential
    % representation from :func:`st_layer_normal_gradu` with the appropriate surface
    % limits. Similarly, the tangential velocity gradient is computed using
    % the layer potential representation from :func:`st_layer_tangent_gradu`.
    %
    % The velocity gradients we are computing are given by
    %
    % .. math::
    %
    %     \begin{aligned}
    %     \mathbf{n} \cdot \nabla \mathbf{u} =~ &
    %         n_j \frac{\partial u_i}{\partial x_j}, \\
    %     \mathbf{t} \cdot \nabla \mathbf{u} =~ &
    %         t_j \frac{\partial u_i}{\partial x_j}.
    %     \end{aligned}
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    % :arg bc: see :func:`st_boundary_condition_options`.
    % :arg repr: string denoting the desired result: ``'normal'`` or ``'tangent'``.
    % :type repr: default ``'normal'``.
    %
    % :returns: if the density is given, it returns the desired velocity
    %   gradient, otherwise the corresponding operator is returned.

    if ~exist('repr', 'var')
        repr = 'normal';
    end
    repr = lower(repr);

    if ~any(contains({'normal', 'tangent', 'tangentfft'}, repr))
        error('unknown representation: %s', repr);
    end

    if ~isempty(q)
        switch repr
        case 'normal'
            duinf = bc.fn.dudninf(x);
        case { 'tangent', 'tangentfft' }
            duinf = bc.fn.dudtinf(x);
        end

        du_ext = duinf;
        du_int = duinf;
        if norm(q, Inf) < 1.0e-12
            return;
        end

        switch repr
        case 'normal'
            vlp = st_layer_normal_gradu(x, y, q);

            du_ext = du_ext - 4.0 * pi * st_dot(q, x.t) .* x.t - vlp;
            du_int = du_int + 4.0 * pi * st_dot(q, x.t) .* x.t - vlp;
        case 'tangent'
            vlp = st_layer_tangent_gradu(x, y, q);

            du_ext = du_ext + vlp;
            du_int = du_int + vlp;
        case 'tangentfft'
            u = st_repr_density_velocity(x, y, q, bc);
            dudt = st_derivative(x, u, x.xi, 'fft');

            du_ext = dudt;
            du_int = dudt;
        end
    else
        switch repr
        case 'normal'
            J = [...
                4.0 * pi * diag(real(x.t) .* real(x.t)), ...
                4.0 * pi * diag(imag(x.t) .* real(x.t)); ...
                4.0 * pi * diag(real(x.t) .* imag(x.t)), ...
                4.0 * pi * diag(imag(x.t) .* imag(x.t))];

            V = st_layer_normal_gradu(x, y);
            du_ext = -J - V;
            du_int = +J - V;
        case 'tangent'
            V = st_layer_tangent_gradu(x, y);
            du_ext = V;
            du_int = V;
        case 'tangentfft'
            error('`tangentfft` is not supported.');
        end
    end

    if nargout == 1
        switch repr
        case 'normal'
            du_ext = struct('dudn_ext', du_ext, 'dudn_int', du_int);
        case {'tangent', 'tangentfft'}
            du_ext = struct('dudt_ext', du_ext, 'dudt_int', du_int);
        end
    end
end

% vim:foldmethod=marker:
