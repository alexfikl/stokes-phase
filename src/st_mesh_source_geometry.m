% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [s] = st_mesh_source_geometry(x, y, method)
    % Compute geometry at source points.
    %
    % The source geometry is computed at quadrature points. Since the
    % quadrature points are non-uniform, we cannot use the standard FFT
    % to also compute the source geometry spectrally, like the target
    % geometry in :func:`st_mesh_target_geometry`.
    %
    % For this reason, the source geometry is computed by interpolation
    % from the target geometry with the method given by *method*:
    %
    % * if the method is ``'fft'``, we expect the target points ``x`` to be
    %   obtained from :func:`st_mesh_target_geometry` so that they contain
    %   the Fourier modes.
    % * ``'lagrange'``, we need the Lagrange polynomials from
    %   :func:`st_point_collocation`.
    %
    % :arg x: target point information.
    % :arg y: source point coordinates on the computational grid shared with
    %   the target points.
    % :arg method: interpolation method to use.
    % :type method: default ``'fft'``.
    %
    % :returns: source geometry with the same fields as
    %   :func:`st_mesh_target_geometry`.

    if isstruct(y)
        xi = y.xi;
    else
        xi = y;
    end

    if ~exist('method', 'var') || isempty(method)
        method = 'fft';
    end
    method = lower(method);

    switch method
    case 'fft'
        s = st_mesh_source_geometry_fft(x, xi);
    case 'lagrange'
        s = st_mesh_source_geometry_lgr(x, xi);
    otherwise
        error('unknown method: %s', method);
    end

    s.xi = xi;
    if isfield(x, 'vertices')
        s.vertices = x.vertices;
    end

    if isstruct(y)
        s = st_struct_merge(y, s, true);
    end
end

function [s] = st_mesh_source_geometry_fft(x, xi)
    z = x.zhat * exp(-x.zk' * xi) / length(x.zk);
    dx = (x.zk .* x.zhat) * exp(-x.zk' * xi) / length(x.zk);
    ddx = (x.zk.^2 .* x.zhat) * exp(-x.zk' * xi) / length(x.zk);

    s = st_get_geometry(z, dx, ddx);
    s.x = z;
end

function [s] = st_mesh_source_geometry_lgr(x, xi)
    z = st_interpolate(x, x.x, xi);
    dx = st_interp_evaluate(x, x.x, xi, 1);
    ddx = st_interp_evaluate(x, x.x, xi, 2);

    s = st_get_geometry(z, dx, ddx);
    s.x = z;
end

function [g] = st_get_geometry(z, dx, ddx)
    % NOTE: this is just copied from `st_mesh_target_geometry`. keep in sync!

    % compute arc length
    s = cumsum([0, abs(diff(z))]);
    % compute metric term
    J = abs(dx);
    % compute normal
    normal = -1j * dx ./ J;
    % compute curvature in the (x, y) plane
    kappa_x = -st_dot(normal, ddx) ./ J.^2;
    % compute mean curvature
    kappa_p = kappa_x;
    kappa_p(2:end - 1) = imag(normal(2:end - 1)) ./ imag(z(2:end - 1));

    % construct struct
    g.n = normal;
    g.t = 1j * normal;
    g.kappa = kappa_x + kappa_p;
    g.kappa_x = kappa_x;
    g.kappa_p = kappa_p;
    g.s = s;
    g.J = J;
end

% vim:foldmethod=marker:
