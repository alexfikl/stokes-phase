% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [N] = st_ops_adjoint_normal(x, y, sigma)
    % Construct the adjoint operator of the linearized normal vector.
    %
    % Consider the shape derivative of the normal vector, as given in
    % Lemma 5.5 in [Walker2015]_, and its corresponding adjoint operator
    % defined by the relation
    %
    % .. math::
    %
    %     \int_\Sigma \mathbf{X}^* \cdot \mathbf{n}'[\mathbf{V}] \,\mathrm{d}S =
    %     \int_\Sigma n^*[\mathbf{X}^*] \mathbf{n} \cdot \mathbf{V} \,\mathrm{d}S.
    %
    % This function discretizes a parametrized version
    %
    % .. math::
    %
    %     g_{out} n^*[g_{in} X^*]
    %
    % of the operator. This allows building up the full operator for
    % multiple cases, including the one above, which is built in
    % :func:`st_ops_adjoint_vector_normal`.
    %
    % :arg x: target geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg y: source geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg sigma: coefficients evaluated at target points. This can be
    %   a struct containing the fields ``g_in``, ``g_out`` and ``dg_out``, all
    %   of which are optional and default to :math:`1`.
    %
    % :returns: the operator ``N`` as described above.

    if ~exist('sigma', 'var')
        sigma = struct();
    end

    if ~isstruct(sigma)
        sigma = struct('g_in', sigma);
    end

    % number of targets
    ntargets = length(x.x);
    % number of panels
    npanels = length(x.panels) - 1;
    % number of basis functions
    nbasis = length(x.nodes.basis);
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);

    % evaluate basis on each point of the reference element
    [phi, dphi] = st_interp_evaluate_basis(x, y.xi(1:nnodes));
    phi = phi'; dphi = dphi';
    % compute area element
    dS = imag(y.x) .* y.w;
    % get coefficients at source points
    sigma = st_get_coefficients(x, y, sigma);
    g1 = sigma.g_out .* sigma.g_in;
    g2 = sigma.dg_out .* sigma.g_in;

    % compute operator
    N = zeros(ntargets, ntargets);

    for n = 1:npanels
        % source (quadrature) points in the current panel
        k = ((n - 1) * nnodes + 1):(n * nnodes);

        % evaluate block
        for p = 1:nbasis
            i = (n - 1) * (nbasis - 1) + p;
            for q = 1:nbasis
                j = (n - 1) * (nbasis - 1) + q;

                N(i, j) = N(i, j) - ...
                    sum(g1(k) .* dphi(p, :) .* phi(q, :) .* dS(k)) - ...
                    sum(g2(k) .*  phi(p, :) .* phi(q, :) .* dS(k));
            end
        end
    end
end

function [r] = st_get_coefficients(x, y, sigma)
    % get variables
    r.g_out = st_struct_field(sigma, 'g_out', @() ones(size(y.x)));
    r.g_in = st_struct_field(sigma, 'g_in', @() ones(size(y.x)));
    r.dg_out = st_struct_field(sigma, 'dg_out', @() dg('g_out'));

    % interpolate as necessary
    for f = fieldnames(r)'
        g = r.(f{1});

        if isa(g, 'function_handle')
            g = g();
        end
        if length(g) ~= length(y.x)
            g = st_interpolate(x, g, y.xi);
        end

        r.(f{1}) = g;
    end

    r.dg_out = y.J .* r.dg_out;

    function [d] = dg(name)
        if isfield(sigma, name)
            d = st_derivative(x, sigma.(name), y.xi);
        else
            d = zeros(size(y.x));
        end
    end
end

% vim:foldmethod=marker:
