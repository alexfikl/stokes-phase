% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [N] = st_ops_adjoint_vector_normal(x, y, sigma)
    % Construct the adjoint normal vector operator.
    %
    % This function builds the default adjoint normal vector operator, as
    % described in :func:`st_ops_adjoint_normal`. The operator is related to
    % its scalar version implemented in :func:`st_ops_adjoint_normal` by
    %
    % .. math::
    %
    %     \mathbf{n}^*[\mathbf{X}^*] = n^*[\mathbf{X}^* \cdot \mathbf{s}] \mathbf{n}
    %
    % The returned operator is block :math:`2 \times 2`. To apply it to a
    % vector expressed as a complex array, we can do
    %
    % .. code:: matlab
    %
    %     w = st_array_stack(v);
    %     Nv = st_array_unstack(N * w);
    %
    % :arg x: target geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg y: source geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg sigma: coefficient multiplying :math:`\mathbf{X}^*`
    %
    % :returns: the operator ``N`` as described above.

    if ~exist('sigma', 'var')
        sigma = ones(size(x.x));
        sigma = sigma + 1.0j * sigma;
    end

    % compute blocks
    % xx
    sigma0 = struct('g_out',  real(x.n), ...
                    'dg_out', x.kappa_x .* real(x.t), ...
                    'g_in',   real(sigma));
    Nxx = st_ops_adjoint_normal(x, y, sigma0);
    % xy
    sigma0 = struct('g_out',  real(x.n), ...
                    'dg_out', x.kappa_x .* real(x.t), ...
                    'g_in',   imag(sigma));
    Nxy = st_ops_adjoint_normal(x, y, sigma0);
    % yx
    sigma0 = struct('g_out',  imag(x.n), ...
                    'dg_out', x.kappa_x .* imag(x.t), ...
                    'g_in',   real(sigma));
    Nyx = st_ops_adjoint_normal(x, y, sigma0);
    % yy
    sigma0 = struct('g_out',  imag(x.n), ...
                    'dg_out', x.kappa_x .* imag(x.t), ...
                    'g_in',   imag(sigma));
    Nyy = st_ops_adjoint_normal(x, y, sigma0);

    % construct final operator
    N = [Nxx, Nxy; Nyx, Nyy];
end

% vim:foldmethod=marker:
