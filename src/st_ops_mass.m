% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [M] = st_ops_mass(x, y, sigma)
    % Construct the mass operator for the given geometry.
    %
    % This function computes the mass operator
    %
    % .. math::
    %     M_{ij} = \int_\Gamma \sigma \phi_i \phi_j \,\rho \mathrm{d}s,
    %
    % where :math:`\phi_i` are the Lagrange polynomial basis functions.
    %
    % :arg x: target geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg y: source geometry struct, as returned by :func:`st_mesh_geometry`.
    % :arg sigma: additional weight function.
    %
    % :returns: the operator ``M`` as described above.

    if ~exist('sigma', 'var')
        sigma = ones(1, length(x.x));
    end
    sigma = reshape(sigma, size(x.x));

    % number of targets
    ntargets = length(x.x);
    % number of panels
    npanels = length(x.panels) - 1;
    % number of basis functions
    nbasis = length(x.nodes.basis);
    % number of quadrature nodes
    nnodes = length(y.quad.unit_xi);

    % evaluate basis on each point of the reference element
    [phi, ~] = st_interp_evaluate_basis(x, y.xi(1:nnodes));
    phi = phi';
    % compute area element
    sigma = st_interpolate(x, sigma, y.xi);
    dS = sigma .* imag(y.x) .* y.J .* y.w;

    % compute operator
    M = zeros(ntargets, ntargets);

    for n = 1:npanels
        % source (quadrature) points in the current panel
        k = ((n - 1) * nnodes + 1):(n * nnodes);

        % evaluate block
        for p = 1:nbasis
            i = (n - 1) * (nbasis - 1) + p;
            for q = 1:nbasis
                j = (n - 1) * (nbasis - 1) + q;

                M(i, j) = M(i, j) + sum(phi(q, :) .* phi(p, :) .* dS(k));
            end
        end
    end
end

% vim:foldmethod=marker:
