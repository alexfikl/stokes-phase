% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [cache] = st_cache_create(datadir, cache_id, prefix, cachedir, overwrite)
    % Simple structure for handling caches and general checkpointing.
    %
    % If *datadir* is not provided, the constructed directory has the form::
    %
    %   $PWD/{cachedir}/{prefix}_{cache_id}
    %
    % The returned struct has attached a few file generation routines:
    %
    % * ``filename = filename(cache, suffix, ext)`` can be used to generate
    %   filename in the cache directory with a canonical name.
    % * ``[next, prev] = filenext(cache, suffix, ext)`` is similar to
    %   ``filename``, but generates files with ``{suffix}_%05d`` in the
    %   cache directory and returns the next file that does not exist on disk.
    % * ``[cache, filename] = fileseries(cache, suffix, ext)`` is similar to
    %   ``filenext``, but generates index filenames without checking on
    %   disk. All the generated files can also be accessed in the
    %   ``file_series_cache`` field. Only one series can be active at the
    %   time and must have a matching suffix.
    %
    % :arg datadir: a full directory name for the cache; if empty, it gets
    %   created from the remaining data.
    % :type datadir: default ``[]``
    % :arg cache_id: a unique identifier for the cache directory; the special
    %   value ``'now'`` creates a timestamp for the identifier.
    % :type cache_id: default ``'now'``
    % :arg prefix: a non-unique prefix for the cache subfolder.
    % :type prefix: default ``'cache'``
    % :arg cachedir: name of the cache directory.
    % :type name: default ``'cache'``.
    %
    % :returns: a struct containing cache details.

    if ~exist('cache_id', 'var')
        cache_id = 'now';
    end

    if ~exist('prefix', 'var') || isempty(prefix)
        prefix = 'cache';
    end

    if strcmp(cache_id, 'now')
        cache_id = datestr(now, 'yyyy_mm_dd_HHMMSS');
    end

    if ~exist('cachedir', 'var') || isempty(cachedir)
        cachedir = 'cache';
    end

    if ~exist('overwrite', 'var') || isempty(overwrite)
        overwrite = false;
    end

    if ~exist('datadir', 'var') || isempty(datadir)
        if isempty(cache_id)
            datadir = fullfile(pwd, cachedir, prefix);
        else
            datadir = fullfile(pwd, cachedir, sprintf('%s_%s', cache_id, prefix));
        end
    end

    if ~isfolder(datadir)
        st_log('datadir: %s (miss)\n', datadir);
        mkdir(datadir);
    else
        st_log('datadir: %s (hit)\n', datadir);
    end

    cache.datadir = datadir;
    cache.prefix = prefix;
    cache.cache_id = cache_id;
    cache.overwrite = overwrite;

    cache.filesave = @fn_filesave;
    cache.filename = @fn_filename;
    cache.filenext = @fn_filenext;

    cache.fileseries = @fn_fileseries;
    cache.file_series_id = [];
    cache.file_series_cache = {};
end

function st_log(varargin)
    global verbosity;
    if verbosity
        fprintf(varargin{:});
    end
end

function fn_filesave(cache, filename, varargin)
    s = st_struct_varargin(varargin{:});

    if ~cache.overwrite && exist(filename, 'file') == 2
        error('file already exists: `%s`', filename);
    else
        % NOTE: v7.3 is basically HDF5 files, which makes it easier to read
        save(filename, '-struct', 's', '-v7.3');
    end
end

function [filename] = fn_filename(cache, suffix, ext)
    if ~exist('ext', 'var') || isempty(ext)
        ext = 'mat';
    end

    filename = fullfile(cache.datadir, ...
        sprintf('%s_%s.%s', cache.prefix, suffix, ext));
end

function [next, prev] = fn_filenext(cache, suffix, ext)
    if ~exist('ext', 'var') || isempty(ext)
        ext = 'mat';
    end

    index = 0;
    next = fn_filename(cache, sprintf('%s_%05d', suffix, index), ext);
    prev = [];

    while exist(next, 'file') == 2
        prev = next;
        next = fn_filename(cache, sprintf('%s_%05d', suffix, index), ext);

        index = index + 1;
    end
end

function [cache, filename] = fn_fileseries(cache, suffix, ext)
    if ~exist('ext', 'var') || isempty(ext)
        ext = 'mat';
    end

    series_id = sprintf("%s_%s", suffix, ext);
    if isempty(cache.file_series_id)
        cache.file_series_id = series_id;
    end

    if ~strcmp(cache.file_series_id, series_id)
        error('requested suffix is not part of the current series');
    end

    index = length(cache.file_series_cache);
    filename = cache.filename(cache, ...
        sprintf('%s_%05d', suffix, index), ext);
    cache.file_series_cache{end + 1} = filename;
end
