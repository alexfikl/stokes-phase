% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = st_array_reshape_like(x, y)
    % Match the column / row shape of an existing array.
    %
    % :arg x: array to reshape.
    % :arg y: source array.
    %
    % :returns: *x* reshaped to match *y*.

    if ~isvector(y) || ~isvector(x)
        error('arrays are not row/column vectors');
    end

    if isrow(y)
        shape = [1, length(x)];
    else
        shape = [length(x), 1];
    end

    r = reshape(x, shape);
end
