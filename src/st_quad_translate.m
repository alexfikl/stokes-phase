% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xi, w] = st_quad_translate(xi, w, a, b, y)
    % Translates from [-1, 1] to [a, b] linearly.
    %
    % :arg xi: quadrature nodes.
    % :arg w: quadrature weights.
    % :arg y: location of the singularity.
    %
    % :returns: translated ``(xi, w)``.

    if nargin == 4
        [xi, w] = st_translate(xi, w, a, b);
    else
        if abs(y - a) < 1.0e-14 || abs(y - b) < 1.0e-14
            [xi, w] = st_translate(xi, w, a, b);
        else
            [xi_l, w_l] = st_translate(xi, w, a, y);
            [xi_r, w_r] = st_translate(xi, w, y, b);

            xi = [xi_l, xi_r];
            w = [w_l, w_r];
        end
    end
end

function [xi, w] = st_translate(xi, w, a, b)
    xi = (b + a) / 2.0 + (b - a) / 2.0 * xi;
    w = (b - a) / 2.0 * w;
end
