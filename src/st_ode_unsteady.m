% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [x, y, state] = st_ode_unsteady(x0, y0, ...
        bc, forcing, cost, checkpoint, varargin)
    % Advance the unsteady kinematic condition in time.
    %
    % This is similar to :func:`st_ode_steady`, but with a very different
    % purpose. While :func:`st_ode_steady` is meant to drive the system to
    % steady state as efficiently as possible, this function is meant for
    % adjoint optimization.
    %
    % As such, it uses a simpler time stepper and computes the
    % full velocity field at each time step, instead of relying on a fixed
    % point-type iteration. Furthermore, it allows checkpointing and other
    % adjoint-focused features. However, it can still be used to drive the
    % system to steady state in the same way, but we expect it to be
    % slower (in computational time).
    %
    % The system we are solving is the kinematic condition
    %
    % .. math::
    %
    %     \frac{\partial \mathbf{X}}{\partial t} = \mathbf{f}(\mathbf{u}, \mathbf{X}).
    %
    % The right-hand side of the kinematic condition is described by
    % forcing terms conforming to :func:`st_forcing_options`.
    % If a `cost` struct is passed, with information about the cost functional,
    % we computed it in the following way
    %
    % .. math::
    %
    %     \begin{aligned}
    %     J =\,\, & j_\Sigma(T) + \int_0^T j_\Sigma(t) \,\mathrm{d}[t] \\
    %       \approx\,\, & j_\Sigma(T) + \sum_{n = 0}^N \Delta t^n j_\Sigma(t^n),
    %     \end{aligned}
    %
    % where :math:`j_\Sigma(t)` is computed by ``cost.evaluate``. This is
    % a first-order approximation of the time integral, which matches the
    % first-order forward Euler method we are using for the time integration.
    %
    % Providing the cost functional is optional, but if it is provided, the
    % following two fields are expected:
    %
    % * `evaluate`: a function handle taking ``(t, x, y, st)``,
    %   for evaluating the integrand :math:`j_\Sigma(t)` of the cost
    %   functional at a given time :math:`t`, where ``st`` is a struct
    %   containing the solution to the Stokes problem.
    % * `variables`: a cell array of variables necessary for the right-hand
    %   side and the cost functional, which is passed directly to
    %   :func:`st_repr_density_solution`.
    %
    % The additional ode are meant for the time stepper and the forcing
    % term in the kinematic condition. They are defined in the common
    % :func:`st_ode_options`.
    %
    % :arg x0: initial target geometry information.
    % :arg y0: source geometry information. This only requires information
    %   about the quadrature rules, as provided by :func:`st_point_quadrature`
    %   and :func:`st_layer_quadrature`. The actual geometry is updated
    %   from the target points during the evolution.
    % :arg fn: boundary conditions for the Stokes problem.
    % :arg cost: optional cost functional information.
    % :arg checkpoint: created using :func:`st_cache_create`.
    % :arg ode: time stepper information.
    %
    % :returns: a tuple ``(x, y, state)``, where `x` and `y` contain the
    %   geometry information at `tmax`. The `state` contains information
    %   about the time stepping, like all the checkpoints that were saved,
    %   the times at which they were saved, the value of the cost functional,
    %   etc.

    % {{{ ode

    % get cost variables
    cost_variables = {};
    if ~isempty(cost)
        cost_variables = cost.variables;
    end

    % check checkpointing
    enable_checkpointing = ~isempty(checkpoint);
    enable_iteration_checkpointing = false;
    if enable_checkpointing
        enable_iteration_checkpointing = isempty(checkpoint.file_series_id);
    end

    if ~enable_iteration_checkpointing
        warning('iterations are not checkpointed');
    end

    % parse all options
    ode = st_ode_options(varargin{:});
    ode.variables = unique([forcing.variables, cost_variables]);

    if ode.display >= 2
        disp(ode);
        disp(ode.param);
    end

    % }}}

    % {{{ initialize loop

    if ode.enable_equidistance
        [x0, y0] = st_mesh_equidistance(x0, y0, 'interp', ode.equidistance);
    end

    L = max(real(x0.x));
    B = max(imag(x0.x));

    % iteration
    t = 0.0;
    it = 0;
    dt = ode.timestep(ode, t, x0, y0, bc, []);

    % statistics for each iteration
    state.cost = 0.0;
    state.times = t;
    state.timesteps = [];
    state.checkpoints = {};

    state.geometry.deformation = (L - B) / (L + B);
    state.geometry.surface_area = st_ax_integral(y0, 1.0);
    state.geometry.centroid = st_mesh_centroid(x0, y0);
    state.geometry.volume = st_mesh_volume(x0, y0);

    state.timing.update = [];
    state.timing.geometry = [];
    state.timing.forward = [];
    state.timing.total = [];

    x = x0;
    y = y0;

    % put everything in a single struct for the right-hand side calculations
    problem.ode = ode;
    problem.cost = cost;
    problem.bc = bc;
    problem.forcing = forcing;
    problem.x = x0;
    problem.y = y0;

    if ode.display >= 1
        if isinf(ode.maxit)
            nitems = 1.25 * floor(ode.tmax / dt);
        else
            nitems = ode.maxit;
        end

        progress = st_util_progress('nitems', nitems);
    end

    % }}}

    % {{{ loop

    last_progress_line_size = -1;
    stages_next = struct();

    while t < ode.tmax && it < ode.maxit
        if ode.display >= 1
            if ode.display == 1 && last_progress_line_size > 0
                fprintf(repmat('\b', 1, last_progress_line_size));
            end

            progress = st_util_progress(progress, 'action', 'tic');
        end

        t_total = tic();
        state.timing.geometry(end + 1) = 0.0;
        state.timing.forward(end + 1) = 0.0;

        % {{{ update geometry

        stages = stages_next;

        t_update = tic();
        switch ode.stepper
        case {'euler', 'imex'}
            stages.k0 = x.x;
            [st, state] = update(t, stages.k0, problem, state);

            xnext = x.x + dt * st.forcing;
        case {'ssprk3', 'ssprk3a'}
            stages.k0 = x.x;
            [st, state] = update(t, stages.k0, problem, state);

            stages.k1 = stages.k0 + dt * st.forcing;
            [st1, state] = update(t, stages.k1, problem, state);

            stages.k2 = 3.0 / 4.0 * stages.k0 ...
                + 1.0 / 4.0 * (stages.k1 + dt * st1.forcing);
            [st1, state] = update(t, stages.k2, problem, state);

            xnext = 1.0 / 3.0 * stages.k0 ...
                + 2.0 / 3.0 * (stages.k2 + dt * st1.forcing);
        otherwise
            error('unknown method: %s', ode.stepper);
        end
        state.timing.update(end + 1) = toc(t_update);

        % }}}

        % {{{ update cost functional

        % TODO: would be nice to update this with rk3 too
        if ~isempty(cost)
            state.cost = state.cost + ...
                dt * cost.fn.evaluate(cost, t, x, y, st);
        end

        % }}}

        % {{{ post-process geometry

        tic();
        if ode.enable_equidistance && mod(it - 1, ode.equidistance.freq) == 0
            stages_next = struct('kp', xnext);
            [x, y] = st_mesh_geometry_update(x, y, xnext);
            [x, y] = st_mesh_equidistance(x, y, 'interp', ode.equidistance);
        else
            stages_next = struct();
            if ode.filter_strength > 0.0 && mod(it - 1, ode.filter_freq) == 0
                xnext = st_ax_filter(xnext, 'fft', ode.filter_strength);
            end
            [x, y] = st_mesh_geometry_update(x, y, xnext);
        end
        state.timing.geometry(end) = state.timing.geometry(end) + toc();

        % }}}

        % {{{ checkpoint

        % plot
        if mod(it - 1, ode.odefreq) == 0
            try
                if ~isempty(ode.odeplot)
                    ode.odeplot(t, x, y, st);
                end
            catch
                warning('Error occured during plotting. Disabling ...');
                ode.odeplot = [];
                ode.odefreq = Inf;
            end
        end

        % checkpoint
        if enable_iteration_checkpointing
            [checkpoint, filename] = checkpoint.fileseries(checkpoint, 'evolution');
            save_checkpoint(filename, st, stages, it, t);
        end

        % stdout
        L = max(real(x.x));
        B = max(imag(x.x));

        state.times(end + 1) = t + dt;
        state.timesteps(end + 1) = dt;

        state.geometry.deformation(end + 1) = (L - B) / (L + B);
        state.geometry.surface_area(end + 1) = st_ax_integral(y, 1.0);
        state.geometry.centroid(end + 1) = st_mesh_centroid(x, y);
        state.geometry.volume(end + 1) = st_mesh_volume(x, y);

        state.timing.total(end + 1) = toc(t_total);

        if ode.display >= 1
            progress = st_util_progress(progress, 'action', 'toc');

            if ode.display == 1
                if ~isinf(ode.maxit)
                    last_progress_line_size = fprintf('[%05d/%05d] %s', ...
                        it, ode.maxit, progress.strfmt(progress));
                else
                    last_progress_line_size = fprintf('[%05d] %s', ...
                        it, progress.strfmt(progress));
                end
            end
        end

        if ode.display >= 2
            display_timestep(ode, progress, state, st, x, y);
        end

        % }}}

        % {{{ update time step

        t = t + dt;
        it = it + 1;
        dt = ode.timestep(ode, t, x, y, st, state.timesteps);

        % }}}
    end

    % }}}

    % compute cost functional
    if ~isempty(cost)
        st = st_repr_density_solution(x, y, bc, ode.variables);
        state.cost = state.cost + cost.fn.evaluate(cost, t, x, y, st);
        if ode.display >= 1
            if ode.display == 1
                fprintf('\n');
            end
            fprintf('   cost:           %.6e\n', state.cost);
        end
    end

    % save final time
    state.maxit = it;
    state.tmax = t;
    state.x = x.x;
    state.lambda = bc.param.lambda;
    state.Ca = bc.param.Ca;

    if enable_checkpointing
        if enable_iteration_checkpointing
            if isempty(cost)
                st = st_repr_density_solution(x, y, bc, ode.variables);
            end

            % save last time step
            [checkpoint, filename] = checkpoint.fileseries(checkpoint, 'evolution');
            save_checkpoint(filename, st, struct('k0', x.x), it, t);

            % NOTE: this is maxit + 1 because it includes the first and last step
            state.checkpoints = checkpoint.file_series_cache;
            assert(length(state.checkpoints) == state.maxit + 1);
        end

        % save evolution
        filename = checkpoint.filename(checkpoint, 'state');
        save(filename, '-struct', 'state');
    end
end

function save_checkpoint(filename, st, stages, it, t)
    save(filename, 'st', 'stages', 'it', 't');
end

function [st, state] = update(t, xprev, problem, state)
    % unroll
    ode = problem.ode;
    bc = problem.bc;
    forcing = problem.forcing;

    % update geometry
    tic();
    [x, y] = st_mesh_geometry_update(problem.x, problem.y, xprev);
    state.timing.geometry(end) = state.timing.geometry(end) + toc();

    % compute forcing term
    tic();
    st = st_repr_density_solution(x, y, bc, ode.variables);
    st.forcing = forcing.fn.evaluate(forcing, t, x, y, st);
    state.timing.forward(end) = state.timing.forward(end) + toc();
end

function display_timestep(ode, progress, state, st, x, y)
    persistent volume_state;
    if isempty(volume_state)
        volume_state = {'shrinking', 'growing'};
    end

    it = length(state.times) - 1;
    dt = state.times(end) - state.times(end - 1);
    t = state.times(end - 1);

    if ~isinf(ode.maxit)
        fprintf('[%05d/%05d] dt = %.5e t = %.5e\n', ...
            it, ode.maxit, dt, t);
    else
        fprintf('[%05d] dt = %.5e t = %.5e\n', it, dt, t);
    end

    % relative volume change
    volume = state.geometry.volume;
    dv = (volume(end) - volume(1)) / volume(1);
    m = (volume(end) > volume(end - 1)) + 1;

    fprintf('   source:         f %.5e udotn %.5e\n', ...
        norm(st_dot(st.forcing, x.n), Inf), ...
        norm(st_dot(st.u, x.n), Inf));
    fprintf('   deformation:    squish %.6e surf %.6e xc %.5e, vol %.6e (%s)\n', ...
        state.geometry.deformation(end), ...
        state.geometry.surface_area(end), ...
        real(state.geometry.centroid(end)), ...
        dv, volume_state{m});
    % fprintf('   cost:           %.6e\n', state.cost);
    fprintf('   timing:         %s\n', progress.strfmt(progress));
    fprintf('                   upd %.2fs fwd %.2fs geo %.2fs\n', ...
        state.timing.update(end), ...
        state.timing.forward(end), ...
        state.timing.geometry(end))
end

% vim:foldmethod=marker:
