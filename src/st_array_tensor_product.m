% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [T] = st_array_tensor_product(x, y)
    % Computes the tensor product of two vectors.
    %
    % The resulting matrix is defined in such a way that
    %
    %   .. code:: matlab
    %
    %       T = st_array_tensor_product(x, y);
    %       st_array_unstack(T * st_array_stack(z)') == st_dot(z, y) .* x;
    %
    % :arg x: array (complex).
    % :arg y: array (complex).
    % :return: a dense matrix representing the tensor product of the two
    %   vectors.

    if length(x) ~= length(y)
        error('array lengths do not match');
    end

    n = length(x);
    x = st_array_to_row(x)';
    y = st_array_to_row(y)';

    T = [...
        spdiags(real(x) .* real(y), 0, n, n), ...
        spdiags(real(x) .* imag(y), 0, n, n); ...
        spdiags(imag(x) .* real(y), 0, n, n), ...
        spdiags(imag(x) .* imag(y), 0, n, n)];
end
