% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [eoc] = st_util_eoc(eoc, capacity, window, verbose)
    % Compute step by step estimated order of convergence (EOC).
    %
    % The returned struct will have the following information:
    %
    % * `index`: current in the error array, i.e. values after this are
    %   not initialized.
    % * `errors`: error arrays for each variable.
    % * `orders`: order slopes with a window of 1 for each variable.
    % * `h`: array containing grid sizes.
    % * `npoints`: array containing number of grid points (optional).
    %
    % Intended use is as follows:
    %
    % .. code:: matlab
    %
    %     npoints = [8, 16, 32, 64];
    %     eoc = st_util_eoc({'var1', 'var2'}, length(npoints));
    %
    %     for n = 1:length(npoints)
    %         % ... perform computations ...
    %
    %         % update errors with latest results
    %         eoc.errors.var1(n) = norm(var1 - var1_exact);
    %         eoc.errors.var2(n) = norm(var2 - var2_exact);
    %         eoc.h(n) = hmax;
    %
    %         % update and print eoc
    %         eoc = st_util_eoc(eoc);
    %     end
    %
    % :arg eoc: struct containing information about previous errors.
    % :arg capacity: excepted number of values, only used at initialization.
    % :arg window: window to compute eoc.
    % :arg verbose: if true, also print errors at each call.
    %
    % :return: updated struct.

    if ~exist('capacity', 'var') || isempty(capacity)
        capacity = 16;
    end

    if ~exist('window', 'var') || isempty(window)
        window = 1;
    end

    global verbosity;
    if ~exist('verbose', 'var') || isempty(verbose)
        verbose = ~isempty(verbosity) && verbosity;
    end

    if ~isstruct(eoc)
        variables = eoc;

        eoc = struct();
        eoc.variables = variables;
        eoc.window = window;
        eoc.index = 0;

        eoc.npoints = nan * zeros(1, capacity);
        eoc.h = zeros(1, capacity);

        for n = 1:length(eoc.variables)
            eoc.errors.(variables{n}) = zeros(1, capacity);
            eoc.orders.(variables{n}) = nan * zeros(1, capacity);
        end
    end

    n = eoc.index;
    if n > eoc.window
        for m = 1:length(eoc.variables)
            v_er = eoc.errors.(eoc.variables{m})(n - eoc.window:n);
            v_or = polyfit(log(eoc.h(n - eoc.window:n)), log(v_er), 1);

            eoc.orders.(eoc.variables{m})(n) = v_or(1);
        end
    end

    if verbose && n > 0
        if isnan(eoc.npoints(n))
            header = sprintf('Errors: %5.3e ', eoc.h(n));
        else
            header = sprintf('Errors: %5d ', eoc.npoints(n));
        end

        if n == 1
            fmt = sprintf('%%%ds', length(header));
            fprintf(fmt, ' ');
            for m = 1:length(eoc.variables)
                fprintf('%13s %7s', eoc.variables{m}, ' ');
            end
            fprintf('\n');
        end

        fprintf('%s', header);
        if n <= eoc.window
            for m = 1:length(eoc.variables)
                v_er = eoc.errors.(eoc.variables{m})(n);
                fprintf('%13.6e  ----- ', v_er);
            end
        else
            for m = 1:length(eoc.variables)
                v_er = eoc.errors.(eoc.variables{m})(n);
                v_or = eoc.orders.(eoc.variables{m})(n);
                fprintf('%13.6e %6.3f ', v_er, v_or);
            end
        end

        fprintf('\n');
    end

    eoc.index = eoc.index + 1;
end

% vim:foldmethod=marker:
