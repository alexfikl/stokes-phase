% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [st] = st_sym_solutions(name, opts)
    % Get symbolic Stokes solutions.
    %
    % All solutions are returned in Cartesian coordinates. Currently the
    % following solutions are known
    %
    % * ``'HadamardRybczynski'``: from Equation 3-7 in [Clift1978]_. Optional
    %   parameters: ``R`` sphere radius (default 1.0),
    %   ``lambda`` viscosity ratio (default 1.0), ``pinf`` farfield pressure
    %   (default 0.0), ``uinf`` farfield x velocity (default 1.0),
    %   ``alpha`` interface velocity (default
    %   :math:`1/4 \lambda / (1 + \lambda)` gives a steady state interface).
    % * ``'SolidSphere'``: same as the Hadamard-Rybczynski solution, but with
    %   and infinite interior viscosity. Optional parameters:
    %   ``R`` sphere radius (default 1.0), ``pinf`` farfield pressure
    %   (default 0.0), ``uinf`` farfield velocity field (default 1.0) and
    %   ``usurf`` surface tangential velocity (default 0.0).
    % * ``'SolidQuadratic'``: from Equation 18a in [Chwang1975]_. Optional
    %   parameters: ``R`` sphere radius (default 1.0) and
    %   ``pinf`` farfield pressure (default 0.0).
    %
    % The solution struct that is returned has the following fields
    %
    % * ``u_ext``, ``p_ext``, ``sigma_ext``, ``gradu_ext``: exterior
    %   velocity, pressure, stress tensor and velocity gradient.
    % * ``u_int``, ``p_int``, ``sigma_int``, ``gradu_int``: interior
    %   velocity, pressure, stress tensor and velocity gradient.
    % * ``x``: symbolic Cartesian coordinates.
    % * ``n``: symbolic normal vector, as a function of :math:`\mathbf{x}`.
    % * ``t``: symbolic tangent vector, as a function of :math:`\mathbf{x}`.
    % * ``kappa``: mean curvature.
    % * ``theta``: symbolic curve parametrization in the :math:`x-y` plane.
    % * `xs`: coordinate transformation from :math:`\mathbf{x}` to
    %   :math:`\theta` on the curve. Should be used as
    %   ``subs(exprs, st.x, st.xs)``.
    %
    % :arg name: name of the solution (case insensitive).
    % :arg opts: option struct for the respective solution.
    % :returns: a struct with the solution.

    % REFERENCES:
    %   [1] R. Clift, J. R. Grace, M. E. Weber, Bubbles, Drops and Particles,
    %   Academic Press, 1978.

    if ~exist('opts', 'var')
        opts = struct();
    end

    name = lower(name);
    switch name
    case 'hadamardrybczynski'
        st = st_hadamard_rybczynski(opts);
    case 'solidsphere'
        st = st_solid_sphere(opts);
    case 'solidquadratic'
        st = st_solid_quadratic(opts);
    case 'electric'
        st = st_electric_stokes(opts);
    otherwise
        error('unknown solution: %s', name);
    end

    % add additional helpful variables
    % NOTE: at the momemnt all solutions are on the sphere, so we know the
    % geometry and can compute the quantities below
    rx = st.x(1) / sqrt(st.x(1)^2 + st.x(2)^2);
    ry = st.x(2) / sqrt(st.x(1)^2 + st.x(2)^2);
    % normal
    st.n = [rx, ry];
    % tangent
    st.t = [-ry, rx];

    % surface equation
    st.theta = sym('theta', 'real');
    st.xs = [st.param.R * cos(st.theta), st.param.R * sin(st.theta)];

    % compute velocity gradients
    if isfield(st, 'u_ext')
        st.gradu_ext = st_cart_gradient(st.u_ext, st.x);
        st.gradu_int = st_cart_gradient(st.u_int, st.x);
        st.tau_ext = simplify(1 / 2 * (st.gradu_ext + st.gradu_ext'));
        st.tau_int = simplify(1 / 2 * (st.gradu_int + st.gradu_int'));
    end

    % compute stress gradients
    if isfield(st, 'sigma_ext')
        st.grads_ext = 0;
        st.grads_int = 0;
        for i = 1:2
            for j = 1:2
                for k = 1:2
                    nnn = st.n(i) * st.n(j) * st.n(k);

                    st.grads_ext = st.grads_ext + ...
                        diff(st.sigma_ext(i, j), st.x(k)) * nnn;
                    st.grads_int = st.grads_int + ...
                        diff(st.sigma_int(i, j), st.x(k)) * nnn;
                end
            end
        end
    end

    % compute electric field
    if isfield(st, 'phi_ext')
        st.e_ext = st_cart_gradient(st.phi_ext, st.x);
        st.e_int = st_cart_gradient(st.phi_int, st.x);
    end
end

% {{{ Stokes solutions

function [st] = st_hadamard_rybczynski(opts)
    syms r theta phi real;

    % get parameters
    R = st_struct_field(opts, 'R', 1.0);
    lambda = st_struct_field(opts, 'lambda', 1.0);
    pinf = st_struct_field(opts, 'pinf', 0.0);
    alpha = R^3 * st_struct_field(opts, 'alpha', ...
        0.25 * lambda / (1.0 + lambda));
    uinf = st_struct_field(opts, 'uinf', 1.0);

    % define exterior solution
    A1 = alpha;
    B1 = -alpha * (2.0 + 3.0 * lambda) / lambda / R^2;
    C1 = 0.5 * uinf;
    D1 = 0.0;
    st.psi_ext = (A1 / r + B1 * r + C1 * r^2 + D1 * r^4) * sin(theta)^2;

    st.p_ext = lambda * pinf + 2.0 * B1 / r^2 * cos(theta);
    st.u_ext = st_velocity_from_stokes_streamfunction(st.psi_ext, r, theta);
    st.sigma_ext = st_sph_stress(st.u_ext, st.p_ext, r, theta, phi);

    % define interior solution
    A2 = 0.0;
    B2 = 0.0;
    C2 = 0.5 - alpha * (3.0 + 2.0 * lambda) / lambda / R^3;
    D2 = alpha / lambda / R^5;
    st.psi_int = (A2 / r + B2 * r + C2 * r^2 + D2 * r^4) * sin(theta)^2;

    st.p_int = pinf + 20.0 * D2 * r * cos(theta);
    st.u_int = st_velocity_from_stokes_streamfunction(st.psi_int, r, theta);
    st.sigma_int = st_sph_stress(st.u_int, st.p_int, r, theta, phi);

    % transform to cartesian coordinates
    st = st_spherical_to_cartesian(st, r, theta, phi);
    % save parameters
    st.param.R = R;
    st.param.lambda = lambda;
    st.param.pinf = pinf;
    st.param.alpha = alpha;
    st.param.uinf = uinf;
end

function [st] = st_solid_sphere(opts)
    syms r theta phi real;

    % get parameters
    R = st_struct_field(opts, 'R', 1.0);
    pinf = st_struct_field(opts, 'pinf', 0.0);
    uinf = st_struct_field(opts, 'uinf', 1.0);
    usurf = st_struct_field(opts, 'usurf', 0.0);

    % define exterior solution
    A1 = 1.0 / 4.0 * R^3 * (1.0 + 2.0 * usurf);
    B1 = -1.0 / 4.0 * R * (3.0 + 2.0 * usurf);
    C1 = uinf / 2.0;
    D1 = 0.0;
    st.psi_ext = (A1 / r + B1 * r + C1 * r^2 + D1 * r^4) * sin(theta)^2;

    st.p_ext = pinf + 2.0 * B1 / r^2 * cos(theta);
    st.u_ext = st_velocity_from_stokes_streamfunction(st.psi_ext, r, theta);
    st.sigma_ext = st_sph_stress(st.u_ext, st.p_ext, r, theta, phi);

    % define interior solution
    A2 = 0.0;
    B2 = 0.0;
    C2 = 0.0;
    D2 = 0.0;
    st.psi_int = (A2 / r + B2 * r + C2 * r^2 + D2 * r^4) * sin(theta)^2;

    st.p_int = pinf + 20.0 * D2 * r * cos(theta);
    st.u_int = st_velocity_from_stokes_streamfunction(st.psi_int, r, theta);
    st.sigma_int = st_sph_stress(st.u_int, st.p_int, r, theta, phi);

    % transform to cartesian coordinates
    st = st_spherical_to_cartesian(st, r, theta, phi);
    % save parameters
    st.param.R = R;
    st.param.pinf = pinf;
    st.param.uinf = uinf;
    st.param.usurf = usurf;
end

function [st] = st_solid_quadratic(opts)
    syms x y z real;

    % save coordinates
    st.x = [x, y, z];
    rsqrt = sqrt(x^2 + y^2 + z^2);

    % get parameters
    R = st_struct_field(opts, 'R', 1.0);
    pinf = st_struct_field(opts, 'pinf', 0.0);

    % define exterior solution
    e0 = [1, 0, 0];
    T0 = e0 / rsqrt + x * st.x / rsqrt^3;
    T1 = gradient(diff(1 / rsqrt, x), st.x)';

    st.u_ext = (y^2 + z^2) * e0 - ...
        R^3 / 2.0 * (T0 - 7.0 * R^2 / 12.0 * diff(T0, x, 2)) + ...
        R^5 / 12.0 * (5.0 * T1 - R^2 / 2.0 * diff(T1, x, 2));

    st.p_ext = pinf + simplify(int(laplacian(st.u_ext(1), st.x), st.x(1)));
    st.sigma_ext = st_cart_stress(st.u_ext, st.p_ext, st.x);

    % define interior solution
    st.u_int = sym(zeros(1, 2));
    st.p_int = sym(0);
    st.sigma_int = sym(zeros(2, 2));

    % only keep (x, y) components
    st.x = st.x(1:2);
    st.u_ext = simplify(subs(st.u_ext(1:2), z, 0));
    st.p_ext = simplify(subs(st.p_ext, z, 0));
    st.sigma_ext = simplify(subs(st.sigma_ext(1:2, 1:2), z, 0));

    % save parameters
    st.param.R = R;
    st.param.pinf = pinf;
end

% }}}

% {{{ electric solutions

function [st] = st_electric_constant(opts)
    syms r theta phi real;

    % get parameters
    R = st_struct_field(opts, 'R', 1.0);
    S = st_struct_field(opts, 'S', 1.0);
    einf = st_struct_field(opts, 'einf', 1.0);

    % define exterior problem
    Ap = einf;
    Bp = (1 - S) / (2 + S) * R^3;
    st.phi_ext = (Ap * r + Bp / r^2) * cos(theta);

    % define interior problem
    Am = 3 / (2 + S);
    Bm = 0;
    st.phi_int = (Am * r + Bm / r^2) * cos(theta);

    % transform to cartesian coordinates
    st = st_spherical_to_cartesian(st, r, theta, phi);
    % save parameters
    st.param.R = R;
    st.param.S = S;
    st.param.einf = einf;
end

function [st] = st_electric_stokes(opts)
    st = st_electric_constant(opts);
    st = st_struct_merge(st, st_hadamard_rybczynski(opts), true);
end

% }}}

% {{{ derivatives: cartesian

function [g] = st_cart_gradient(f, x)
    g = sym(zeros(length(f), length(x)));

    for i = 1:length(f)
        for j = 1:length(x)
            g(i, j) = simplify(diff(f(i), x(j)));
        end
    end
end

function [sigma] = st_cart_stress(u, p, x)
    du = st_cart_gradient(u, x);
    sigma = -p * eye(length(x)) + (du + du');
end

% }}}

% {{{ derivatives: spherical
%

function [g] = st_sph_gradient(f, r, theta, phi)
    if length(f) == 1
        g = [diff(f, r), diff(f, theta) / r, diff(r, phi) / (r * sin(theta))];
        return;
    end

    g = sym(zeros(length(f), length(f)));
    g(1, 1) = diff(f(1), r);
    g(1, 2) = diff(f(1), theta) / r - f(2) / r;
    g(2, 1) = diff(f(2), r);
    g(2, 2) = diff(f(2), theta) / r + f(1) / r;

    if length(f) == 3
        g(1, 3) = diff(f(1), phi) / (r * sin(theta)) -f(3) / r;
        g(2, 3) = diff(f(2), phi) / (r * sin(theta)) - cot(theta) * f(3) / r;
        g(3, 1) = diff(f(3), r);
        g(3, 2) = diff(f(3), theta) / r;
        g(3, 3) = diff(f(3), phi) / (r * sin(theta)) + (f(1) + cot(theta) * f(2)) / r;
    end

    g = simplify(g);
end

function d = st_sph_divergence(u, r, theta, phi) %#ok
    d = 1 / r^2 * diff(r^2 * u(1), r) + ...
        1 / (r * sin(theta)) * diff(sin(theta) * u(2), theta);
end

function [u] = st_velocity_from_stokes_streamfunction(psi, r, theta)
    % https://en.wikipedia.org/wiki/Stokes_stream_function#Spherical_coordinates
    u = [1.0 / (r^2 * sin(theta)) * diff(psi, theta); ...
        -1.0 / (r * sin(theta)) * diff(psi, r)];
end

function [sigma] = st_sph_stress(u, p, r, theta, phi)
    du = st_sph_gradient(u, r, theta, phi);
    sigma = -p * eye(2) + (du + du');
end

function [st0] = st_spherical_to_cartesian(st, r, theta, ~)
    syms x y real;

    % save coordinates
    st0.x = [x, y];

    % define transformation
    fs = [r, theta];
    fc = [sqrt(x^2 + y^2), atan2(y, x)];

    % define rotation matrix
    J = [cos(theta), -sin(theta);
         sin(theta), cos(theta)];

    % transform exterior variables
    fields = fieldnames(st);
    for i = 1:numel(fields)
        switch numel(st.(fields{i}))
        case 1
            st0.(fields{i}) = simplify(subs(st.(fields{i}), fs, fc));
        case 2
            st0.(fields{i}) = simplify(subs(J * st.(fields{i}), fs, fc));
        case 4
            st0.(fields{i}) = simplify(subs(J * st.(fields{i}) * J', fs, fc));
        otherwise
            error('dimension not supported: %s', fields{i});
        end
    end

    % NOTE: for some reason, the symbolic engine replaces cos(atan2(y, x))
    % with x / abs(x + y * 1i), which should be equivalent. However, when
    % differentiating it seems to forget where that came from and gives
    % incorrect results with imaginary parts..
    fields = fieldnames(st0);
    for i = 1:numel(fields)
        if isempty(symvar(st0.(fields{i})))
            continue;
        end

        st0.(fields{i}) = subs(st0.(fields{i}), abs(x + y * 1i), sqrt(x^2 + y^2));
    end
end

% }}}

% vim:foldmethod=marker:
