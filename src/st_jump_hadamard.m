% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [jump] = st_jump_hadamard(varargin)
    % Defines the traction jump in the Hadamard-Rybczynski solution.
    %
    % This is basically a mix of :func:`st_jump_capillary` and
    % :func:`st_jump_angle`. See :func:`st_sym_solutions` for more details.
    % The parameters can include a ``enable_capillary`` and ``enable_angle``
    % flag to turn each part on and off.
    %
    % :returns: a struct compatible with :func:`st_jump_options`.

    d.user.enable_capillary = true;
    d.user.enable_angle = true;
    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    jumps = {};
    if d.user.enable_capillary
        jump = st_jump_capillary(varargin{:});
        jumps = [jumps, {'capillary', jump}];
    end

    if d.user.enable_angle
        jump = st_jump_angle(varargin{:});
        jumps = [jumps, {'angle', jump}];
    end

    jump = st_gradient_combine(jumps{:});
end
