% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [forcing] = st_forcing_normal_velocity(varargin)
    % Defines a forcing condition based on the normal velocity.
    %
    % It is given by :func:`st_ode_steady_forcing`. One important node here
    % regards the gradients that are also computed: they only consider the
    % normal velocity component of the forcing condition, even if the
    % tangential and curvature terms are active.
    %
    % :returns: a struct compatible with :func:`st_forcing_options`.

    % default options
    d.user.tangential_forcing = false;
    d.user.curvature_forcing = false;
    d.user.mobility = 1.0e-4;
    d = st_struct_merge(d, st_struct_varargin(varargin{:}), true);

    forcing = st_forcing_options(d, ...
        'variables', {'two_phase', 'u'});

    forcing.fn.evaluate = @forcing_normal_velocity_evaluate;
    forcing.fn.gradient_x = @forcing_normal_velocity_gradient_x;
    forcing.fn.gradient_u = @forcing_normal_velocity_gradient_u;
    forcing.fn.gradient_ca = @forcing_normal_velocity_gradient_ca;

    forcing.fn.operator_x = @forcing_normal_velocity_operator_x;
end

% {{{ gradient function handles

function [r] = forcing_normal_velocity_evaluate(forcing, t, x, y, st)
    if isfield(st, 'forward')
        st = st.forward;
    end

    r = st_ode_steady_forcing(forcing, t, x, y, st);
end

function [r] = forcing_normal_velocity_gradient_x(forcing, t, x, y, st)
    F = forcing.fn.operator_x(forcing, t, x, y, st);
    r = st_array_unstack(F * st_array_stack(st.adjoint.x));
end

function [r] = forcing_normal_velocity_gradient_u(~, ~, x, ~, st)
    r = st_dot(st.adjoint.x, x.n) .* x.n;
end

function [r] = forcing_normal_velocity_gradient_ca(~, ~, ~, ~, ~)
    r = 0.0;
end

% }}}

% {{{ operators

function [F] = forcing_normal_velocity_operator_x(~, ~, x, y, st)
    sf = st.forward;

    % coefficients
    dudn = 0.5 * st_dot(sf.dudn_ext + sf.dudn_int, x.n);
    u_dot_t = st_dot(sf.u, x.t);
    u_dot_n = st_dot(sf.u, x.n);
    sigma = u_dot_t .* x.n + u_dot_n .* x.t;

    % operators
    N = st_ops_adjoint_vector_normal(x, y, sigma);
    M = st_ops_vector_mass_product(x, y, x.n, dudn .* x.n);

    F = M + N;
end

% }}}
