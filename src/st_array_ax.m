% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [r] = st_array_ax(v, cclosed)
    % Complete an axisymmetric array.
    %
    % We represent an axisymmetric scalar or vector field only by its components
    % in the top half plane. This function returns an array in the whole
    % :math:`x-y` plane by flipping the components accordingly. Mostly
    % equivalent to
    %
    % .. code:: matlab
    %
    %     w = [v, fliplr(conj(v:2:end - 1))];
    %
    % :arg v: array representing an axisymmetric quantity.
    % :arg cclosed: if true, the first and last element will be the same.
    % :return: complete vector field.

    if ~exist('cclosed', 'var') || isempty(cclosed)
        cclosed = false;
    end
    i = 1 + (cclosed == false);

    r = st_array_to_row(v);
    r = st_array_reshape_like([r, fliplr(conj(r(i:end - 1)))], v);
end
