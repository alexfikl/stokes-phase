% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [y] = st_ops_vector_solve(x, M, A, b, boundary_type)
    % Solve a generalized system of equations.
    %
    % This problem solves for :math:`y` in
    %
    % .. math::
    %
    %     M y = A b
    %
    % where :math:`M` is a block diagonal mass matrix, with each block being
    % the scalar mass matrix obtained from :func:`st_ops_mass`. The vectors
    % :math:`y` and :math:`b` are passed as complex arrays, which can be
    % stacked appropriately to give matching sizes in the above equation.
    %
    % :arg x: target point information.
    % :arg M: scalar mass matrix.
    % :arg A: vector operator.
    % :arg b: right-hand side vector (complex array).
    % :arg boundary_type: type of the boundary condition, see :func:`st_ops_boundary`.
    %
    % :return: a complex array that is the solution to the above system.

    % number of points
    npoints = length(x.x);
    % keep shape
    b_size = size(b);

    % compute right-hand side
    b = st_array_reshape_like(st_array_stack(b), [1; 1]);
    b = reshape(st_array_unstack(A * b), npoints, 1);

    % solve system
    yr = st_ops_solve(x, M, real(b), boundary_type{1});
    yi = st_ops_solve(x, M, imag(b), boundary_type{2});
    y = reshape(yr + 1j * yi, b_size);
end

% vim:foldmethod=marker:
