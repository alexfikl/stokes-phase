% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xi, w] = st_quad_carley(nnodes, y, order, npoly, ...
                                  a, b, ls_solver, nodes_type)
    % Quadrature nodes and weights for singular functions.
    %
    % This function generates nodes and weights to integrate:
    %
    % .. math::
    %
    %     \int_{-1}^{1} f(x) \,\mathrm{d}x \approx \sum f(x_i) w_i,
    %
    % where :math:`f` is assumed to be of the type from Equation 2.5
    % in [Carley2007]_:
    %
    % .. math::
    %
    %     f = A(x) + B(x) log|x - y| + C(x) \frac{1}{|x - y|} +
    %         D(x) \frac{1}{|x - y|^2}.
    %
    % The analytic results required for the integration can be found in
    % [Brandao1987]_. The order of the singularity can be prescribed in
    % several ways:
    %
    % * using an integer in :math:`\{\pm 1, \pm 2, \pm 3\}`, denoting, in
    %   order, a logarithmic singularity, a Cauchy Principal Value or a
    %   Hadamard Finite Part. If the order has a positive sign, we construct
    %   a least squares system with all the singularities up to that order.
    %   Otherwise, if it is negative, we just construct a quadrature rule
    %   for that specific singularity type.
    % * using string identifiers ``'log'``, ``'cpv'`` or ``'hfp'``, which
    %   correspond to :math:`1, 2` or :math:`3`.
    %
    % Note that the weights are obtained in a least square sense, so they
    % will likely not be exact for any degree polynomial. However, according
    % to [Carley2007]_, we can expect errors of about ``1.0e-12`` to be
    % attainable.
    %
    % :arg nnodes: number of quadrature nodes.
    % :arg y: location of the singularity in :math:`[a, b]`.
    % :type y: default: :math:`a + b / 2`
    % :arg order: maximum order of the singularity.
    % :type order: default `1`
    % :arg npoly: order of the polynomials :math:`A` or :math:`B`.
    % :type npoly: default ``0.5 nnodes``
    % :arg a: domain interval.
    % :type a: default `-1`
    % :arg b: domain interval.
    % :type b: default `1`
    % :arg ls_solver: solver used for the least squares system.
    % :type ls_solver: default ``'qr'``.
    % :arg nodes_type: underlying regular quadrature nodes.
    % :type nodes_type: default ``'leggauss'``.
    %
    % :returns: a tuple ``(x, w)`` of quadrature nodes and weights.

    % REFERENCES:
    %   [1] M. Carley, Numerical Quadratures for Singular and hfpersingular
    %   Integrals in Boundary Element ls_solvers, SIAM Journal on Scientific
    %   Computing, 2007.
    %   [2] https://github.com/mjcarley/gqr/blob/master/src/kolrokh.c
    %   [3] M. P. Brandao, Improper Integrals in Theoretical Aerodynamics,
    %   AIAA Journal, 1987.

    if ~exist('order', 'var') || isempty(order)
        order = 1;
    end

    if ~exist('npoly', 'var') || isempty(npoly)
        npoly = floor(nnodes / 2);
    end

    if ~exist('a', 'var') || isempty(a)
        a = -1.0;
    end

    if ~exist('b', 'var') || isempty(b)
        b = 1.0;
    end

    if ~exist('y', 'var') || isempty(y)
        y = (a + b) / 2.0;
    end

    if ~exist('ls_solver', 'var') || isempty(ls_solver)
        ls_solver = 'qr';
    end
    ls_solver = lower(ls_solver);

    if ~exist('nodes_type', 'var') || isempty(nodes_type)
        nodes_type = 'leggauss';
    end
    nodes_type = lower(nodes_type);

    % clean input
    if a > b
        error('a > b: %g > %g', a, b);
    end

    if y < a || y > b
        error('singularity not in domain: %g not in [%g, %g].', y, a, b);
    end

    if ischar(order) || isstring(order)
        order = lower(order);
        switch order
        case 'log'
            order = 1;
        case 'cpv'
            order = 2;
        case 'hfp'
            order = 3;
        otherwise
            erorr('unknown singularity order: %s', order);
        end
    end

    % generate quadrature nodes
    switch nodes_type
    case 'leggauss'
        fn = @(n) st_quad_leggauss(n);
    case 'fejer'
        fn = @(n) st_quad_fejer(n);
    otherwise
        error('unknown underlying quadrature nodes: %s', nodes_type);
    end
    xi = st_quadrature_nodes(fn, y, a, b, nnodes, true);

    % compute right-hand side M [1, Equation 2.9]
    % compute matrix psi [1, Equation 2.8]
    lg = st_legendre_polynomial(fn, xi, y, a, b, npoly);
    M = st_legendre_integral(a, b, lg);
    P = lg.Px;

    if order >= 1 || order == -1
        M = [M; st_log_integral(y, a, b, lg)];
        P = [P; st_log_matrix(xi, y, lg)];
    end
    if order >= 2 || order == -2
        M = [M; st_cpv_integral(y, a, b, lg)];
        P = [P; st_cpv_matrix(xi, y, lg)];
    end
    if order >= 3 || order == -3
        M = [M; st_hfp_integral(y, a, b, lg)];
        P = [P; st_hfp_matrix(xi, y, lg)];
    end

    % solve system to get the weights
    %   P w = M
    switch ls_solver
    case 'qr'
        linopts.RECT = true;
        [w, ~] = linsolve(P, M, linopts);
    case 'lsqr'
        [w, ~] = lsqr(P, M, 1.0e-15, 5000 * length(M));
    case 'lsqnonneg'
        opts = optimset('TolX', 1.0e-14, 'MaxIter', 1000 * length(M));
        w = lsqnonneg(P, M, opts);
    otherwise
        error('unknown solver: %s', ls_solver);
    end
    w = w';

    if nargout == 1
        xi = struct('xi', xi, 'w', w);
    end
end

% Legendre Polynomials {{{

function [xi] = st_quadrature_nodes(quad, y, a, b, nnodes, no_split)
    translate = @(z, z0, z1) st_quad_translate(z, [], z0, z1);

    if no_split || abs(y - a) < 1.0e-14 || abs(y - b) < 1.0e-14
        [xi, ~] = quad(nnodes);
        xi = translate(xi, a, b);
    else
        [xi, ~] = quad(floor(nnodes / 2));
        xi = [translate(xi, a, y), translate(xi, y, b)];
    end
end

function [lg] = st_legendre_polynomial(quad, x, y, a, b, npoly)
    % evaluate Legendre polynomials of the first kind
    Px = zeros(npoly, length(x));
    Py = zeros(npoly, 1);
    Pa = zeros(npoly + 1, 1);
    Pb = zeros(npoly + 1, 1);

    Px(1, :) = ones(1, length(x));
    Px(2, :) = x;
    Py(1) = 1.0;    Pa(1) = 1.0;    Pb(1) = 1.0;
    Py(2) = y;      Pa(2) = a;      Pb(2) = b;
    for n = 2:size(Py, 1) - 1
        Px(n + 1, :) = ((2 * n - 1) * x .* Px(n, :) - (n - 1) * Px(n - 1, :)) / n;
        Py(n + 1) = ((2 * n - 1) * y .* Py(n) - (n - 1) * Py(n - 1)) / n;
        Pa(n + 1) = ((2 * n - 1) * a .* Pa(n) - (n - 1) * Pa(n - 1)) / n;
        Pb(n + 1) = ((2 * n - 1) * b .* Pb(n) - (n - 1) * Pb(n - 1)) / n;
    end

    % last two elements used in st_legendre_integral
    n = npoly;
    Pa(n + 1) = ((2 * n - 1) * a .* Pa(n) - (n - 1) * Pa(n - 1)) / n;
    Pb(n + 1) = ((2 * n - 1) * b .* Pb(n) - (n - 1) * Pb(n - 1)) / n;

    % evaluate the first derivative of the Legendre polynomials
    dPy = zeros(npoly, 1);
    dPy(1) = 0.0;
    if abs(abs(y) - 1.0) < 1.0e-15
        for n = 2:npoly
            dPy(n) = y * y^(n - 1) * (n - 1) * n / 2.0;
        end
    else
        for n = 2:npoly
            dPy(n) = (n - 1) * (y * Py(n) - Py(n - 1)) / (y^2 - 1.0);
        end
    end

    lg.npoly = npoly;
    lg.Px = Px;
    lg.Py = Py;
    lg.Pa = Pa;
    lg.Pb = Pb;
    lg.dPy = dPy;
    lg.quad = quad;
end

function [M] = st_legendre_integral(a, b, lg)
    M = zeros(lg.npoly, 1);
    M(1) = b - a;

    for n = 2:size(M, 1)
        M(n) = ((lg.Pb(n + 1) - lg.Pa(n + 1)) - ...
                (lg.Pb(n - 1) - lg.Pa(n - 1))) / (2 * (n - 1) + 1);
    end
end

% }}}

% {{{ (Weak) Logarithmic

function [M] = st_log_integral(y, a, b, lg)
    % compute monomial integrals
    Ilog = @(t, m) (-1.0)^(m + 1) / (m + 1)^2 * (y - t)^(m + 1) * ...
        ((m + 1) * log(abs(y - t) + 1.0e-15) - 1.0);

    I = zeros(lg.npoly, 1);
    for n = 0:lg.npoly - 1
        I(n + 1) = Ilog(b, n) - Ilog(a, n);
        for k = 1:n
            I(n + 1) = I(n + 1) - (-1.0)^k * y^k * nchoosek(n, k) * I(n - k + 1);
        end
    end

    % compute Legendre integrals
    M = zeros(lg.npoly, 1);
    for n = 0:lg.npoly - 1
        % k == 0
        n_ = uint64(n);
        kf = double(factorial(2 * n_) / factorial(n_)^2);

        m = floor(n / 2);
        for k = 0:m
            M(n + 1) = M(n + 1) + kf * I(n - 2 * k + 1);

            kf = -kf * (n - k) * (n - 2 * k) * (n - 2 * k - 1) / ...
                       ((k + 1) * (2 * n - 2 * k) * (2 * n - 2 * k - 1));
        end

        M(n + 1) = M(n + 1) / 2.0^n;
    end
end

function [P] = st_log_matrix(x, y, lg)
    P = zeros(lg.npoly, length(x));
    for n = 1:size(P, 1)
        P(n, :) = lg.Px(n, :) .* log(abs(x - y));
    end
end

% }}}

% {{{ (Strong) Cauchy Principal Value 1/r

function [M] = st_cpv_integral(y, a, b, lg)
    % quadrature rules
    [z, w] = lg.quad(lg.npoly);
    [z, w] = st_quad_translate(z, w, a, b, y);

    % use [1, Equation 2.3e]
    pv = st_principal_value(y, a, b);
    M = lg.Py * pv;

    for m = 1:length(z)
        Px0 = 0.0; Px = 1.0;
        for n = 1:size(M, 1)
            M(n) = M(n) + w(m) * (Px - lg.Py(n)) / (z(m) - y);

            Px1 = ((2 * n - 1) * z(m) .* Px - (n - 1) * Px0) / n;
            Px0 = Px; Px = Px1;
        end
    end
end

function [P] = st_cpv_matrix(x, y, lg)
    P = zeros(lg.npoly, length(x));
    for n = 1:size(P, 1)
        P(n, :) = lg.Px(n, :) ./ (x - y);
    end
end

% }}}

% {{{ (Hypersingular) Hadamard Finite Part 1/r^2

function [M] = st_hfp_integral(y, a, b, lg)
    % quadrature nodes
    [z, w] = lg.quad(2 * lg.npoly);
    [z, w] = st_quad_translate(z, w, a, b);

    % use [1, Equation 2.3f]
    pv = st_principal_value(y, a, b);
    fp = st_finite_part(y, a, b);
    M = lg.Py * fp + lg.dPy * pv;

    for m = 1:length(z)
        Px0 = 0.0; Px = 1.0;
        for n = 1:size(M, 1)
            M(n) = M(n) + ...
                w(m) * (Px - lg.Py(n) - lg.dPy(n) * (z(m) - y)) / (z(m) - y)^2;

            Px1 = ((2 * n - 1) * z(m) .* Px - (n - 1) * Px0) / n;
            Px0 = Px; Px = Px1;
        end
    end
end

function [P] = st_hfp_matrix(x, y, lg)
    P = zeros(lg.npoly, length(x));
    for n = 1:size(P, 1)
        P(n, :) = lg.Px(n, :) ./ (x - y).^2;
    end
end

% }}}

function [pv] = st_principal_value(y, a, b)
    % [3, Equation 9b, 6a]
    if abs(y - b) < 1.0e-15       % y == b
        pv = -log(b - a);
    elseif abs(y - a) < 1.0e-15   % y == a
        pv = +log(b - a);
    else
        pv = log(b - y) - log(y - a);
    end
end

function [fp] = st_finite_part(y, a, b)
    % [3, Equation 9a, 6]
    if abs(y - b) < 1.0e-15       % y == b
        fp = -1.0 / (b - a);
    elseif abs(y - a) < 1.0e-15   % y == a
        fp = +1.0 / (a - b);
    else
        fp = +1.0 / (y - b) - 1.0 / (y - a);
    end
end

% vim:foldmethod=marker:
