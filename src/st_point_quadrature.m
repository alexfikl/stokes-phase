% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [sources] = st_point_quadrature(npanels, nnodes, fn, method, a, b)
    % Generate quadrature nodes and weights.
    %
    % The struct that is returned contains the following fields
    %
    % * ``quad.unit_xi``: array of size, ``(nnodes,)`` of quadrature
    %   nodes on a reference :math:`[-1, 1]` element.
    % * ``quad.unit_w``: array of size ``(nnodes,)`` of quadrature
    %   weights on a reference :math:`[-1, 1]` element.
    % * ``nnodes``.
    % * ``npanels``.
    % * ``xi``: array of size ``(1, npanels * nnodes)`` of
    %   quarature nodes on a uniform :math:`[0, 0.5]` parametrization.
    % * ``w``: array of size ``(1, npanels * nnodes)`` of
    %   quadrature weights on a uniform :math:`[0, 0.5]` parametrization.
    % * ``x``: complex array of size ``(1, npanels * nnodes)``
    %   describing the curve in 2D. (only if `fn` is provided)
    % * ``vertices``: complex array of size ``(1, npanels + 1)`` containing
    %   the panel endpoints on the 2D curve. (only if `fn` is provided)
    %
    % :arg npanels: number of panels.
    % :arg nnodes: number of quadrature nodes per panel.
    % :arg fn: ``@(xi)`` function handle defining the geometry that takes
    %   the parametrization :math:`\xi \in [0, 0.5]` and returns the
    %   :math:`x` coordinates of each point as a complex number.
    % :arg method: quadrature method that is used. The two regular quadrature
    %   rules supported are ``'leggauss'`` for :func:`st_quad_leggauss` and
    %   ``'fejer'`` for :func:`st_quad_fejer`.
    % :arg a: *xi* interval start.
    % :type a: default ``0.0``
    % :arg b: *xi* interval end.
    % :type b: default ``0.5``
    %
    % :returns: a struct of describing the surface quadrature.

    if ~exist('method', 'var') || isempty(method)
        method = 'leggauss';
    end
    method = lower(method);

    if ~exist('a', 'var') || isempty(a)
        a = 0.0;
    end

    if ~exist('b', 'var') || isempty(b)
        b = 0.5;
    end

    % generate Gauss-Legendre quadrature points
    switch method
    case 'leggauss'
        [z, w] = st_quad_leggauss(nnodes);
    case 'fejer'
        [z, w] = st_quad_fejer(nnodes);
    otherwise
        error('unknown quadrature method: %s', method);
    end
    % generate panels
    panels = linspace(a, b, npanels + 1);
    % panel centers
    xim = 0.5 * (panels(1:end - 1) + panels(2:end));
    % panel sizes
    dxi = diff(panels);

    % tile weights
    wi = 0.5 * dxi(1) * repmat(w, 1, npanels);

    % translate nodes to their respective panels
    xi = repmat(z', 1, npanels);
    xi = reshape(xim + 0.5 * dxi .* xi, 1, []);

    sources.npanels = npanels;
    sources.nnodes = nnodes;
    sources.quad.unit_xi = z;
    sources.quad.unit_w = w;
    sources.panels = panels;
    sources.xi = xi;
    sources.w = wi;

    % generate geometry
    if exist('fn', 'var')
        if ischar(fn)
            sources.x = st_mesh_curve(sources.xi, fn);
            sources.vertices = st_mesh_curve(sources.panels, fn);
        elseif isa(fn, 'function_handle')
            sources.x = fn(sources.xi);
            sources.vertices = fn(sources.panels);
        elseif isa(fn, 'float') && length(fn) == length(xi)
            sources.x = fn;
            xv = interp1(sources.xi, sources.x, panels(2:end - 1));
            sources.vertices = [real(xv(1)), xv, real(xv(end))];
        elseif isempty(fn)
            return;
        else
            error('unknown shape variable type.');
        end
    end
end

% vim:foldmethod=marker:
