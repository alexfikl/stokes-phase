% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [grads_ext, grads_int] = st_repr_density_grads(x, y, q, bc)
    % Compute the normal stress gradient.
    %
    % The stress normal gradient is computed by using the layer potential
    % representation from :func:`st_layer_normal_grads` with the appropriate surface
    % limits. The normal stress gradient is given by
    %
    % .. math::
    %     \nabla \sigma \cdot (\mathbf{n} \otimes \mathbf{n} \otimes \mathbf{n}).
    %         =
    %     \frac{\partial \sigma_{ij}}{\partial x_k} n_i n_j n_k.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: an array ``ds`` containing the on-surface normal jump in the
    %   normal stress gradient.

    grads_ext = bc.fn.dsinf(x);
    grads_int = grads_ext;
    if norm(q, Inf) < 1.0e-10
        return;
    end

    % NOTE: the jump for the stress is given by:
    %       \mp d/ds (q dot t)
    % which expands to:
    %       \pm (kappa * (q dot n) - dqds dot t)

    % compute tangential derivative of the density
    dqds = st_derivative(x, q, x.xi, 'fft');
    dqdt = x.kappa .* st_dot(q, x.n) - 2.0 * st_dot(dqds, x.t);
    % compute layer potential
    grads = st_layer_normal_grads(x, y, q);

    grads_ext = grads_ext - 4.0 * pi * dqdt - grads;
    grads_int = grads_int + 4.0 * pi * dqdt - grads;

    if nargout == 1
        grads_ext = struct('grads_ext', grads_ext, 'grads_int', grads_int);
    end
end

% vim:ts=4:sw=4:sts=4:
