% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [G] = st_knl_ax_electric_slp(~, x, ~, I10)
    % Single-layer potential kernel for Laplace equation.
    %
    % The kernel is not normalized by :math:`4 \pi` to match the
    % Stokes kernels, which are also not normalized.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg I: elliptic integrals, see :func:`st_ellint_order3`.
    %
    % :returns: an array of size ``(1, 1)`` containing the kernel.
    % :see also: :func:`st_ellipke`.

    % build Green's function
    G = -imag(x) * I10;
end
