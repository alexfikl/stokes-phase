% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [b, db] = st_interp_evaluate_basis(x, xi)
    % Evaluate basis functions at a given set of points.
    %
    % :arg x: target geometry information.
    % :arg xi: points at which to evaluate the basis functions.
    %
    % :returns: two arrays of size ``(length(xi), nbasis)`` of evaluations.
    %   The first one is the basis functions and the second one is the
    %   first derivative of the basis functions.

    if isstruct(xi)
        xi = xi.xi;
    end

    % number of source points
    nsources = length(xi);
    % number of basis functions
    nbasis = length(x.nodes.basis);

    % find panel index for each source point
    j = discretize(xi, x.panels);

    % translate coordinates to panel local
    xim = 0.5 * (x.panels(j + 1) + x.panels(j));
    dxi = 0.5 * (x.panels(j + 1) - x.panels(j));
    eta = (xi - xim) ./ dxi;

    % evaluate basis at each point
    b = zeros(nsources, nbasis);
    for p = 1:nbasis
        b(:, p) = double(x.nodes.basis{p}(eta));
    end

    if nargout == 2
        db = zeros(nsources, nbasis);
        for p = 1:nbasis
            db(:, p) = double(x.nodes.dbasis{p}(eta));
        end
    end
end

% vim:foldmethod=marker:
