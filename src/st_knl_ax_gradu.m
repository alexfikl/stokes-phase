% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [U] = st_knl_ax_gradu(U, x, x0, I30, I31, I50, I51, I52, I53)
    % Axisymmetric free space velocity gradient kernel.
    %
    % The layer potentials corresponding to this kernel are computed in
    % :func:`st_layer_normal_gradu` and :func:`st_layer_tangent_gradu`.
    % Given in Equation 3.8 in [Gonzalez2009]_.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg I: elliptic integrals, see :func:`st_ellint_order5`.
    %
    % :returns: an array of size ``(2, 2, 2)`` containing the kernel components.
    % :see also: :func:`st_ellipke`.

    X  = real(x);  Y  = imag(x);
    X0 = real(x0); Y0 = imag(x0);
    Y2 = Y * Y;     Y02 = Y0 * Y0;
    Y3 = Y2 * Y;    Y03 = Y02 * Y0;
    XX0 = (X - X0)^2;
    YX = Y * (X - X0);

    % kernel
    U(1, 1, 1) = 3.0 * YX * XX0 * I50 - ...
                       YX * I30;
    U(1, 1, 2) = 3.0 * YX * (X - X0) * (Y * I51 - Y0 * I50) + ...
                       Y * (Y * I31 - Y0 * I30);
    U(1, 2, 1) = 3.0 * YX * (X - X0) * (Y * I50 - Y0 * I51) - ...
                       Y * (Y * I30 - Y0 * I31);
    U(1, 2, 2) = 3.0 * YX * ((Y2 + Y02) * I51 - Y * Y0 * (I50 + I52)) - ...
                       YX * I31;

    U(2, 1, 1) = 3.0 * Y * XX0 * (Y * I51 - Y0 * I50) - ...
                       Y * (Y * I31 - Y0 * I30);
    U(2, 1, 2) = 3.0 * Y * (X - X0) * (Y2 * I52 + Y02 * I50 - 2.0 * Y * Y0 * I51) - ...
                       YX * I30;
    U(2, 2, 1) = 3.0 * Y * (X - X0) * ((Y2 + Y02) * I51 - Y * Y0 * (I50 + I52)) + ...
                       YX * I31;
    U(2, 2, 2) = 3.0 * Y * (Y3 * I52 - Y2 * Y0 * (I53 + 2.0 * I51) + ...
                       Y * Y02 * (I50 + 2.0 * I52) - Y03 * I51) - ...
                       Y * (Y * I30 - Y0 * I31);
end

% vim:foldmethod=marker:
