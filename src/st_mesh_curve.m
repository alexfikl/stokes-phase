% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [x, xi] = st_mesh_curve(xi, name, varargin)
    % Construct a 2D curve.
    %
    % All curves returned by this function are axisymmetric, i.e. the full
    % closed curve can be reconstructed using :func:`st_array_ax`.
    %
    % The curves are
    %
    % * ``'circle'``: with radius ``R``.
    % * ``'ellipse'``: with axes ``a`` and ``b``.
    % * ``'starfish'``: with radius ``R`` an number of arms ``narms``.
    % * ``'flower'``: with radius ``R``.
    % * ``'ufo'``: with radius ``R`` and parameters ``k, a, b``.
    % * ``'spheroid'``: with radius ``R`` and perturbation ``epsilon``.
    %
    % :arg xi: parametrization in :math:`[0, 0.5]` or number of points.
    % :arg name: name of the curve.
    % :arg varargin: option key-value pairs for each curve.
    %
    % :returns: complex array representing the :math:`(x, \rho)` coordinates of
    %   the curve.

    if length(xi) == 1
        xi = linspace(0.0, 0.5, xi);
    end
    opts = st_struct_varargin(varargin{:});

    name = lower(name);
    switch name
    case 'circle'
        R = st_struct_field(opts, 'R', 1);

        x = R * exp(2.0j * pi * xi);
    case 'ellipse'
        a = st_struct_field(opts, 'a', 1);
        b = st_struct_field(opts, 'b', 2);

        r = a * b ./ sqrt((a * cos(2.0 * pi * xi)).^2 + (b * sin(2.0 * pi * xi)).^2);
        x = r .* exp(2.0j * pi * xi);
    case 'starfish'
        R = st_struct_field(opts, 'R', 1);
        narms = st_struct_field(opts, 'narms', 6);

        r = R * (1.0 + 20 / 81 * cos(2.0 * pi * narms * xi));
        x = r .* exp(2.0j * pi * xi);
    case 'flower'
        R = st_struct_field(opts, 'R', 1);

        x = R * exp(2j * pi * xi) .* (1.0 + 6 / 10 * cos(12 * pi * xi)) .* ...
                                     (1.0 + 4 / 10 * cos(2 * pi * xi));
    case 'ufo'
        R = st_struct_field(opts, 'R', 1);
        k = st_struct_field(opts, 'k', 4);
        a = st_struct_field(opts, 'a', 3);
        b = st_struct_field(opts, 'b', 1);

        r = sqrt((a * cos(2 * pi * xi)).^2 + (b * sin(2 * pi * xi)).^2) + ...
            cos(2.0 * pi * k * xi).^2;
        x = R * r .* exp(2.0j * pi * xi);
    case 'spheroid'
        R = st_struct_field(opts, 'R', 1);
        epsilon = st_struct_field(opts, 'epsilon', 0.2);

        r = R * (1 + epsilon * legendreP(2, cos(2.0 * pi * xi)));
        x = r .* exp(2.0j * pi * xi);
    otherwise
        error('unknown curve name: \"%s\"', name);
    end
end

% vim:foldmethod=marker:
