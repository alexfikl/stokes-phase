% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

 function [r] = st_array_to_row(x)
    % Ensures a given array is a row vector.
    %
    % :arg x: array (row or column).
    % :returns: the same array as a row vector.

    r = x;
    if ~isrow(r)
        r = r';
    end
 end
