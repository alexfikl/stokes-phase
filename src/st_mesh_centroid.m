% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [xc] = st_mesh_centroid(x, y)
    % Compute the centroid of a closed curve.
    %
    % The centroid is usually given in terms of the volume formula
    %
    % .. math::
    %
    %     \mathbf{x}_c = \frac{1}{|\Omega|}
    %         \int_\Omega \mathbf{x} \,\mathrm{d}V.
    %
    % This can be rewritten as a surface integral using the divergence theorem
    % to obtain
    %
    % .. math::
    %
    %     \mathbf{x}_c = \frac{1}{|V|} \int_\Sigma
    %         \frac{1}{2} \|\mathbf{x}\|^2 \mathbf{n} \,\mathrm{d} S,
    %
    % where the volume is obtained by :func:`st_mesh_volume`. Note that the
    % centroid components in the :math:`(y, z)` directions are always zero
    % by symmetry.
    %
    % :arg x: target point information.
    % :arg y: source point information.
    % :returns: the centroid of the 3D axisymmetric shape.

    volume = st_mesh_volume(x, y);
    xc = st_ax_integral(y, 1.0 / 2.0 * st_dot(y.x, y.x) .* real(y.n)) / volume;
end
