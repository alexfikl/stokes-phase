% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [I50, I51, I52, I53] = st_ellint_order5(x, x0, F, E)
    % Compute elliptic integrals in the Stresslet.
    %
    % :arg x: source point.
    % :arg x0: target point.
    % :arg F: elliptic integral of the first kind, see :func:`st_ellipke`.
    % :arg E: elliptic integral of the second kind.
    %
    % :returns: elliptic integrals described in Section 2.4 of
    %   [Pozrikidis1992]_ used to compute the Stresslet. Namely, it computes
    %   :math:`I_{50}, I_{51}, I_{52}` and :math:`I_{53}`.

    % REFERENCES:
    %   [1] C. Pozrikidis, Boundary Integral and Singularity Methods
    %   for Linearized Viscous Flow, 1999.
    %   [2] C. Pozrikidis, The Instability of a Moving Viscous Drop,
    %   Journal of Fluid Mechanics, 1990.

    R = 1.0 / ((real(x) - real(x0))^2 + (imag(x) + imag(x0))^2);

    % compute K
    % see [1, Equation 2.4.8]
    K2 = 4.0 * imag(x) * imag(x0) * R;
    K4 = K2 * K2;
    K6 = K2 * K4;
    K8 = K2 * K6;
    % NOTE: can cause division by zero lower down
    K21 = 1.0 - K2;
    % compute coefficient for I_{mn}
    % see [1, Equation 2.4.7]
    C5 = 4.0 * R^2 * sqrt(R);

    I10 = F;
    I30 = E / K21;

    I50 = C5 * (2.0 * (2.0 - K2) * I30 - I10) / (3.0 * K21);
    I51 = C5 * (2.0 * (1.0 - K2 + K4) * I30 - ...
                (2.0 - K2) * I10) / (3.0 * K2 * K21);
    I52 = C5 * (2.0 * (6.0 * K2 - K6 - 4.0) * I30 - ...
                (K4 + 8.0 * K2 - 8.0) * I10) / (3.0 * K4 * K21);
    I53 = C5 * (2.0 * (K8 + K6 - 33.0 * K4 + 64.0 * K2 - 32.0) * I30 - ...
                (96.0 * K2 - 30.0 * K4 - K6 - 64.0) * I10) / (3.0 * K6 * K21);
end

% vim:foldmethod=marker:
