% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [grad, st] = st_shape_adjoint_gradient(x, y, cost, bc, grads)
    % Compute the shape gradient of a given cost functional.
    %
    % The constrained optimization problem we are looking at is
    %
    % .. math::
    %
    %     \begin{cases}
    %     \min \mathcal{J}(\mathbf{X}), \\
    %     \text{subject to the Stokes equations.}
    %     \end{cases}
    %
    % This function computes the shape gradient for such a setup. However,
    % most of the information is given by the user, while we just compute all
    % the necessary forward and adjoint variables. The user must provide the
    % cost functional in `cost` and all the boundary conditions and problem
    % parameters in `bc` (directly passed to :func:`st_repr_density_solution`).
    %
    % :arg x: target geometry struct.
    % :arg y: source geometry struct.
    % :arg cost: cost functional information. This is mainly used to construct
    %   all the necessary solutions.
    % :arg bc: boundary conditions and parameters.
    % :arg grads: a sum of all the gradients around gathered using
    %   :func:`st_gradient_combine` or equivalent. If
    %
    % :return: the adjoint-based shape gradient of the cost functional.

    if ~exist('grads', 'var') || isempty(grads)
        grads = {'cost', cost};
        if isfield(bc, 'jump')
            grads = [grads, {'jump', bc.jump}];
        end

        grads = st_gradient_combine(grads{:});
    end

    % compute solutions
    st.forward = st_repr_density_solution(x, y, bc, ...
        cost.forward_variables);
    st.adjoint = st_repr_density_adjoint_solution(x, y, ...
        grads, st, cost.adjoint_variables);

    % compute gradient
    if any(contains(cost.variables, 'exterior'))
        [grad, st.terms] = st_gradient_exterior(x, y, grads, st);
    else
        [grad, st.terms] = st_gradient_two_phase(x, y, grads, st);
    end
end

function [grad, terms] = st_gradient_exterior(x, y, grads, st)
    M = st_ops_mass(x, y);

    % 1. gradient of the problem data
    terms.T1 = grads.fn.gradient_x(grads, 0.0, x, y, st);

    % 2. gradient of the boundary condition ``u = g``
    terms.T2 = M * st_dot(st.forward.dudn_ext, st.adjoint.fn_ext)';

    % 3. put them all together
    grad = terms.T1 + terms.T2;
    grad = st_ops_solve(x, M, grad, 'neumann');
end

function [grad, terms] = st_gradient_two_phase(x, y, grads, st)
    terms = struct();
    M = st_ops_mass(x, y);

    % 1. gradient term from the problem data
    T1 = grads.fn.gradient_x(grads, 0.0, x, y, st);

    % 2. gradient terms from the Stokes equations
    if has_stokes_terms(grads, 0.0, x, y, st)
        [T2, sterms] = st_shape_adjoint_gradient_stokes(x, y, st);
        T2 = (M * T2.').';
        terms = st_struct_merge(terms, sterms);
    else
        T2 = 0.0;
    end

    % 3. get gradient
    grad = st_ops_solve(x, M, T1 + T2, 'neumann');
    terms.T1 = st_ops_solve(x, M, T1, 'neumann');
end

function [r] = has_stokes_terms(grads, t, x, y, st)
    if ~isfield(grads.members, 'jump')
        r = false;
        return;
    end

    jump = grads.members.jump;
    if isempty(jump)
        r = false;
        return;
    end

    lambda = jump.param.lambda;
    if abs(lambda - 1.0) > 1.0e-14
        r = true;
        return;
    end

    j = jump.fn.evaluate(jump, t, x, y, st);
    j_dot_t = norm(st_dot(j, x.t), Inf);

    r = j_dot_t > 1.0e-10;
end

% vim:foldmethod=marker:
