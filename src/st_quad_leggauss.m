% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [x, w] = st_quad_leggauss(nnodes)
    % Gauss-Legendre quadrature nodes and weights.
    %
    % :arg nnodes: number of quadrature nodes.
    %
    % :returns: a tuple ``(x, w)`` of quadrature nodes and weights.

    % default interval
    a = -1.0;
    b =  1.0;

    % compute points
    x = zeros(1, nnodes);
    w = zeros(1, nnodes);
    m = (nnodes + 1.0) / 2.0;
    h = b - a;

    for i = 1:m
        % newton iteration to find nodes
        z = cos(pi * (i - 0.25) / (nnodes + 0.5));
        z1 = z + 1.0;
        while abs(z - z1) > 1.0e-15
            p1 = 1.0;
            p2 = 0.0;

            for j = 1:nnodes
                p3 = p2;
                p2 = p1;
                p1 = ((2.0 * j - 1.0) * z * p2 - (j - 1.0) * p3) / j;
            end

            pp = nnodes * (z * p1 - p2) / (z^2 - 1.0);
            z1 = z;
            z = z1 - p1/pp;
        end

        % build up results
        x(i) = -z;
        x(nnodes + 1 - i) = z;

        w(i) = h / ((1.0 - z^2) * (pp^2));
        w(nnodes + 1 - i) = w(i);
    end
end

% vim:foldmethod=marker:
