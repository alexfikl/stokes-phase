% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function st_util_checkcode(directory)
    if isfolder(directory)
        files = dir(directory);

        mfiles = cell(1, length(files));
        k = 1;
        for m = 1:length(mfiles)
            if ~endsWith(files(m).name, '.m')
                continue;
            end

            mfiles{k} = fullfile(files(m).folder, files(m).name);
            k = k + 1;
        end
    else
        mfiles = {directory};
    end

    checkcode(mfiles(1:k - 1), '-id');
end

% vim:foldmethod=marker:
