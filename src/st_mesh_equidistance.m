% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [x, y] = st_mesh_equidistance(x, y, method, varargin)
    % Equidistance a set of points.
    %
    % This function computes a new set of the same number of points that is
    % equidistant along the curve, with respect to arc length.
    %
    % The supported methods are:
    %
    % * ``'interp'``: computes the arclength and then interpolated to a
    %   grid with uniform arclength.
    % * ``'fft'``: uses the MATLAB2020a ``nfft`` function to do the
    %   arclength interpolation.
    % * ``'optim'``: minimizes a cost functional to get the point distribution.
    %
    % :arg x: target point information.
    % :arg y: source point information.
    % :arg method: method used for equidistancing.
    % :arg varargin: additional options.
    %
    % :returns: equidistant point coordinates.

    if ~exist('method', 'var') || isempty(method)
        method = 'interp';
    end
    method = lower(method);

    switch method
    case 'interp'
        [x, y] = st_mesh_equidistance_interp(x, y, varargin{:});
    case 'fft'
        [x, y] = st_mesh_equidistance_fft(x, y, varargin{:});
    case 'optim'
        [x, y] = st_mesh_equidistance_optim(x, y, varargin{:});
    otherwise
        error('unknown equidistancing method: %s', method);
    end
end

% {{{ interp

function [x, y] = st_mesh_equidistance_interp(x, y, varargin)
    % {{{ options

    d.atol = 1.0e-6;
    d.maxit = 4;
    d.pfilter = -1.0;
    % NOTE: this choice seemed to give a smooth curvature, so we're using it
    d.interp = 'spline';

    d = st_struct_update(d, st_struct_varargin(varargin{:}));

    % }}}

    % {{{ interp

    npanels = length(x.panels) - 1;
    it = 0;
    ds = [0, 1];

    while norm(diff(ds), Inf) > d.atol && it < d.maxit
        % compute panel arclengths
        ds = st_mesh_arclength(x, y);
        s = st_arclength_from_panels(x, [0, cumsum(ds)]);

        % interpolate to new uniform points
        s_uniform = linspace(0, s(end), length(x.x));
        % s_panels = linspace(0.0, s(end), npanels + 1);
        % s_uniform = st_arclength_from_panels(x, s_panels);
        xnew = interp1(s, x.x, s_uniform, d.interp);

        % recompute geometry
        [x, y] = st_mesh_geometry_update(x, y, xnew);
        it = it + 1;
    end

    % }}}

    if d.pfilter > 0
        x.x = st_ax_filter(x.x, 'fft', d.pfilter);
        [x, y] = st_mesh_geometry(x, y);
    end
end

function [ss] = st_arclength_from_panels(x, s)
    ds = diff(s);
    s_mid = 0.5 * (s(1:end - 1) + s(2:end));

    xi = repmat(x.nodes.unit_xi(1:end - 1)', 1, length(s_mid));
    ss = [reshape(s_mid + 0.5 * ds .* xi, 1, []), s(end)];
end

% }}}

% {{{ fft

function [x, y] = st_mesh_equidistance_fft(x, y, varargin)
    % compute panel arclength
    ds = st_mesh_arclength(x, y);
    s = [0, cumsum(ds)];

    % make variables periodic
    s_ = [s, s(end) + s(2:end)];
    z_ = st_array_ax(x.vertices, true);

    % compute fft
    % zk = st_fftfreq(2 * (length(s) - 1), 2.0*pi / s_(end));
    zk = (0:(length(s_) - 1)) / length(s_);
    zhat = nufft(z_, s_, zk);

    s_uniform = linspace(0.0, s(end), length(x.x));
    z0 = zhat * exp((1.0j * zk)' * s_uniform) / length(zhat);

    [x, y] = st_mesh_geometry_update(x, y, z0);
end

% }}}

% {{{ optim

function [xeq] = st_mesh_equidistance_optim(x, y, varargin)
    % {{{ options

    d.atol = 1.0e-4;
    d.maxit = 8;
    d.pfilter = 1.0e-4;
    d = st_struct_update(d, st_struct_varargin(varargin{:}));

    % }}}

    % {{{ optimization

    cg.x0 = x.xi(2:end - 1);
    cg.fn = @(xi) st_mesh_grad(xi, x, y);
    cg.gtol = d.atol;
    cg.maxitier = d.maxit;
    cg.display = false;

    xi = st_optim_cg(cg);

    % }}}

    xi = [x.xi(1), xi, x.xi(end)];
    xeq = x.zhat * exp(-x.zk' * xi) / length(x.zk);

    if d.pfilter > 0
        xeq = st_ax_filter(xeq, 'fft', d.pfilter);
    end
    [x, y] = st_mesh_geometry_update(x, y, xeq);
end

function [f, df] = st_mesh_grad(xi, x_, y_)
    % add endpoints
    xi = [x_.xi(1), xi, x_.xi(end)];
    % number of panels
    npanels = length(xi) - 1;

    % compute length of each panel
    [s, J] = st_mesh_arclength_optim(xi, x_, y_);
    % compute total length
    L = sum(s);

    f = 0.5 * norm(s - L / npanels)^2;
    if nargout == 2
        df = -(s(2:end) - s(1:end - 1)) .* J(2:end - 1);
    end
end

function [s, Jx] = st_mesh_arclength_optim(xi, x_, y_)
    % number of panels
    npanels = length(xi) - 1;
    % number of quadrature nodes per panel
    nnodes = length(y_.quad.unit_xi);

    % compute panel mindpoint and sizes
    xim = 0.5 * (xi(1:end - 1) + xi(2:end));
    dxi = 0.5 * diff(xi);
    % weights and nodes
    w = y_.quad.unit_w;
    zi = repmat(y_.quad.unit_xi', 1, npanels);
    zi = reshape(xim + dxi .* zi, 1, []);

    % compute geometry at panel endpoints
    Jx = abs(x_.zk .* x_.zhat * exp(-x_.zk' * xi) / length(x_.zk));
    % compute geometry at quadrature points
    Jy = abs(x_.zk .* x_.zhat * exp(-x_.zk' * zi) / length(x_.zk));

    s = zeros(1, npanels);
    for j = 1:npanels
        js = (j - 1) * nnodes + 1;
        je = j * nnodes;

        s(j) = sum(Jy(js:je) .* dxi(j) .* w);
    end
end

% }}}

% vim:foldmethod=marker:
