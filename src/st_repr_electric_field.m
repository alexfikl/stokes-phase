% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [e_ext, e_int] = st_repr_electric_field(x, y, q, bc)
    % Compute surface electric field.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density at target points.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: electric potential at target points.

    einf = bc.fn.einf(x);
    e_ext = st_dot(einf, x.n);
    e_int = st_dot(einf, x.n);
    if norm(q, Inf) < 1.0e-14
        return;
    end

    dlp = st_layer_electric_dlp(x, y, q, 'target');
    e_ext = e_ext - 2.0 * pi * q + dlp;
    e_int = e_int + 2.0 * pi * q + dlp;
end
