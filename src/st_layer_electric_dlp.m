% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [D] = st_layer_electric_dlp(x, y, q, repr)
    % Evaluate axisymmetric Laplace double-layer potential.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density.
    %
    % :returns: single-layer potential.

    if ~exist('repr', 'var') || isempty(repr)
        repr = 'target';
    end
    repr = lower(repr);

    if ~any(contains({'source', 'target'}, repr))
        error('unknown representation: %s', repr);
    end

    if isfield(y.quad, 'phi')
        knl.quad = y.quad.phi;
    else
        knl.quad = y.quad.reg;
    end

    knl.D_s = zeros(1, 2);
    knl.P_s = zeros(1, 2);
    knl.V_c = zeros(1, x.nbasis);

    % compute layer potential
    if exist('q', 'var') && ~isempty(q)
        knl.D = 2.0 * pi * q;
        switch repr
        case 'source'
            knl.evaluate = @st_layer_electric_source_dlp_apply;
        case 'target'
            knl.evaluate = @st_layer_electric_target_dlp_apply;
        end

        knl = st_layer_evaluate(x, y, knl, q);
        D = knl.D;
    else
        knl.D = 2.0 * pi * eye(length(x.x));
        switch repr
        case 'source'
            error('NotImplementedError');
        case 'target'
            knl.evaluate = @st_layer_electric_target_dlp_matrix;
        end

        knl = st_layer_evaluate(x, y, knl);
        D = knl.D;
    end
end

function [knl] = st_layer_electric_target_dlp_apply(knl, target, source)
    % target index
    i = target.i;
    % sources and targets
    y = source.x; ny = source.n;
    x = target.x; nx = target.n;

    D = knl.D_s;
    P = knl.P_s;
    ellint = @st_ellint_order3;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % integrate kernel
    S = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute kernel components
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint(y(n), x(i), F, E);
        D = st_knl_ax_electric_dlp_target(D, y(n), x(i), I30, I31);
        P = st_knl_ax_electric_dlp_source(P, y(n), x(i), I30, I31);

        % sum up
        S = S + (real(nx(i)) * D(1) + imag(nx(i)) * D(2)) * source.q(n) * dS - ...
                (real(ny(n)) * P(1) + imag(ny(n)) * P(2)) * target.q(i) * dS;
    end

    knl.D(i) = knl.D(i) + S;
end

function [knl] = st_layer_electric_target_dlp_matrix(knl, target, source)
    % number of basis functions
    nbasis = target.jend - target.jstart + 1;
    % basis functions
    basis = source.basis;
    % target index
    i = target.i;
    % sources and targets
    y = source.x;       ny = source.n;
    x = target.x;       nx = target.n;

    D = knl.D_s;
    P = knl.P_s;
    ellint = @st_ellint_order3;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % compute operators
    D0 = knl.V_c; D0(:) = 0.0;
    D1 = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute kernel components
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint(y(n), x(i), F, E);
        D = st_knl_ax_electric_dlp_target(D, y(n), x(i), I30, I31);
        P = st_knl_ax_electric_dlp_source(P, y(n), x(i), I30, I31);

        % add interactions
        for k = 1:nbasis
            bnk = dS * basis(n, k);

            D0(k) = D0(k) + ...
                (real(nx(i)) * D(1) + imag(nx(i)) * D(2)) * bnk;
        end

        D1 = D1 + (real(ny(n)) * P(1) + imag(ny(n)) * P(2)) * dS;
    end

    % add source interactions in panel
    for j = target.jstart:target.jend
        k = j - target.jstart + 1;

        knl.D(i, j) = knl.D(i, j) + D0(k);
    end

    % add target interactions in panel
    knl.D(i, i) = knl.D(i, i) - D1;
end

function [knl] = st_layer_electric_source_dlp_apply(knl, target, source)
    % target index
    i = target.i;
    % sources and targets
    y = source.x;
    x = target.x;
    ny = source.n;

    P = knl.P_s;
    ellint = @st_ellint_order3;
    if abs(imag(target.x(i))) < 1.0e-8
        ellint = @st_ellint_order3_axis;
    end

    % integrate kernel
    S = 0.0;
    for n = source.jstart:source.jend
        dS = source.J(n) * source.w(n);

        % compute kernel components
        [F, E] = st_ellipke(y(n), x(i));
        [~, ~, I30, I31, ~] = ellint(y(n), x(i), F, E);
        P = st_knl_ax_electric_dlp_source(P, y(n), x(i), I30, I31);

        % sum up
        S = S + (real(ny(n)) * P(1) + imag(ny(n)) * P(2)) * ...
                (source.q(n) - target.q(i)) * dS;
    end

    knl.D(i) = knl.D(i) + S;
end
