% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function [phi] = st_repr_electric_potential(x, y, q, bc)
    % Compute surface electric potential.
    %
    % :arg x: target geometry information.
    % :arg y: source geometry information.
    % :arg q: density at target points.
    % :arg bc: see :func:`st_boundary_condition_options`.
    %
    % :returns: electric potential at target points.

    phi = bc.fn.phinf(x);
    if norm(q, Inf) < 1.0e-10
        return;
    end

    phi = phi - st_layer_electric_slp(x, y, q);
end
