#!/usr/bin/zsh
# SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: MIT

set -euf -o pipefail

for f in $(ls ../src); do
    func=$(basename $f .m)
    if ! grep -q "$func" *.rst; then
        echo "'\033[0;33m$func\033[0m' is not documented anywhere"
    fi
done
