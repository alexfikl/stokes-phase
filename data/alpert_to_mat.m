% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

function alpert_to_mat()
    filenames = {'alpert-log-quadrature.csv', 'alpert-reg-quadrature.csv'};
    for n = 1:length(filenames)
        filename = fullfile(pwd, filenames{n});
        convert_csv(filename);
    end
end

function convert_csv(filename)
    % read csv file
    table = readtable(filename);
    orders = unique(table.order);

    % get all quadrature rules
    rules(max(orders)) = struct('a', 1, 'x', 1, 'w', 1);
    for n = 1:length(orders)
        mask = table.order == orders(n);
        rules(orders(n)).a = unique(table.a(mask));
        rules(orders(n)).x = table.x(mask);
        rules(orders(n)).w = table.w(mask);
    end

    % save
    [dirname, basename] = fileparts(filename);
    filename = fullfile(dirname, sprintf('%s.mat', basename));
    save(filename, 'rules');
end
