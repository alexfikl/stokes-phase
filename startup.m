% SPDX-FileCopyrightText: 2020 Alexandru Fikl <alexfikl@gmail.com>
% SPDX-License-Identifier: MIT

addpath(fullfile(pwd, "src"));
global verbosity;
verbosity = true;
