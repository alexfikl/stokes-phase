Two-Phase Adjoint Stokes Problems
=================================

This project aims to do some adjoint-based optimization on simple two-phase
axisymmetric Stokes problems. The preferred numerical method in use is based
on boundary integral equations.

For more details and examples on how everything works, there's an out of date
`docs` folder where one can generate documentation using Sphinx. For some
details about the methods, derivations, etc., there are also a healthy set
of `tex` files and `Mathematica` notebooks that do some nitty-gritty
work.

Requirements
============

The software was developed using MATLAB version `9.4.0.813654`, i.e. `R2018a`.
It requires the [Symbolic Toolbox](https://www.mathworks.com/products/symbolic.html)
in quite a few places. It has not been tested on Octave, but it may work
there too with some small modifications, since we don't make use of any other
advanced MATLAB features. Other required software (any recent versions should
work well enough):

* [Sphinx](http://www.sphinx-doc.org/en/stable/index.html) for generating
  documentation. This also means it requires some version of Python.
* [MATLAB domain](https://github.com/sphinx-contrib/matlabdomain) for Sphinx
  to actually work in MATLAB.
* [Mathematica](https://www.wolfram.com/mathematica/) for running some
  of the notebooks.
* [LaTeX](https://www.latex-project.org/) for generating some other
  documentation.

Usage
=====

This is not a MATLAB package, so there's no installing it. The current
recommended usage pattern is to just start MATLAB in the root directory
(which runs the included ``startup`` file).

The [documentation](https://stokes-phase.readthedocs.io/en/latest/index.html)
can be generated using:
```bash
just html
```

To check that everything is running as well as can be expected run the tests
in `autotests`. They use MATLAB's unit testing framework, so simply start MATLAB,
navigate to the folder and run:
```matlab
runtests
```

License
=======

Most of the code is licensed under the MIT license. Several files have been
copied and slightly modified from the [BEMLIB](http://dehesa.freeshell.org/BEMLIB/)
project, which are under the LGP 2.1 or later license.

See included `LICENSES` folder for details on each license.

Links
=====

* [Documentation](https://stokes-phase.readthedocs.io/en/latest/index.html)
* [Code](https://gitlab.com/alexfikl/stokes-phase)
* [BEMLIB](http://dehesa.freeshell.org/BEMLIB/) (initial inspiration, some small
  pieces of code still remain to this day!)
