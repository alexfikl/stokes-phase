\documentclass[DIV=14,parskip=half*]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[activate={true,nocompatibility},
            final,
            tracking=true,
            factor=1100,
            stretch=10,
            shrink=10]{microtype}
\microtypecontext{spacing=nonfrench}

\usepackage{scrlayer-scrpage}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{xparse}
\usepackage{xfrac}
\usepackage{enumerate}
\usepackage{hyperref}
\usepackage{tikz}

% custom headers and footers
\pagestyle{plain}
\clearscrheadfoot
\refoot[\pagemark]{\pagemark}
\rofoot[\pagemark]{\pagemark}

\hypersetup{
    colorlinks=true,
    urlcolor=blue,
    citecolor=black,
    linkcolor=black
}

% todo
\usepackage{todonotes}
\newcommand{\unsure}[1]{\todo[linecolor=red,backgroundcolor=red!25,bordercolor=red,inline]{#1}}

% title
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}

\title{
    \normalfont \normalsize
    \horrule{0.5pt} \\[0.4cm]
    \huge \textbf{Boundary Limits of the Pressure Kernel} \\
    \horrule{2pt} \\[0.5cm]
}
\author{}
\date{}

% some math commands
\NewDocumentCommand \dx { O{x} } {\,\mathrm{d} #1}
\NewDocumentCommand \vect { m } { \mathbf{#1} }
\NewDocumentCommand \jump { m } { \left\llbracket #1 \right\rrbracket }
\NewDocumentCommand \md { m m } { \dfrac{\mathrm{D} #1}{\mathrm{D} #2} }
\NewDocumentCommand \od { m m } { \dfrac{\mathrm{d} #1}{\mathrm{d} #2} }
\NewDocumentCommand \pd { m m } { \dfrac{\partial #1}{\partial #2} }
\DeclareMathOperator{\tr}{tr}

\newtheorem{remark}{Remark}
\newtheorem{theorem}{Theorem}

\begin{document}

\maketitle

\section{Introduction}

The single-layer representation of the pressure is given
by~\cite[Equation 5.3.2]{P1992}:
\[
p^\pm(\vect{x}) = \int_\Sigma p_j(\vect{x}, \vect{y}) q_j(\vect{y})
    \dx[\vect{y}],
\]
where $q_j(\vect{y})$ is the density and
\[
p_j(\vect{x}, \vect{y}) \triangleq
p_j(\vect{r} \triangleq \vect{x} - \vect{y}) = 2 \frac{r_j}{\|\vect{r}\|^3}
\]
is the kernel. We are interested in the finding the limit:
\[
p^\pm(\vect{x}) = \lim_{\epsilon \to 0}
    \int_\Sigma p_j(\vect{x} + \epsilon \vect{n}, \vect{y}) q_j(\vect{y}).
    \dx[\vect{y}]
\]

To find the limit, we will make use of some classical limits of the Laplace
Green function, which is given by
\[
G(\vect{r}) = \frac{1}{\|\vect{r}\|},
\]
with an appropriate scaling. We can now write the pressure kernel as:
\[
p_j(\vect{x}, \vect{y}) = 2 \nabla_\vect{y} G(\vect{x}, \vect{y}).
\]
To make use of this, we break up the density into its normal and tangential
components:
\[
\vect{q} = (\vect{q} \cdot \vect{n}) \vect{n} +
           (\vect{q} \times \vect{n}) \times \vect{n} =
           q_n \vect{n} + \vect{q}_t \times \vect{n}.
\]
Inserting into the single-layer potential, we get:
\[
\begin{aligned}
p^\pm(\vect{x}) =\,\, &
\int_\Sigma (\vect{p}(\vect{x}, \vect{y}) \cdot \vect{n}(\vect{y})) q_n(\vect{y}) \dx[\vect{y}] +
\int_\Sigma (\vect{n} \times \vect{p}(\vect{x}, \vect{y})) \cdot
            \vect{q}_t(\vect{y}) \dx[\vect{y}] \\
=\,\, &
2 \left\{
\int_\Sigma \vect{n} \cdot \nabla_\vect{y} G(\vect{x}, \vect{y}) q_n(\vect{y})
    \dx[\vect{y}]
+
\int_\Sigma (\vect{n} \times \nabla_\vect{y} G(\vect{x}, \vect{y})) \cdot
            \vect{q}_t(\vect{y})
    \dx[\vect{y}]
\right\}.
\end{aligned}
\]

With the details following in the next sections, our main result is given by:
\[
\lim_{\vect{x}^\pm \to \Sigma}
p^\pm(\vect{x}^\pm) =
\mp 4 \pi q_n(\vect{x}) +
\mathrm{p.v.} \int_\Sigma p_j(\vect{x}, \vect{y}) q_j(\vect{y}) \dx[\vect{y}].
\]

\section{Normal Limit}

First, we will look at the more complicated case of:
\[
\lim_{\vect{x}^+ \to \Sigma}
\int_\Sigma
    \vect{n} \cdot
    \nabla G(\vect{x}, \vect{y})
    q_n(\vect{y}) \dx[\vect{y}],
\]
The main idea of the following proof is from~\cite[Section 4.2.4]{GKW2013} and
it goes as follows: we create a perturbed source geometry $\Sigma'$ (that becomes
$\Sigma$ in the limit), break up the integral into parts belonging to $\Sigma$ and
an addition $\Sigma_\epsilon$ and finally take the limits. More explicitly, we
will divide the boundary $\Sigma$ as follows:
\[
\Sigma' = (\Sigma - \Sigma_\epsilon^*) + \Sigma_\epsilon.
\]
As we can see in Figure~\ref{fig:geometry}, this creates a fictitious boundary
$\Sigma' \to \Sigma$ as $\epsilon \to 0$. The fictious $\Sigma_\epsilon$ is
essentially a ball of radius $\epsilon$ on the exterior of $\Sigma$.
Another important point to make is that, on $\Sigma_\epsilon$, we can parametrize
by $(\theta, \phi)$ and get the area element
\[
\dx[\vect{y}] \triangleq \epsilon^2 \sin \phi \dx[\theta] \dx[\phi]
\]
in 3D. The 2D case is illustrated in Figure~\ref{fig:geometry}, where the
area element is the simpler
\[
\dx[\vect{y}] \triangleq \epsilon \dx[\theta].
\]

\begin{figure}[ht!]
\centering
\large
\begin{tikzpicture}[scale=1.5]
\filldraw[fill=black!10, draw=black!10] (0, 0) arc (0:180:3);
\filldraw[fill=black!10, draw=black!10] (-2, 2.8) arc (-10:190:1);
\draw[thick,dashed] (0, 0) arc (0:180:3);
\draw[thick] (0, 0) arc (0:70.5:3);
\begin{scope}[yscale=1,xscale=-1]
\draw[thick] (6, 0) arc (0:71:3);
\end{scope}
\draw[thick] (-2, 2.8) arc (-10:188:1);

\draw[fill=black] (-3, 3) circle [radius=0.075];
\draw[fill=black] (-2.695, 3.925) circle [radius=0.075]
                  node [right] {$\vect{y}$};
\draw[thick, ->] (-3, 3) -- (-2.5, 4.5) node [left] {$\mathbf{n}$};
\draw[thick, ->] (-2.5, 2.95) arc (0:40:1) node [right] {$\theta$};

\node at (-2.9, 3.5) [left] {$\epsilon$};
\node at (-6, 2) {$\Sigma'$};
\node at (0, 2) {$\Sigma$};
\node at (-3.5, 3) [below] {$\Sigma^*_\epsilon$};
\node at (-4, 4) {$\Sigma_\epsilon$};
\node at (-3, 3) [below] {$\vect{x}$};

\end{tikzpicture}
\caption{Local geometry of the boundary.}
\label{fig:geometry}
\end{figure}

Breaking up the integral can is done in the following way:
\[
\begin{aligned}
\int_\Sigma (\vect{n} \cdot \nabla_\vect{y} G) q_n \dx[\vect{y}]
=\,\, &
\lim_{\epsilon \to 0} \int_{\Sigma - \Sigma^*_\epsilon}
    \frac{\vect{r} \cdot \vect{n}}{\|\vect{r}\|^3} q_n \dx[\vect{y}] +
\lim_{\epsilon \to 0} \int_{\Sigma^\epsilon}
    \frac{\vect{r} \cdot \vect{n}}{\|\vect{r}\|^3} q_n \dx[\vect{y}].
\end{aligned}
\]
The first integral limit is strongly singular and represents the Cauchy
principal value of the limit. However, the second limit allows additional
manipulations:
\[
\begin{aligned}
\lim_{\epsilon \to 0} \int_{\Sigma^\epsilon}
    \frac{\vect{r} \cdot \vect{n}}{\|\vect{r}\|^3} q_n \dx[\vect{y}]
=\,\, &
\lim_{\epsilon \to 0} \int_{0}^\alpha \int_0^\beta
    \frac{(\vect{x} - \vect{y}) \cdot \vect{n}}{\epsilon^3}
    q_n(\vect{y})
    \epsilon^2 \sin \phi \dx[\theta] \dx[\phi] \\
=\,\, &
\lim_{\epsilon \to 0} \int_{0}^\alpha \int_0^\beta
    -q_n(\vect{y}) \sin \phi \dx[\theta] \dx[\phi] \\
=\,\, &
\alpha (\cos \beta - 1) q_n(\vect{x}),
\end{aligned}
\]
where, for $\vect{y} \in \Sigma_\epsilon$,
\[
(\vect{x} - \vect{y}) \cdot \vect{n} =
(\vect{x} - (\vect{x} + \epsilon \vect{n})) \cdot \vect{n} = -\epsilon.
\]
In the case of a smooth surface, i.e. without corners or other discontinuities,
we can assume that as $\epsilon \to 0$, the neighborhood we are looking at is
essentially flat so
\[
\alpha = \beta = \pi.
\]
This leaves the limit:
\[
\lim_{\epsilon \to 0} \int_{\Sigma^\epsilon}
    \frac{\vect{r} \cdot \vect{n}}{\|\vect{r}\|^3} q_n \dx[\vect{y}] =
-2 \pi q_n(\vect{x})
\]

Therefore, our final result for the normal component is:
\[
\lim_{\vect{x}^+ \to \Sigma}
\int_\Sigma p_j(\vect{x}, \vect{y}) \vect{n}_j(\vect{y}) q_n(\vect{y})
    \dx[\vect{y}] =
-4 \pi q_n(\vect{x}) +
\mathrm{p.v.}
\int_\Sigma p_j(\vect{x}, \vect{y}) \vect{n}_j(\vect{y}) q_n(\vect{y})
    \dx[\vect{y}],
\]
where $\vect{x}^+ \to \Sigma$ is meant to represent the exterior limit.

\begin{remark}
The usual 3D Laplace Green function is scaled as follows:
\[
G(\vect{r}) = -\frac{1}{4 \pi} \frac{1}{\|\vect{r}\|}.
\]
Using this definition, we obtain the classic result~\cite[Theorem 6.18]{K1989}:
\[
\lim_{\vect{x}^\pm \to \Sigma}
\int_\Sigma (\vect{n} \cdot \nabla_\vect{y} G) \sigma \dx[\vect{y}] =
\pm \frac{1}{2} \sigma +
\mathrm{p.v.} \int_\Sigma (\vect{n} \cdot \nabla_\vect{y} G) \sigma \dx[\vect{y}].
\]
\end{remark}

\section{Tangential Limit}

The tangential limits follow a very similar reasoning. For simplicity, we
denote by $(\vect{t}_1, \vect{t}_2)$ the two vector that span the tangent
space at a source point $\vect{y}$. We consider the following limit:
\[
\lim_{\vect{x}^+ \to \Sigma}
\int_\Sigma \vect{t}_j \cdot \nabla_{\vect{y}} G q_{t_j} \dx[\vect{y}],
\]
where $q_{t_j} = \vect{q} \cdot \vect{t}_j$, for $j \in \{1, 2\}$. We proceed
by breaking up the integral in two parts like in the previous section and
will focus on the integral on $\Sigma_\epsilon$, since the integral on
$\Sigma - \Sigma^*_\epsilon$ will converge to the Cauchy principal value
in this case as well. We have that:
\[
\begin{aligned}
\lim_{\epsilon \to 0} \int_{\Sigma^\epsilon}
    \frac{\vect{r} \cdot \vect{t}_j}{\|\vect{r}\|^3} q_n \dx[\vect{y}]
=\,\, &
\lim_{\epsilon \to 0} \int_{0}^\alpha \int_0^\beta
    \frac{(\vect{x} - \vect{y}) \cdot \vect{t}_j}{\epsilon^3}
    q_n(\vect{y})
    \epsilon^2 \sin \phi \dx[\theta] \dx[\phi] \\
=\,\, &
\lim_{\epsilon \to 0} \int_{0}^\alpha \int_0^\beta
    -(\vect{n} \cdot \vect{t}_j) q_n(\vect{y}) \sin \phi \dx[\theta] \dx[\phi] \\
=\,\, &
0,
\end{aligned}
\]
since $\vect{n}$ is orthogonal to the tangent space, and in particular to the
vectors $\vect{t}_j$. As expected, we get no additional contributions from the
tangential direction. We are then left with:
\[
\lim_{\vect{x}^+ \to \Sigma}
\int_\Sigma \vect{t}_j \cdot \nabla_{\vect{y}} G q_{t_j} \dx[\vect{y}]
= \mathrm{p.v.}
\int_\Sigma \vect{t}_j \cdot \nabla_{\vect{y}} G q_{t_j} \dx[\vect{y}].
\]

\begin{thebibliography}{9}
\bibitem{P1992}
    C. Pozrikidis,
    \emph{Boundary Integral and Singularity Methods for Linearized Viscous Flow},
    Cambridge University Press, 1992.
\bibitem{GKW2013}
    L. Gaul, M. Kogl, M. Wagner,
    \emph{Boundary Element Methods for Engineers and Scientists},
    Springer, 2013.
\bibitem{K1989}
    R. Kress,
    \emph{Linear Integral Equations},
    Springer, 1989.
\end{thebibliography}

\end{document}
