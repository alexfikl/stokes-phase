.. currentmodule:: src

Interpolation
=============

.. autofunction:: st_interpolate
.. autofunction:: st_derivative
.. autofunction:: st_interp_evaluate_basis
.. autofunction:: st_interp_evaluate

Quadrature
==========

.. autofunction:: st_quad_fejer
.. autofunction:: st_quad_leggauss

.. autofunction:: st_quad_carley
.. autofunction:: st_quad_log_kress
.. autofunction:: st_quad_log_alpert
.. autofunction:: st_quad_cpv_shia

.. autofunction:: st_quad_translate

.. autofunction:: st_ax_integral
.. autofunction:: st_ax_mean
.. autofunction:: st_ax_norm
.. autofunction:: st_ax_error
