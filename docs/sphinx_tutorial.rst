Tutorial
========

In this tutorial, we will attempt to solve a simple shape optimization problem,
which will hopefully illustrate what this little code can do. The equations
we work with are the two-phase static Stokes equations

.. math::

    \begin{cases}
    \nabla \cdot \mathbf{u}_\pm = 0, \\
    \nabla \cdot \sigma_\pm = 0, \\
    \mathbf{u}_+ = \mathbf{u}_\infty.
    \end{cases}

The main usecase for these equations is to describe droplets suspended in an
ambient fluid. At the interface, we pose the existence of a constant surface
tension. This gives rise to the well-known Young-Laplace law. Specifically,
we have the following jump conditions

.. math::

    \begin{cases}
    [\![ \mathbf{u} ]\!] = 0, \\
    \displaystyle
    [\![ \mathbf{n} \cdot \sigma ]\!] = \frac{1}{\mathrm{Ca}} \kappa \mathbf{n},
    \end{cases}

where :math:`\kappa` is the sum of the principal curvatures, :math:`\mathbf{n}`
is the normal exterior to the drop (here denoted by the :math:`-`) and
:math:`\mathrm{Ca}` is the Capillary number.

Using these constraints, we propose to minimize the cost functional

.. math::

    \mathcal{J} = \frac{1}{2} \int_\Sigma
        |\mathbf{u} \cdot \mathbf{n} - u_d| \,\mathrm{d}S.

The desired velocity field :math:`u_d` is solved on a desired geometry
:math:`\mathbf{X}_d`, which we will see at the end. We now proceed to setting
up this problem in code as follows.

.. literalinclude:: ../examples/example_tutorial.m
    :language: matlab
    :lines: 4-
    :linenos:
