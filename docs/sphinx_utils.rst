.. currentmodule:: src

Utils
=====

Optimization
^^^^^^^^^^^^

.. autofunction:: st_optim_cg
.. autofunction:: st_optim_options
.. autofunction:: st_optim_geometry_alpha_max

Elliptic Integrals
^^^^^^^^^^^^^^^^^^

.. autofunction:: st_ellipke
.. autofunction:: st_ellint_order3
.. autofunction:: st_ellint_order5
.. autofunction:: st_ellint_order7

Struct Enhancements
^^^^^^^^^^^^^^^^^^^

.. autofunction:: st_struct_field
.. autofunction:: st_struct_hash
.. autofunction:: st_struct_merge
.. autofunction:: st_struct_update
.. autofunction:: st_struct_varargin

Misc
^^^^

.. autofunction:: st_print
.. autofunction:: st_sym_handle

.. autofunction:: st_ax_filter
.. autofunction:: st_fftfreq

.. autofunction:: st_dot
.. autofunction:: st_array_stack
.. autofunction:: st_array_unstack
.. autofunction:: st_array_ax

.. autofunction:: st_cache_create

.. autofunction:: st_util_timeit
.. autofunction:: st_util_eoc
.. autofunction:: st_util_eoc_show
.. autofunction:: st_util_latex_table
