.. currentmodule:: src

Geometry
========

.. autofunction:: st_point_collocation
.. autofunction:: st_point_quadrature

.. autofunction:: st_mesh_geometry
.. autofunction:: st_mesh_geometry_update
.. autofunction:: st_mesh_target_geometry
.. autofunction:: st_mesh_source_geometry

.. autofunction:: st_mesh_target_associate
.. autofunction:: st_mesh_h_max
.. autofunction:: st_mesh_h_min
.. autofunction:: st_mesh_volume
.. autofunction:: st_mesh_centroid

.. autofunction:: st_mesh_curve
.. autofunction:: st_mesh_equidistance
