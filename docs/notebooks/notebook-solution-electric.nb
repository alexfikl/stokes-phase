(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      7182,        210]
NotebookOptionsPosition[      6709,        195]
NotebookOutlinePosition[      7042,        210]
CellTagsIndexPosition[      6999,        207]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{
    RowBox[{"R", "\[Element]", "Reals"}], "&&", 
    RowBox[{"R", ">", "0"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7897351578729267`*^9, 3.789735158055977*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"a1209e04-db2d-4ff9-b88c-34394d5a9f49"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"phip", "[", 
    RowBox[{"\[Rho]_", ",", "\[Theta]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"V", " ", "\[Rho]"}], "+", 
      RowBox[{"Bp", "/", 
       RowBox[{"\[Rho]", "^", "2"}]}]}], ")"}], 
    RowBox[{"Cos", "[", "\[Theta]", "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"phim", "[", 
    RowBox[{"\[Rho]_", ",", "\[Theta]_"}], "]"}], ":=", 
   RowBox[{"Am", " ", "\[Rho]", " ", 
    RowBox[{"Cos", "[", "\[Theta]", "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"C1", "=", 
  RowBox[{
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{
         RowBox[{"phip", "[", 
          RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], "\[Equal]", 
         RowBox[{"phim", "[", 
          RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}]}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{
          RowBox[{"Grad", "[", 
           RowBox[{
            RowBox[{"phip", "[", 
             RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
            "\"\<Spherical\>\""}], "]"}], "[", 
          RowBox[{"[", "1", "]"}], "]"}], "\[Equal]", 
         RowBox[{"Q", " ", 
          RowBox[{
           RowBox[{"Grad", "[", 
            RowBox[{
             RowBox[{"phim", "[", 
              RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
             "\"\<Spherical\>\""}], "]"}], "[", 
           RowBox[{"[", "1", "]"}], "]"}]}]}]}], "\[IndentingNewLine]", "}"}],
       "/.", 
      RowBox[{"{", 
       RowBox[{"\[Rho]", "\[Rule]", "R"}], "}"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"Am", ",", "Bp"}], "}"}]}], "]"}], "[", 
   RowBox[{"[", "1", "]"}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.7897365191415358`*^9, 3.78973656143589*^9}, {
  3.789736604215295*^9, 3.789736619968781*^9}, {3.78973875852568*^9, 
  3.789739141017782*^9}, {3.789739369706513*^9, 3.789739380961872*^9}, {
  3.78976950023608*^9, 3.789769500641506*^9}},
 CellLabel->"In[69]:=",ExpressionUUID->"7d6147d7-8893-41d5-9230-b5282e22fd9e"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"phip", "[", 
      RowBox[{"R", ",", "\[Theta]"}], "]"}], "-", 
     RowBox[{"phim", "[", 
      RowBox[{"R", ",", "\[Theta]"}], "]"}]}], ")"}], "/.", "C1"}], "//", 
  "Simplify"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"phim", "[", 
    RowBox[{"0", ",", "\[Theta]"}], "]"}], "/.", "C1"}], "//", 
  "Simplify"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"Grad", "[", 
        RowBox[{
         RowBox[{"phip", "[", 
          RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
         "\"\<Spherical\>\""}], "]"}], "[", 
       RowBox[{"[", "1", "]"}], "]"}], "-", 
      RowBox[{"Q", " ", 
       RowBox[{
        RowBox[{"Grad", "[", 
         RowBox[{
          RowBox[{"phim", "[", 
           RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
          "\"\<Spherical\>\""}], "]"}], "[", 
        RowBox[{"[", "1", "]"}], "]"}]}]}], ")"}], "/.", 
    RowBox[{"{", 
     RowBox[{"\[Rho]", "\[Rule]", "R"}], "}"}]}], "/.", "C1"}], "//", 
  "Simplify"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"Grad", "[", 
        RowBox[{
         RowBox[{"phip", "[", 
          RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
         "\"\<Spherical\>\""}], "]"}], "[", 
       RowBox[{"[", "2", "]"}], "]"}], "-", 
      RowBox[{
       RowBox[{"Grad", "[", 
        RowBox[{
         RowBox[{"phim", "[", 
          RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
         "\"\<Spherical\>\""}], "]"}], "[", 
       RowBox[{"[", "2", "]"}], "]"}]}], ")"}], "/.", 
    RowBox[{"{", 
     RowBox[{"\[Rho]", "\[Rule]", "R"}], "}"}]}], "/.", "C1"}], "//", 
  "Simplify"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"Grad", "[", 
        RowBox[{
         RowBox[{"phip", "[", 
          RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
         "\"\<Spherical\>\""}], "]"}], "[", 
       RowBox[{"[", "3", "]"}], "]"}], "-", 
      RowBox[{"Z", " ", 
       RowBox[{
        RowBox[{"Grad", "[", 
         RowBox[{
          RowBox[{"phim", "[", 
           RowBox[{"\[Rho]", ",", "\[Theta]"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"\[Rho]", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
          "\"\<Spherical\>\""}], "]"}], "[", 
        RowBox[{"[", "3", "]"}], "]"}]}]}], ")"}], "/.", 
    RowBox[{"{", 
     RowBox[{"\[Rho]", "\[Rule]", "R"}], "}"}]}], "/.", "C1"}], "//", 
  "Simplify"}]}], "Input",
 CellChangeTimes->{{3.7897393659827557`*^9, 3.789739484902832*^9}, {
  3.789739551336486*^9, 3.7897395880291033`*^9}, {3.789739655436404*^9, 
  3.789739657575889*^9}},
 CellLabel->"In[64]:=",ExpressionUUID->"da800d00-33cb-4c0a-8915-7621475a14ef"]
},
WindowSize->{956, 1021},
WindowMargins->{{4, Automatic}, {0, Automatic}},
FrontEndVersion->"12.0 for Linux x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 413, 10, 57, "Input",ExpressionUUID->"a1209e04-db2d-4ff9-b88c-34394d5a9f49"],
Cell[974, 32, 2335, 61, 147, "Input",ExpressionUUID->"7d6147d7-8893-41d5-9230-b5282e22fd9e"],
Cell[3312, 95, 3393, 98, 193, "Input",ExpressionUUID->"da800d00-33cb-4c0a-8915-7621475a14ef"]
}
]
*)

(* End of internal cache information *)

