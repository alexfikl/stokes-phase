(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     37036,        886]
NotebookOptionsPosition[     36268,        868]
NotebookOutlinePosition[     36601,        883]
CellTagsIndexPosition[     36558,        880]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{
    RowBox[{"R", "\[Element]", "Reals"}], "&&", 
    RowBox[{"R", ">", "0"}]}]}], ";"}]}], "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"6ab9b0e7-431c-4e7a-a7f3-345febd3df4a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Helper", " ", "Functions"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"Ext", "[", 
      RowBox[{"S_", ",", "x_"}], "]"}], ":=", 
     RowBox[{"x", "/.", 
      RowBox[{"(", 
       RowBox[{"Exterior", "/.", "S"}], ")"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Int", "[", 
      RowBox[{"S_", ",", "x_"}], "]"}], ":=", 
     RowBox[{"x", "/.", 
      RowBox[{"(", 
       RowBox[{"Interior", "/.", "S"}], ")"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Ext2D", "[", 
      RowBox[{"S_", ",", "x_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"x", "/.", 
        RowBox[{"(", 
         RowBox[{"Exterior", "/.", "S"}], ")"}]}], ")"}], "[", 
      RowBox[{"[", 
       RowBox[{
        RowBox[{"1", ";;", "2"}], ",", 
        RowBox[{"1", ";;", "2"}]}], "]"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Int2D", "[", 
      RowBox[{"S_", ",", "x_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"x", "/.", 
        RowBox[{"(", 
         RowBox[{"Interior", "/.", "S"}], ")"}]}], ")"}], "[", 
      RowBox[{"[", 
       RowBox[{
        RowBox[{"1", ";;", "2"}], ",", 
        RowBox[{"1", ";;", "2"}]}], "]"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"radius", "[", 
      RowBox[{"x", ",", "y"}], "]"}], "=", 
     RowBox[{"Sqrt", "[", 
      RowBox[{
       RowBox[{"x", "^", "2"}], "+", 
       RowBox[{"y", "^", "2"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"angle", "[", 
      RowBox[{"x", ",", "y"}], "]"}], "=", 
     RowBox[{"ArcTan", "[", 
      RowBox[{"x", ",", "y"}], "]"}]}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.763069970086615*^9, 3.7630700221870747`*^9}, {
  3.7634664331130533`*^9, 3.763466455364779*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"af5c1d44-fcb2-4d73-a54c-6f54ea4b6899"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"Solve", " ", "Hadamard"}], "-", "Rybczynski"}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"HadamardRybczynski", "[", 
      RowBox[{"\[Alpha]_", ",", "R0_", ",", "U0_"}], "]"}], ":=", 
     RowBox[{"Module", "[", 
      RowBox[{
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{
        "psip", ",", "urp", ",", "uthetap", ",", "dup", ",", "ddup", ",", 
         "pp", ",", "sigmap", ",", "\[IndentingNewLine]", "psim", ",", "urm", 
         ",", "uthetam", ",", "dum", ",", "ddum", ",", "pm", ",", "sigmam", 
         ",", "\[IndentingNewLine]", "C1"}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"(*", "Streamfunction", "*)"}], "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{
         RowBox[{"psip", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"A0", "/", "r"}], "+", 
            RowBox[{"A1", " ", "r"}], "+", 
            RowBox[{
             RowBox[{"1", "/", "2"}], "U0", " ", 
             RowBox[{"r", "^", "2"}]}]}], ")"}], 
          RowBox[{
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "^", "2"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"psim", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"A2", " ", 
             RowBox[{"r", "^", "2"}]}], "+", 
            RowBox[{"A3", " ", 
             RowBox[{"r", "^", "4"}]}]}], ")"}], 
          RowBox[{
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "^", "2"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"(*", "Velocity", "*)"}], "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"urp", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{"D", "[", 
           RowBox[{
            RowBox[{"psip", "[", 
             RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "\[Theta]"}], "]"}], 
          "/", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"r", "^", "2"}], " ", 
            RowBox[{"Sin", "[", "\[Theta]", "]"}]}], ")"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"uthetap", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{"-", 
           RowBox[{"D", "[", 
            RowBox[{
             RowBox[{"psip", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "r"}], "]"}]}], "/", 
          RowBox[{"(", 
           RowBox[{"r", " ", 
            RowBox[{"Sin", "[", "\[Theta]", "]"}]}], ")"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"urm", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{"D", "[", 
           RowBox[{
            RowBox[{"psim", "[", 
             RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "\[Theta]"}], "]"}], 
          "/", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"r", "^", "2"}], " ", 
            RowBox[{"Sin", "[", "\[Theta]", "]"}]}], ")"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"uthetam", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{"-", 
           RowBox[{"D", "[", 
            RowBox[{
             RowBox[{"psim", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "r"}], "]"}]}], "/", 
          RowBox[{"(", 
           RowBox[{"r", " ", 
            RowBox[{"Sin", "[", "\[Theta]", "]"}]}], ")"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"(*", "Pressure", "*)"}], "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"ddup", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{"Simplify", "[", 
          RowBox[{
           RowBox[{"Laplacian", "[", 
            RowBox[{
             RowBox[{"{", 
              RowBox[{
               RowBox[{"urp", "[", 
                RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", 
               RowBox[{"uthetap", "[", 
                RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "0"}], "}"}], ",", 
             RowBox[{"{", 
              RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
             "\"\<Spherical\>\""}], "]"}], "[", 
           RowBox[{"[", "1", "]"}], "]"}], "]"}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"ddum", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{"Simplify", "[", 
          RowBox[{
           RowBox[{"Laplacian", "[", 
            RowBox[{
             RowBox[{"{", 
              RowBox[{
               RowBox[{"urm", "[", 
                RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", 
               RowBox[{"uthetam", "[", 
                RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "0"}], "}"}], ",", 
             RowBox[{"{", 
              RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
             "\"\<Spherical\>\""}], "]"}], "[", 
           RowBox[{"[", "1", "]"}], "]"}], "]"}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"pp", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{"Integrate", "[", 
          RowBox[{
           RowBox[{"ddup", "[", 
            RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "r"}], "]"}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"pm", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{"Integrate", "[", 
          RowBox[{
           RowBox[{"ddum", "[", 
            RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "r"}], "]"}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"(*", "Stress", "*)"}], "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"dup", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{"Grad", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"urp", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", 
             RowBox[{"uthetap", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "0"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
           "\"\<Spherical\>\""}], "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"dum", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{"Grad", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"urm", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", 
             RowBox[{"uthetam", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "0"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
           "\"\<Spherical\>\""}], "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"sigmap", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{
           RowBox[{"-", 
            RowBox[{"pp", "[", 
             RowBox[{"r", ",", "\[Theta]"}], "]"}]}], 
           RowBox[{"IdentityMatrix", "[", "3", "]"}]}], "+", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"dup", "[", 
             RowBox[{"r", ",", "\[Theta]"}], "]"}], "+", 
            RowBox[{"Transpose", "[", 
             RowBox[{"dup", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], "]"}]}], ")"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"sigmam", "[", 
          RowBox[{"r_", ",", "\[Theta]_"}], "]"}], "=", 
         RowBox[{
          RowBox[{
           RowBox[{"-", 
            RowBox[{"pm", "[", 
             RowBox[{"r", ",", "\[Theta]"}], "]"}]}], 
           RowBox[{"IdentityMatrix", "[", "3", "]"}]}], "+", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"dum", "[", 
             RowBox[{"r", ",", "\[Theta]"}], "]"}], "+", 
            RowBox[{"Transpose", "[", 
             RowBox[{"dum", "[", 
              RowBox[{"r", ",", "\[Theta]"}], "]"}], "]"}]}], ")"}]}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"(*", 
         RowBox[{"Find", " ", "Coefficients"}], "*)"}], "\[IndentingNewLine]", 
        RowBox[{"C1", "=", 
         RowBox[{
          RowBox[{
           RowBox[{"Solve", "[", 
            RowBox[{
             RowBox[{
              RowBox[{"{", "\[IndentingNewLine]", 
               RowBox[{
                RowBox[{
                 RowBox[{"urp", "[", 
                  RowBox[{"r", ",", "\[Theta]"}], "]"}], "\[Equal]", 
                 RowBox[{"urm", "[", 
                  RowBox[{"r", ",", "\[Theta]"}], "]"}]}], ",", 
                "\[IndentingNewLine]", 
                RowBox[{
                 RowBox[{"uthetap", "[", 
                  RowBox[{"r", ",", "\[Theta]"}], "]"}], "\[Equal]", 
                 RowBox[{"uthetam", "[", 
                  RowBox[{"r", ",", "\[Theta]"}], "]"}]}], ",", 
                "\[IndentingNewLine]", 
                RowBox[{
                 RowBox[{
                  RowBox[{"D", "[", 
                   RowBox[{
                    RowBox[{"uthetap", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "r"}], "]"}], 
                  "-", 
                  RowBox[{
                   RowBox[{"uthetap", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], "/", "r"}], "+", 
                  RowBox[{
                   RowBox[{"D", "[", 
                    RowBox[{
                    RowBox[{"urp", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "\[Theta]"}], 
                    "]"}], "/", "r"}]}], "\[Equal]", 
                 RowBox[{"\[Lambda]", 
                  RowBox[{"(", 
                   RowBox[{
                    RowBox[{"D", "[", 
                    RowBox[{
                    RowBox[{"uthetam", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "r"}], "]"}], 
                    "-", 
                    RowBox[{
                    RowBox[{"uthetam", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], "/", "r"}], "+", 
                    RowBox[{
                    RowBox[{"D", "[", 
                    RowBox[{
                    RowBox[{"urm", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "\[Theta]"}], 
                    "]"}], "/", "r"}]}], ")"}]}]}]}], "\[IndentingNewLine]", 
               "}"}], "/.", 
              RowBox[{"{", 
               RowBox[{"r", "\[Rule]", "R"}], "}"}]}], ",", 
             RowBox[{"{", 
              RowBox[{"A1", ",", "A2", ",", "A3"}], "}"}]}], "]"}], "//", 
           "Rationalize"}], "//", "Simplify"}]}], ";", "\[IndentingNewLine]", 
        
        RowBox[{"C1", "=", 
         RowBox[{"Join", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"A0", "\[Rule]", 
              RowBox[{"\[Alpha]", " ", 
               RowBox[{"R0", "^", "3"}]}]}], ",", 
             RowBox[{"R", "\[Rule]", "R0"}]}], "}"}], ",", 
           RowBox[{
            RowBox[{"C1", "[", 
             RowBox[{"[", "1", "]"}], "]"}], "/.", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"A0", "\[Rule]", 
               RowBox[{"\[Alpha]", " ", 
                RowBox[{"R0", "^", "3"}]}]}], ",", 
              RowBox[{"R", "->", "R0"}]}], "}"}]}]}], "]"}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{"Print", "[", 
         RowBox[{"C1", "//", "Simplify"}], "]"}], ";", "\[IndentingNewLine]", 
        
        RowBox[{"{", 
         RowBox[{
          RowBox[{"Exterior", "->", 
           RowBox[{"{", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"\[Psi]", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"psip", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"u", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{
                  RowBox[{"urp", "[", 
                   RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", 
                  RowBox[{"uthetap", "[", 
                   RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "0"}], "}"}], "//.",
                 "C1"}], "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"du", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"dup", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"\[Tau]", "\[Rule]", 
              RowBox[{
               RowBox[{"1", "/", "2"}], 
               RowBox[{"Simplify", "[", 
                RowBox[{
                 RowBox[{"(", 
                  RowBox[{
                   RowBox[{"dup", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], "+", 
                   RowBox[{"Transpose", "[", 
                    RowBox[{"dup", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], "]"}]}], ")"}], "//.",
                  "C1"}], "]"}]}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"p", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"pp", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"\[Sigma]", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"sigmap", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}]}], "\[IndentingNewLine]", "}"}]}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{"Interior", "\[Rule]", 
           RowBox[{"{", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"\[Psi]", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"psim", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"u", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"{", 
                 RowBox[{
                  RowBox[{"urm", "[", 
                   RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", 
                  RowBox[{"uthetam", "[", 
                   RowBox[{"r", ",", "\[Theta]"}], "]"}], ",", "0"}], "}"}], "//.",
                 "C1"}], "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"du", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"dum", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"\[Tau]", "\[Rule]", 
              RowBox[{
               RowBox[{"1", "/", "2"}], 
               RowBox[{"Simplify", "[", 
                RowBox[{
                 RowBox[{"(", 
                  RowBox[{
                   RowBox[{"dum", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], "+", 
                   RowBox[{"Transpose", "[", 
                    RowBox[{"dum", "[", 
                    RowBox[{"r", ",", "\[Theta]"}], "]"}], "]"}]}], ")"}], "//.",
                  "C1"}], "]"}]}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"p", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"pm", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}], ",", "\[IndentingNewLine]", 
             RowBox[{"\[Sigma]", "\[Rule]", 
              RowBox[{"Simplify", "[", 
               RowBox[{
                RowBox[{"sigmam", "[", 
                 RowBox[{"r", ",", "\[Theta]"}], "]"}], "//.", "C1"}], 
               "]"}]}]}], "\[IndentingNewLine]", "}"}]}]}], 
         "\[IndentingNewLine]", "}"}]}]}], "\[IndentingNewLine]", "]"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Rd", "=", "2"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"ForwardSolution", "=", 
     RowBox[{"HadamardRybczynski", "[", 
      RowBox[{
       RowBox[{"1", "/", "4"}], ",", "Rd", ",", "0"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"AdjointSolution", "=", 
     RowBox[{"HadamardRybczynski", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"\[Lambda]", "/", 
         RowBox[{"(", 
          RowBox[{"1", "+", "\[Lambda]"}], ")"}]}], "/", "4"}], ",", "Rd", 
       ",", "1"}], "]"}]}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.763066895220922*^9, 3.763066947396181*^9}, {
   3.763067032956476*^9, 3.7630675040854177`*^9}, {3.76306753906398*^9, 
   3.763067561365098*^9}, {3.763067592601721*^9, 3.7630676265767403`*^9}, {
   3.7630677775198107`*^9, 3.7630678646699333`*^9}, {3.7630679107318373`*^9, 
   3.7630680496845083`*^9}, {3.763068186475749*^9, 3.763068439895122*^9}, {
   3.7630684784530687`*^9, 3.763068529789164*^9}, {3.763068587164824*^9, 
   3.763069254618133*^9}, {3.76306954614205*^9, 3.7630695961168756`*^9}, {
   3.763069773275589*^9, 3.76306982143187*^9}, 3.763069965163123*^9, {
   3.763070109981902*^9, 3.763070115697797*^9}, {3.763070176344426*^9, 
   3.763070177530294*^9}, {3.763071046189959*^9, 3.763071047607827*^9}, {
   3.763071137881258*^9, 3.763071139103075*^9}, {3.7630713732396097`*^9, 
   3.763071379228219*^9}, {3.763071729558917*^9, 3.763071729690825*^9}, {
   3.763072385575596*^9, 3.7630723857225513`*^9}, {3.763072487833268*^9, 
   3.763072506253721*^9}, {3.76307257437451*^9, 3.763072579205738*^9}, {
   3.763072689678846*^9, 3.763072701056967*^9}, {3.7630736926931553`*^9, 
   3.763073737813789*^9}, {3.763073921250163*^9, 3.7630739340099077`*^9}, {
   3.763074266807764*^9, 3.7630742701807213`*^9}, {3.763074305707788*^9, 
   3.763074320294383*^9}, {3.7630744018306417`*^9, 3.763074443352166*^9}, {
   3.763074548187234*^9, 3.763074560695817*^9}, {3.763074898250071*^9, 
   3.763074950402794*^9}, {3.7630750430122004`*^9, 3.7630751695996847`*^9}, {
   3.763075209796832*^9, 3.763075216736904*^9}, {3.763075349726577*^9, 
   3.763075443080184*^9}, {3.763128641684987*^9, 3.7631286635032377`*^9}, {
   3.7631290103066874`*^9, 3.7631290455023937`*^9}, {3.763138551138897*^9, 
   3.763138583141974*^9}, {3.763140687315351*^9, 3.763140702035956*^9}, {
   3.763299433909144*^9, 3.7632994466774597`*^9}, {3.763466487430109*^9, 
   3.763466493795712*^9}, {3.7634766646279182`*^9, 3.763476667657517*^9}, {
   3.76409121775322*^9, 3.764091236987864*^9}, {3.76409134480823*^9, 
   3.76409134589436*^9}, {3.764265025636829*^9, 3.764265061847807*^9}, {
   3.764332457498103*^9, 3.764332462706546*^9}, {3.765802809183652*^9, 
   3.7658028148441467`*^9}, {3.765802937319364*^9, 3.76580293852435*^9}, {
   3.77797909592233*^9, 3.777979170027071*^9}, 3.777981013755012*^9},
 CellLabel->
  "In[155]:=",ExpressionUUID->"e5af06c3-73df-4a96-9610-6807179fa8a3"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Plot", " ", "Streamfunction"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"ContourPlot", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Evaluate", "[", 
       RowBox[{
        RowBox[{"Ext", "[", 
         RowBox[{"ForwardSolution", ",", "\[Psi]"}], "]"}], "/.", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"\[Lambda]", "\[Rule]", "2"}], ",", 
          RowBox[{"r", "\[Rule]", 
           RowBox[{"radius", "[", 
            RowBox[{"x", ",", "y"}], "]"}]}], ",", 
          RowBox[{"\[Theta]", "\[Rule]", 
           RowBox[{"angle", "[", 
            RowBox[{"x", ",", "y"}], "]"}]}]}], "}"}]}], "]"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"x", ",", 
        RowBox[{"-", "2"}], ",", "2"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"y", ",", 
        RowBox[{"-", "2"}], ",", "2"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{"Contours", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"0", ",", 
         RowBox[{"-", "0.5"}], ",", 
         RowBox[{"-", "1"}], ",", 
         RowBox[{"-", "2"}], ",", 
         RowBox[{"-", "3"}]}], "}"}]}], ",", 
      RowBox[{"ContourLabels", "\[Rule]", "True"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"RegionFunction", "\[Rule]", 
       RowBox[{"Function", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"x", ",", "y"}], "}"}], ",", 
         RowBox[{
          RowBox[{"Sqrt", "[", 
           RowBox[{
            RowBox[{"x", "^", "2"}], "+", 
            RowBox[{"y", "^", "2"}]}], "]"}], ">", "1"}]}], "]"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"PlotLegends", "\[Rule]", "Automatic"}]}], "]"}], 
    "\[IndentingNewLine]", 
    RowBox[{"ContourPlot", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Evaluate", "[", 
       RowBox[{
        RowBox[{"Ext", "[", 
         RowBox[{"AdjointSolution", ",", "\[Psi]"}], "]"}], "/.", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"\[Lambda]", "\[Rule]", "2"}], ",", 
          RowBox[{"r", "\[Rule]", 
           RowBox[{"radius", "[", 
            RowBox[{"x", ",", "y"}], "]"}]}], ",", 
          RowBox[{"\[Theta]", "\[Rule]", 
           RowBox[{"angle", "[", 
            RowBox[{"x", ",", "y"}], "]"}]}]}], "}"}]}], "]"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"x", ",", 
        RowBox[{"-", "3"}], ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"y", ",", 
        RowBox[{"-", "3"}], ",", "3"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{"Contours", "\[Rule]", "32"}], ",", "\[IndentingNewLine]", 
      RowBox[{"RegionFunction", "\[Rule]", 
       RowBox[{"Function", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"x", ",", "y"}], "}"}], ",", 
         RowBox[{
          RowBox[{"Sqrt", "[", 
           RowBox[{
            RowBox[{"x", "^", "2"}], "+", 
            RowBox[{"y", "^", "2"}]}], "]"}], ">", "2"}]}], "]"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"PlotLegends", "\[Rule]", "Automatic"}]}], "]"}]}], 
   "*)"}]}]], "Input",
 CellChangeTimes->{{3.7630698289924717`*^9, 3.7630699545563917`*^9}, {
  3.763070032078871*^9, 3.763070086577979*^9}, {3.763070151981318*^9, 
  3.763070152402713*^9}, {3.763070190109334*^9, 3.763070472701633*^9}, {
  3.763070724260977*^9, 3.7630708136318283`*^9}, {3.763070847367425*^9, 
  3.763071004435931*^9}, {3.763071062292218*^9, 3.763071328770857*^9}, {
  3.763073975745253*^9, 3.76307397821412*^9}},
 CellLabel->
  "In[533]:=",ExpressionUUID->"9addf5ee-54e4-4657-b35c-2ac37c212e41"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Check", " ", "Solution"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"CheckSolution", "[", "S_", "]"}], ":=", 
     RowBox[{"Module", "[", 
      RowBox[{
       RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{"(*", 
         RowBox[{"Check", " ", "State", " ", "Equations"}], "*)"}], 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{
          RowBox[{
           RowBox[{"Div", "[", 
            RowBox[{
             RowBox[{"Ext", "[", 
              RowBox[{"S", ",", "u"}], "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
             "\"\<Spherical\>\""}], "]"}], "\[Equal]", "0"}], "//", 
          "Simplify"}], ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"Div", "[", 
            RowBox[{
             RowBox[{"Int", "[", 
              RowBox[{"S", ",", "u"}], "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
             "\"\<Spherical\>\""}], "]"}], "\[Equal]", "0"}], "//", 
          "Simplify"}], ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"Div", "[", 
            RowBox[{
             RowBox[{"Ext", "[", 
              RowBox[{"S", ",", "\[Sigma]"}], "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
             "\"\<Spherical\>\""}], "]"}], "\[Equal]", 
           RowBox[{"{", 
            RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "//", "Simplify"}], 
         ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"Div", "[", 
            RowBox[{
             RowBox[{"Int", "[", 
              RowBox[{"S", ",", "\[Sigma]"}], "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"r", ",", "\[Theta]", ",", "\[Phi]"}], "}"}], ",", 
             "\"\<Spherical\>\""}], "]"}], "\[Equal]", 
           RowBox[{"{", 
            RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "//", "Simplify"}], 
         ",", "\[IndentingNewLine]", 
         RowBox[{"(*", 
          RowBox[{"Check", " ", "Jump", " ", "Conditions"}], "*)"}], 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"Ext", "[", 
               RowBox[{"S", ",", "u"}], "]"}], "-", 
              RowBox[{"Int", "[", 
               RowBox[{"S", ",", "u"}], "]"}]}], "/.", 
             RowBox[{"r", "\[Rule]", "Rd"}]}], ")"}], "\[Equal]", 
           RowBox[{"{", 
            RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "//", "Simplify"}], 
         ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{"Ext", "[", 
              RowBox[{"S", ",", "\[Sigma]"}], "]"}], "-", 
             RowBox[{"\[Lambda]", " ", 
              RowBox[{"Int", "[", 
               RowBox[{"S", ",", "\[Sigma]"}], "]"}]}]}], ")"}], "/.", 
           RowBox[{"r", "\[Rule]", "Rd"}]}], "//", "Simplify"}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{"Ext", "[", 
              RowBox[{"S", ",", "p"}], "]"}], "-", 
             RowBox[{"\[Lambda]", " ", 
              RowBox[{"Int", "[", 
               RowBox[{"S", ",", "p"}], "]"}]}]}], ")"}], "/.", 
           RowBox[{"r", "\[Rule]", "Rd"}]}], "//", "Simplify"}]}], 
        "\[IndentingNewLine]", "}"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"CheckSolution", "[", "ForwardSolution", "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"CheckSolution", "[", "AdjointSolution", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.76307240370722*^9, 3.763072458059544*^9}, {
   3.7630725370262747`*^9, 3.763072552091663*^9}, {3.76307275568637*^9, 
   3.7630729952148857`*^9}, {3.763073135833599*^9, 3.763073152033683*^9}, {
   3.7630732426343*^9, 3.763073244836555*^9}, {3.763073279272708*^9, 
   3.763073435446246*^9}, {3.763073523544606*^9, 3.763073549438129*^9}, {
   3.7630737467566633`*^9, 3.763073778158095*^9}, 3.7630754817789717`*^9, {
   3.7630760803171787`*^9, 3.763076092903533*^9}, {3.7631386208240433`*^9, 
   3.7631386245476723`*^9}, {3.763298564272418*^9, 3.7632986187187967`*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"7ecf8cad-5217-4319-8873-e42dcb484a66"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Adjoint", "-", 
    RowBox[{"based", " ", "Gradient"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"ud", "=", 
     RowBox[{
      RowBox[{
       RowBox[{"Ext", "[", 
        RowBox[{"ForwardSolution", ",", "u"}], "]"}], "-", 
       RowBox[{"Ext", "[", 
        RowBox[{"AdjointSolution", ",", "u"}], "]"}]}], "//", "Simplify"}]}], 
    ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"J", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Cos", "[", "\[Theta]", "]"}], ",", 
         RowBox[{"-", 
          RowBox[{"Sin", "[", "\[Theta]", "]"}]}], ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Sin", "[", "\[Theta]", "]"}], ",", 
         RowBox[{"Cos", "[", "\[Theta]", "]"}], ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Ext", "[", 
      RowBox[{"ForwardSolution", ",", "u"}], "]"}], "/.", 
     RowBox[{"r", "\[Rule]", "Rd"}]}], "/.", 
    RowBox[{"\[Lambda]", "\[Rule]", "1"}]}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Ext", "[", 
      RowBox[{"AdjointSolution", ",", "u"}], "]"}], "/.", 
     RowBox[{"r", "\[Rule]", "Rd"}]}], "/.", 
    RowBox[{"\[Lambda]", "\[Rule]", "1"}]}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.763071440833507*^9, 3.763071561791164*^9}, {
   3.7630716345397797`*^9, 3.763071699769281*^9}, {3.763071743770521*^9, 
   3.763072084580906*^9}, {3.7630721175979843`*^9, 3.763072121782175*^9}, {
   3.763072202740952*^9, 3.763072343369932*^9}, 3.76307264004006*^9, {
   3.763075490886998*^9, 3.7630755030357656`*^9}, {3.763075737201709*^9, 
   3.76307593140248*^9}, {3.76307596208679*^9, 3.763075982070304*^9}, {
   3.763076253429019*^9, 3.7630762877311487`*^9}, {3.7630763633181753`*^9, 
   3.7630763950975246`*^9}, {3.763076429127117*^9, 3.763076431266626*^9}, {
   3.763128686650592*^9, 3.763128710357781*^9}, {3.7631288683061657`*^9, 
   3.7631289043952827`*^9}, {3.763128952420308*^9, 3.763128952789534*^9}, {
   3.763128989107078*^9, 3.7631290050509863`*^9}, {3.763129064189941*^9, 
   3.763129081427883*^9}, {3.763129143138948*^9, 3.7631292453095093`*^9}, {
   3.763129275877056*^9, 3.76312931312947*^9}, {3.763129425426456*^9, 
   3.763129435285837*^9}, {3.763131259307633*^9, 3.763131276522356*^9}, {
   3.763131362421143*^9, 3.763131382986746*^9}, {3.763131460736944*^9, 
   3.763131498278871*^9}, {3.76313153954379*^9, 3.763131761249997*^9}, {
   3.763131815757782*^9, 3.763131958772567*^9}, 3.763132414746666*^9, {
   3.763138414728767*^9, 3.763138431479903*^9}, {3.7631384624777527`*^9, 
   3.7631385335343027`*^9}, {3.763138629072925*^9, 3.7631386637585573`*^9}, {
   3.763298401481563*^9, 3.763298414384194*^9}, {3.763298446124251*^9, 
   3.763298551140485*^9}, {3.763298651659533*^9, 3.763298660003647*^9}, {
   3.763298692281496*^9, 3.763298807684317*^9}, {3.763298848211261*^9, 
   3.763298857162134*^9}, {3.7633022358961573`*^9, 3.763302348690008*^9}, {
   3.763302383955571*^9, 3.763302407965951*^9}, {3.763302847340084*^9, 
   3.7633028549804363`*^9}, {3.763466360103351*^9, 3.7634664097793303`*^9}, {
   3.7634664635318747`*^9, 3.763466471598803*^9}, {3.7634665084703283`*^9, 
   3.763466555206291*^9}, {3.763466585500078*^9, 3.7634665863951406`*^9}, {
   3.763466683196197*^9, 3.7634668654128*^9}, {3.763466903115436*^9, 
   3.7634669094853086`*^9}, {3.763466966379219*^9, 3.763466989984726*^9}, {
   3.763467034594632*^9, 3.763467040782043*^9}, {3.763467110078944*^9, 
   3.763467149523901*^9}, {3.7634680378068666`*^9, 3.763468054190687*^9}, {
   3.7634680901735973`*^9, 3.763468098841758*^9}, {3.763469994374998*^9, 
   3.7634700028126993`*^9}, {3.763470038399624*^9, 3.763470098521502*^9}, {
   3.763470202100814*^9, 3.763470210573201*^9}, {3.7634705894802856`*^9, 
   3.763470602925825*^9}, {3.763471375482072*^9, 3.7634714009265614`*^9}, {
   3.763476568742589*^9, 3.763476629266227*^9}, {3.763476675189761*^9, 
   3.763476675601192*^9}, {3.763476868763959*^9, 3.7634769052865467`*^9}, {
   3.76347697141973*^9, 3.7634769748650703`*^9}, {3.764262234068159*^9, 
   3.764262262826953*^9}, {3.76426232375986*^9, 3.764262356366764*^9}, {
   3.764262448141246*^9, 3.7642624555897093`*^9}, {3.764262906406855*^9, 
   3.764262944438612*^9}, {3.76426299261977*^9, 3.76426326367025*^9}, {
   3.764263309048078*^9, 3.764263329828335*^9}, {3.7642650930832663`*^9, 
   3.764265106180653*^9}, {3.764266919971814*^9, 3.7642669450852327`*^9}, {
   3.767714785541978*^9, 3.767714873910996*^9}, {3.7677149157750063`*^9, 
   3.767715161992145*^9}, {3.767715246108378*^9, 3.767715267340477*^9}, {
   3.767715345356596*^9, 3.76771535006961*^9}, {3.7687725119064083`*^9, 
   3.7687726190401897`*^9}, {3.7687726527256393`*^9, 
   3.7687728101930523`*^9}, {3.7687733758215513`*^9, 3.768773668010013*^9}, {
   3.7687738421734867`*^9, 3.768773923814849*^9}, {3.7779786875565157`*^9, 
   3.7779787260607233`*^9}, {3.777978779954554*^9, 3.7779788530092497`*^9}, {
   3.7779788912032824`*^9, 3.7779789440313387`*^9}, {3.7779789772107573`*^9, 
   3.777978979292912*^9}, {3.7779791961121187`*^9, 3.777979197247616*^9}, {
   3.777979558646996*^9, 3.77797963978743*^9}, {3.77797972784865*^9, 
   3.777979786282528*^9}, {3.777980315074581*^9, 3.7779803305383577`*^9}, {
   3.777980415533918*^9, 3.7779804790950336`*^9}, {3.777980511219982*^9, 
   3.777980540133174*^9}, {3.777980941706194*^9, 3.7779809434002237`*^9}, {
   3.777981032135405*^9, 3.7779810617892313`*^9}},
 CellLabel->
  "In[166]:=",ExpressionUUID->"db2b1457-2944-4ce1-b4b1-a8294976d817"]
},
WindowSize->{956, 1021},
WindowMargins->{{Automatic, 4}, {0, Automatic}},
FrontEndVersion->"12.0 for Linux x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 344, 9, 57, "Input",ExpressionUUID->"6ab9b0e7-431c-4e7a-a7f3-345febd3df4a"],
Cell[905, 31, 2038, 65, 170, "Input",ExpressionUUID->"af5c1d44-fcb2-4d73-a54c-6f54ea4b6899"],
Cell[2946, 98, 19365, 458, 1228, "Input",ExpressionUUID->"e5af06c3-73df-4a96-9610-6807179fa8a3"],
Cell[22314, 558, 3642, 93, 308, "Input",ExpressionUUID->"9addf5ee-54e4-4657-b35c-2ac37c212e41"],
Cell[25959, 653, 4574, 109, 354, "Input",ExpressionUUID->"7ecf8cad-5217-4319-8873-e42dcb484a66"],
Cell[30536, 764, 5728, 102, 147, "Input",ExpressionUUID->"db2b1457-2944-4ce1-b4b1-a8294976d817"]
}
]
*)

