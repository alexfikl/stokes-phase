(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     12057,        358]
NotebookOptionsPosition[     11296,        340]
NotebookOutlinePosition[     11629,        355]
CellTagsIndexPosition[     11586,        352]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{
    RowBox[{"Element", "[", 
     RowBox[{"R", ",", "Reals"}], "]"}], "&&", 
    RowBox[{"R", ">", "0"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"JK", "[", 
    RowBox[{"m_", ",", "n_"}], "]"}], "=", 
   RowBox[{
    RowBox[{
     RowBox[{"Cos", "[", "\[Phi]", "]"}], "^", "n"}], "/", 
    RowBox[{
     RowBox[{"Sqrt", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"x", "-", 
          SubscriptBox["x", "0"]}], ")"}], "^", "2"}], "+", 
       RowBox[{"r", "^", "2"}], "+", 
       RowBox[{
        SubscriptBox["r", "0"], "^", "2"}], "-", 
       RowBox[{"2", "r", " ", 
        SubscriptBox["r", "0"], 
        RowBox[{"Cos", "[", "\[Phi]", "]"}]}]}], "]"}], "^", "m"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WithRadius", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"r", "^", "2"}], "+", 
      RowBox[{"x", "^", "2"}], "+", 
      RowBox[{
       SubscriptBox["x", "0"], "^", "2"}], "-", 
      RowBox[{"2", "x", " ", 
       SubscriptBox["x", "0"]}]}], "\[Rule]", 
     RowBox[{"R", "^", "2"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ExpansionOrder", "=", "0"}], ";"}]}], "Input",
 CellChangeTimes->{{3.743193666579116*^9, 3.743193674060212*^9}, {
  3.743193712711811*^9, 3.743193776810101*^9}, {3.743193973681426*^9, 
  3.743193983955463*^9}, {3.743194044951836*^9, 3.743194072800515*^9}, {
  3.743194199307489*^9, 3.743194199901308*^9}, {3.743194384993465*^9, 
  3.7431943898003893`*^9}, {3.743194430533554*^9, 3.743194430579424*^9}, {
  3.74319446360561*^9, 3.743194463791363*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"9e6bd43f-aac1-4c3b-a906-357eb5ee765e"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "Stokeslet", " ", "Axis", " ", "Limits", " ", "of", " ", "Elliptic", " ", 
    "Integrals"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"1", ",", "0"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"1", ",", "1"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"3", ",", "0"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"3", ",", "1"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"3", ",", "2"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}]}]}]], "Input",
 CellChangeTimes->{{3.7431937824778843`*^9, 3.743193812022395*^9}, {
  3.743193857822567*^9, 3.7431938707748938`*^9}, {3.7431939027399187`*^9, 
  3.743193969611629*^9}, {3.743194013841386*^9, 3.743194034933414*^9}, {
  3.743194084319055*^9, 3.743194241753001*^9}, {3.7431943925927153`*^9, 
  3.74319440350532*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"7db039f9-e8b7-46a1-9a13-b2f96157f485"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "Stresslet", " ", "Axis", " ", "Limits", " ", "of", " ", "Elliptic", " ", 
    "Integrals"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"5", ",", "0"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"5", ",", "1"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"5", ",", "2"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"5", ",", "3"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}]}]}]], "Input",
 CellChangeTimes->{{3.743194263055134*^9, 3.74319430391789*^9}, {
  3.74319440526223*^9, 3.743194410094043*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"85e6f0e2-3d54-487e-9d37-6f214a0ec728"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "Stresslet", " ", "Gradient", " ", "Axis", " ", "Limits", " ", "of", " ", 
    "Elliptic", " ", "Integrals"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"7", ",", "0"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"7", ",", "1"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"7", ",", "2"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"7", ",", "3"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Integrate", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{
         RowBox[{"JK", "[", 
          RowBox[{"7", ",", "4"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{
           SubscriptBox["r", "0"], ",", "0", ",", "ExpansionOrder"}], "}"}]}],
         "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[Phi]", ",", "0", ",", 
         RowBox[{"2", "Pi"}]}], "}"}]}], "]"}], "/.", "WithRadius"}], "//", 
    "Simplify"}]}]}]], "Input",
 CellChangeTimes->{{3.743194317873032*^9, 3.7431943403448153`*^9}, {
  3.743194413251019*^9, 3.743194421038148*^9}},
 CellLabel->"In[14]:=",ExpressionUUID->"8ee71f27-85ad-4e2b-bd8e-c466757bce65"],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.743194585387744*^9, 
  3.7431945920450706`*^9}},ExpressionUUID->"007227d2-b0c3-4612-88f6-\
b8bc9799edef"],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7431940411408997`*^9, 
  3.743194042365037*^9}},ExpressionUUID->"f0778e3d-dc42-4347-b365-\
50147c5b8fe4"]
},
WindowSize->{956, 1021},
WindowMargins->{{Automatic, 4}, {0, Automatic}},
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1759, 48, 101, "Input",ExpressionUUID->"9e6bd43f-aac1-4c3b-a906-357eb5ee765e"],
Cell[2320, 70, 3185, 92, 147, "Input",ExpressionUUID->"7db039f9-e8b7-46a1-9a13-b2f96157f485"],
Cell[5508, 164, 2462, 73, 124, "Input",ExpressionUUID->"85e6f0e2-3d54-487e-9d37-6f214a0ec728"],
Cell[7973, 239, 3005, 89, 147, "Input",ExpressionUUID->"8ee71f27-85ad-4e2b-bd8e-c466757bce65"],
Cell[10981, 330, 154, 3, 31, "Input",ExpressionUUID->"007227d2-b0c3-4612-88f6-b8bc9799edef"],
Cell[11138, 335, 154, 3, 31, "Input",ExpressionUUID->"f0778e3d-dc42-4347-b365-50147c5b8fe4"]
}
]
*)

