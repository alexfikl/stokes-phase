# SPDX-FileCopyrightText: 2020-2024 Alexandru Fikl <alexfikl@gmail.com>
# SPDX-License-Identifier: CC0-1.0

import os

# {{{ project information

project = 'stokes-phase'
copyright = '2020, Alexandru Fikl'
author = 'Alexandru Fikl'

version = '0.2'
release = '0.2'

# }}}

# {{{ sphinx configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinxcontrib.matlab',
    'sphinx.ext.mathjax',
]

source_suffix = '.rst'
master_doc = 'index'
exclude_patterns = ['_build']
pygments_style = 'sphinx'

html_theme = 'sphinx_book_theme'
html_theme_options = {
    'show_toc_level': 2,
    'use_source_button': True,
    'use_repository_button': True,
    'repository_url': 'https://gitlab.com/alexfikl/stokes-phase',
    'repository_branch': 'main',
}

# }}}

# {{{ matlab domain

primary_domain = 'mat'

parent_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
matlab_src_dir = os.path.abspath(parent_directory)

# https://github.com/sphinx-contrib/matlabdomain/issues/81
add_module_names = False

# }}}
