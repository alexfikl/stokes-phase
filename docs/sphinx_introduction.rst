Introduction
============

This project proposes to do adjoint-based optimization on a simple two-phase
Stokes system. To achieve this goal, some useful components have been
developed:

* A fairly accurate high-order collocation method has been implemented
  to solve the various necessary boundary integral equations.
* A complete set of kernels and layer potentials involved in the Stokes
  system have been implemented. These are mainly used for the two-phase
  problems, but they are separated enough that they can easily be used
  to solve other types of Stokes problems with little effort.
* A simple and very accurate Fourier representation of the geometry
  is available.
* Many different generalized quadrature rules have been implemented to
  aid in accurately computing the singular integrals.

The main drawback is the focus on an axisymmetric flow configuration. This
greatly simplifies the geometry, quadrature and interpolation routines
(effectively making them one dimensional problems), but limits the types
of problems that can be solved using this code. However, extending the
code to 2D problems should not be too hard.

A simple example program of how the code can be currently used is given
below. The boundary conditions are taken from the know Hadamard-Rybczynski
exact solution for a fluid droplet in Stokes flow, given in [Clift1978]_.

.. literalinclude:: ../examples/example_introduction.m
    :language: matlab
    :lines: 4-
    :linenos:
