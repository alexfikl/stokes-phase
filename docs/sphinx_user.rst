.. currentmodule:: src

User Defined Functionality
==========================

The code does not make use of MATLAB object-oriented functionality and all
the objects are described as ``struct``\ s. The interface for each struct
is documented in the ``st_[name]_options`` functions, with several
concrete implementations given.

All the user-provided functionality must provide the directional derivatives
used throughout the code. For example, the cost functional must define
derivatives with respect to the velocity field :math:`\mathbf{u}`, the
traction :math:`\mathbf{f}`, the geometry :math:`\mathbf{X}` and the
surface tension :math:`\gamma` (or capillary number if constant).

Each type can be further combined using :func:`st_gradient_combine` to obtain
combined derivatives.

.. autofunction:: st_gradient_combine

Boundary Conditions
-------------------

.. autofunction:: st_boundary_condition_options

.. autofunction:: st_sym_boundary
.. autofunction:: st_sym_solutions

Jump Conditions
---------------

.. autofunction:: st_jump_options

.. autofunction:: st_jump_angle
.. autofunction:: st_jump_capillary
.. autofunction:: st_jump_capillary_variable
.. autofunction:: st_jump_gravity
.. autofunction:: st_jump_hadamard
.. autofunction:: st_jump_zero

.. autofunction:: st_jump_shape_gradient
.. autofunction:: st_jump_surface_tension

Forcing Terms
-------------

.. autofunction:: st_forcing_options

.. autofunction:: st_forcing_normal_velocity
.. autofunction:: st_forcing_adjoint

.. autofunction:: st_ode_steady_forcing

Cost Functionals
----------------

.. autofunction:: st_cost_options

.. autofunction:: st_cost_centroid
.. autofunction:: st_cost_exterior_traction
.. autofunction:: st_cost_geometry
.. autofunction:: st_cost_normal_velocity
.. autofunction:: st_cost_surface_area
