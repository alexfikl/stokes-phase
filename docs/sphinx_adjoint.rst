.. currentmodule:: src

Adjoint Gradients
=================

Shape Gradients
---------------

.. autofunction:: st_shape_adjoint_gradient

.. autofunction:: st_shape_finite_gradient
.. autofunction:: st_shape_finite_bump
.. autofunction:: st_shape_finite_bump_sigma

Unsteady Gradients
------------------

.. autofunction:: st_ode_unsteady_adjoint

Interface Operators
-------------------

.. autofunction:: st_ops_mass
.. autofunction:: st_ops_adjoint_normal
.. autofunction:: st_ops_adjoint_curvature

.. autofunction:: st_ops_vector_mass
.. autofunction:: st_ops_adjoint_vector_normal
.. autofunction:: st_ops_adjoint_vector_curvature

.. autofunction:: st_ops_boundary
.. autofunction:: st_ops_solve
.. autofunction:: st_ops_vector_solve


