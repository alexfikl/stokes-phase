.. only:: html

References
==========

.. [Chwang1975] A. T. Chwang,
    *Hydromechanics of Low-Reynolds-Number Flow. Part 3. Motion of a Spheroidal
    Particle in Quadratic Flows*,
    Journal of Fluid Mechanics, Vol. 72, pp. 17--17, 1975,
    `DOI <https://doi.org/10.1017/s0022112075002911>`__.

.. [Clift1978] R. Clift, J. R. Grace, M. E. Weber, M. F. Weber,
    *Bubbles, Drops, and Particles*,
    1978.

.. [Brandao1987] M. P. Brandao,
    *Improper Integrals in Theoretical Aerodynamics - The Problem Revisited*,
    AIAA Journal, Vol. 25, pp. 1258--1260, 1987,
    `DOI <https://doi.org/10.2514/3.9775>`__.

.. [Pozrikidis1990] C. Pozrikidis,
    *The Instability of a Moving Viscous Drop*,
    Journal of Fluid Mechanics, Vol. 210, pp. 1--1, 1990,
    `DOI <https://doi.org/10.1017/s0022112090001203>`__.

.. [Lele1992] S. K. Lele,
    *Compact Finite Difference Schemes With Spectral-Like Resolution*,
    Journal of Computational Physics, Vol. 103, pp. 16--42, 1992,
    `DOI <https://doi.org/10.1016/0021-9991(92)90324-r>`__.

.. [Sidi1993] A. Sidi,
    *A New Variable Transformation for Numerical Integration*,
    Numerical Integration IV, pp. 359--373, 1993,
    `DOI <https://doi.org/10.1007/978-3-0348-6338-4_27>`__.

.. [Loewenberg1996] M. Loewenberg, E. J. Hinch,
    *Numerical Simulation of a Concentrated Emulsion in Shear Flow*,
    Journal of Fluid Mechanics, Vol. 321, pp. 395--395, 1996,
    `DOI <https://doi.org/10.1017/s002211209600777x>`__.

.. [Kress1998] R. Kress,
    *Numerical Analysis*,
    Springer Science & Business Media, 1998.

.. [Alpert1999] B. K. Alpert,
    *Hybrid Gauss-Trapezoidal Quadrature Rules*,
    SIAM Journal on Scientific Computing, Vol. 20, pp. 1551--1584, 1999,
    `DOI <https://doi.org/10.1137/s1064827597325141>`__.

.. [Hui1999] C. Y. Hui, D. Shia,
    *Evaluations of Hypersingular Integrals Using Gaussian Quadrature*,
    International Journal for Numerical Methods in Engineering, Vol. 44, pp. 205--214, 1999,
    `DOI <https://doi.org/10.1002/(sici)1097-0207(19990120)44:2%3C205::aid-nme499%3E3.0.co;2-8>`__.

.. [Pozrikidis1992] C. Pozrikidis,
    *Boundary Integral and Singularity Methods for Linearized Viscous Flow*,
    Cambridge University Press, 1992.

.. [Chunrungsikul2001] S. Chunrungsikul,
    *Numerical Quadrature Methods for Singular and Nearly Singular Integrals*,
    Brunel University, 2001.

.. [Li2001] Z. Li, S. R. Lubkin,
    *Numerical Analysis of Interfacial Two-Dimensional Stokes Flow With
    Discontinuous Viscosity and Variable Surface Tension*,
    International Journal for Numerical Methods in Fluids, Vol. 37, pp. 525--540, 2001,
    `DOI <https://doi.org/10.1002/fld.185>`__.

.. [Eilers2003] P. H. C. Eilers,
    *A Perfect Smoother*,
    Analytical Chemistry, Vol. 75, pp. 3631--3636, 2003,
    `DOI <https://doi.org/10.1021/ac034173t>`__.

.. [Hager2006] W. W. Hager, H. Zhang,
    *A Survey of Nonlinear Conjugate Gradient Methods*,
    Pacific Journal of Optimization, Vol. 2, pp. 35--58, 2006.

.. [Waldvogel2006] J. Waldvogel,
    *Fast Construction of the Fejér and Clenshaw–Curtis Quadrature Rules*,
    BIT Numerical Mathematics, Vol. 46, pp. 195--202, 2006,
    `DOI <https://doi.org/10.1007/s10543-006-0045-4>`__.

.. [Carley2007] M. Carley,
    *Numerical Quadratures for Singular and Hypersingular Integrals in Boundary Element Methods*,
    SIAM Journal on Scientific Computing, Vol. 29, pp. 1207--1216, 2007,
    `DOI <https://doi.org/10.1137/060666093>`__.

.. [Gonzalez2009] O. Gonzalez,
    *On Stable, Complete, and Singularity-Free Boundary Integral Formulations
    of Exterior Stokes Flow*,
    SIAM Journal on Applied Mathematics, Vol. 69, pp. 933--958, 2009,
    `DOI <https://doi.org/10.1137/070698154>`__.

.. [Walker2015] S. W. Walker,
    *The Shape of Things: A Practical Guide to Differential Geometry and the
    Shape Derivative*,
    SIAM, 2015.

.. [Denner2015] F. Denner, B. G. M. Van Wachem,
    *Numerical Time-Step Restrictions as a Result of Capillary Waves*,
    Journal of Computational Physics, Vol. 285, pp. 24--40, 2015,
    `DOI <https://doi.org/10.1016/j.jcp.2015.01.021>`__.

.. [Seric2018] I. Seric, S. Afkhami, L. Kondic,
    *Direct Numerical Simulation of Variable Surface Tension Flows Using a
    Volume-of-Fluid Method*,
    Journal of Computational Physics, Vol. 352, pp. 615--636, 2018,
    `DOI <https://doi.org/10.1016/j.jcp.2017.10.008>`__.

Indices
=======

* :ref:`genindex`
* :ref:`search`
