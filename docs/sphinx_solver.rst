.. currentmodule:: src

Boundary Integral Methods
=========================

Solvers
-------

The functions in this section solve a Boundary Integral Equation and return
the main unknown (e.g. the density or velocity fields). Once the equation is
solved, any additional variables can be obtained by simply evaluating
the other layer potentials in `Flow Variables`_.

.. autofunction:: st_repr_velocity
.. autofunction:: st_repr_density
.. autofunction:: st_repr_density_exterior
.. autofunction:: st_repr_electric

Flow Variables
--------------

.. autofunction:: st_repr_density_velocity
.. autofunction:: st_repr_density_pressure
.. autofunction:: st_repr_density_traction
.. autofunction:: st_repr_density_gradu
.. autofunction:: st_repr_density_strain
.. autofunction:: st_repr_density_stress
.. autofunction:: st_repr_density_grads

.. autofunction:: st_repr_density_solution

Electric Variables
------------------

.. autofunction:: st_repr_electric_potential
.. autofunction:: st_repr_electric_field

Layer Potentials
----------------

The layer potentials that are implemented here are mainly to complement
the single-layer representation of the two-phase Stokes flow obtained
from :func:`st_repr_density`. However, they should not be used as is, since
they are just building blocks for the complete representations in `Flow Variables`_
and `Electric Variables`_.

.. autofunction:: st_layer_slp
.. autofunction:: st_layer_dlp
.. autofunction:: st_layer_pressure
.. autofunction:: st_layer_normal_gradu
.. autofunction:: st_layer_tangent_gradu
.. autofunction:: st_layer_tangent_stress
.. autofunction:: st_layer_slp_complete
.. autofunction:: st_layer_normal_grads

.. autofunction:: st_layer_electric_slp
.. autofunction:: st_layer_electric_dlp

.. autofunction:: st_layer_quadrature
.. autofunction:: st_layer_evaluate

Axisymmetric Kernels
--------------------

The kernel functions allow evaluting any of the kernels at specific points.
The axismymmetric kernels are particularly non-trivial to evaluate
(just check out the code for :func:`st_knl_ax_stress_gradient`), so they
are conveniently wrapped here. They are generally meant to be used as follows

.. code:: matlab

    Q = zeros(2, 2, 2);
    [F, E] = st_ellipke(x, x0);
    [I50, I51, I52, I53] = st_ellint_order5(x, x0, F, E);
    Q = st_knl_ax_stress_target(Q, x, x0, I50, I51, I52, I53);

This format allows us to separate the computation of the elliptic integrals
:math:`F` and :math:`E`, the integrals :math:`I_{mn}` (see [Pozrikidis1992]_)
and the actual kernel. The reason for this is performance, since now we can
compute some of these only once for multiple kernels.

.. autofunction:: st_knl_ax_pressure
.. autofunction:: st_knl_ax_pressure_ss
.. autofunction:: st_knl_ax_velocity
.. autofunction:: st_knl_ax_stress_source
.. autofunction:: st_knl_ax_stress_target
.. autofunction:: st_knl_ax_gradu
.. autofunction:: st_knl_ax_stress_gradient

.. autofunction:: st_knl_ax_electric_slp
.. autofunction:: st_knl_ax_electric_dlp_source
.. autofunction:: st_knl_ax_electric_dlp_target

Two-dimensional Kernels
-----------------------

.. note::

    Currently unused, but extensions to 2D should not be too hard.

.. autofunction:: st_knl_2d_pressure
.. autofunction:: st_knl_2d_velocity
.. autofunction:: st_knl_2d_stress
